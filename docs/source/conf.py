# Configuration file for the Sphinx documentation builder.
#
# This file only contains a selection of the most common options. For a full
# list see the documentation:
# https://www.sphinx-doc.org/en/master/usage/configuration.html

# -- Path setup --------------------------------------------------------------

# If extensions (or modules to document with autodoc) are in another directory,
# add these directories to sys.path here. If the directory is relative to the
# documentation root, use os.path.abspath to make it absolute, like shown here.
#
import os
import sys
sys.path.insert(0, os.path.abspath('../../'))


# -- Project information -----------------------------------------------------

project = 'open-ViewMin'
# noinspection PyShadowingBuiltins
copyright = '2021-2024, Daniel Beller'
author = 'Daniel Beller'
version = '0.2'
release = '0.2.6'

# -- General configuration ---------------------------------------------------

# Add any Sphinx extension module names here, as strings. They can be
# extensions coming with Sphinx (named 'sphinx.ext.*') or your custom
# ones.
extensions = [
    'myst_parser',
    'sphinx.ext.doctest',
    'sphinx.ext.autodoc',
    'sphinx.ext.autosummary',
    'sphinx.ext.autosectionlabel',
    'sphinx.ext.napoleon',
    'sphinx.ext.intersphinx',
    'autoapi.extension',
    'sphinx_autodoc_typehints',
    'sphinx_math_dollar',
    'sphinx.ext.mathjax',
    'sphinx_copybutton',
    'sphinx.ext.coverage'
]
source_suffix = ['.rst', '.md']
autosummary_generate = True  # Turn on sphinx.ext.autosummary
autoapi_type = 'python'
autoapi_dirs = [
    '../../src/open_viewmin'
]
autoapi_root = 'api'
autoapi_member_order = 'groupwise'
autoapi_add_toctree_entry = True
autosectionlabel_prefix_document = True
add_module_names = False
napoleon_use_param = True
always_document_param_types = True
# typehints_defaults = 'braces'
typehints_use_signature = False
typehints_use_signature_return = False

# Show typehints in the signature and as content of the function or method
autodoc_typehints = 'both'

# Both the class’ and the __init__ method’s docstring are concatenated and inserted.
autoclass_content = 'both'



# Add any paths that contain templates here, relative to this directory.
templates_path = ['_templates']

# List of patterns, relative to source directory, that match files and
# directories to ignore when looking for source files.
# This pattern also affects html_static_path and html_extra_path.
exclude_patterns = ["_build", "Thumbds.db", ".DS_Store", ".venv", ".ipynb_checkpoints"]


# -- Options for HTML output -------------------------------------------------

# The theme to use for HTML and HTML Help pages.  See the documentation for
# a list of builtin themes.
#
html_theme = 'sphinx_book_theme'

# Add any paths that contain custom static files (such as style sheets) here,
# relative to this directory. They are copied after the builtin static files,
# so a file named "default.css" will overwrite the builtin "default.css".
html_static_path = ['_static']
# for sphinx-math-dollar (https://www.sympy.org/sphinx-math-dollar/)

mathjax_config = {
    'tex2jax': {
        'inlineMath':  [["\\(", "\\)"]],
        'displayMath': [["\\[", "\\]"]],
    },
}

mathjax3_config = {
  "tex": {
    "inlineMath":  [['\\(', '\\)']],
    "displayMath": [["\\[", "\\]"]],
  }
}
