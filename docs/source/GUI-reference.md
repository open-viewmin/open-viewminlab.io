# Reference for open-ViewMin GUI

## Keypress shortcuts 

| key | action                            |
|----:|:----------------------------------|
| 'c' | toggle most recent color bar      |
| 'F' | toggle floor                      |
| 'H' | hide all actors                   |
| 'o' | toggle visibility of camera orbit |
| 'O' | animate camera orbit              |

In addition, `NematicPlot` inherits the [keyboard shortcuts](https://docs.pyvista.org/api/plotting/plotting.html#plotting) of `pyvistaqt.BackgroundPlotter` except for 'q'.


## Main menu bar

### File menu 

|         menu item | description                                                                                                                                                                                                                                                                     |
|------------------:|:--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
|    *Open file(s)* | Select files to import                                                                                                                                                                                                                                                          |
|  *Export as html* | Export scene as interactive html element. See [`pyvista.Plotter.export_html`](https://docs.pyvista.org/user-guide/jupyter/pythreejs.html#export-to-html). Note: Disabled (invisible) meshes will be included in the html scene; a workaround is to set their opacities to zero. | 
| *Take Screenshot* | Export current view as image, using [`pyvista.Plotter.screenshot`](https://docs.pyvista.org/api/plotting/_autosummary/pyvista.Plotter.html#pyvista.Plotter.show).                                                                                                               |
| *Export as VTKjs* | Save scene as vtkjs file using `pyvista.Plotter.export_vtkjs`.                                                                                                                                                                                                                  |
|     *Export mesh* | Save a mesh using [`pyvista.DataObject.save`](https://docs.pyvista.org/api/core/_autosummary/pyvista.DataObject.html#pyvista.DataObject.save).                                                                                                                                  |
|            *Exit* |                                                                                                                                                                                                                                                                                 |


### View menu

|                              menu item | description                                                                                                                                                                                                                                         |
|---------------------------------------:|:----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
|                        *Lighting type* | Choose between "Light kit" and "3 lights"; see *PyVista* [Plotter Lighting Systems](https://docs.pyvista.org/examples/04-lights/plotter_builtins.html#three-lights-illumination).                                                                   |                                                                                                                                                                                                | 
|                           *Scale axes* | Launch dialog to stretch/compress axes.                                                                                                                                                                                                             |
| *Camera -> Toggle Parallel Projection* | See [`pyvista.Camera.enable_parallel_projection`](https://docs.pyvista.org/api/core/_autosummary/pyvista.Camera.html#pyvista.Camera.enable_parallel_projection).                                                                                    |
|                   *Orientation Marker* | Set visibility of orientation markers such as axes widget.                                                                                                                                                                                          |
|                          *Bounds Axes* | Control visibility of axes and bounding boxes. See *PyVista* [Plotting Bounds](https://docs.pyvista.org/examples/02-plot/bounds.html).                                                                                                              |
|                               *Toggle* | Toggle visibility of actors not belonging to the filter tree.                                                                                                                                                                                       |
|                      *Hide all actors* | Set all actors invisible.                                                                                                                                                                                                                           |
|                         *Toggle Floor* | Toggle visibility of floor plane. See *PyVista* [Plot with Floors](https://docs.pyvista.org/examples/02-plot/floors.html). Only the `-z` floor is available here.                                                                                   |   
|             *Toggle eye dome lighting* | See *PyVista* [Eye Dome Lighting](https://docs.pyvista.org/examples/02-plot/edl.html).                                                                                                                                                              |
|                            *Set Orbit* | Launch dialog to define camera circular orbit. See *PyVista* [Orbiting](https://docs.pyvista.org/examples/02-plot/orbit.html).                                                                                                                      | 
|                      *Show/Hide Orbit* | Toggle visibility of points visualizing camera orbit. See *PyVista* [Orbiting](https://docs.pyvista.org/examples/02-plot/orbit.html).                                                                                                               |
|                                *Orbit* | Animate camera orbit. See *PyVista* [Orbiting](https://docs.pyvista.org/examples/02-plot/orbit.html).                                                                                                                                               |
|                     *Toggle Stereo 3D* | Toggle red-blue anaglyph 3D. Calls [`pyvista.Plotter.enable_stereo_render`](https://docs.pyvista.org/version/stable/api/plotting/_autosummary/pyvista.Plotter.enable_stereo_render.html#pyvista.Plotter.enable_stereo_render).                      |
|                       *Toggle Shadows* | Toggle shadows calculation. See [`pyvista.Plotter.enable_shadows`](https://docs.pyvista.org/api/plotting/_autosummary/pyvista.Renderer.html#pyvista.Renderer.enable_shadows). Warning: This can be computationally intensive and may crash PyVista. | 

### Tools menu

#### *Enable Picking...*
  
|     submenu item | description                                                                                                                                                                                                                                                                                                                                     |
|-----------------:|:------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| *Cell (through)* | calls [`pyvista.Plotter.enable_cell_picking`](https://docs.pyvista.org/version/stable/api/plotting/_autosummary/pyvista.Plotter.enable_cell_picking.html#pyvista-plotter-enable-cell-picking); picked cells are assigned to `NematicPlot.picked_mesh`.                                                                                          |
| *Cell (visible)* | calls [`pyvista.Plotter.enable_cell_picking`](https://docs.pyvista.org/version/stable/api/plotting/_autosummary/pyvista.Plotter.enable_cell_picking.html#pyvista-plotter-enable-cell-picking); picked cells are assigned to `NematicPlot.picked_mesh`.                                                                                          |
|       *Geodesic* | calls [`pyvista.Plotter.enable_geodesic_picking`](https://docs.pyvista.org/version/stable/api/plotting/_autosummary/pyvista.Plotter.enable_geodesic_picking.html#pyvista.Plotter.enable_geodesic_picking); a mesh for the picked geodesic is assigned to `NematicPlot.picked_geodesic`.                                                         |
|        *Horizon* | calls [`pyvista.Plotter.enable_horizon_picking`](https://docs.pyvista.org/version/stable/api/plotting/_autosummary/pyvista.Plotter.enable_horizon_picking.html#pyvista.Plotter.enable_horizon_picking); picked path is assigned to `NematicPlot.picked_horizon`.                                                                                |
|           *Mesh* | calls [`pyvista.Plotter.enable_mesh_picking`](https://docs.pyvista.org/version/stable/api/plotting/_autosummary/pyvista.Plotter.enable_mesh_picking.html#pyvista.Plotter.enable_mesh_picking); picked mesh is assigned to `NematicPlot.picked_mesh`.                                                                                            |
|           *Path* | calls [`pyvista.Plotter.enable_path_picking`](https://docs.pyvista.org/version/stable/api/plotting/_autosummary/pyvista.Plotter.enable_path_picking.html#pyvista.Plotter.enable_path_picking); picked path is assigned to `NematicPlot.picked_path`.                                                                                            |
|          *Point* | calls [`pyvista.Plotter.enable_point_picking`](https://docs.pyvista.org/version/stable/api/plotting/_autosummary/pyvista.Plotter.enable_point_picking.html#pyvista.Plotter.enable_point_picking); coordinates of picked point are assigned to `NematicPlot.picked_point`, and the picked surface mesh is assigned to `NematicPlot.picked_mesh`. |
|        *Surface* | calls `pyvista.Plotter.enable_surface_picking`; picked point is assigned to `NematicPlotter.picked_point`.                                                                                                                                                                                                                                      |

#### *Mouse Interaction Mode*

|                         submenu item | description                                                                                                                                                            |
|-------------------------------------:|:-----------------------------------------------------------------------------------------------------------------------------------------------------------------------|
|                        *Image Style* | calls [`pyvista.Plotter.enable_image_style`](https://docs.pyvista.org/version/stable/api/plotting/_autosummary/pyvista.Plotter.enable_image_style.html)                |
|                     *Joystick Style* | calls [`pyvista.Plotter.enable_joystick_style`](https://docs.pyvista.org/version/stable/api/plotting/_autosummary/pyvista.plotter.enable_joystick_style)               |
|               *Rubber Band 2D Style* | calls [`pyvista.Plotter.enable_rubber_band_2d_style`](https://docs.pyvista.org/version/stable/api/plotting/_autosummary/pyvista.plotter.enable_rubber_band_2d_style)   |
|        *Rubber Band Style (default)* | calls [`pyvista.Plotter.enable_rubber_band_style`](https://docs.pyvista.org/version/stable/api/plotting/_autosummary/pyvista.plotter.enable_rubber_band_style)         |
|                      *Terrain Style* | calls [`pyvista.Plotter.enable_terrain_style`](https://docs.pyvista.org/version/stable/api/plotting/_autosummary/pyvista.plotter.enable_terrain_style)                 |
|                    *Trackball Style* | calls [`pyvista.Plotter.enable_trackball_style`](https://docs.pyvista.org/version/stable/api/plotting/_autosummary/pyvista.plotter.enable_trackball_style)             |
|                         *Zoom Style* | calls [`pyvista.Plotter.enable_zoom_style`](https://docs.pyvista.org/version/stable/api/plotting/_autosummary/pyvista.plotter.enable_zoom_style)                       |
|  *Joystick Style Actor Manipulation* | calls [`pyvista.Plotter.enable_joystick_actor_style`](https://docs.pyvista.org/version/stable/api/plotting/_autosummary/pyvista.plotter.enable_joystick_actor_style)   | 
| *Trackball Style Actor Manipulation* | calls [`pyvista.Plotter.enable_trackball_actor_style`](https://docs.pyvista.org/version/stable/api/plotting/_autosummary/pyvista.plotter.enable_trackball_actor_style) | 
|          *Enable Fly To Right Click* | calls [`pyvista.Plotter.enable_fly_to_right_click`](https://docs.pyvista.org/version/stable/api/plotting/_autosummary/pyvista.plotter.enable_fly_to_right_click)       |
  
To remove messages that *PyVista* prints on the plotter, use *View -> Toggle*. 

### "Add" menu 

|                 menu item | description                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        |
|--------------------------:|:---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
|                   *Slice* | Add a plane widget with plane mesh colored by the chosen scalar dataset. Similar but not identical to [`pyvista.Plotter.add_mesh_slice`](https://docs.pyvista.org/examples/03-widgets/plane-widget.html#plane-widget).                                                                                                                                                                                                                                                                                                                                                                                                                             |
|              *Isosurface* | Add single-value isosurface on the chosen scalar dataset, with isosurface value controlled by a slider and spinbox in the actor control toolbar. See [`pyvista.DataSetFilters.contour`](https://docs.pyvista.org/api/core/_autosummary/pyvista.DataSetFilters.html#pyvista.DataSetFilters.contour).                                                                                                                                                                                                                                                                                                                                                |
|               *Threshold* | Add threshold on the chosen scalar dataset, with min and max values set by spinboxes in the actor control toolbar. See [`pyvista.DataSetFilters.threshold`](https://docs.pyvista.org/api/core/_autosummary/pyvista.DataSetFilters.html#pyvista.DataSetFilters.contour).                                                                                                                                                                                                                                                                                                                                                                            |
| *Pontryagin-Thom surface* | Add Pontryagin-Thom surface for the chosen vector dataset, with plane widget controlling the normal direction for the director preimage in the Pontryagin-Thom construction. The plane's center is meaningless. A slider and spinbox in the actor control toolbar set the polar angle between the chosen normal direction and the band of directors in the preimage; this should typically be as close as possible to pi radians while still including enough points to construct a smooth surface. For background information on usage of the Pontryagin-Thom construction for nematic liquid crystals, see [^Chen2013][^Copar2013][^Machon2014]. |
|                  *Glyphs* | Launch dialog to add glyphs. See [`pyvista.DataSetFilters.glyph`](https://docs.pyvista.org/api/core/_autosummary/pyvista.DataSetFilters.html#pyvista.DataSetFilters.glyph).                                                                                                                                                                                                                                                                                                                                                                                                                                                                        |
|       *Tensor ellipsoids* | Launch dialog to add ellipsoid glyphs visualizing a symmetric, rank-2 tensor field. The ellipsoid axes lengths are proportional to the eigenvalues plus a uniform shift given by the value labeled "eigenvalues shift".                                                                                                                                                                                                                                                                                                                                                                                                                            |
|     *Surface streamlines* | Launch dialog to add surface streamlines. See [`pyvista.DataSetFilters.streamlines_from_source`](https://docs.pyvista.org/api/core/_autosummary/pyvista.DataSetFilters.html#pyvista.DataSetFilters.streamlines_from_source).                                                                                                                                                                                                                                                                                                                                                                                                                       |
|      *Orientation widget* | Add a widget that shows the orientation of a single actor, in miniature, as a guide for the orientation of the scene. See [`pyvista.Renderer.add_orientation_widget`](https://docs.pyvista.org/api/plotting/_autosummary/pyvista.Renderer.html#pyvista.Renderer.add_orientation_widget).                                                                                                                                                                                                                                                                                                                                                           |
|             *Line widget* | Add a line widget that defines a probe of the parent mesh. See *PyVista* [Line Widget](https://docs.pyvista.org/examples/03-widgets/line-widget.html).                                                                                                                                                                                                                                                                                                                                                                                                                                                                                             |
|  *Defect tangents glyphs* | Compute defect tangents and visualize them as glyphs on the defects. See [`open_viewmin.NematicPlot.calculate_and_show_defect_tangent`](https://open-viewmin.gitlab.io/api/open_viewmin/nematic/nematic/index.html?highlight=calculate_and_show_defect_tangent#open_viewmin.nematic.nematic.NematicPlotNoQt.calculate_and_show_defect_tangent)                                                                                                                                                                                                                                                                                                     |
|     *Jones matrix planes* | Add planes normal to x, y, and z visualizing the transmitted light intensity calculated using the Jones matrix approach. Float-rgb data for the *x*, *y*, and *z* Jones planes are stored in the dictionary `NematicPlot.jones_data` with keys `"x"`, `"y"`, `"z"`. Each of the three color arrays can be viewed separately in 2D using, e.g., `matplotlib.pyplot.imshow(my_plotter.jones_data["z"])`.                                                                                                                                                                                                                                             | 
|   *Disclination analysis* | See below.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                         |

The *Disclination analysis* menu item does several things:
  * subdivides the defect set by finding a collection of its centroids. Note: You may need to increase the number of centroids to have them fall on the defect curves; you can do so by clicking on the centroids settings (rightmost) button in the control toolbar for `"defects_centroids"`.
  * creates a set of small circuit meshes encircling each centroid and probing the parent mesh
  * plot the director as ellipsoid glyphs at evenly spaced points along each circuit, colored by "LdG_L24"
  * plot the rotation vector Omega, calculated from the chi-tensor, as double-headed arrows at the centroids, colored by "LdG_L24"
  * make the defects isosurface transparent

### Theme menu button 

Launch theme dialog to set colors in the plot's theme:

|                 button | description                                                                                                                                                                        |
|-----------------------:|:-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
|     *background color* | see [`pyvista.Renderer.set_background`](https://docs.pyvista.org/api/plotting/_autosummary/pyvista.Renderer.html#pyvista.Renderer.set_background).                                 |
| *background color top* | for color gradient backgrounds. See [`pyvista.Renderer.set_background`](https://docs.pyvista.org/api/plotting/_autosummary/pyvista.Renderer.html#pyvista.Renderer.set_background). |
|           *font color* | set color of text labels. See [`pyvista.themes._Font.color`](https://docs.pyvista.org/api/plotting/_autosummary/pyvista.themes._Font.html#pyvista.themes._Font.color).             |
|          *floor color* | set `color` in [`pyvista.Renderer.add_floor`](https://docs.pyvista.org/api/plotting/_autosummary/pyvista.Renderer.html#pyvista.Renderer.add_floor).                                |

### Calculate Menu 

|                  menu item | description                                                                                                                                                                                                                                                                                                                                                                  |
|---------------------------:|:-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
|                  *Scalars* | Calculate gradient or Laplacian of scalar array defined on mesh "fullmesh"                                                                                                                                                                                                                                                                                                   | 
|                  *Vectors* | Calculate div or curl, or get vector components, of vector array defined on mesh "fullmesh"                                                                                                                                                                                                                                                                                  | 
| *Symmetric rank-2 tensors* | Calculate eigenvalues or eigenvectors (which come with eigenvalues) of 9-element data arrays defined on mesh "fullmesh" that, when reshaped into 3x3 matrices, are symmetric. Eigenvalues/eigenvectors become available as scalar/vector (respectively) arrays defined on "fullmesh".                                                                                        | 
|             *Frank energy* | See [Nematic calculations: Frank free energy components](https://open-viewmin.gitlab.io/calculations.html#frank-free-energy-components)                                                                                                                                                                                                                                      | 
|                 *D-tensor* | See [Nematic calculations: D-tensor](https://open-viewmin.gitlab.io/calculations.html#d-tensor). Along with rank-2 tensor array "D", create symmetric rank-2 tensor arrays "D^T D" and "D D^T".                                                                                                                                                                              | 
|               *Chi-tensor* | See [Nematic calculations: chi-tensor](https://open-viewmin.gitlab.io/calculations.html#chi-tensor). Along with rank-2 tensor array "chi", create symmetric rank-2 tensor arrays "chi^T chi" and "chi chi^T".                                                                                                                                                                |  
|           *Westin metrics* | See [Nematic calculations: Westin metrics](https://open-viewmin.gitlab.io/calculations.html#westin-metrics)                                                                                                                                                                                                                                                                  | 
|                   *Set q0* | Affects data arrays "LdG_K2" and "Frank_K2"; see [Nematic calculations](https://open-viewmin.gitlab.io/calculations.html)                                                                                                                                                                                                                                                    | 
|   *Defect tangent vectors* | Calculate the vector array "defects_tangent" as the approximate unit tangent vector (with arbitrary sign) along the disclination contour; see [`open_viewmin.filter_tree_plot.filters.glyphs.calculate_tangent`](https://open-viewmin.gitlab.io/api/open_viewmin/filter_tree_plot/filters/glyphs/index.html#open_viewmin.filter_tree_plot.filters.glyphs.calculate_tangent). | 

### Filter Menu (experimental)

This experimental menu is designed to help in exploring the many filters available in *PyVista*. The approach here aims for generality, but it will fail in many cases if bad combinations of parameters are given. Some of these failures may cause *PyVista* to crash, so use this feature with caution.   

For each mesh in the filters tree, this menu generates a submenu of available *PyVista* filters. Clicking any of these filter names launches a dialog of filter parameters. Clicking "OK" causes *open-ViewMin* to attempt to create a `FilterFormula`; if successful, this will result in a new mesh, and the plotter will display an associated actor. 

Some filters are not available in this menu, including all `boolean_*` filters and `plot_*` filters. 

For filters with the `inplace` keyword argument, you should generally leave this as `False` (checkbox unchecked), since in-place operations are incompatible with filter tree updates.

For filters that return an array rather than a new mesh, *open-ViewMin* will create a new child mesh identical to the parent except that it additionally possesses the newly calculated array. 

If the child mesh has a new scalar array, typically this will be the mesh's "active scalars" array. To color the mesh by this array, click the visualization dropdown menu ("⠇") in the mesh's control toolbar, and select "color by active scalars".

After selecting a filter, you can see the beginning of its docstring by right-clicking anywhere in the body of the dialog widget and selecting "What's This?".

## Controls toolbar area

* *Animation*: change "frame" when data from multiple timesteps is imported. The number of frames is shown after the slash. The frame number ranges from 0 to one less than the number of frames. 
* *lighting*: Use slider to rescale intensity of all lights.
* *widgets*: 
  * Use checkboxes to toggle widgets enabled/disabled.
  * Click on the widget name to set the widget's color using the system color picker.
* *filters tree*: Schematic representation of the filter tree structure. If an actor control toolbar is disabled, you can re-enable it by clicking on the actor's name in the filters tree widget. See [The Filters Tree](https://open-viewmin.gitlab.io/filters-tree.html).
* actor control toolbars
  * *visibility checkbox*: toggle visibility of actor visualizing mesh
  * mesh visualization dropdown menu ("⠇")
    * *solid color*: set solid color using system color picker
    * *color array*: choose data array and color map to color surface
    * *color by active scalars*: use active scalar array and default color map to color surface 
    * *show/hide colorbar*: toggle visibility of colorbar
    * *lighting*: launch dialog to set actor opacity and lighting options
    * *glyph settings* (for glyph meshes): launch dialog to set glyph shape and sampling properties
    * *rename*: change name of mesh
    * *remove (!)*: delete the mesh and its filter formula, along with those of all descendants in the filters tree
  * new mesh dropdown menu ("↪︎")
    * *add glyphs*: add filter formula creating glyphs
    * *add streamlines*: add filter formula creating surface streamlines
    * *add threshold (on active scalars)*: add filter formula creating threshold on this mesh's active scalars
    * *clip box*: add widget formula for box widget to clip mesh
    * *clip plane*: add widget formula for plane widget to clip mesh
    * *connectivity*: add filter formula for mesh connectivity
    * *connectivity (periodic boundaries)*: add filter formula for mesh accounting for periodic boundary conditions 
    * *calculate Euler characteristic*: add filter formula calculating the Euler characteristic of each connected subset as a new dataset named "chi" 
    * *find loops*: keep only loops
  * isosurface controls
    * *isosurface value spinbox and slider*: set contour value
  * threshold controls
    * *min spinbox*: set threshold minimum value
    * *max spinbox*: set threshold maximum value
  * slice controls
    * *θ spinbox and slider*: control slice normal polar angle
    * *φ spinbox and slider*: control slice normal azimuthal angle
    * *⟂ spinbox and slider*: control slice origin displacement along normal
    * *slice normal buttons*: set slice normal equal to one of the axes
    * *slice "center" button*: set slice origin to center of box along normal
  * Pontryagin-Thom surface controls
    * *|∠(n,N)| spinbox and slider*: set polar angle (in radians) between widget normal and band of directors defining the preimage of the Pontryagin-Thom construction
  * Jones planes controls
    * *x, y, z checkboxes*: toggle visibility of each plane
    * *1/4 waveplate checkbox*: toggle insertion of quarter waveplate between polarizer and sample
    * *n_ord, n_ext boxes*: enter ordinary, extraordinary indices of refraction
    * *res*: enter resolution in nanometers. When the simulation is smaller than the experiment, the Jones calculation may better resemble the experiment if the resolution is set to the experimental dimensions divided by the system size (in lattice spacings). 


[^Chen2013]: Chen, Ackerman, Alexander, Kamien, and Smalyukh (2013). Generating the Hopf fibration experimentally in nematic liquid crystals. *Phys. Rev. Lett.* 110, 237801. [DOI: 10.1103/PhysRevLett.110.237801](https://doi.org/10.1103/PhysRevLett.110.237801)

[^Copar2013]: Čopar, Porenta and Žumer (2013). Visualisation methods for complex nematic fields,
    *Liquid Crystals*, 40, 1759-1768. [DOI: 10.1080/02678292.2013.853109](https://doi.org/10.1080/02678292.2013.853109)

[^Machon2014]: Machon and Alexander (2014). Knotted Defects in Nematic Liquid Crystals, *Phys. Rev. Lett.* 113, 027801. [DOI: 10.1103/PhysRevLett.113.027801](https://doi.org/10.1103/PhysRevLett.113.027801)