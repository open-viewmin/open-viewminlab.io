
# Calculations 

This page summarizes the calculations that `open_viewmin.NematicPlot` 
can perform on imported Q-tensor data. The calculations are stored as
data arrays of the mesh `"fullmesh"` and are inherited by all child meshes.

## Free energy components calculated from Q-tensor

The following arrays are calculated automatically from imported Q-tensor data.

| array name       | calculation                                        |
|------------------|----------------------------------------------------|
| "LdG_L1"         | $f_1 = \partial_i Q_{jk} \partial_i Q_{jk}$        |
| "LdG_L2"         | $f_2 = \partial_j Q_{ij} \partial_k Q_{ik}$        |
| "LdG_L3"         | $f_3 = \partial_j Q_{ik} \partial_k Q_{ij}$        |
| "LdG_L4"         | $f_4 = \epsilon_{lik} Q_{lj} \partial_k Q_{ij} $   |
| "LdG_L6"         | $f_6 = Q_{ij} \partial_i Q_{kl} \partial_j Q_{kl}$ |
| "LdG_L24"        | $f_{24} = f_2 - f_3$                               |
| "twist"          | $t_Q = -4/(9 S^2) f_4$                             |
| "saddle-splay_Q" | $\sigma_Q = 4/(3S) f_{24}$                         | 
| "cholestericity" | $C_Q = \mathrm{t}_Q^2 - 2 \mathrm{\sigma_Q} $      | 

Linear combinations of Landau-de Gennes free energy components give
the Frank free energy components in the uniaxial limit. 

| array name | calculation                                                     | uniaxial limit                                            |
|------------|-----------------------------------------------------------------|-----------------------------------------------------------|
| "LdG_K1"   | $f_{\mathrm{splay}} = 2/(9 S^2) (-f_1/3 + 2 f_2 - 2/(3 S) f_6)$ | $\rightarrow (\nabla \cdot \hat n)^2 $                    |
| "LdG_K2"   | $f_{\mathrm{twist}} = (\mathrm{t}_Q + q_0)^2 $                  | $\rightarrow (\hat n \cdot \nabla \times \hat n + q_0)^2 $ |
| "LdG_K3"   | $f_{\mathrm{bend}} = 2/(9S^2) (f_1/3 + 2/(3S) f_6)$             | $\rightarrow \| \hat n \times (\nabla \times \hat n ) \| ^2 $ |


## Frank free energy components


The arrays below are computed from finite difference calculations on the 
director field (rather than taking the uniaxial limit of the LdG calculations,
which take derivatives on the Q-tensor). 
These arrays are calculated
when the user clicks *Calculate -> Frank energy* or calls `NematicPlot.calculate_frank_energy_comps()`
from Python.

| array name          | calculation                                                                          |
|---------------------|--------------------------------------------------------------------------------------|
| "splay"             | $s_n = \partial_i n_i$                                                               |
| "splay_vec"         | $\mathbf{s}_n = n_i \partial_j n_j$                                                  |
| "curl_n"            | $\epsilon_{ijk} \partial_j n_k$                                                      |
| "twist_n"           | $t_n = n_i \epsilon_{ijk} \partial_j n_k$                                            |
| "bend"              | $\mathbf{b}_n = n_i \partial_i n_j$                                                  |
| "rotation_vector_n" | $\hat \Omega_n = \nabla \times \hat n - \hat n (\hat n \cdot \nabla \times \hat n )$ |
| "Frank_K1"          | $f_{K1} = s_n^2$                                                                     |
| "Frank_K2"          | $f_{K2} = (t_n + q_0)^2$                                                             |
| "Frank_K3"          | $f_{K3} = {\bf b}_n \cdot {\bf b}_n$                                                 |
| "Frank_K24"         | $f_{K24} = \partial_i n_j \partial_j n_i - f_{K3}$                                   |
| "Frank_oneconst"    | $f_{K1} + f_{K2} + f_{K3}$                                                           |

## chi-tensor

The Efrati-Irvine "handedness" pseudotensor [^Efrati2014][^Beller2014], 
$$\chi_{ij} = n_l \epsilon_{jlk} \partial_i n_k$$ 
is adapted as a calculation on the Q-tensor,
$$\chi_{ij} = Q_{lm} \epsilon_{jlk} \partial_i Q_{km}.$$ 
It is useful in describing both cholesteric configurations and disclination lines. 
The following arrays are calculated when the user clicks *Calculate -> Chi_tensor*
or calls `NematicPlot.calculate_chi_tensor()` from Python.

| array name  | calculation                               |
|-------------|-------------------------------------------|
| "Chi"       | $Q_{lm} \epsilon_{jlk} \partial_i Q_{km}$ |
| "Chi^T Chi" | $\chi_{ij}\chi_{jk}$                      |
| "Chi Chi^T" | $\chi_{ji} \chi_{kj}$                     |

The eigenvector of $\chi^T \chi$ with the greatest eigenvalue provides an 
estimate of the rotation vector $\hat \Omega$. 

The eigenvector of $\chi \chi^T$ with the greatest eigenvalue provides an
estimate of the azimuthal direction around the disclination. 

In the cholesteric ground state, only one eigenvalue is nonzero and the 
corresponding eigenvector is the cholesteric pitch axis. 
In a distorted cholesteric,
the two nonzero eigenvalues correspond to eigenvectors of which one is the 
cholesteric pitch axis.

## D-tensor

The $D_{ij}$ tensor gives information about a disclination line's 
tangent vector and rotation vector.[^Schimming2022] The following are 
calculated when the user clicks *Calculate -> D_tensor* or calls 
`NematicPlot.calculate_D_tensor()` from Python.

| array name | calculation                                                                   |
|------------|-------------------------------------------------------------------------------|
| "D_tensor" | $\epsilon_{gmn} \epsilon_{i \ell k} \partial_\ell Q_{m p} \partial_k Q_{n p}$ | 
| "D^T D"    | $D_{ij} D_{jk}$                                                               |
| "D D^T"    | $D_{ji} D_{kj}$                                                               |


The eigenvector of $DD^T$ with the greatest eigenvalue provides an estimate
of the rotation vector $\hat \Omega$.

The eigenvector of $D^TD$ with the greatest eigenvalue provides an estimate of 
the disclination line tangent vector.


## Westin metrics

Westin metrics for analyzing Q-tensor eigenvalues[^Callan-Jones2006] are 
related to nematic uniaxial order $S$ and biaxial order $S_B$ as described
in the table below. These arrays are calculated when the user clicks 
*Calculate -> Westin metrics* or calls `NematicPlot.calculate_Westin_metrics()`
from Python.

| array name                 | calculation     |
|----------------------------|-----------------|
| "Westin_l"[^Westin_l_note] | $S - S_B$       |
| "Westin_p"                 | $4 S_B$         |
| "Westin_s"                 | $1 - S - 3 S_B$ | 

[^Westin_l_note]: The subscript is a lowercase "L".

[^Efrati2014]: Efrati and Irvine. *Phys Rev X* 4:011003 (2014).
[^Beller2014]: Beller et al. *Phys Rev X* 4:031050 (2014).
[^Schimming2022]: Schimming and Viñals. *Soft Matter* 18:2234 (2022).
[^Callan-Jones2006]: Callan-Jones et al., *Phys Rev E* 74:061701 (2006).