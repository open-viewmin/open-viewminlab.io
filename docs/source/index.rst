open-ViewMin documentation
========================================

Welcome to open-ViewMin's documentation.

*Human usin' open-Qmin, you can Zoom in!*

New to open-ViewMin? Start with the Overview for installation and usage instructions.


.. toctree::
   :maxdepth: 1

   Overview <overview>
   Filters tree <filters-tree>
   GUI reference <GUI-reference>
   Nematic calculations <calculations>
   License <LICENSE>

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

