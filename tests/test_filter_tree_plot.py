import numpy as np
import pyvista as pv
from open_viewmin import FilterTreePlot

test_data_path = "./data/"
scalar_data_filename = "test_scalar_data.dat"


def make_random_scalar_data(system_size=10):
    filename = test_data_path + scalar_data_filename
    test_data = np.random.random(
        (system_size, system_size, system_size)
    ).flatten().reshape((system_size * system_size * system_size, 1))
    x_vals = np.arange(system_size)
    y_vals = np.arange(system_size)
    z_vals = np.arange(system_size)
    Z, Y, X = np.meshgrid(z_vals, y_vals, x_vals)
    coord_cols = np.stack([thing.flatten() for thing in (X, Y, Z)], axis=-1)
    coords_and_data = np.concatenate((coord_cols, test_data), axis=-1)
    np.savetxt(filename, coords_and_data, delimiter="\t")


class TestNullInitialize:
    def make_plotter(self):
        if not hasattr(self, "plotter"):
            self.plotter = FilterTreePlot()
        return self.plotter

    def test_null_initialize(self):
        plotter = self.make_plotter()
        assert isinstance(plotter, FilterTreePlot)


class TestLoad(TestNullInitialize):
    def test_load_scalar_data(self):
        L = 10
        make_random_scalar_data(system_size=L)
        plotter = self.make_plotter()
        plotter.load(test_data_path + scalar_data_filename)
        data = plotter.data
        assert len(data) == 1
        assert data[0].shape == (L * L * L, 4)
        assert plotter.num_frames() == 1
        assert isinstance(plotter.fullmesh, pv.ImageData)
        plotter.fullmesh["test_scalars"] = plotter.data[0][:, -1]
        assert set(plotter.fullmesh.array_names) == {'test_scalars', 'ones'}
        plotter.add_filter_formula(
            "test_scalars_slice",
            parent_mesh_name="fullmesh",
            filter_callable="slice"
        )
