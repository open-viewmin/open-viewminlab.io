from open_viewmin import NematicPlot
import numpy as np

test_data_path = "./data/"
file_list = [
    test_data_path + 'test_open_qmin_style_data_1.txt',
    test_data_path + 'test_open_qmin_style_data_2.txt'
]

file_glob_str = test_data_path + 'test_open_qmin_style_data_?.txt'

auto_setup_mesh_names = [
    'fullmesh',
    'boundaries',
    'defects',
    'director_plane',
    'director_plane_sampled',
    'director'
]

auto_setup_actor_names = [
    'boundaries', 'defects', 'director_plane', 'director'
]

auto_setup_widget_names = ["director_plane_widget"]

auto_setup_scalar_field_names = [
    'LdG_K1',
    'LdG_K2',
    'LdG_K3',
    'LdG_L1',
    'LdG_L2',
    'LdG_L24',
    'LdG_L3',
    'LdG_L4',
    'LdG_L6',
    'cholestericity',
    'inverse_order',
    'nematic_sites',
    'ones',
    'order_biaxial',
    'order_uniaxial',
    'saddle-splay_Q',
    'site_types',
    'splay-bend',
    'twist_Q'
]

auto_setup_vector_field_names = [
    'active_force_Q',
    'director',
    'saddle-splay_vector_Q'
]

auto_setup_tensor_field_names = ['Q']


def make_random_director_field(lx, ly, lz):
    n = np.random.random((lx, ly, lz, 3))
    norms = np.sum(n * n, axis=-1)
    for i in range(3):
        n[..., i] /= norms
    n_flattened = np.array([n[..., i].flatten() for i in range(3)]).T
    return n_flattened


def coordinates_list(lx, ly, lz):
    x_vals = np.arange(lx)
    y_vals = np.arange(ly)
    z_vals = np.arange(lz)
    Z, Y, X = np.meshgrid(z_vals, y_vals, x_vals)
    coords_flattened = np.array([X.flatten(), Y.flatten(), Z.flatten()]).T
    return coords_flattened


def make_random_director_data(
    lx=10, ly=11, lz=12, filename="test_director_data.dat"
):
    n_flattened = make_random_director_field(lx, ly, lz)
    coords_flattened = coordinates_list(lx, ly, lz)
    n_data = np.concatenate((coords_flattened, n_flattened), axis=-1)
    np.savetxt(filename, n_data, delimiter="\t")


def make_random_q_tensor_field(lx, ly, lz, s=1.):
    q_comps = 2 * (np.random.random((lx * ly * lz, 5)) - 0.5)
    q_comps *= 3 * s / 2
    return q_comps


def make_random_open_qmin_style_data(
    lx=10, ly=11, lz=12, filename="test_open_qmin_data.dat"
):
    S = 1.
    q_components = make_random_q_tensor_field(lx, ly, lz, s=S)
    coords_flattened = coordinates_list(lx, ly, lz)
    site_types = np.zeros(lx * ly * lz)
    S_vals = np.full((lx * ly * lz,), S)
    data = np.concatenate(
        (
            coords_flattened,
            q_components,
            np.array([site_types, S_vals]).T,
        ),
        axis=-1
    )
    np.savetxt(filename, data, delimiter="\t")


class TestPlotType:

    for filename in file_list:
        make_random_open_qmin_style_data(10, 11, 12, filename=filename)

    def make_empty_plotter(self):
        self.plotter = NematicPlot()

    def make_plotter_with_one_file(self):
        self.plotter = NematicPlot(file_list[0])

    def make_plotter_with_file_list(self):
        self.plotter = NematicPlot(file_list)

    def make_plotter_with_glob(self):
        self.plotter = NematicPlot(file_glob_str)

    def test_plot_type_no_files(self):
        self.make_empty_plotter()
        assert isinstance(self.plotter, NematicPlot)

    def test_plot_type_one_file(self):
        self.make_plotter_with_one_file()
        assert isinstance(self.plotter, NematicPlot)

    def test_plot_type_file_list(self):
        self.make_plotter_with_file_list()
        assert isinstance(self.plotter, NematicPlot)

    def test_plot_type_file_glob(self):
        self.make_plotter_with_glob()
        assert isinstance(self.plotter, NematicPlot)

    def test_plot_type_load_single_file(self):
        self.plotter = NematicPlot()
        self.plotter.load(file_list[0])
        assert isinstance(self.plotter, NematicPlot)

    def test_plot_type_load_file_list(self):
        self.plotter = NematicPlot()
        self.plotter.load(file_list)
        assert isinstance(self.plotter, NematicPlot)

    def test_plot_type_load_file_glob(self):
        self.plotter = NematicPlot()
        self.plotter.load(file_glob_str)
        assert isinstance(self.plotter, NematicPlot)


class TestNematicPlotSingleFile:

    def make_plotter(self):
        if not hasattr(self, "plotter"):
            # noinspection PyAttributeOutsideInit
            self.plotter = NematicPlot(file_list[0])
        return self.plotter


class TestDirectorImport:
    def test_load_director(self):
        Lx = 10
        Ly = 11
        Lz = 12
        filename = "data/test_director_data.dat"
        make_random_director_data(Lx, Ly, Lz, filename=filename)
        self.plotter = NematicPlot(filename, data_format="director")
        assert self.plotter.data[0].shape == (Lx * Ly * Lz, 10)


class TestAutoSetup(TestNematicPlotSingleFile):

    def test_mesh_names(self):
        plotter = self.make_plotter()
        # check that we have all the expected meshes
        assert plotter.mesh_names() == auto_setup_mesh_names

    def test_filter_formula_names(self):
        plotter = self.make_plotter()
        # check that each mesh has a FilterFormula
        assert (
            list(plotter.filter_formulas.keys())
            == plotter.mesh_names()
        )

    def test_actor_names(self):
        plotter = self.make_plotter()
        # check that we have all the expected actors
        assert plotter.actor_names() == auto_setup_actor_names

    def test_widget_names(self):
        plotter = self.make_plotter()
        # check that we have the expected widget
        widget_names = list(plotter.widgets.keys())
        assert widget_names == auto_setup_widget_names

    def test_widget_formula_names(self):
        plotter = self.make_plotter()
        # check that each widget has a widget formula
        widget_formula_names = list(plotter.widget_formulas.keys())
        assert widget_formula_names == auto_setup_widget_names

    def test_toolbar_names(self):
        plotter = self.make_plotter()
        toolbar_names = plotter.toolbars.keys()
        # check that each actor has a control toolbar
        for actor_name in auto_setup_actor_names:
            assert actor_name in toolbar_names

    def test_scalar_field_names(self):
        plotter = self.make_plotter()
        assert plotter.scalar_fields() == auto_setup_scalar_field_names

    def test_vector_field_names(self):
        plotter = self.make_plotter()
        assert plotter.vector_fields() == auto_setup_vector_field_names

    def test_tensor_field_names(self):
        plotter = self.make_plotter()
        assert plotter.tensor_fields() == auto_setup_tensor_field_names


class TestControlPanel(TestNematicPlotSingleFile):

    def test_toolbar_names(self):
        # check for expected toolbar names in control panel
        plotter = self.make_plotter()
        toolbar_names = plotter.toolbars.keys()
        assert "animation" in toolbar_names
        assert "lighting" in toolbar_names
        assert "widgets" in toolbar_names
        assert "filters tree" in toolbar_names

#
# class TestAddMenu(TestNematicPlotSingleFile):
#
#     def test_add_slice_menu(self):
#         plotter = self.make_plotter()
#         the_slice_submenu = plotter.the_Add_menu.children()[1]
#
#         # menu is filled when it's about to show
#         the_slice_submenu.aboutToShow.emit()
#
#         # test on solid color and all color arrays
#         for idx, child in enumerate(the_slice_submenu.children()[1:]):
#             old_num_actors = len(plotter.renderer.actors)
#             child.trigger()
#
#             # check that a new actor was created
#             assert len(plotter.renderer.actors) > old_num_actors
#
#             names = list(plotter.renderer.actors.keys())
#             mesh_actor_names = [
#                 name for name in names if not name.startswith("colorbar:")
#             ]
#             actor_name = mesh_actor_names[-1]
#
#             # check that a mesh with the same name was created
#             assert actor_name in plotter.mesh_names
#
#             # check that a filter formula with the same name was created
#             assert actor_name in plotter.filter_formulas.keys()
#
#             # check that a widget and widget formula were created
#             widget_name = plotter.get_filter_formula(actor_name).widget_name
#             assert widget_name in plotter.widgets.keys()
#             assert widget_name in plotter.widget_formulas.keys()
#
#             # check that the actor is visible
#             actor = plotter.renderer.actors.get(actor_name)
#             assert actor.GetVisibility() == 1
#
#             # check that the actor has a control toolbar
#             assert actor_name in plotter.toolbars.keys()
#
#             # check that the control toolbar's visibility checkbox is checked
#             toolbar = plotter.toolbars[actor_name]
#             visibility_checkbox = toolbar.visibility_checkbox
#             assert visibility_checkbox.isChecked()
#
#             # check that the visibility checkbox works
#             visibility_checkbox.setChecked(0)
#             assert actor.GetVisibility() == 0
#             visibility_checkbox.setChecked(1)
#             assert actor.GetVisibility() == 1
#
#             filter_formula = plotter.get_filter_formula(actor_name)
#             # for slices with color array, check that a colorbar exists
#             if idx > 0:
#                 assert filter_formula.colorbar is not None
#
