"""
open-ViewMin project top-level module
"""

from open_viewmin.filter_tree_plot.filter_tree_plot import FilterTreePlot
from open_viewmin.filter_tree_plot_Qt.filter_tree_plot_Qt import FilterTreePlotQt
from open_viewmin.nematic.nematic_plot import NematicPlotNoQt
from open_viewmin.nematic.nematic_plot import NematicPlot
from open_viewmin.filter_tree_plot.filter_formulas import FilterFormula
from open_viewmin.filter_tree_plot.widget_formulas import WidgetFormula
from pyvista import start_xvfb, set_jupyter_backend

