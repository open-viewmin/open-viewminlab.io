#!/usr/bin/env python3

""" Executable "front-end" file """

import sys
import glob
from filter_tree_plot.utilities.ovm_utilities import sort_filenames_by_timestamp
from open_viewmin import NematicPlot

# FilterTreePlot/Qt todos
# TODO fix off-screen/server interaction
# TODO save, load state
# TODO make do_after_update a dictionary so that unwanted tasks can be overwritten
# TODO fix bug where Refresh resets color array bounds
# TODO merge default_mesh, fullmesh concepts?

# Nematic todos
# TODO chi, tau defects in cholesterics
# TODO enforce uniform sign choice for defect line tangents
# TODO add defect azimuthal direction
# TODO add sign corrections for rotation vector calculations

# Documentation todos
# TODO Separate documentation for FilterTreePlot, ViewMinPlot, and NematicPlot
# TODO explain default datasets in documentation


plotter_class = NematicPlot
""" subtype of :class:`~open_viewmin.filter_tree_plot.filter_tree_plot.FilterTreePlot` to set up """

if __name__ == '__main__':
    filenames = []
    """ names of files to import """

    kwargs = {}
    if len(sys.argv) > 1:
        skip = False
        for argi in range(1, len(sys.argv)):
            if skip:
                skip = False
                continue
            else:
                arg = sys.argv[argi]
                if arg[:2] == '--':
                    kwargs[arg[2:]] = eval(sys.argv[argi + 1])
                    skip = True
                else:
                    filenames += glob.glob(arg)
    if len(filenames) > 0:
        filenames = sort_filenames_by_timestamp(filenames)
        print('Found these files (and importing in this order):')
        for filename in filenames:
            print(filename)
        my_viewmin_plot = plotter_class(filenames, **kwargs)
    else:
        my_viewmin_plot = plotter_class(**kwargs)
    input("\nPress Enter in this window to exit or return control to interpreter.\n")
