""" Define actions and set up dialogs for actor visualization options accessed
by a dropdown menu in the actor's control toolbar
"""


from matplotlib import colormaps
from qtpy import QtWidgets as qw
from qtpy import QtCore as Qt
from qtpy import QtGui as qg
import numpy as np
from open_viewmin.filter_tree_plot_Qt.widgets_Qt import utilities_Qt
from open_viewmin.filter_tree_plot_Qt.menus import glyph_modify, streamlines_modify, add_actors_widgets
from open_viewmin.filter_tree_plot_Qt.widgets_Qt.utilities_Qt import DropdownMenu


colormaps_list = colormaps()


# noinspection PyUnresolvedReferences,PyArgumentList
def color_picker(plotter, actor_name):
    """ Launch system color dialog to set solid color of a mesh

    Parameters
    ----------
    plotter : :class:`~open_viewmin.filter_tree_plot_Qt.filter_tree_plot_Qt.FilterTreePlotQt`
    actor_name : str
        Name of mesh to color
    """

    filter_formula = plotter.get_filter_formula(actor_name)
    mesh_kwargs = filter_formula.mesh_kwargs
    initial_color = mesh_kwargs.get('color', plotter.settings["actor_color"])

    utilities_Qt.ColorDialogValid(
        lambda color: filter_formula.set(color=color),
        initial_color,
        plotter.app_window
    )


# noinspection PyUnresolvedReferences,PyArgumentList
def make_color_array_widget(plotter, actor_name):
    """ Launch dialog for coloring a mesh by a scalar array.

    Parameters
    ----------
    plotter : :class:`~open_viewmin.filter_tree_plot_Qt.filter_tree_plot_Qt.FilterTreePlotQt`
    actor_name : str

    """

    filter_formula = plotter.get_filter_formula(actor_name)
    mesh_kwargs = filter_formula.mesh_kwargs

    color_array_dropdown = DropdownMenu(width=150)
    le_clim_min = qw.QLineEdit()  # holds color min
    le_clim_max = qw.QLineEdit()  # holds color max

    # ensure text entry gives float values
    validator = qg.QDoubleValidator()
    n_decimals = 6
    n_slider_subdivisions = 100
    # arbitrary large range
    validator.setRange(-1e9, 1e9, decimals=n_decimals)

    for le in (le_clim_min, le_clim_max):
        le.setFixedWidth(100)
        le.setValidator(validator)

    # sliders to accompany QLineEdit
    qs_clim_min = qw.QSlider(orientation=Qt.Qt.Horizontal)
    qs_clim_max = qw.QSlider(orientation=Qt.Qt.Horizontal)

    # color maps
    cmap_dropdown = DropdownMenu(width=color_array_dropdown.width())
    label_action_pairs = []

    def cmap_callback(callback_cmap_name):
        def return_func():
            plotter.update_actor(actor_name, cmap=callback_cmap_name)
        return return_func

    for cmap_name in colormaps_list:
        label_action_pairs.append((cmap_name, cmap_callback(cmap_name)))
    cmap_dropdown.populate(label_action_pairs)
    current_cmap = mesh_kwargs.get("cmap", plotter.theme['cmap'])
    current_color_array = mesh_kwargs.get("scalars")
    if current_color_array is not None:
        color_array_dropdown.setText(current_color_array)
    if current_cmap in colormaps_list:
        cmap_dropdown.setText(current_cmap)

    def get_clim(use_previous=True):
        mesh = plotter.get_mesh(actor_name)
        scalar_field_name = mesh_kwargs["scalars"]

        if scalar_field_name in plotter.scalar_fields(mesh_name=mesh):
            scalar_field = mesh[scalar_field_name]
            data_range = (np.min(scalar_field), np.max(scalar_field))
            if use_previous:
                vmin, vmax = mesh_kwargs.get("clim", data_range)
            else:
                vmin, vmax = data_range
            return vmin, vmax
        return None

    def display_clim_val(_le, val):
        _le.setText(f"{val:.{n_decimals}f}")

    # noinspection PyUnresolvedReferences
    def update_clim_maxmin_display():
        """ update clim QLineEdits and sliders"""
        clim = get_clim()
        if clim is not None:
            plotter.update_actor(actor_name, clim=clim)
            vmin, vmax = clim
            display_clim_val(le_clim_min, vmin)
            display_clim_val(le_clim_max, vmax)
            set_slider_from_lineedit(le_clim_min, qs_clim_min)
            set_slider_from_lineedit(le_clim_max, qs_clim_max)

    def retitle_colorbar():
        """ Apply new title to colorbar actor """
        title = le_colorbar_title.text()
        plotter.update_actor(actor_name, scalar_bar_args=dict(title=title))

    def colorby_callback(color_array_name):
        """ Update actor to be colored by a given scalar dataset.

        Automatically updates color limits and colorbar title.

        Parameters
        ----------
        color_array_name : str
            name of scalar array to use for colors
        """
        mesh = plotter.get_mesh(actor_name)
        vmin = np.min(mesh[color_array_name])
        vmax = np.max(mesh[color_array_name])
        plotter.update_actor(
            actor_name,
            scalars=color_array_name,
            clim=[vmin, vmax]
        )
        update_clim_maxmin_display()
        callback_filter_formula = plotter.get_filter_formula(actor_name)
        callback_mesh_kwargs = callback_filter_formula.mesh_kwargs
        new_colorbar_title = callback_mesh_kwargs['scalar_bar_args']['title']
        le_colorbar_title.setText(new_colorbar_title)

    def populate_cb_colorby():
        """ Populate "color by" dropdown menu with available scalar fields.

        Clicking the name of a scalar field activates an action that updates
        an actor's color as well as the associated colorbar.
        """

        scalar_fields = plotter.scalar_fields(mesh_name=actor_name)

        def this_colorby_callback(field_name):
            def return_func():
                return colorby_callback(field_name)
            return return_func
        color_array_dropdown.populate([
            (field_name, this_colorby_callback(field_name))
            for field_name in scalar_fields
        ])

    le_colorbar_title = qw.QLineEdit()
    le_colorbar_title.setFixedWidth(200)
    le_colorbar_title.setText(mesh_kwargs['scalar_bar_args'].get('title', ''))
    le_colorbar_title.editingFinished.connect(retitle_colorbar)

    def set_slider_from_lineedit(_le, qs):
        """ Set a slider's value from a numerical text entry.

        Divide-by-zero and overflow errors pass silently.

        Parameters
        ----------
        _le : qtpy.QtWidgets.QLineEdit
        qs : qtpy.QtWidgets.QSlider
        """
        min_val, max_val = get_clim(use_previous=False)
        val = float(_le.text())
        try:
            qs.setValue(
                int(
                    (val - min_val) / (max_val - min_val)
                    * n_slider_subdivisions
                )
            )
        except (ZeroDivisionError, OverflowError):
            pass

    def lineedit_val_from_slider(qs):
        """ Calculate a value to fill a QLineEdit widget to update it in
        accordance with a QSlider's value.

        Parameters
        ----------
        qs : qtpy.QtWidgets.QSlider

        Returns
        -------
        float
            Numerical value for QLineEdit
        """
        min_val, max_val = get_clim(use_previous=False)
        val = qs.value()
        return val / n_slider_subdivisions * (max_val - min_val) + min_val

    def set_le_clim_min(value, set_slider=True):
        clim_max_val = float(le_clim_max.text())
        # don't let clim min exceed clim max
        if value > clim_max_val:
            value = clim_max_val
            set_slider = True  # force slider away from invalid setting

        display_clim_val(le_clim_min, value)
        plotter.update_actor(actor_name, clim=[value, clim_max_val])
        if set_slider:
            set_slider_from_lineedit(le_clim_min, qs_clim_min)

    def le_clim_min_callback():
        value = float(le_clim_min.text())
        set_le_clim_min(value)

    le_clim_min.editingFinished.connect(le_clim_min_callback)

    def set_le_clim_max(value, set_slider=True):
        clim_min_val = float(le_clim_min.text())
        # don't let clim max drop below clim min
        if value < clim_min_val:
            value = clim_min_val
            set_slider = True  # force slider away from invalid setting

        display_clim_val(le_clim_max, value)

        plotter.update_actor(actor_name, clim=[clim_min_val, value])
        if set_slider:
            set_slider_from_lineedit(le_clim_max, qs_clim_max)

    def le_clim_max_callback():
        value = float(le_clim_max.text())
        set_le_clim_max(value)

    le_clim_max.editingFinished.connect(le_clim_max_callback)

    def qs_clim_min_callback():
        set_le_clim_min(
            lineedit_val_from_slider(qs_clim_min),
            set_slider=False
        )

    qs_clim_min.sliderMoved.connect(qs_clim_min_callback)
    qs_clim_min.sliderReleased.connect(qs_clim_min_callback)

    def qs_clim_max_callback():
        set_le_clim_max(
            lineedit_val_from_slider(qs_clim_max),
            set_slider=False
        )

    qs_clim_max.sliderMoved.connect(qs_clim_max_callback)
    qs_clim_max.sliderReleased.connect(qs_clim_max_callback)

    color_array_widget = utilities_Qt.FormDialog(
        f'Color for \"{actor_name}\"',
        parent=plotter.app_window,
        apply_button=True
    )
    color_array_widget.add_row('color array', color_array_dropdown)
    color_array_widget.add_row('colormap', cmap_dropdown)

    sublayout_min = qw.QHBoxLayout()
    sublayout_min.addWidget(le_clim_min)
    sublayout_min.addWidget(qs_clim_min)

    # pushbutton to symmetrize range about zero by changing min
    pb_symmetrize_min = qw.QPushButton('= -max')

    def pb_symmetrize_min_callback():
        """ Set color limit min value to be the negative of the color limit
        max value.

        If the max value is negative, this does nothing.
        """
        current_max_val = float(le_clim_max.text())
        if current_max_val > 0:
            new_min_val = -current_max_val
            display_clim_val(le_clim_min, new_min_val)

    pb_symmetrize_min.clicked.connect(pb_symmetrize_min_callback)
    sublayout_min.addWidget(pb_symmetrize_min)
    color_array_widget.add_row('min value', sublayout_min)

    sublayout_max = qw.QHBoxLayout()
    sublayout_max.addWidget(le_clim_max)
    sublayout_max.addWidget(qs_clim_max)

    # pushbutton to symmetrize range about zero by changing max
    pb_symmetrize_max = qw.QPushButton('= |min|')

    def pb_symmetrize_max_callback():
        """ Set color limit max value to be the absolute value of the color
        limit min value.

        If the min value is positive, this does nothing.
        """
        current_min_val = float(le_clim_min.text())
        if current_min_val < 0:
            new_max_val = -current_min_val
            display_clim_val(le_clim_max, new_max_val)

    pb_symmetrize_max.clicked.connect(pb_symmetrize_max_callback)
    sublayout_max.addWidget(pb_symmetrize_max)
    color_array_widget.add_row('max value', sublayout_max)

    sublayout_title = qw.QHBoxLayout()
    sublayout_title.addWidget(le_colorbar_title)
    color_array_widget.add_row('colorbar title', sublayout_title)

    populate_cb_colorby()
    update_clim_maxmin_display()

    def apply_le_clims():
        le_clim_max_callback()
        le_clim_min_callback()

    color_array_widget.set_accepted_callback(apply_le_clims)
    color_array_widget.show()


def active_scalars_color_callback(plotter, actor_name):
    """ Color an actor by its most recently used scalar dataset

    Parameters
    ----------
    plotter : :class:`~open_viewmin.filter_tree_plot.filter_tree_plot.FilterTreePlot`
    actor_name : str
    """

    plotter.get_filter_formula(actor_name).color_by_active_scalars()


# noinspection PyUnresolvedReferences,PyArgumentList
def set_opacity_callback(plotter, actor_name):
    """ Launch dialog for opacity and lighting settings.

    Parameters
    ----------
    plotter : :class:`~open_viewmin.filter_tree_plot_Qt.filter_tree_plot_Qt.FilterTreePlotQt`
    actor_name : str

    """

    filter_formula = plotter.get_filter_formula(actor_name)
    mesh_kwargs = filter_formula.mesh_kwargs
    defaults = plotter.settings['default_mesh_kwargs']
    opacity = mesh_kwargs.get("opacity", 1)

    set_opacity_widget = utilities_Qt.FormDialog(
        title="Opacity & lighting",
        message=f'Opacity & lighting options for '
              f'\"{actor_name}\"'
    )

    sb_set_opacity = qw.QDoubleSpinBox(minimum=0, maximum=1)
    sb_set_opacity.setSingleStep(0.1)
    sb_set_opacity.valueChanged.connect(
        lambda value: plotter.update_actor(actor_name, opacity=value)
    )
    set_opacity_widget.add_row("", qw.QLabel('<b>Opacity</b>'))
    set_opacity_widget.add_row('opacity: ', sb_set_opacity)

    set_opacity_widget.add_row(
        "", qw.QLabel('<b>Physically Based Rendering (PBR)</b>')
    )
    cb_pbr = qw.QCheckBox()
    cb_pbr.stateChanged.connect(
        lambda: plotter.update_actor(actor_name, pbr=cb_pbr.isChecked())
    )
    set_opacity_widget.add_row('PBR:', cb_pbr)
    sb_metallic = qw.QDoubleSpinBox(
        minimum=0, maximum=1, singleStep=0.1, decimals=2
    )
    sb_metallic.valueChanged.connect(
        lambda value: plotter.update_actor(actor_name, metallic=value)
    )
    set_opacity_widget.add_row('metallic: ', sb_metallic)

    sb_roughness = qw.QDoubleSpinBox(
        minimum=0, maximum=1, singleStep=0.1, decimals=2
    )
    sb_roughness.valueChanged.connect(
        lambda value: plotter.update_actor(actor_name, roughness=value)
    )
    set_opacity_widget.add_row('roughness: ', sb_roughness)

    set_opacity_widget.add_row("", qw.QLabel('<b>Other options</b>'))

    sb_diffuse = qw.QDoubleSpinBox(
        minimum=0, maximum=1, singleStep=0.1, decimals=2
    )
    sb_diffuse.valueChanged.connect(
        lambda value: plotter.update_actor(actor_name, diffuse=value)
    )
    set_opacity_widget.add_row('diffuse: ', sb_diffuse)

    sb_ambient = qw.QDoubleSpinBox(
        minimum=0, maximum=1, singleStep=0.1, decimals=2
    )
    sb_ambient.valueChanged.connect(
        lambda value: plotter.update_actor(actor_name, ambient=value)
    )
    set_opacity_widget.add_row('ambient: ', sb_ambient)

    sb_specular = qw.QDoubleSpinBox(
        minimum=0, maximum=1, singleStep=0.1, decimals=2
    )
    sb_specular.valueChanged.connect(
        lambda value: plotter.update_actor(actor_name, specular=value)
    )
    set_opacity_widget.add_row('specular: ', sb_specular)

    sb_specular_power = qw.QSpinBox(
        minimum=1, maximum=100, singleStep=1
    )
    sb_specular_power.valueChanged.connect(
        lambda value: plotter.update_actor(actor_name, specular_power=value)
    )
    set_opacity_widget.add_row('specular power: ', sb_specular_power)
    sb_set_opacity.setValue(opacity)
    current_pbr = mesh_kwargs.get("pbr", defaults['pbr'])
    cb_pbr.setChecked(current_pbr)
    for spinbox, attr_string in (
        (sb_metallic, "metallic"),
        (sb_roughness, "roughness"),
        (sb_diffuse, "diffuse"),
        (sb_ambient, "ambient"),
        (sb_specular, "specular"),
        (sb_specular_power, "specular_power")
    ):
        current_val = mesh_kwargs.get(attr_string, defaults[attr_string])
        spinbox.setValue(current_val)
    set_opacity_widget.show()


def toggle_colorbar_callback(plotter, actor_name):
    """ Toggle visibility of mesh's colorbar, if it exists.

    Parameters
    ----------
    plotter : :class:`~open_viewmin.filter_tree_plot_Qt.filter_tree_plot_Qt.FilterTreePlotQt`
    actor_name : str

    """

    colorbar = plotter.get_filter_formula(actor_name).colorbar
    if colorbar is not None:
        plotter.toggle_scalar_bar_visibility(colorbar)


def rename_callback(plotter, actor_name):
    """ Launch dialog to rename a mesh.

    Parameters
    ----------
    plotter : :class:`~open_viewmin.filter_tree_plot_Qt.filter_tree_plot_Qt.FilterTreePlotQt`
    actor_name : str

    """
    rename_widget = utilities_Qt.FormDialog(
        title="Rename",
        message=f"Rename mesh \"{actor_name}\"",
        parent=plotter.app_window
    )
    rename_line_edit = qw.QLineEdit()
    rename_widget.add_row("Rename as: ", rename_line_edit)
    rename_widget.set_accepted_callback(
        lambda: plotter.rename_mesh(actor_name, rename_line_edit.text())
    )
    rename_widget.show()


def remove_callback(plotter, actor_name):
    """ Launch dialog to remove a mesh.

    Parameters
    ----------
    plotter : :class:`~open_viewmin.filter_tree_plot_Qt.filter_tree_plot_Qt.FilterTreePlotQt`
    actor_name : str

    """

    descendants = []

    def get_descendants_of(mesh_name):
        children = plotter.get_filter_formula(mesh_name).children
        for child in children:
            if child not in descendants:
                descendants.append(child)
            get_descendants_of(child)

    get_descendants_of(actor_name)
    message = f"Really remove mesh \"{actor_name}\"?"
    if len(descendants) > 0:
        message += (
            f"\nThis will also remove the following descendant meshes:\n  "
            + str(descendants) + "\n"
        )
    message += "\nThis action cannot be undone."
    remove_dialog = utilities_Qt.FormDialog(
        title="Remove mesh",
        message=message
    )
    # noinspection PyUnresolvedReferences
    remove_dialog.set_accepted_callback(
        lambda: plotter.remove_actor_completely(actor_name)
    )
    remove_dialog.show()


def apolar_callback(plotter, actor_name):
    """ Toggle `apolar` flag for orientations.

    Parameters
    ----------
    plotter : :class:`~open_viewmin.filter_tree_plot_Qt.filter_tree_plot_Qt.FilterTreePlotQt`
    actor_name : str

    """

    filter_formula = plotter.get_filter_formula(actor_name)
    ovm_info = filter_formula.ovm_info
    current_apolar = ovm_info.get("apolar", False)
    ovm_info["apolar"] = not current_apolar
    normal_widget_name = plotter.get_filter_formula(actor_name).widget_name
    plotter.get_widget_formula(normal_widget_name).update()


def streamlines_settings_callback(plotter, actor_name):
    """ Launch dialog to set streamlines filter settings.

    Parameters
    ----------
    plotter : :class:`~open_viewmin.filter_tree_plot_Qt.filter_tree_plot_Qt.FilterTreePlotQt`
    actor_name : str

    """

    streamlines_settings_widget = (
        streamlines_modify.make_streamlines_settings_widget(
            plotter, actor_name
        )
    )
    streamlines_settings_widget.show()


def line_settings_callback(plotter, actor_name):
    line_settings_widget = make_line_settings_widget(plotter, actor_name)
    line_settings_widget.show()


def make_line_settings_widget(plotter, actor_name):
    min_width = 300
    dlg = utilities_Qt.FormDialog(
        title=f'Line settings for \"{actor_name}\"'
    )
    dlg.setMinimumWidth(min_width)

    filter_formula = plotter.get_filter_formula(actor_name)
    filter_kwargs = filter_formula.filter_kwargs

    # noinspection PyArgumentList
    sb_resolution = qw.QSpinBox(
        minimum=1,
        maximum=max(plotter.dims())
    )
    sb_resolution.setValue(filter_kwargs.get("resolution", 4))
    sb_resolution.setSingleStep(1)
    sb_resolution.setToolTip('Press Enter to apply.')

    def resolution_callback():
        plotter.update_filter(
            actor_name,
            resolution=sb_resolution.value(),
            update_actor=True
        )

    sb_resolution.editingFinished.connect(resolution_callback)
    dlg.add_row('resolution', sb_resolution)

    return dlg


def glyph_settings_choice_callback(plotter, actor_name):
    """ Launch dialog to set glyph filter settings.

    Parameters
    ----------
    plotter : :class:`~open_viewmin.filter_tree_plot_Qt.filter_tree_plot_Qt.FilterTreePlotQt`
    actor_name : str

    """

    glyph_settings_widget = glyph_modify.make_glyph_settings_widget(
        plotter, actor_name
    )
    glyph_settings_widget.show()


def tensor_ellipsoids_settings_callback(plotter, actor_name):
    """ Launch dialog to set tensor ellipsoids filter settings.

    Parameters
    ----------
    plotter : :class:`~open_viewmin.filter_tree_plot_Qt.filter_tree_plot_Qt.FilterTreePlotQt`
    actor_name : str

    """

    filter_formula = plotter.get_filter_formula(actor_name)
    add_actors_widgets.make_add_tensor_ellipsoids_widget(
        plotter,
        actor_name=actor_name,
        mesh_name=filter_formula.parent_mesh_name,
        tensor_field_name=filter_formula.ovm_info.get(
            "tensor_field_name"
        ),
        inplace=True
    )


def num_centroids_callback(plotter, actor_name):
    """ Launch dialog to set centroids filter settings.

    Parameters
    ----------
    plotter : :class:`~open_viewmin.filter_tree_plot_Qt.filter_tree_plot_Qt.FilterTreePlotQt`
    actor_name : str

    """

    set_num_centroids_widget = utilities_Qt.FormDialog(
        title="Number of centroids",
        message=f"Set number of centroids in mesh \"{actor_name}\""
    )
    filter_formula = plotter.get_filter_formula(actor_name)
    current_num_centroids = filter_formula.filter_kwargs.get(
        "num_centroids", 1
    )
    sb_set_num_centroids = qw.QSpinBox()
    sb_set_num_centroids.setMinimum(0)
    sb_set_num_centroids.setMaximum(1000)
    sb_set_num_centroids.setValue(current_num_centroids)
    sb_set_num_centroids.setSingleStep(1)
    # noinspection PyUnresolvedReferences
    sb_set_num_centroids.editingFinished.connect(
        lambda: filter_formula.update(
            update_actor=True, num_centroids=sb_set_num_centroids.value()
        )
    )
    set_num_centroids_widget.add_row("num centroids", sb_set_num_centroids)
    set_num_centroids_widget.show()


def circuit_options_callback(plotter, actor_name):
    """ Launch dialog to set circuit filter settings.

    Parameters
    ----------
    plotter : :class:`~open_viewmin.filter_tree_plot_Qt.filter_tree_plot_Qt.FilterTreePlotQt`
    actor_name : str

    """

    circuit_options_widget = utilities_Qt.FormDialog(
        title="Circuit options",
        message=f"Set circuit options for mesh \"{actor_name}\""
    )
    filter_formula = plotter.get_filter_formula(actor_name)

    sb_radius = qw.QDoubleSpinBox()
    sb_radius.setMinimum(0)
    sb_radius.setMaximum(100)
    sb_radius.setValue(filter_formula.filter_kwargs.get("radius", 1))
    # noinspection PyUnresolvedReferences
    sb_radius.editingFinished.connect(
        lambda: filter_formula.update(
            update_actor=True, radius=sb_radius.value()
        )
    )
    circuit_options_widget.add_row("radius", sb_radius)

    sb_n_samples = qw.QSpinBox()
    sb_n_samples.setMinimum(0)
    sb_n_samples.setMaximum(100)
    sb_n_samples.setValue(filter_formula.filter_kwargs.get("n_samples", 1))
    # noinspection PyUnresolvedReferences
    sb_n_samples.editingFinished.connect(
        lambda: filter_formula.update(
            update_actor=True, n_samples=sb_n_samples.value()
        )
    )
    circuit_options_widget.add_row("num samples", sb_n_samples)

    cb_use_ints = qw.QCheckBox()
    cb_use_ints.setChecked(filter_formula.filter_kwargs.get("use_ints", False))
    # noinspection PyUnresolvedReferences
    cb_use_ints.stateChanged.connect(
        lambda: filter_formula.update(
            update_actor=True, use_ints=cb_use_ints.isChecked()
        )
    )
    circuit_options_widget.add_row("use ints", cb_use_ints)
    circuit_options_widget.show()


def populate_viz_options_menu(plotter, actor_name, viz_options_button=None):
    """ Update visualization options dropdown menu in an actor's control
    toolbar.

    Parameters
    ----------
    plotter : :class:`~open_viewmin.filter_tree_plot_Qt.filter_tree_plot_Qt.FilterTreePlotQt`
    actor_name : str
    viz_options_button : qtpy.QtWidgets.QPushButton, optional

    """

    filter_formula = plotter.get_filter_formula(actor_name)
    if viz_options_button is None:
        toolbar = plotter.toolbars[actor_name]
        viz_options_button = toolbar.visualization_options_button
    viz_options_menu = viz_options_button.menu()
    viz_options_name_action_pairs = [
        ["solid color", color_picker],
        ["color array", make_color_array_widget],
        ["color by active scalars", active_scalars_color_callback],
        ["show/hide colorbar", toggle_colorbar_callback],
        ["lighting", set_opacity_callback]
    ]
    if "apolar" in filter_formula.ovm_info.keys():
        viz_options_name_action_pairs.append(
            ["toggle apolar", apolar_callback]
        )
    viz_options_name_action_pairs += [
        ["rename", rename_callback],
        ["remove (!)", remove_callback]
    ]
    viz_options_menu.clear()

    def meta_callback(the_callback):
        return lambda: the_callback(plotter, actor_name)

    for label, callback in viz_options_name_action_pairs:
        viz_options_menu.addAction(
            label,
            meta_callback(callback)
        )
    viz_options_button.setToolTip(
        f'Visualization options and other customizations for \"{actor_name}\"'
    )


def add_visualization_options_menu(actor_control_toolbar):
    """ Create dropdown menu of actor customizations

    Parameters
    ----------
    actor_control_toolbar : open_viewmin.filter_tree_plot_Qt.widgets_Qt.controls.ActorControlToolbar
        Actor control toolbar to which menu will be added.

    Returns
    -------
    qtpy.QtWidgets.QPushButton

    """

    plotter = actor_control_toolbar.plotter
    toolbar = actor_control_toolbar.options_toolbar
    actor_name = actor_control_toolbar.actor_name

    viz_options_button = qw.QPushButton(plotter.settings["actor_menu_symbol"])
    viz_options_button.setFixedWidth(55)
    viz_options_menu = qw.QMenu()

    viz_options_button.setMenu(viz_options_menu)
    populate_viz_options_menu(plotter, actor_name, viz_options_button)
    toolbar.addWidget(viz_options_button)
    return viz_options_button
