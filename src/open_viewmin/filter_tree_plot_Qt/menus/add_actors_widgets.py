""" Dialogues for creation of certain data visualization types """


import qtpy.QtWidgets as qw
from open_viewmin.filter_tree_plot_Qt.widgets_Qt import utilities_Qt
from open_viewmin.filter_tree_plot.filters import glyphs, streamlines


# noinspection PyUnresolvedReferences
def make_add_glyphs_widget(plotter, mesh_name=None):
    """ Launch a Qt dialog for defining new glyphs

    Parameters
    ----------
    plotter : open_viewmin.FilterTreePlotQt
    mesh_name : str or None, optional
        Mesh on which to draw glyphs

    """

    if mesh_name is None:
        message = None
    else:
        message = f"New glyphs on mesh \"{mesh_name}\":"
    plotter.add_glyphs_widget = utilities_Qt.FormDialog(
        title="New glyphs", message=message
    )
    w = plotter.add_glyphs_widget
    w.setMinimumWidth(200)
    add_glyphs_kwargs = dict(
        mesh=mesh_name,
        orient=None,
        glyph_shape="arrow",
        scale=None
    )

    def add_glyphs_orient_callback(vector_field_name):
        """ Make callback that sets glyphs orientation dataset

        Parameters
        ----------
        vector_field_name : str

        Returns
        -------
        callable
        """

        def return_func():
            add_glyphs_kwargs['orient'] = vector_field_name
        return return_func

    glyphs_orient_dropdown = utilities_Qt.DropdownMenu()

    def populate_glyphs_orient_dropdown(dropdown_mesh_name):
        """ Populate glyphs orientation dropdown

        Populates with names of available vector fields

        Parameters
        ----------
        dropdown_mesh_name : str
        """

        vector_field_names = plotter.vector_fields(mesh=dropdown_mesh_name)
        glyphs_orient_dropdown.populate([
            (vector_field_name, add_glyphs_orient_callback(vector_field_name))
            for vector_field_name in vector_field_names
        ])

    populate_glyphs_orient_dropdown(mesh_name)

    def add_glyphs_mesh_callback(callback_mesh_name):
        """ Create callback that sets the parent mesh for a glyphs filter

        Parameters
        ----------
        callback_mesh_name : str

        Returns
        -------
        callable
        """

        def return_func():
            add_glyphs_kwargs['mesh'] = callback_mesh_name
            populate_glyphs_orient_dropdown(callback_mesh_name)
        return return_func

    if mesh_name is None:
        glyphs_mesh_dropdown = utilities_Qt.DropdownMenu()
        glyphs_mesh_dropdown.populate([
            (plotter_mesh_name, add_glyphs_mesh_callback(plotter_mesh_name))
            for plotter_mesh_name in plotter.mesh_names()
        ])
        w.add_row('locations:', glyphs_mesh_dropdown)
    else:
        add_glyphs_mesh_callback(mesh_name)
    w.add_row('orientations:', glyphs_orient_dropdown)

    glyphs_shape_dropdown = utilities_Qt.DropdownMenu()

    def add_glyphs_shape_callback(shape_name):
        """ Create callback that sets glyphs shape

        Parameters
        ----------
        shape_name : str
        Must be a key of :attr:`~open_viewmin.filter_tree_plot_Qt.filter_tree_plot_Qt.FilterTreePlotQt.geometric_objects`

        Returns
        -------
        callable
        """

        def return_func():
            add_glyphs_kwargs['glyph_shape'] = shape_name
        return return_func

    glyphs_shape_dropdown.populate([
        (shape_name, add_glyphs_shape_callback(shape_name))
        for shape_name in plotter.geometric_objects.keys()
    ])
    # add_glyphs_kwargs['glyph_shape'] = 'arrow'
    glyphs_shape_dropdown.setText(add_glyphs_kwargs['glyph_shape'])
    w.add_row('shape:', glyphs_shape_dropdown)

    def add_glyphs_scale_callback(field_name):
        """ Create callback that sets glyphs size scale field

        Parameters
        ----------
        field_name : name of scalar field

        Returns
        -------
        callable
        """

        def return_func():
            add_glyphs_kwargs['scale'] = field_name
        return return_func

    glyphs_scale_dropdown = utilities_Qt.DropdownMenu()
    glyphs_scale_dropdown.populate([
        (field_name, add_glyphs_scale_callback(field_name))
        for field_name in plotter.scalar_fields()
    ])
    add_glyphs_scale_callback(plotter.settings['scale_mesh_name'])()
    glyphs_scale_dropdown.setText(add_glyphs_kwargs.get('scale', ''))
    w.add_row('scale:', glyphs_scale_dropdown)

    def ok_btn_callback():
        """ Add glyphs to mesh

        Does nothing if either `mesh` or 'orient' is not a key of :attr:`add_glyphs_kwargs`
        """
        if (
            add_glyphs_kwargs.get('mesh') is not None
            and add_glyphs_kwargs['orient'] is not None
        ):
            # noinspection PyTypeChecker
            glyphs.add_glyphs_to_mesh(
                plotter,
                plotter.name_mesh_without_overwriting(
                    add_glyphs_kwargs['orient']
                ),
                mesh_name=add_glyphs_kwargs['mesh'],
                glyph_shape=add_glyphs_kwargs['glyph_shape'],
                scale=add_glyphs_kwargs['scale'],
                orient=add_glyphs_kwargs['orient']
            )

    w.set_accepted_callback(ok_btn_callback)
    w.show()


# noinspection PyUnresolvedReferences
def make_add_surface_streamlines_widget(plotter, mesh_name=None):
    """ Launch a Qt dialog for defining new streamlines

    Parameters
    ----------
    plotter : open_viewmin.FilterTreePlotQt
    mesh_name : str, optional
        Mesh on which to draw streamlines

    """

    if mesh_name is None:
        message = None
    else:
        message = f"New glyphs on mesh \"{mesh_name}\":"

    plotter.add_surface_streamlines_widget = utilities_Qt.FormDialog(
        title='Add surface streamlines', message=message
    )
    dlg = plotter.add_surface_streamlines_widget
    dlg.setMinimumWidth(250)

    add_streamlines_kwargs = dict(
        n_points=0,  # default to all points
        vectors_name=None,
        max_steps=10,
        mesh_name=None
    )

    def add_streamlines_vectors_callback(vectors_name):
        def return_func():
            add_streamlines_kwargs['vectors_name'] = vectors_name
        return return_func

    streamlines_vectors_dropdown = utilities_Qt.DropdownMenu()

    def populate_streamlines_vectors_dropdown(dropdown_mesh_name):
        streamlines_vectors_dropdown.populate([
            (vectors_name, add_streamlines_vectors_callback(vectors_name))
            for vectors_name in plotter.vector_fields(mesh=dropdown_mesh_name)
        ])

    populate_streamlines_vectors_dropdown(mesh_name)

    def add_streamlines_mesh_callback(streamlines_mesh_name):
        def return_func():
            add_streamlines_kwargs['parent_mesh_name'] = streamlines_mesh_name
            populate_streamlines_vectors_dropdown(streamlines_mesh_name)
        return return_func

    streamlines_mesh_dropdown = utilities_Qt.DropdownMenu()
    streamlines_mesh_dropdown.populate([
        (mesh_name, add_streamlines_mesh_callback(mesh_name))
        for mesh_name in plotter.mesh_names()
    ])
    if mesh_name in plotter.mesh_names():
        add_streamlines_mesh_callback(mesh_name)()
        streamlines_mesh_dropdown.setText(mesh_name)

    def ok_btn_callback():
        if (
            add_streamlines_kwargs['vectors_name'] is not None
            and add_streamlines_kwargs['parent_mesh_name'] is not None
        ):
            # noinspection PyUnresolvedReferences,PyTypeChecker
            streamlines.add_streamlines_to_surface(
                plotter,
                plotter.name_mesh_without_overwriting(
                    add_streamlines_kwargs['vectors_name'] + "_streamlines"
                ),
                add_streamlines_kwargs['parent_mesh_name'],
                vectors_name=add_streamlines_kwargs['vectors_name'],
                max_steps=add_streamlines_kwargs['max_steps'],
                n_points=add_streamlines_kwargs['n_points']
            )

    dlg.add_row('surface:', streamlines_mesh_dropdown)
    dlg.add_row('vectors:', streamlines_vectors_dropdown)
    dlg.set_accepted_callback(ok_btn_callback)
    dlg.show()


def make_add_tensor_ellipsoids_widget(
    plotter, actor_name=None, mesh_name=None, tensor_field_name=None,
    inplace=False
):
    # noinspection GrazieInspection
    """ Launch a Qt dialog for defining new tensor ellipsoids

        Parameters
        ----------
        plotter : open_viewmin.FilterTreePlotQt
        actor_name : str or None, optional
            Name of new ellipsoids mesh
        mesh_name : str or None, optional
            Mesh on which to draw streamlines
        tensor_field_name : str or None, optional
            Array name of symmetric tensor field to be visualized
        inplace : bool, optional
            Whether to update an existing ellipsoids mesh
        """

    plotter.add_tensor_ellipsoids_widget = utilities_Qt.FormDialog(
        title='Add tensor ellipsoids', apply_button=False
    )
    dlg = plotter.add_tensor_ellipsoids_widget

    if inplace:
        filter_formula = plotter.get_filter_formula(actor_name)
        ovm_info = filter_formula.ovm_info
        filter_kwargs = filter_formula.filter_kwargs
        parent_filter_formula = filter_formula.parent_filter_formula
        parent_filter_kwargs = parent_filter_formula.filter_kwargs
    else:
        ovm_info = dict()
        parent_filter_kwargs = dict()
        filter_kwargs = dict()

    add_tensor_ellipsoids_kwargs = dict(
        tensor_field_name=ovm_info.get("tensor_field_name", tensor_field_name,),
        stride=parent_filter_kwargs.get("stride", plotter.settings["glyph_stride"]),
        scale=filter_kwargs.get("scale", "ones"),
        factor=filter_kwargs.get("factor", 1.),
        random=parent_filter_kwargs.get("random", True),
        eigenvalue_shift=filter_kwargs.get("eigenvalue_shift2", 1.),
        ellipsoid_kwargs=dict()
    )

    def choose_tensor_field_callback(tensors_name):
        def return_func():
            add_tensor_ellipsoids_kwargs['tensor_field_name'] = tensors_name
            add_tensor_ellipsoids_kwargs["ellipsoid_mesh_name"] = (
                plotter.name_mesh_without_overwriting(
                    add_tensor_ellipsoids_kwargs.get("tensor_field_name", "")
                    + "_ellipsoids"
                )
            )
        return return_func

    def set_scale(scalars_name):
        def return_func():
            add_tensor_ellipsoids_kwargs['scale'] = scalars_name
        return return_func

    choose_tensor_field_dropdown = utilities_Qt.DropdownMenu()
    scale_dropdown = utilities_Qt.DropdownMenu()

    def populate_choose_tensor_field_dropdown(dropdown_mesh_name):
        choose_tensor_field_dropdown.populate([
            (tensors_name, choose_tensor_field_callback(tensors_name))
            for tensors_name in plotter.symmetric_tensor_fields(
                mesh_name=dropdown_mesh_name
            )
        ])

    def populate_scale_dropdown(dropdown_mesh_name):
        scalars_names = plotter.scalar_fields(mesh_name=dropdown_mesh_name)
        scale_dropdown.populate(
            (scalars_name, set_scale(scalars_name))
            for scalars_name in scalars_names
        )
        if "ones" in scalars_names:
            scale_dropdown.setText("ones")

    def choose_mesh_callback(mesh_name_choice):
        def return_func():
            add_tensor_ellipsoids_kwargs['parent_mesh_name'] = mesh_name_choice
            populate_choose_tensor_field_dropdown(mesh_name_choice)
            populate_scale_dropdown(mesh_name_choice)
        return return_func

    choose_mesh_dropdown = utilities_Qt.DropdownMenu()
    choose_mesh_dropdown.populate([
        (mesh_name, choose_mesh_callback(mesh_name))
        for mesh_name in plotter.mesh_names()
    ])
    if mesh_name in plotter.mesh_names():
        choose_mesh_callback(mesh_name)()
        choose_mesh_dropdown.setText(mesh_name)
        choose_mesh_dropdown.setEnabled(False)
        tensor_fields = plotter.symmetric_tensor_fields(mesh_name=mesh_name)
        if tensor_field_name in tensor_fields:
            choose_tensor_field_dropdown.setText(tensor_field_name)
            choose_tensor_field_dropdown.setEnabled(False)

    def ok_btn_callback():
        if inplace:
            add_tensor_ellipsoids_kwargs['ellipsoid_mesh_name'] = actor_name
        required_vars = ["tensor_field_name", "ellipsoid_mesh_name", "parent_mesh_name"]
        if all([
            add_tensor_ellipsoids_kwargs.get(key) is not None
            for key in required_vars
        ]):
            actor_name2 = add_tensor_ellipsoids_kwargs['ellipsoid_mesh_name']
            ellipsoid_kwargs2 = {
                key: val
                for (key, val) in add_tensor_ellipsoids_kwargs.items()
                if key not in required_vars + ["stride", "random"]
            }
            if inplace:
                filter_formula2 = plotter.get_filter_formula(actor_name2)
                filter_formula2.parent_filter_formula.update(
                    stride=add_tensor_ellipsoids_kwargs["stride"],
                    random=add_tensor_ellipsoids_kwargs["random"]
                )
                filter_formula2.update(
                    scale=add_tensor_ellipsoids_kwargs["scale"],
                    factor=add_tensor_ellipsoids_kwargs["factor"],
                    eigenvalue_shift2=add_tensor_ellipsoids_kwargs["eigenvalue_shift"],
                    update_actor=True
                )
            else:
                # noinspection PyUnresolvedReferences,PyTypeChecker
                glyphs.add_tensor_ellipsoids(
                    plotter,
                    actor_name2,
                    add_tensor_ellipsoids_kwargs['tensor_field_name'],
                    add_tensor_ellipsoids_kwargs['parent_mesh_name'],
                    **ellipsoid_kwargs2
                )
                _ovm_info = plotter.get_filter_formula(actor_name2).ovm_info
                _ovm_info["tensor_field_name"] = (
                    add_tensor_ellipsoids_kwargs['tensor_field_name']
                )

    def set_stride():
        add_tensor_ellipsoids_kwargs["stride"] = stride_spinbox.value()

    stride_spinbox = qw.QSpinBox()
    stride_spinbox.setMinimum(1)
    stride_spinbox.setValue(add_tensor_ellipsoids_kwargs["stride"])
    # noinspection PyUnresolvedReferences
    stride_spinbox.editingFinished.connect(set_stride)

    def set_res():
        val = res_spinbox.value()
        for key in ["u_res", "v_res", "w_res"]:
            add_tensor_ellipsoids_kwargs['ellipsoid_kwargs'][key] = val

    # noinspection PyArgumentList
    res_spinbox = qw.QSpinBox(minimum=2, maximum=50, value=10)
    # noinspection PyUnresolvedReferences
    res_spinbox.editingFinished.connect(set_res)

    def set_factor():
        add_tensor_ellipsoids_kwargs["factor"] = factor_spinbox.value()

    # noinspection PyArgumentList
    factor_spinbox = qw.QDoubleSpinBox(minimum=0, maximum=1000, singleStep=0.5)
    factor_spinbox.setValue(add_tensor_ellipsoids_kwargs["factor"])
    # noinspection PyUnresolvedReferences
    factor_spinbox.editingFinished.connect(set_factor)

    def set_random():
        add_tensor_ellipsoids_kwargs["factor"] = random_checkbox.isChecked()

    random_checkbox = qw.QCheckBox()
    random_checkbox.setChecked(add_tensor_ellipsoids_kwargs["random"])
    # noinspection PyUnresolvedReferences
    random_checkbox.stateChanged.connect(set_random)

    def set_eigenvalue_shift():
        add_tensor_ellipsoids_kwargs["eigenvalue_shift"] = (
            eigval_shift_spinbox.value()
        )

    # noinspection PyArgumentList
    eigval_shift_spinbox = qw.QDoubleSpinBox(
        minimum=0, maximum=100, singleStep=1
    )

    eigval_shift_spinbox.setValue(add_tensor_ellipsoids_kwargs["eigenvalue_shift"])
    # noinspection PyUnresolvedReferences
    eigval_shift_spinbox.editingFinished.connect(set_eigenvalue_shift)

    if actor_name is not None:
        filter_formula = plotter.get_filter_formula(actor_name)
        parent_filter_formula = filter_formula.parent_filter_formula
        filter_kwargs = filter_formula.filter_kwargs

        current_stride = parent_filter_formula.filter_kwargs.get("stride")
        if current_stride is not None:
            stride_spinbox.setValue(current_stride)
            set_stride()

        current_res = filter_kwargs.get("u_res")
        if current_res is not None:
            res_spinbox.setValue(current_res)
            set_res()

        current_scale = filter_kwargs.get("scale")
        if current_scale is not None:
            scale_dropdown.setText(current_scale)
            set_scale(current_scale)

        current_factor = filter_kwargs.get("factor")
        if current_factor is not None:
            factor_spinbox.setValue(current_factor)
            set_factor()

        current_eigenvalue_shift = filter_kwargs.get("eigenvalue_shift")
        if current_eigenvalue_shift is not None:
            eigval_shift_spinbox.setValue(current_eigenvalue_shift)
            set_eigenvalue_shift()

        current_random = parent_filter_formula.filter_kwargs.get("random")
        if current_random is not None:
            random_checkbox.setChecked(current_random)

    dlg.add_row('surface:', choose_mesh_dropdown)
    dlg.add_row('tensors:', choose_tensor_field_dropdown)
    dlg.add_row('eigenvalues shift:', eigval_shift_spinbox)
    dlg.add_row('stride:', stride_spinbox)
    # dlg.add_row('random:', random_checkbox)  # non-random causes PyVista error
    dlg.add_row('ellipsoid resolution:', res_spinbox)
    dlg.add_row('scale array:', scale_dropdown)
    dlg.add_row('global scale:', factor_spinbox)
    dlg.set_accepted_callback(ok_btn_callback)
    dlg.show()
