""" Utilities for using and editing Qt menus """

from qtpy import QtWidgets as qw


def get_menus(plotter):
    """ Get list of plotter's menus

    Parameters
    ----------
    plotter : :class:`~open_viewmin.filter_tree_plot_Qt.filter_tree_plot_Qt.FilterTreePlotQt`

    Returns
    -------
    list[qtpy.QtWidgets.QMenu]
    """

    return [
        child for child in plotter.app_window.menuBar().children()
        if isinstance(child, qw.QMenu)
    ]


def get_menu(parent, title):
    """ Get menu by title

    Parameters
    ----------
    parent : qtpy.QtWidgets.QWidget
    title : str

    Returns
    -------
    qtpy.QtWidgets.QMenu
    """

    for item in parent.children():
        if isinstance(item, qw.QMenu):
            if item.title() == title:
                return item
    raise KeyError(f"Cannot find menu named {title} in {parent}")


def get_menu_action(parent, label):
    """ Get menu action by label

    Parameters
    ----------
    parent : qtpy.QtWidgets.QMenu
    label : str

    Returns
    -------
    qtpy.QtWidgets.QMenu | qtpy.QtWidgets.QAction
    """

    for item in parent.children():
        if hasattr(item, "text"):
            if item.text() == label:
                return item
    raise KeyError(f"Cannot find action named {label} in {parent}")


def remove_menu_action(parent, label):
    """ Remove action from menu

    Parameters
    ----------
    parent : qtpy.QtWidgets.QMenu
    label : str
        menu action's label
    """
    menu_action = get_menu_action(parent, label)
    parent.removeAction(menu_action)
