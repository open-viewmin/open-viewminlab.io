""" Dialog for controlling appearance of existing streamlines """


from qtpy import QtWidgets as qw
from open_viewmin.filter_tree_plot_Qt.widgets_Qt import utilities_Qt


# noinspection PyUnresolvedReferences
def make_streamlines_settings_widget(plotter, actor_name):
    """ Create dialog for streamlines settings

    Parameters
    ----------
    plotter : :class:`~open_viewmin.filter_tree_plot_Qt.filter_tree_plot_Qt.FilterTreePlotQt`
    actor_name : str

    Returns
    -------
    :class:`~open_viewmin.filter_tree_plot_Qt.widgets_Qt.utilities_Qt.FormDialog`

    """
    min_width = 300
    dlg = utilities_Qt.FormDialog(
        title=f'Surface streamlines settings for \"{actor_name}\"'
    )
    dlg.setMinimumWidth(min_width)

    filter_formula = plotter.get_filter_formula(actor_name)
    filter_kwargs = filter_formula.filter_kwargs

    # noinspection PyArgumentList
    sb_max_steps = qw.QSpinBox(
        minimum=1,
        maximum=1000
    )
    sb_max_steps.setValue(filter_kwargs['max_steps'])
    sb_max_steps.setSingleStep(1)
    sb_max_steps.setToolTip('Press Enter to apply.')

    def max_steps_callback():
        plotter.update_filter(
            actor_name,
            max_steps=sb_max_steps.value(),
            update_actor=True
        )

    sb_max_steps.editingFinished.connect(max_steps_callback)
    dlg.add_row('max steps', sb_max_steps)

    # noinspection PyArgumentList
    sb_max_step_length = qw.QDoubleSpinBox(minimum=0, maximum=1)
    sb_max_step_length.setValue(
        filter_kwargs['max_step_length']
        if 'max_step_length' in filter_kwargs.keys()
        else 1.0
    )
    sb_max_step_length.setDecimals(3)
    sb_max_step_length.setSingleStep(0.001)
    sb_max_step_length.setToolTip('Press Enter to apply.')

    def max_step_length_callback():
        plotter.update_filter(
            actor_name,
            max_step_length=sb_max_step_length.value(),
            update_actor=True
        )

    sb_max_step_length.editingFinished.connect(max_step_length_callback)
    dlg.add_row('max step length', sb_max_step_length)

    # noinspection PyArgumentList
    sb_n_points = qw.QSpinBox(
        minimum=0,
        maximum=plotter.get_mesh(filter_formula.parent_mesh_name).n_points
    )
    sb_n_points.setValue(filter_kwargs['n_points'])
    sb_n_points.setSingleStep(1)
    sb_n_points.setToolTip('Press Enter to apply.')

    def n_points_callback():
        plotter.update_filter(
            actor_name,
            n_points=sb_n_points.value(),
            update_actor=True
        )

    sb_n_points.editingFinished.connect(n_points_callback)
    dlg.add_row('num points', sb_n_points)

    cb_tube = qw.QCheckBox()
    cb_tube.setChecked(
        filter_kwargs['tube']
        if 'tube' in filter_kwargs.keys()
        else False
    )

    def tube_callback():
        plotter.update_filter(
            actor_name,
            tube=cb_tube.isChecked(),
            update_actor=True
        )

    cb_tube.stateChanged.connect(tube_callback)
    dlg.add_row('tube', cb_tube)

    # noinspection PyArgumentList
    sb_n_sides = qw.QSpinBox(minimum=3, maximum=40)
    sb_n_sides.setValue(filter_kwargs['n_sides'])
    sb_n_sides.setSingleStep(1)
    sb_n_sides.setToolTip('Press Enter to apply.')

    def n_sides_callback():
        plotter.update_filter(
            actor_name,
            n_sides=sb_n_sides.value(),
            update_actor=True
        )

    sb_n_sides.editingFinished.connect(n_sides_callback)
    dlg.add_row('tube num sides', sb_n_sides)

    # noinspection PyArgumentList
    sb_tube_radius = qw.QDoubleSpinBox(minimum=0, maximum=10)
    sb_tube_radius.setValue(
        filter_kwargs['radius']
        if 'radius' in filter_kwargs.keys()
        else 0.1
    )
    sb_tube_radius.setSingleStep(0.01)
    sb_tube_radius.setToolTip('Press Enter to apply.')

    def tube_radius_callback():
        plotter.update_filter(
            actor_name,
            radius=sb_tube_radius.value(),
            update_actor=True
        )

    sb_tube_radius.editingFinished.connect(tube_radius_callback)
    dlg.add_row('tube radius', sb_tube_radius)
    return dlg
