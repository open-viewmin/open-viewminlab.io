import qtpy.QtCore as Qt
from open_viewmin.filter_tree_plot_Qt.menus import menu_utilities
from open_viewmin.filter_tree_plot_Qt.menus import menu_utilities


def setup_view_menu(plotter):
    """ Set up the View menu

    Parameters
    ----------
    plotter : :class:`~open_viewmin.filter_tree_plot_Qt.filter_tree_plot_Qt.FilterTreePlotQt`
    """

    # remove PyVistaQt's "Clear All" action
    menu_utilities.remove_menu_action(plotter.the_View_menu, "Clear All")

    plotter.floor_toggle = plotter.the_View_menu.addAction(
        'Toggle Floor',
        plotter.toggle_floor
    )
    plotter.floor_toggle.setCheckable(True)

    def update_floor_toggle_check():
        key = 'Floor(-z)'
        if key in plotter.renderer.actors.keys():
            floor_visible = plotter.renderer.actors[key].GetVisibility()
        else:
            floor_visible = False
        plotter.floor_toggle.setChecked(floor_visible)
    plotter.the_View_menu.aboutToShow.connect(update_floor_toggle_check)

    plotter.lighting_type_menu = plotter.the_View_menu.addMenu("Lighting Type")
    plotter.lighting_type_menu.addAction(
        "Light kit", plotter.enable_pyvista_lightkit
    )
    plotter.lighting_type_menu.addAction(
        "3 lights", plotter.enable_pyvista_3_lights
    )
    # move to top of menu
    plotter.the_View_menu.insertMenu(
        plotter.the_View_menu.actions()[0], plotter.lighting_type_menu
    )

    # override "Toggle Eye Dome Lighting"
    toggle_eye_dome_lighting_action = [
        action for action in plotter.the_View_menu.actions()
        if action.text() == "Toggle Eye Dome Lighting"
    ][0]
    plotter.the_View_menu.removeAction(toggle_eye_dome_lighting_action)

    toggle_eye_dome_lighting_action = plotter.the_View_menu.addAction(
        "Toggle Eye Dome Lighting",
        plotter.toggle_eye_dome_lighting
    )
    toggle_eye_dome_lighting_action.setCheckable(True)

    def update_toggle_dome_lighting_check():
        toggle_eye_dome_lighting_action.setChecked(
            plotter.is_enabled_eye_dome_lighting
        )

    plotter.the_View_menu.aboutToShow.connect(update_toggle_dome_lighting_check)

    plotter.the_View_menu.addSeparator()
    plotter.the_View_menu.addAction(
        'Set Orbit', lambda: plotter.set_orbit_dlg()
    )
    plotter.the_View_menu.addAction(
        'Show/Hide Orbit', plotter.toggle_orbit_visibility
    )
    plotter.the_View_menu.addAction('Orbit', plotter.do_orbit)

    plotter.the_View_menu.addSeparator()
    toggle_stereo_render_action = plotter.the_View_menu.addAction(
        'Toggle Stereo 3D',
        plotter.toggle_stereo_render
    )
    toggle_stereo_render_action.setCheckable(True)
    toggle_stereo_render_action.setChecked(False)
    toggle_shadows_action = plotter.the_View_menu.addAction(
        'Toggle Shadows', plotter.toggle_shadows
    )
    toggle_shadows_action.setCheckable(True)
    toggle_shadows_action.setChecked(False)

    if plotter.shape[0] > 1 or plotter.shape[1] > 1:
        # multiplotter mode
        plotter.the_View_menu.addSeparator()
        plotter.the_View_menu.addAction('Link views', plotter.link_views)
        plotter.the_View_menu.addAction('Unlink views', plotter.unlink_views)
        plotter.subplot_menu = plotter.the_View_menu.addMenu("Subplot")

        def subplot_callback(row, col):
            return lambda: plotter.subplot(row, col)

        for i in range(plotter.shape[0]):
            for j in range(plotter.shape[1]):
                plotter.subplot_menu.addAction(
                    str((i, j)),
                    subplot_callback(i, j)
                )
