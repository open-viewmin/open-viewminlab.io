def setup_non_ovm_actors_toggle(plotter):
    plotter.non_ovm_actors_toggle_menu.setToolTip('Show/hide elements')
    plotter.non_ovm_actors_toggle_menu.aboutToShow.connect(
        lambda: repopulate_non_ovm_actors_toggle_menu(plotter)
    )
    plotter.the_View_menu.aboutToShow.connect(
        lambda: whether_toggle_menu_enabled(plotter)
    )


def repopulate_non_ovm_actors_toggle_menu(plotter):
    menu = plotter.non_ovm_actors_toggle_menu
    menu.clear()
    non_filter_tree_actor_names = plotter.non_filter_tree_actor_names()

    def add_toggle_action(actor_name):
        actor = plotter.renderer.actors[actor_name]
        current_vis = actor.GetVisibility()
        if actor in plotter.scalar_bars.values():
            def toggle_func():
                plotter.toggle_scalar_bar_visibility(actor)
        else:
            def toggle_func():
                actor.SetVisibility(1 - current_vis)

        action = menu.addAction(actor_name, toggle_func)
        action.setCheckable(True)
        action.setChecked(current_vis)
        return action

    for element_name in non_filter_tree_actor_names:
        add_toggle_action(element_name)


def whether_toggle_menu_enabled(plotter):
    menu = plotter.non_ovm_actors_toggle_menu
    non_filter_tree_actor_names = plotter.non_filter_tree_actor_names()
    menu.setEnabled(len(non_filter_tree_actor_names) > 0)
