import qtpy.QtWidgets as qw
import qtpy.QtCore as Qt
from .filter_menu import update_filter_menu


# noinspection PyUnresolvedReferences
def setup_refresh_button(plotter):
    plotter.the_refresh_toolbar.setWindowTitle('Refresh')
    plotter.the_refresh_button = qw.QToolButton()
    plotter.the_refresh_button.setIcon(
        plotter.style().standardIcon(qw.QStyle.SP_BrowserReload)
    )
    plotter.the_refresh_button.setToolTip(
        'Update all meshes and actors in scene'
    )

    def refresh_callback():
        for mesh_name in plotter.filter_tree_roots():
            plotter.update_filter(mesh_name)
        update_filter_menu(plotter)
    plotter.the_refresh_button.clicked.connect(refresh_callback)
    plotter.the_refresh_toolbar.addWidget(plotter.the_refresh_button)
    icon_size = plotter.settings['icon_size']
    plotter.the_refresh_toolbar.setIconSize((Qt.QSize(icon_size, icon_size)))
    plotter.app_window.addToolBar(
        Qt.Qt.TopToolBarArea,
        plotter.the_refresh_toolbar
    )
