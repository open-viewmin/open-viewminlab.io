import inspect
import qtpy.QtWidgets as qw
import qtpy.QtCore as Qt
import numpy as np
from pyvista import PolyData

from open_viewmin.filter_tree_plot.utilities import ovm_utilities
from open_viewmin.filter_tree_plot_Qt.widgets_Qt import utilities_Qt


def setup_filter_menu(plotter):
    """ Experimental! For each mesh in the filters tree, create a submenu
    of all PyVista filters for that mesh type, which will launch a dialog
    for the filter's arguments.

    Parameters
    ----------
    plotter

    Returns
    -------

    """

    plotter.filter_menu = plotter.main_menu.addMenu('Filter')
    plotter.filter_menu.setToolTip(
        'Choose an existing mesh and a filter to apply to it. Experimental!'
    )
    plotter.filter_menu.aboutToShow.connect(
        lambda: update_filter_menu(plotter)
    )


def update_filter_menu(plotter):
    plotter.filter_menu.clear()
    for mesh_name in plotter.mesh_names():
        create_filter_submenu(plotter, mesh_name)


def create_filter_submenu(plotter, mesh_name):
    fltrs = ovm_utilities.available_filters(plotter.get_mesh(mesh_name))
    if len(fltrs) > 0:
        submenu = plotter.filter_menu.addMenu(mesh_name)
        for fltr in fltrs:
            submenu.addAction(
                fltr, open_filter_dialog_aux(plotter, mesh_name, fltr)
            )


def open_filter_dialog_aux(plotter, mesh_name, filter_name):
    def return_function():
        return open_filter_dialog(plotter, mesh_name, filter_name)

    return return_function


# noinspection PyUnresolvedReferences
def open_filter_dialog(plotter, mesh_name, filter_name):
    mesh = plotter.get_mesh(mesh_name)
    filter_func = getattr(mesh, filter_name)
    options_dict = ovm_utilities.get_kwargs_and_defaults(
        filter_func, as_strings=True
    )
    param_types_dict = ovm_utilities.get_arg_types(filter_func)
    filter_dialog = utilities_Qt.FormDialog(
        title="Filter",
        message=f'New <i>{filter_name}</i> filter for \"{mesh_name}\"'
    )
    filter_docstring = f"{filter_name}\n\n" \
                       f"{inspect.cleandoc(filter_func.__doc__)}"

    filter_dialog.setWhatsThis(filter_docstring)

    options_dialog_dict = {}
    # list of keywords that refer only to PyVista datasets
    other_mesh_synonyms = ["other_mesh", "dataset", "surface", "target"]
    other_mesh_names = [
        other_mesh_name
        for other_mesh_name in plotter.mesh_names()
        if other_mesh_name != mesh_name
    ]
    other_polydata_mesh_names = [
        other_mesh_name
        for other_mesh_name in other_mesh_names
        if isinstance(plotter.get_mesh(mesh_name), PolyData)
    ]
    for key in options_dict.keys():
        param_type_str = param_types_dict.get(key)
        default_val = eval(options_dict.get(key))
        if key == "capping":
            default_val = True
        if key == "inplace":
            # 'inplace' not supported, must have value `False`
            val = qw.QCheckBox()
            val.setChecked(False)
        if key == "vectors" and filter_name != "decimate":
            val = qw.QComboBox()
            val.addItems(plotter.vector_fields(mesh=mesh))
        elif key == "scalars" and filter_name != "decimate":
            val = qw.QComboBox()
            val.addItems(
                ["None"] + plotter.scalar_fields(mesh_name=mesh)
            )
        elif param_type_str == "pyvista.PolyData":
            val = qw.QComboBox()
            val.addItems(other_polydata_mesh_names)
        elif (
            (
                param_type_str in ["bool", "bool, optional"]
                and isinstance(default_val, bool)
            )
            or key == "capping"
        ):
            val = qw.QCheckBox()
            val.setChecked(default_val)
        elif key in other_mesh_synonyms:
            val = qw.QComboBox()
            val.addItems(other_mesh_names)
        elif (
            param_type_str in ["float", "float, optional"]
            and isinstance(default_val, float)
        ):
            # noinspection PyArgumentList
            val = qw.QDoubleSpinBox(
                minimum=-1e10, maximum=1e10, value=default_val, decimals=8
            )
        elif (
            param_type_str in ["int", "int, optional"]
            and isinstance(default_val, int)
        ):
            # noinspection PyArgumentList
            val = qw.QSpinBox(
                minimum=int(-1e10), maximum=int(1e10), value=default_val
            )
        else:
            # noinspection PyArgumentList
            val = qw.QLineEdit(text=options_dict[key])
        options_dialog_dict[key] = val

    # noinspection PyArgumentList
    le_name = qw.QLineEdit(text=f"{mesh_name}_{filter_name}")
    filter_dialog.add_row('name', le_name)
    for key in options_dialog_dict.keys():
        filter_dialog.add_row(key, options_dialog_dict[key])

    def ok_btn_callback():
        new_mesh_name = plotter.name_mesh_without_overwriting(le_name.text())
        kwargs = {}
        for callback_key in options_dialog_dict.keys():
            line = options_dialog_dict[callback_key]
            if callback_key[:2] == "**":
                kdict = eval(line.text())
                for kwarg_key in kdict.keys():
                    kwargs[kwarg_key] = kdict[kwarg_key]
            # elif callback_key in other_mesh_synonyms:
            #     kwargs[callback_key] = plotter.get_mesh(line.text())
            # elif callback_key == "scalars":
            #     scalars_name = line.currentText()
            #     if scalars_name == "None":
            #         kwargs[callback_key] = None
            #     else:
            #         kwargs[callback_key] = scalars_name
            elif isinstance(line, qw.QComboBox):
                txt = line.currentText()
                if txt == "None":
                    txt = None
                if callback_key in other_mesh_synonyms:
                    txt = plotter.get_mesh(txt)
                kwargs[callback_key] = txt
            elif isinstance(line, qw.QCheckBox):
                kwargs[callback_key] = line.isChecked()
            elif isinstance(line, (qw.QSpinBox, qw.QDoubleSpinBox)):
                kwargs[callback_key] = line.value()
            elif isinstance(line, qw.QLineEdit):
                try:
                    mesh_choice = eval(line.text())
                    if callback_key in other_mesh_synonyms:
                        mesh_choice = plotter.get_mesh(mesh_choice)
                    kwargs[callback_key] = mesh_choice
                except (NameError, SyntaxError) as err:
                    print(
                        f"SyntaxError while evaluating input \'{line.text()}\' "
                        f"for parameter \'{callback_key}\':\n "
                        f"{err}"
                    )
                    if isinstance(err, NameError):
                        print(f"Did you forget to put quotes around a string?")
                    filter_dialog.close()
                    return
                else:
                    if isinstance(kwargs[callback_key], list):
                        kwargs[callback_key] = np.array(kwargs[callback_key])

        try:
            plotter.add_filter_formula(
                name=new_mesh_name,
                parent_mesh_name=mesh_name,
                filter_callable=filter_name,
                **kwargs
            )
        except (ValueError, TypeError, KeyError) as err:
            print(
                f"Error {type(err)} while attempting to apply "
                f"filter \'{filter_name}\' to mesh \'{mesh_name}\':"
            )
            print(err)
            filter_dialog.close()
            return

        # if new mesh has no points, remove it
        else:
            new_mesh = plotter.get_mesh(new_mesh_name)
            if (
                isinstance(new_mesh, type(None))
                or (
                    hasattr(new_mesh, "n_points")
                    and plotter.get_mesh(new_mesh_name).n_points == 0
                )
            ):
                plotter.remove_mesh_completely(new_mesh_name)
                empty_mesh_warning = (
                    "Filter produced empty mesh; not adding."
                )
                print(empty_mesh_warning)
                warning_dlg = qw.QMessageBox()
                warning_dlg.setText(empty_mesh_warning)
                filter_dialog.close()
                warning_dlg.exec()
                return

        filter_dialog.close()

    filter_dialog.set_accepted_callback(ok_btn_callback)
    filter_dialog.show()
