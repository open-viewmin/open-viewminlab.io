import qtpy.QtWidgets as qw
import numpy as np

from open_viewmin.filter_tree_plot_Qt.menus import add_actors_widgets
from open_viewmin.filter_tree_plot.widgets import plane_widgets, line_widgets


def setup_add_menu(plotter):
    """ Define callbacks to connect to actions in "Add" main menu

    Parameters
    ----------
    plotter : :class:`~open_viewmin.filter_tree_plot_Qt.filter_tree_plot_Qt.FilterTreePlotQt`

    """
    plotter.the_Add_menu.setToolTip(
        'Add new visualization element and/or widget'
    )
    the_add_slice_menu = plotter.the_Add_menu.addMenu("Slice")

    def _add_slice_menu_update():
        _add_fields_filter_menu_update(
            plotter,
            the_add_slice_menu,
            plotter.add_slice_with_controls,
            list_before=[
                ("solid_color", lambda: plotter.add_slice_with_controls(None))
            ]
        )

    the_add_slice_menu.aboutToShow.connect(_add_slice_menu_update)
    the_add_isosurface_menu = plotter.the_Add_menu.addMenu("Isosurface")

    def _add_isosurface_action(scalars_name):
        """ Add an isosurface filter formula on the default mesh for a scalar
        dataset

        Parameters
        ----------
        scalars_name : str

        Returns
        -------
        callable

        """
        def return_func():
            dataset = plotter.default_mesh[scalars_name]
            new_mesh_name = plotter.name_mesh_without_overwriting(
                scalars_name + "_isosurface"
            )
            plotter.add_filter_formula(
                new_mesh_name,
                filter_callable="contour",
                scalars=scalars_name,
                isosurfaces=[np.average(dataset)]
            )
            toolbar = plotter.actor_control_toolbars[new_mesh_name]
            toolbar.add_isosurface_qslider()
        return return_func

    def _add_isosurface_menu_update():
        """ Repopulate "Add -> isosurface" submenu with scalar dataset names """
        _add_fields_filter_menu_update(
            plotter,
            the_add_isosurface_menu,
            _add_isosurface_action
        )

    the_add_isosurface_menu.aboutToShow.connect(_add_isosurface_menu_update)
    the_add_threshold_menu = plotter.the_Add_menu.addMenu("Threshold")

    def _add_threshold_action(scalars_name):
        """ Add a threshold filter formula on the default mesh for a scalar
        dataset

        Parameters
        ----------
        scalars_name : str

        Returns
        -------
        callable

        """
        def return_func():
            new_mesh_name = plotter.name_mesh_without_overwriting(
                scalars_name + "_threshold"
            )
            dataset = plotter.default_mesh[scalars_name]
            plotter.add_filter_formula(
                new_mesh_name,
                filter_callable="threshold",
                scalars=scalars_name,
                value=[np.min(dataset), np.max(dataset)],
                invert=False
            )
            toolbar = plotter.actor_control_toolbars[new_mesh_name]
            toolbar.add_threshold_text_controls()
        return return_func

    def add_threshold_menu_update():
        """ Repopulate "Add -> threshold" submenu with scalar dataset names """
        _add_fields_filter_menu_update(
            plotter,
            the_add_threshold_menu,
            _add_threshold_action
        )

    the_add_threshold_menu.aboutToShow.connect(add_threshold_menu_update)
    the_add_PT_menu = plotter.the_Add_menu.addMenu("Pontryagin-Thom surface")

    def _add_pontryagin_thom_callback(vectors_name):
        """ Callback to add a Pontryagin-Thom surface filter on the default
        mesh based on a vector dataset

        Parameters
        ----------
        vectors_name : str

        Returns
        -------
        callable
        """

        def return_func():
            actor_name = plane_widgets.add_pontryagin_thom_widget(
                plotter, vectors_name
            )
            toolbar = plotter.actor_control_toolbars[actor_name]
            toolbar.add_isosurface_qslider(
                label_txt='|∠(n,N)|', num_divs=1000, init_val_percent=90
            )
            widget_name = plotter.get_filter_formula(actor_name).widget_name
            toolbar.make_slice_slider_controls(
                widget_name=widget_name,
                make_origin_slider=False
            )
        return return_func

    def _add_pontryagin_thom_surface_update():
        """ Repopulate "Add -> Pontyagin-Thom" submenu with vector dataset
            names

        """
        _add_fields_filter_menu_update(
            plotter,
            the_add_PT_menu,
            _add_pontryagin_thom_callback,
            field_names_source=plotter.vector_fields
        )

    the_add_PT_menu.aboutToShow.connect(_add_pontryagin_thom_surface_update)

    plotter.the_Add_menu.addAction(
        "Glyphs",
        lambda: add_actors_widgets.make_add_glyphs_widget(plotter)
    )
    plotter.the_add_surface_streamlines_menu = plotter.the_Add_menu.addAction(
        "Surface streamlines",
        lambda: add_actors_widgets.make_add_surface_streamlines_widget(plotter)
    )

    plotter.the_Add_menu.addAction(
        "Tensor ellipsoids",
        lambda: add_actors_widgets.make_add_tensor_ellipsoids_widget(plotter)
    )

    plotter.the_Add_menu.addSeparator()
    plotter.add_orientation_widget_menu = plotter.the_Add_menu.addMenu(
        "Orientation widget"
    )

    def _add_orientation_widget_callback(actor_name):
        """ Create callback to add an orientation widget based on an existing
        actor

        Parameters
        ----------
        actor_name : str

        Returns
        -------
        callable

        """
        def return_func():
            widget_name = plotter.name_widget_without_overwriting(
                actor_name + "_orientation_widget"
            )
            plotter.widgets[widget_name] = (
                plotter.add_orientation_widget(
                    plotter.get_mesh(actor_name),
                    interactive=True
                )
            )
            plotter.toolbars["widgets"].populate()
            plotter.toolbars["widgets"].refresh(widget_name)
        return return_func

    def _add_camera_orientation_widget_callback():
        """ Add a camera orientation widget to the orientation-widget options
        """
        plotter.add_camera_orientation_widget()
        widget_name = plotter.name_widget_without_overwriting("Camera")
        plotter.widgets[widget_name] = plotter.camera_widgets[-1]
        plotter.toolbars["widgets"].populate()
        plotter.toolbars["widgets"].refresh(widget_name)

    def _populate_add_orientation_widget_menu():
        """ Populate "Add -> orientation widget" submenu with "Camera axes"
        and all available actors
        """
        menu = plotter.add_orientation_widget_menu
        menu.clear()
        menu.addAction("Camera axes", _add_camera_orientation_widget_callback)
        for actor_name in plotter.actor_names():
            menu.addAction(
                actor_name,
                _add_orientation_widget_callback(actor_name)
            )

    plotter.add_orientation_widget_menu.aboutToShow.connect(
        _populate_add_orientation_widget_menu
    )
    plotter.the_Add_menu.addAction(
        "Line widget",
        lambda: line_widgets.setup_line_widget(plotter)
    )


def _add_fields_filter_menu_update(
    plotter, menu, callfunc, list_before=None, field_names_source=None
):
    """ Update the dataset names to be listed in a menu.

    Parameters
    ----------
    plotter : :class:`~open_viewmin.filter_tree_plot_Qt.filter_tree_plot_Qt.FilterTreePlotQt`
    menu : PyQt5.QtWidgets.QMenu
    callfunc : callable()
        Function mapping data array name to a menu action
    list_before : list[tuple]
        (name, callable) pairs of menu items to list before the dataset names.
    field_names_source : callable()
        Function that returns a sequence of data array names.

    """

    if list_before is None:
        list_before = []
    if field_names_source is None:
        field_names_source = plotter.scalar_fields
    # noinspection PyUnresolvedReferences
    menu.clear()
    for item_name, item_func in list_before:
        # noinspection PyUnresolvedReferences
        menu_action = menu.addAction(
            item_name,
            item_func()
        )
        menu_action.setCheckable(False)
    # noinspection PyUnresolvedReferences
    menu.addSeparator()
    for array_name in sorted(field_names_source()):
        # noinspection PyUnresolvedReferences
        menu_action = menu.addAction(
            array_name,
            callfunc(array_name)
        )
        menu_action.setCheckable(False)
