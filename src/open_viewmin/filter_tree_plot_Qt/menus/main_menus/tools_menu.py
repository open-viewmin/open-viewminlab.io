import pyvista as pv


def setup_tools_menu(plotter):
    """ Set up the Tools menu

    Parameters
    ----------
    plotter : pv.Plotter
    """
    plotter.the_Tools_menu.clear()
    setup_enable_picking_menu(plotter)
    setup_interaction_style_menu(plotter)


def setup_enable_picking_menu(plotter: pv.Plotter):
    """ Set up the Tools/Enable Picking submenu

    Parameters
    ----------
    plotter : pv.Plotter
    """

    picking_menu = plotter.the_Tools_menu.addMenu("Enable Picking...")

    def _enable_cell_picking(through=True):
        def ret():
            plotter.disable_picking()
            plotter.enable_cell_picking(through=through)
        return ret

    picking_menu.addAction(
        "Cell (through)",
        _enable_cell_picking(through=True)
    )

    picking_menu.addAction(
        "Cell (visible)",
        _enable_cell_picking(through=False)
    )

    def _enable_geodesic_picking():
        def ret():
            plotter.disable_picking()
            plotter.enable_geodesic_picking()
        return ret

    picking_menu.addAction("Geodesic", _enable_geodesic_picking()),

    def _enable_horizon_picking():
        def ret():
            plotter.disable_picking()
            plotter.enable_horizon_picking()
        return ret

    picking_menu.addAction("Horizon", _enable_horizon_picking())

    def _enable_mesh_picking():
        def ret():
            plotter.disable_picking()
            plotter.enable_mesh_picking()
        return ret

    picking_menu.addAction("Mesh", _enable_mesh_picking())

    def _enable_path_picking():
        def ret():
            plotter.disable_picking()
            plotter.enable_path_picking()
        return ret

    picking_menu.addAction("Path", _enable_path_picking())

    def _enable_point_picking():
        def ret():
            plotter.disable_picking()
            plotter.enable_point_picking()
        return ret

    picking_menu.addAction("Point", _enable_point_picking())

    def _enable_surface_point_picking():
        def ret():
            plotter.disable_picking()
            plotter.enable_surface_point_picking()
        return ret

    picking_menu.addAction("Surface", _enable_surface_point_picking())

    picking_menu.addSeparator()
    picking_menu.addAction("Disable", plotter.disable_picking)


def setup_interaction_style_menu(plotter):
    """ Set up the Tools/Interaction Style submenu

    Parameters
    ----------
    plotter : pv.Plotter
    """

    plotter.interaction_style_menu = plotter.the_Tools_menu.addMenu(
        "Mouse Interaction Mode",
    )

    stylename_style_classname_tuples = (
        ("Image Style", plotter.enable_image_style, "Image"),
        ("Joystick Style", plotter.enable_joystick_style, "JoystickCamera"),
        (
            "Rubber Band 2D Style",
            plotter.enable_rubber_band_2d_style,
            "RubberBand2D"
        ),
        (
            "Rubber Band Style",
            plotter.enable_rubber_band_style,
            "RubberBandPick"
        ),
        ("Terrain Style", plotter.enable_terrain_style, "Terrain"),
        ("Trackball Style", plotter.enable_trackball_style, "TrackballCamera"),
        ("Zoom Style", plotter.enable_zoom_style, "RubberBandZoom"),
        (
            "Joystick Style Actor Manipulation",
            plotter.enable_joystick_actor_style,
            "JoystickActor"
        ),
        (
            "Trackball Style Actor Manipulation",
            plotter.enable_trackball_actor_style,
            "TrackballActor"
        )
    )

    for style_name, style, _ in stylename_style_classname_tuples:
        interaction_style_action = plotter.interaction_style_menu.addAction(
            style_name,
            style
        )
        interaction_style_action.setToolTip(style.__doc__)
        interaction_style_action.setCheckable(True)

    def interaction_style_checkmark_callback():
        current_interactor_style = plotter.iren.style.GetClassName()
        for action in plotter.interaction_style_menu.actions():
            _style_name = action.text()
            if "Style" in _style_name:
                style_class_tuple = [
                    t for t in stylename_style_classname_tuples
                    if t[0] == _style_name
                ][0]
                style_class_suffix = style_class_tuple[2]
                style_class_name = "vtkInteractorStyle" + style_class_suffix
                action.setChecked(current_interactor_style == style_class_name)

    plotter.interaction_style_menu.aboutToShow.connect(
        interaction_style_checkmark_callback
    )

    plotter.interaction_style_menu.addSeparator()

    setup_enable_fly_to_right_click_action(plotter)


def setup_enable_fly_to_right_click_action(plotter):
    # noinspection GrazieInspection
    """ Set up the Tools/Enable Fly to Right Click action

        Parameters
        ----------
        plotter : pv.Plotter
        """

    def enable_fly_to_right_click_callback():
        plotter.enable_fly_to_right_click()
        plotter.fly_to_right_click_enabled = True

    enable_fly_to_right_click_action = (
        plotter.interaction_style_menu.addAction(
            "Enable Fly to Right Click",
            enable_fly_to_right_click_callback
        )
    )
    enable_fly_to_right_click_action.setCheckable(True)

    def set_check_enable_fly_to_right_click_action():
        enable_fly_to_right_click_action.setChecked(
            plotter.fly_to_right_click_enabled
        )

    plotter.interaction_style_menu.aboutToShow.connect(
        set_check_enable_fly_to_right_click_action
    )
