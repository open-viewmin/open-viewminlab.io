import qtpy.QtWidgets as qw
import qtpy.QtCore as Qt

from open_viewmin.filter_tree_plot_Qt.widgets_Qt import utilities_Qt


def setup_theme_menu(plotter):
    plotter.theme_menu.setToolTip('Global colors')


# noinspection PyUnresolvedReferences
def open_edit_theme_widget(plotter):
    plotter.edit_theme_widget = utilities_Qt.FormDialog(
        'Global theme colors'
    )
    dlg = plotter.edit_theme_widget
    pb_background = qw.QPushButton('Choose')

    def change_background_callback(color, top=False):
        if top:
            key = "background_top_color"
        else:
            key = "background_color"
        plotter.settings[key] = color
        plotter.set_background()

    def change_background():
        utilities_Qt.ColorDialogValid(
            change_background_callback,
            plotter.settings["background_color"],
            plotter.app_window
        )

    pb_background.clicked.connect(change_background)
    dlg.add_row('background color: ', pb_background)

    pb_background_top = qw.QPushButton('Choose')

    def change_background_top():
        utilities_Qt.ColorDialogValid(
            lambda color: change_background_callback(color, top=True),
            plotter.settings["background_top_color"],
            plotter.app_window
        )
    pb_background_top.clicked.connect(change_background_top)
    dlg.add_row('background color top: ', pb_background_top)

    def change_theme_color_callback(color):
        plotter.theme["font"]["color"] = color

        # change font color for axes widget by re-creating it
        axes_currently_visible = plotter.axes.GetVisibility()
        plotter.add_axes()
        # if axes were hidden, keep them hidden
        if not axes_currently_visible:
            plotter.hide_axes()

    def change_theme_color():
        utilities_Qt.ColorDialogValid(
            change_theme_color_callback,
            plotter.theme["font"]["color"].float_rgb,
            plotter.app_window
        )

    pb_font_color = qw.QPushButton('Choose')
    pb_font_color.clicked.connect(change_theme_color)
    dlg.add_row('font color: ', pb_font_color)

    pb_floor_color = qw.QPushButton('Choose')

    def change_floor_color_callback(color):
        plotter.theme["floor_color"] = color

        # If floor already exists, change the color of that VTK actor directly
        the_floor = plotter.renderer.actors.get("Floor(-z)")
        if the_floor is not None:
            the_floor.GetProperty().SetColor(
                *plotter.theme["floor_color"].float_rgb
            )

    def change_floor_color():
        utilities_Qt.ColorDialogValid(
            change_floor_color_callback,
            plotter.theme["floor_color"],
            plotter.app_window
        )

    pb_floor_color.clicked.connect(change_floor_color)
    dlg.add_row('floor color: ', pb_floor_color)
    dlg.show()
