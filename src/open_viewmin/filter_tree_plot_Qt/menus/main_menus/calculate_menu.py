from open_viewmin.filter_tree_plot.utilities import calculations


def setup_calculate_menu(plotter):
    plotter.calculate_menu.setToolTip(
        'Add field arrays calculated from existing field arrays'
    )

    plotter.calculate_from_scalars_menu = plotter.calculate_menu.addMenu(
        "Scalars"
    )

    def calculate_from_scalars_menu_update():
        plotter.calculate_from_scalars_menu.clear()
        for field in plotter.scalar_fields():
            menu = plotter.calculate_from_scalars_menu
            this_scalar_component_menu = menu.addMenu(field)
            for op in ['abs', 'gradient', 'Laplacian']:
                this_scalar_component_menu.addAction(
                    op,
                    calculations.calculate_from_scalar_field(plotter, field, op)
                )

    plotter.calculate_from_scalars_menu.aboutToShow.connect(
        calculate_from_scalars_menu_update
    )

    plotter.calculate_from_vectors_menu = plotter.calculate_menu.addMenu(
        "Vectors"
    )

    def calculate_from_vectors_menu_update():
        vectors_menu = plotter.calculate_from_vectors_menu
        vectors_menu.clear()
        for field in plotter.vector_fields():
            this_vector_component_menu = vectors_menu.addMenu(field)
            for var in ['x', 'y', 'z', '|x|', '|y|', '|z|', 'div', 'curl']:
                this_vector_component_menu.addAction(
                    var,
                    calculations.calculate_from_vector_field(
                        plotter, field, var
                    )
                )
            for operation_string in ["dot", "cross"]:
                operation_menu = this_vector_component_menu.addMenu(
                    operation_string
                )
                for field2 in plotter.vector_fields():
                    operation_menu.addAction(
                        field2,
                        calculations.calculate_from_two_vector_fields(
                            plotter, field, field2, operation_string
                        )
                    )
            for operation_string in ["scale"]:
                operation_menu = this_vector_component_menu.addMenu(
                    operation_string
                )
                for scalar_field in plotter.scalar_fields():
                    operation_menu.addAction(
                        scalar_field,
                        calculations.calculate_from_one_vector_field_and_one_scalar_field(
                            plotter, field, scalar_field, operation_string
                        )
                    )

    plotter.calculate_from_vectors_menu.aboutToShow.connect(
        calculate_from_vectors_menu_update
    )
    plotter.tensors_menu = plotter.calculate_menu.addMenu(
        "Symmetric rank-2 tensors"
    )

    def tensors_menu_update():
        plotter.tensors_menu.clear()
        for field in plotter.symmetric_tensor_fields():
            this_tensor_menu = plotter.tensors_menu.addMenu(field)
            this_tensor_menu.addAction(
                'eigenvalues',
                calculations.calculate_from_tensor_field(
                    plotter, field, 'eigvalsh')
            )
            this_tensor_menu.addAction(
                'eigenvectors',
                calculations.calculate_from_tensor_field(
                    plotter, field, 'eigh'
                )
            )

    plotter.tensors_menu.aboutToShow.connect(
        tensors_menu_update
    )
    plotter.calculate_menu.addSeparator()
