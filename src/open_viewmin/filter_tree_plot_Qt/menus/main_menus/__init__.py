""" Main menu bar menus and actions, extending those of PyVistaQt """

from open_viewmin.filter_tree_plot_Qt.menus.main_menus.file_menu import setup_file_menu
from open_viewmin.filter_tree_plot_Qt.menus.main_menus.view_menu import setup_view_menu
from open_viewmin.filter_tree_plot_Qt.menus.main_menus.add_menu import setup_add_menu
from open_viewmin.filter_tree_plot_Qt.menus.main_menus.theme import setup_theme_menu
from open_viewmin.filter_tree_plot_Qt.menus.main_menus.calculate_menu import setup_calculate_menu
from open_viewmin.filter_tree_plot_Qt.menus.main_menus.refresh_button import setup_refresh_button
from open_viewmin.filter_tree_plot_Qt.menus.main_menus.non_ovm_actors_toggle_menu import setup_non_ovm_actors_toggle
from open_viewmin.filter_tree_plot_Qt.menus.main_menus.tools_menu import setup_tools_menu
from open_viewmin.filter_tree_plot_Qt.menus.main_menus.filter_menu import setup_filter_menu


def setup_main_menus(plotter):
    """ Set up main menus

    Parameters
    ----------
    plotter : open_viewmin.FilterTreePlotQt

    """

    setup_file_menu(plotter)
    setup_view_menu(plotter)
    setup_add_menu(plotter)
    setup_theme_menu(plotter)
    setup_calculate_menu(plotter)
    setup_refresh_button(plotter)
    setup_non_ovm_actors_toggle(plotter)
    setup_tools_menu(plotter)
    setup_filter_menu(plotter)
