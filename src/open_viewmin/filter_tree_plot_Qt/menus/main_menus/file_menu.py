from importlib.metadata import version
import qtpy.QtWidgets as qw


def setup_file_menu(plotter):
    def insert_action_into_file_menu(*action_args, insert_idx=0):
        action = plotter.the_File_menu.addAction(*action_args)
        plotter.the_File_menu.insertAction(
            plotter.the_File_menu.actions()[insert_idx],
            action
        )
        return action

    def insert_menu_into_file_menu(*menu_args, insert_idx=0):
        menu = plotter.the_File_menu.addMenu(*menu_args)
        plotter.the_File_menu.insertMenu(
            plotter.the_File_menu.actions()[insert_idx],
            menu
        )
        return menu

    insert_action_into_file_menu("Export as html", plotter.save_html)
    insert_action_into_file_menu("Open file(s)", plotter.open_files_dialog)

    plotter.save_mesh_menu = insert_menu_into_file_menu(
        "Export mesh", insert_idx=-3
    )

    def repopulate_save_mesh_menu():
        plotter.save_mesh_menu.clear()

        def save_mesh_action(_mesh_name):
            def ret():
                plotter.save_mesh(_mesh_name)
            return ret

        for mesh_name in plotter.mesh_names():
            plotter.save_mesh_menu.addAction(
                mesh_name,
                save_mesh_action(mesh_name)
            )
        plotter.save_mesh_menu.addAction(
            "(all)",
            plotter.save_meshes_launch_dialog
        )

    plotter.save_mesh_menu.aboutToShow.connect(repopulate_save_mesh_menu)
    insert_action_into_file_menu("About", open_about_dialog, insert_idx=-2)


def open_about_dialog():
    box = qw.QMessageBox()
    ovm_version = version("open_viewmin")
    box.setText(f"open-ViewMin {ovm_version}")
    box.setInformativeText(
        """
        Author: Daniel Beller
                     d.a.beller@jhu.edu 
        ©Daniel Beller, 2021-2024
        
        https://open-viewmin.gitlab.io/    
        """
    )
    box.exec()
