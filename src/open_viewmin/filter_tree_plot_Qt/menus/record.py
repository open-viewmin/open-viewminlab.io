""" Create buttons and dialog for recording and exporting videos of the
scene
"""

import qtpy.QtCore as Qt
import qtpy.QtWidgets as qw
import imageio
from open_viewmin.filter_tree_plot_Qt.widgets_Qt import utilities_Qt


# noinspection PyUnresolvedReferences,PyArgumentList
def setup_record_toolbar(plotter):
    """ Set up the Record toolbar section

    Parameters
    ----------
    plotter : :class:`~open_viewmin.filter_tree_plot_Qt.filter_tree_plot_Qt.FilterTreePlotQt`
    """
    plotter.movie_record_mode = 0
    plotter.the_Record_toolbar = qw.QToolBar(orientation=Qt.Qt.Horizontal)
    plotter.the_Record_toolbar.setWindowTitle('Record movie')
    plotter.app_window.addToolBar(
        Qt.Qt.TopToolBarArea, plotter.the_Record_toolbar
    )
    toolbar = plotter.the_Record_toolbar

    btn_mp4 = qw.QPushButton()
    btn_mp4.setText('◉ mp4')
    btn_mp4.setToolTip('Begin mp4 recording')
    btn_mp4.setFixedWidth(70)
    btn_mp4.clicked.connect(lambda: record_movie(plotter))
    toolbar.addWidget(btn_mp4)

    btn_gif = qw.QPushButton()
    btn_gif.setText('◉ gif')
    btn_gif.setToolTip('Begin GIF recording')
    btn_gif.setFixedWidth(60)
    btn_gif.clicked.connect(lambda: record_gif(plotter))
    toolbar.addWidget(btn_gif)

    plotter.write_frame_button = qw.QPushButton()
    plotter.write_frame_button.released.connect(
        lambda: plotter.movie.append_data(plotter.screenshot())
    )
    toolbar.addWidget(plotter.write_frame_button)
    plotter.write_frame_button.setText('➲')
    plotter.write_frame_button.setToolTip(
        f'Save screenshot as next movie frame (shortcut: {plotter.settings["write_frame_key"]})')
    plotter.write_frame_button.setEnabled(False)
    plotter.stop_recording_button = qw.QPushButton()
    plotter.stop_recording_button.setToolTip(f'Stop recording (shortcut: {plotter.settings["stop_recording_key"]})')

    def stop_recording_callback():
        plotter.movie.close()
        if hasattr(plotter, "movie_timer"):
            plotter.movie_timer.stop()
        plotter.write_frame_button.setEnabled(False)
        plotter.stop_recording_button.setEnabled(False)
        try:
            plotter.clear_events_for_key(plotter.settings['write_frame_key'])
        except KeyError:
            pass
        try:
            plotter.clear_events_for_key(plotter.settings['stop_recording_key'])
        except KeyError:
            pass

    plotter.stop_recording_button.released.connect(stop_recording_callback)
    plotter.stop_recording_button.setText('◼︎')
    plotter.stop_recording_button.setEnabled(False)
    toolbar.addWidget(plotter.stop_recording_button)


# noinspection PyUnresolvedReferences,PyArgumentList
def record_movie(
    plotter, filename='.mp4', fps=5, quality=5, movie_format='mp4'
):
    """

    Parameters
    ----------
    plotter : :class:`~open_viewmin.filter_tree_plot_Qt.filter_tree_plot_Qt.FilterTreePlotQt`
    filename : str
    fps : int
        Frames per second
    quality : int
    movie_format : str
        Must be 'gif' or 'mp4'
    """
    plotter.movie_filename = filename
    plotter.movie_fps = fps
    plotter.movie_quality = quality
    plotter.movie_timer = Qt.QTimer()
    plotter.record_movie_widget = utilities_Qt.FormDialog(title='Record movie')
    dlg = plotter.record_movie_widget
    movie_filename_lineedit = qw.QLineEdit()
    pb_movie_filename = qw.QToolButton()
    pb_movie_filename.setIcon(
        plotter.style().standardIcon(
            qw.QStyle.SP_DirIcon
        )
    )
    if movie_format == 'gif':
        pyvista_filter = 'GIF files (*.gif)'
        suffix = '.gif'
    elif movie_format == 'mp4':
        pyvista_filter = 'MPEG-4 files (*.mp4)'
        suffix = '.mp4'
    else:
        raise ValueError("parameter `movie_format` must be 'gif' or 'mp4'.")

    # noinspection PyArgumentList
    def movie_filename_dialog():
        """ Launch dialog to select or enter movie filename """

        filename_dlg = qw.QFileDialog(
            parent=plotter.app_window, filter=pyvista_filter
        )
        movie_filename = filename_dlg.getSaveFileName()[0]
        if suffix in movie_filename:
            movie_filename = ''.join(movie_filename.split(suffix)[:-1])
        movie_filename_lineedit.setText(movie_filename)

    pb_movie_filename.released.connect(movie_filename_dialog)
    sublayout = qw.QHBoxLayout()
    sublayout.addWidget(pb_movie_filename)
    sublayout.addWidget(movie_filename_lineedit)
    sublayout.addWidget(qw.QLabel(suffix))
    dlg.add_row('Save movie to:', sublayout)
    sb_fps = qw.QSpinBox(minimum=0, maximum=100, singleStep=1)
    sb_fps.setValue(plotter.movie_fps)
    dlg.add_row('Frames per second:', sb_fps)
    if suffix == '.mp4':
        sb_quality = qw.QSpinBox(minimum=1, maximum=9, singleStep=1)
        sb_quality.setValue(plotter.movie_quality)
        dlg.add_row('Quality:', sb_quality)

    cb_record_mode = qw.QComboBox()
    cb_record_mode.addItems([
        'Click ' + plotter.write_frame_button.text() + ' each frame',
        'Live recording',
        'Cycle through data time-frames',
        'Orbit'
    ])

    def record_mode_callback(choice_num):
        """ Set the movie recording mode

        Parameters
        ----------
        choice_num : int
            Must be 0, 1, 2, or 3
        """
        plotter.movie_record_mode = choice_num

    cb_record_mode.currentIndexChanged.connect(record_mode_callback)
    cb_record_mode.setCurrentIndex(0)
    plotter.movie_record_mode = 0
    dlg.add_row('Recording mode:', cb_record_mode)

    # noinspection PyUnresolvedReferences
    def activate_record_frame_on_key_entry(_plotter):
        _plotter.write_frame_button.setEnabled(True)
        _plotter.stop_recording_button.setEnabled(True)
        if not _plotter.off_screen:
            _plotter.add_key_event(
                _plotter.settings['write_frame_key'],
                _plotter.write_frame_button.click
            )

    def activate_record_time_series(_plotter):
        _plotter.first_frame()
        _plotter.movie_timer.setInterval(int(1000 / _plotter.movie_fps))

        def timeout_callback():
            _plotter.movie.append_data(_plotter.screenshot())
            if _plotter.frame_num < _plotter.num_frames() - 1:
                _plotter.next_frame()
            else:
                _plotter.movie_timer.stop()
                _plotter.movie.close()

        _plotter.movie_timer.timeout.connect(timeout_callback)
        _plotter.movie_timer.start()

    def activate_live_recording(_plotter):
        _plotter.movie_timer.setInterval(int(1000 / _plotter.movie_fps))
        _plotter.movie_timer.timeout.connect(
            lambda: _plotter.movie.append_data(_plotter.screenshot())
        )
        _plotter.movie_timer.start()
        _plotter.stop_recording_button.setEnabled(True)

    def activate_record_orbit(_plotter):
        _plotter.movie.close()  # use PyVista export instead

        def do_orbit():
            _plotter.orbit_on_path(
                path=_plotter.orbit_path,
                step=1 / _plotter.movie_fps,
                write_frames=True,
                threaded=True  # render on-screen while recording
            )

        if suffix == '.gif':
            _plotter.open_gif(_plotter.movie_filename)
            do_orbit()
        else:
            _plotter.open_movie(_plotter.movie_filename)
            warning_dlg = utilities_Qt.FormDialog(
                title='Warning',
                message="mp4 orbit recording will not be viewable until "
                        "this plot is closed."
            )
            warning_dlg.set_accepted_callback(do_orbit)
            warning_dlg.show()

    def ok_btn_callback():
        """ Begin recording according to options selected in dialog """
        plotter.record_movie_widget.close()
        plotter.movie_filename = movie_filename_lineedit.text() + suffix
        plotter.movie_fps = sb_fps.value()
        writer_kwargs = dict(fps=plotter.movie_fps)
        if suffix == '.mp4':
            plotter.movie_quality = sb_quality.value()
            writer_kwargs['quality'] = plotter.movie_quality
        plotter.movie = imageio.get_writer(
            plotter.movie_filename, **writer_kwargs
        )
        record_mode = plotter.movie_record_mode
        if record_mode == 0:
            activate_record_frame_on_key_entry(plotter)
        elif record_mode == 1:
            activate_live_recording(plotter)
        elif record_mode == 2:
            activate_record_time_series(plotter)
        elif record_mode == 3:
            activate_record_orbit(plotter)
        if not plotter.off_screen:
            plotter.add_key_event(
                plotter.settings['stop_recording_key'],
                plotter.stop_recording_button.click
            )

    dlg.set_accepted_callback(ok_btn_callback)
    dlg.show()


def record_gif(plotter, movie_filename=None):
    """ Record movie as gif

    Parameters
    ----------
    plotter : :class:`~open_viewmin.filter_tree_plot_Qt.filter_tree_plot_Qt.FilterTreePlotQt`
    movie_filename : Optional[str]
    """
    record_movie(plotter, movie_format='gif', filename=movie_filename)
