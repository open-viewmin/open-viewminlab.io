""" Dialog for controlling appearance of existing glyphs """

import qtpy.QtWidgets as qw
import numpy as np
from open_viewmin.filter_tree_plot.utilities import ovm_utilities
from open_viewmin.filter_tree_plot.filters import sample
from open_viewmin.filter_tree_plot_Qt.widgets_Qt.utilities_Qt import FormDialog, DropdownMenu


# noinspection PyUnresolvedReferences,PyTypeChecker
def make_glyph_settings_widget(plotter, actor_name):
    """  Launch Qt dialog for modifying existing glyphs.

    Activated from the customization dropdown menu in an actor's controls
    toolbar.

    Parameters
    ----------
    plotter : open_viewmin.FilterTreePlotQt
    actor_name : str
        Name of glyphs mesh

    Returns
    -------
    open_viewmin.filter_tree_plot_Qt.widgets_Qt.utilities_Qt.FormDialog

    """
    min_width = 300

    # lines after this many in the widget's "form layout" will be deleted
    # when the glyph shape is changed
    num_form_lines_to_keep = 6

    glyph_settings_widget = FormDialog(
        title="Glyph settings",
        message=f'Glyph settings for \"{actor_name}\"'
    )
    glyph_settings_widget.setMinimumWidth(700)
    glyph_settings_widget.setMinimumWidth(min_width)

    glyph_settings_formlayout = glyph_settings_widget.formlayout

    filter_formula = plotter.get_filter_formula(actor_name)
    ovm_info = plotter.get_filter_formula(actor_name).ovm_info
    filter_kwargs = filter_formula.filter_kwargs
    parent_mesh_name = filter_formula.parent_mesh_name
    parent_filter_formula = filter_formula.parent_filter_formula
    parent_filter_kwargs = parent_filter_formula.filter_kwargs

    def glyph_shape_callback(new_glyph_shape_name):
        def return_func():
            filter_formula.set_glyph_shape(new_glyph_shape_name)
            try:
                while glyph_settings_formlayout.rowCount() > num_form_lines_to_keep:
                    glyph_settings_formlayout.removeRow(
                        num_form_lines_to_keep
                    )
            except NameError:
                pass
            else:
                new_glyph_shape_func = plotter.geometric_objects[
                    new_glyph_shape_name
                ]
                default_kwargs_dict = ovm_utilities.get_kwargs_and_defaults(
                    new_glyph_shape_func
                )

                def linedit_callback(linedit, callback_key, default_val):
                    txt = str(linedit.text())
                    try:
                        val = eval(txt)
                    except NameError:
                        val = txt  # leave entry as string
                    kwargs_dict[callback_key] = val
                    try:
                        filter_formula.set_glyph_shape(
                            new_glyph_shape_name, **kwargs_dict
                        )
                    except TypeError:
                        linedit.setText(str(default_val))
                        pass  # ignore bad entries, reset text to default

                kwargs_dict = dict()
                for key in default_kwargs_dict.keys():
                    kwargs_dict[key] = default_kwargs_dict[key]

                # noinspection PyUnresolvedReferences
                def add_linedit_line(callback_key):
                    default_val = kwargs_dict[callback_key]
                    new_linedit = qw.QLineEdit()
                    new_linedit.setText(str(default_val))
                    new_linedit.editingFinished.connect(
                        lambda: linedit_callback(
                            new_linedit, callback_key, default_val
                        )
                    )
                    glyph_settings_formlayout.addRow(
                        qw.QLabel(callback_key),
                        new_linedit
                    )

                for key in kwargs_dict.keys():
                    add_linedit_line(key)
        return return_func

    glyph_shape_dropdown = DropdownMenu()
    glyph_shape_dropdown.populate([
        (shape_name, glyph_shape_callback(shape_name))
        for shape_name in list(plotter.geometric_objects.keys())
    ])

    # noinspection PyArgumentList
    sb_glyph_stride = qw.QDoubleSpinBox(
        minimum=1,
        maximum=int(
            max(1, max(np.array([plotter.Lx, plotter.Ly, plotter.Lz]) / 6))
        )
    )
    current_glyph_stride = parent_filter_kwargs.get(
        "stride", plotter.settings["glyph_stride"]
    )
    sb_glyph_stride.setValue(current_glyph_stride)
    sb_glyph_stride.setSingleStep(1)
    sb_glyph_stride.setToolTip('Press Enter to apply.')
    sb_glyphs_factor = qw.QDoubleSpinBox()

    def glyph_stride_callback():
        callback_grandparent_mesh_name = (
            plotter.get_grandparent_mesh_name(actor_name)
        )
        glyph_stride = sb_glyph_stride.value()
        sample.make_sampled_mesh(
            plotter, callback_grandparent_mesh_name, glyph_stride,
            sampled_mesh_name=parent_mesh_name,
            random=cb_glyph_random.isChecked()
        )
        # noinspection PyTypeChecker
        plotter.update_filter(
            actor_name,
            update_actor=True,
            factor=glyph_stride
        )
        sb_glyphs_factor.setValue(glyph_stride)

    sb_glyph_stride.editingFinished.connect(glyph_stride_callback)
    cb_glyph_random = qw.QCheckBox()
    parent_filter_info = plotter.get_filter_formula(parent_mesh_name)
    is_currently_random = parent_filter_info.ovm_info.get('random', False)
    grandparent_mesh_name = plotter.get_grandparent_mesh_name(actor_name)

    cb_glyph_random.setChecked(is_currently_random)

    def cb_glyph_random_callback():
        random = cb_glyph_random.isChecked()
        plotter.get_filter_formula(actor_name).ovm_info['random'] = random
        callback_grandparent_mesh_name = (
            plotter.get_grandparent_mesh_name(actor_name)
        )

        plotter.get_filter_formula(actor_name).parent_mesh_name = (
            sample.make_sampled_mesh(
                plotter,
                callback_grandparent_mesh_name,
                sb_glyph_stride.value(),
                sampled_mesh_name=parent_mesh_name,
                random=random
            )
        )
        plotter.update_filter(actor_name, update_actor=True)

    cb_glyph_random.stateChanged.connect(cb_glyph_random_callback)

    def modify_glyphs_orient_callback(vector_field_name):
        def return_func():
            plotter.update_filter(
                actor_name,
                orient=vector_field_name,
                update_actor=True
            )
        return return_func

    glyphs_orient_dropdown = DropdownMenu()
    vector_field_names = plotter.vector_fields(mesh=grandparent_mesh_name)
    glyphs_orient_dropdown.populate([
        (vector_field_name, modify_glyphs_orient_callback(vector_field_name))
        for vector_field_name in vector_field_names
    ])
    current_orientation_array = filter_kwargs.get('orient')
    if isinstance(current_orientation_array, str):
        glyphs_orient_dropdown.setText(current_orientation_array)

    def modify_glyphs_scale_callback(scalar_field_name):
        def return_func():
            plotter.update_filter(
                actor_name,
                scale=scalar_field_name,
                # ovm_info=dict(random=cb_glyph_random.isChecked()),
                update_actor=True
            )
        return return_func

    glyphs_scale_dropdown = DropdownMenu()
    scalar_field_names = plotter.scalar_fields()
    glyphs_scale_dropdown.populate([
        (scalar_field_name, modify_glyphs_scale_callback(scalar_field_name))
        for scalar_field_name in scalar_field_names
    ])
    current_scale_array = filter_kwargs.get('scale')
    if current_scale_array is not None:
        glyphs_scale_dropdown.setText(current_scale_array)

    sb_glyphs_factor = qw.QDoubleSpinBox()
    sb_glyphs_factor.setMinimum(0)
    sb_glyphs_factor.setSingleStep(0.5)
    if 'factor' in filter_kwargs.keys():
        sb_glyphs_factor.setValue(filter_kwargs['factor'])
    else:
        sb_glyphs_factor.setValue(1)
    sb_glyphs_factor.editingFinished.connect(
        lambda: plotter.update_filter(
            actor_name,
            update_actor=True,
            # ovm_info=dict(random=cb_glyph_random.isChecked()),
            factor=sb_glyphs_factor.value()
        )
    )
    label_callback_pairs = (
        ('shape type', glyph_shape_dropdown),
        ('orientations:', glyphs_orient_dropdown),
        ('scale array:', glyphs_scale_dropdown),
        ('glyphs stride', sb_glyph_stride),
        ('global scale:', sb_glyphs_factor),
        ('random locations', cb_glyph_random)
    )
    for label, callback in label_callback_pairs:
        glyph_settings_formlayout.addRow(qw.QLabel(label), callback)

    old_glyph_shape_name = ovm_info.get('glyph_shape')
    if old_glyph_shape_name is not None:
        glyph_shape_dropdown.setText(old_glyph_shape_name)

    return glyph_settings_widget
