""" Additional default values specific to the Qt GUI"""


def mesh_label_style(mesh_name: str) -> str:
    """ Style text for mesh names in filters tree GUI """
    return f"<i>{mesh_name}</i>"


def actor_label_style(actor_name: str) -> str:
    """ Style text for actor names in filters tree GUI """
    return f"<b>{actor_name}</b>"


settings_Qt_dict = dict(
    window_size=(1200, 800),  # window size in pixels
    qt_scale_factor=1,
    controls_area_height=2000,
    controls_area_width=210,
    visible_symbol='',
    invisible_symbol='',
    slice_control_toolbar_height=110,
    icon_size=12,
    actor_menu_symbol="⋮",
    mesh_menu_symbol="↪",
    write_frame_key='m',
    stop_recording_key='x'
)
