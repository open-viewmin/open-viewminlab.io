""" Define classes for dialogs and actor control toolbars """

from functools import wraps
from qtpy import QtWidgets as qw
from qtpy import QtGui
import qtpy.QtCore as Qt
import numpy as np
from pyvista.plotting.colors import Color as pvColor
from open_viewmin.filter_tree_plot.utilities.ovm_utilities import hex_to_rgb


class FormDialog(qw.QDialog):
    """ Convenience dialog with form layout inside a box layout.

    Parameters
    ----------
    title : str, optional
        Dialog widget title.
    message : str or None, optional
        Text displayed at top of dialog.
    parent : qtpy.QtWidgets.QWidget or None, optional
    apply_button : bool, optional
        Whether to place "Apply" button alongside "OK" and "Cancel"
        buttons.
    **kwargs : dict, optional
        Keyword arguments to qtpy.QtWidgets.QDialog

    Examples
    --------
    >>> form_dialog = FormDialog(
    ...    title="This is a FormDialog",
    ...    message="This is an example FormDialog. Please choose an integer."
    ... )
    >>>
    >>> spinbox = qtpy.QtWidgets.QSpinBox()
    >>> form_dialog.add_row("Choose an integer:", spinbox)
    >>>
    >>> def ok_callback():
    ...     print(f"You chose {spinbox.value()}")
    >>>
    >>> form_dialog.set_accepted_callback(ok_callback)
    >>> form_dialog.show()

    """

    # noinspection PyUnresolvedReferences,PyArgumentList
    def __init__(
        self, title="", message=None, parent=None, apply_button=False, **kwargs
    ):
        super().__init__(**kwargs)
        self.setWindowTitle(title)
        self.setSizePolicy(*((qw.QSizePolicy.Maximum,) * 2))
        self.buttonBox = qw.QDialogButtonBox(
            qw.QDialogButtonBox.Ok | qw.QDialogButtonBox.Cancel,
            parent=parent
        )
        self.buttonBox.accepted.connect(self.accept)
        self.buttonBox.rejected.connect(self.reject)
        self.layout = qw.QVBoxLayout(self)
        self.formlayout = qw.QFormLayout()
        if message is not None:
            self.layout.addWidget(qw.QLabel(message + "\n"))
        self.layout.addLayout(self.formlayout)
        self.layout.addWidget(self.buttonBox)
        if apply_button:
            self.apply_button = self.buttonBox.addButton(
                qw.QDialogButtonBox.Apply
            )
            self.apply = lambda: None
            self.apply_button.clicked.connect(self.apply)

    @wraps(qw.QDialog.exec)
    def show(self):
        self.exec()

    def add_row(self, text, input_widget):
        """

        Parameters
        ----------
        text : str
        input_widget : qtpy.QtWidgets.QWidget

        Returns
        -------
        qtpy.QtWidgets.QWidget

        """
        self.formlayout.addRow(qw.QLabel(text), input_widget)
        return input_widget

    def set_accepted_callback(self, callback):
        """ Connect function to "OK" button

        Parameters
        ----------
        callback : callable
        """
        # noinspection PyUnresolvedReferences
        self.accepted.connect(callback)
        if hasattr(self, "apply_button"):
            self.apply_button.clicked.connect(callback)

    def set_rejected_callback(self, callback):
        """ Connect function to "Cancel" button

        Parameters
        ----------
        callback : callable
        """
        # noinspection PyUnresolvedReferences
        self.rejected.connect(callback)


# noinspection PyUnresolvedReferences
def add_close_button_to_toolbar(toolbar, closes=None, add_padding=False):
    """  Add button to close a widget

    Parameters
    ----------
    toolbar : qtpy.QtWidgets.QToolBar
        Toolbar to which button will be added
    closes : qtpy.QtWidgets.QWidget or None, optional
        Widget closed by button. Defaults to `toolbar`
    add_padding : bool, optional
        Whether to place the button on the right side of the toolbar

    Returns
    -------
    qtpy.QtWidgets.QToolButton
        The created "close" button

    """

    if closes is None:
        closes = toolbar
    close_button = qw.QToolButton()
    close_button.setAutoRaise(True)
    close_button.setIcon(
        toolbar.style().standardIcon(qw.QStyle.SP_TitleBarCloseButton)
    )
    close_button.released.connect(closes.hide)
    close_button.setToolTip("Hide this toolbar")

    if add_padding:
        # empty space padding pushes close button to the right
        empty_label = qw.QLabel("")
        empty_label.setSizePolicy(*(qw.QSizePolicy.Expanding,)*2)
        toolbar.addWidget(empty_label)

    toolbar.addWidget(close_button)
    return close_button


class ControlsToolbar(qw.QToolBar):
    """ Toolbar containing controls for visualizing mesh in filters tree

    Parameters
    ----------
    name : str or None, optional
        Key for toolbar in `plotter.toolbars`. Defaults to `label`.
    label : str or None, optional
        Toolbar title to display. Required if `name` is `None`.
    icon_size : float or None, optional
        Size in pixels of icons such as "close" button.
    movable : bool, optional
        Whether toolbar order can be rearranged by dragging with mouse.
    floatable : bool, optional
        Whether toolbar can be detached from its parent widget by dragging
        with mouse.
    orientation : "horizontal" or "vertical", optional
        Toolbar orientation.
    has_close_button : bool, optional
        Whether toolbar has a button that closes itself.
    has_refresh_button : bool, optional
        Whether toolbar has a button that triggers its `refresh_actions`.

    """

    # noinspection PyArgumentList
    def __init__(
        self, name=None, label=None, icon_size=None, movable=True,
        floatable=False, orientation="horizontal", has_close_button=True,
        has_refresh_button=False
    ):
        if orientation == "vertical":
            orientation_Qt = Qt.Qt.Orientation.Vertical
        else:
            orientation_Qt = Qt.Qt.Orientation.Horizontal

        if name is None:
            name = label
        super().__init__(
            name,
            orientation=orientation_Qt,
            movable=movable,
            floatable=floatable
        )
        self.label = ""
        horizontal = Qt.Qt.Orientation.Horizontal
        self.label_toolbar = qw.QToolBar(orientation=horizontal)
        self.addWidget(self.label_toolbar)

        self.options_toolbar = qw.QToolBar(orientation=horizontal)
        self.addWidget(self.options_toolbar)

        # remove padding between contents
        self.layout().setSpacing(0)
        self.options_toolbar.layout().setSpacing(0)

        self.QLabel = qw.QLabel()
        self.QLabel.setSizePolicy(*(qw.QSizePolicy.Policy.Expanding,)*2)
        self.QLabel.setWordWrap(True)
        self.set_label(label)
        self.label_toolbar.addWidget(self.QLabel)
        self.icon_size = icon_size
        self.set_icon_size()
        self.refresh_actions = []
        if has_refresh_button:
            self.add_refresh_button()
        if has_close_button:
            self.add_close_button()

        self.rows = [self.label_toolbar, self.options_toolbar]

    def add_close_button(self):
        """ Add button to close a widget """
        add_close_button_to_toolbar(self.label_toolbar, closes=self)

    def add_refresh_button(self):
        """ Add button that triggers :attr:`refresh` """
        refresh_icon = qw.QStyle.StandardPixmap.SP_BrowserReload
        self.label_toolbar.addAction(
            self.style().standardIcon(refresh_icon),
            "",
            self.refresh
        )

    def refresh(self):
        """ Execute all functions in :attr:`refresh_actions` """
        for action in self.refresh_actions:
            action()

    def set_label(self, text=None):
        """ Change text in label toolbar """
        if text is not None:
            self.label = text
        if self.label is not None:
            self.QLabel.setText(self.label)

    def set_icon_size(self, icon_size=None):
        if icon_size is not None:
            self.icon_size = icon_size
        if self.icon_size is not None:
            qsize = Qt.QSize(*(self.icon_size,) * 2)
            self.label_toolbar.setIconSize(qsize)
            self.options_toolbar.setIconSize(qsize)


class ControlsSliderToolbar(qw.QToolBar):
    """
    Control toolbar containing a slider
    """
    # noinspection PyUnresolvedReferences,PyArgumentList
    def __init__(
        self, update_method, actor=None,
        num_divs=100, init_val_percent=50, max_val=None, min_val=None,
        scalars=None, label_txt="", name=None, spinbox=True,
        has_close_button=False
    ):
        """

        Parameters
        ----------
        update_method: callable
        actor: :class:`pyvista.plotting.actor.Actor` | `None`
        num_divs: int
        init_val_percent: int
        max_val: float | `None`
        min_val: float | `None`
        scalars: :class:`pyvista.pyvista_ndarray`
        label_txt: str
        name: str | `None`
        spinbox: bool
        has_close_button: bool
        """
        super().__init__(name, orientation=Qt.Qt.Horizontal)
        if has_close_button:
            self.add_close_button()
        self.update_method = update_method
        self.actor = actor
        self.num_divs = num_divs
        self.min_val = min_val
        self.max_val = max_val
        self.slider = qw.QSlider(Qt.Qt.Horizontal)
        self.slider.setMinimum(0)
        self.slider.setMaximum(self.num_divs)
        init_val = init_val_percent * num_divs / 100
        self.slider.setValue(int(init_val))
        self.slider.setMaximumWidth(150)
        if max_val is None and scalars is not None:
            self.max_val = np.max(scalars)
        if min_val is None and scalars is not None:
            self.min_val = np.min(scalars)

        self.QLabel = qw.QLabel(label_txt)
        if len(label_txt) > 0:
            self.addWidget(self.QLabel)

        if spinbox:
            self.spinbox = qw.QDoubleSpinBox(
                minimum=self.slider_formula(self.slider.minimum()),
                maximum=self.slider_formula(self.slider.maximum()),
                value=init_val,
                singleStep=0.1,
                decimals=3
            )
            self.spinbox.editingFinished.connect(self.spinbox_callback)
            self.addWidget(self.spinbox)
        else:
            self.spinbox = None
        self.slider.valueChanged.connect(self.value_change_method)
        # self.wiggle_slider_to_update()
        self.addWidget(self.slider)

    def add_close_button(self):
        """ Add button that closes (removes) this toolbar """
        add_close_button_to_toolbar(self)

    def slider_formula(self, slider_value):
        """ Convert from slider integer value to formula float value. """
        return self.min_val + (
                (self.max_val - self.min_val) * slider_value / self.num_divs
        )

    def external_update(self, float_value):
        """ Externally called trigger for update of slider from given value

        Parameters
        ----------
        float_value: float
        """

        slider_value = int(
            self.num_divs * (
                (float_value - self.min_val) / (self.max_val - self.min_val)
            )
        )
        self.slider.setValue(slider_value)

    def spinbox_callback(self):
        """ Update slider from spinbox """
        self.external_update(self.spinbox.value())

    def value_change_method(self, slider_value):
        """ Update actor's visibility and toolbox's spinbox from slider value

        Parameters
        ----------
        slider_value: int
        """
        float_value = self.slider_formula(slider_value)
        try:
            vis = self.actor.GetVisibility()
        except AttributeError:
            vis = 0
        self.update_method(float_value)
        if self.actor is not None:
            try:
                self.actor.SetVisibility(vis)
            except AttributeError:
                pass
        if self.spinbox is not None:
            self.spinbox.setValue(float_value)

    def wiggle_slider_to_update(self):
        """
        For an actor with some feature controlled by a slider,
        move the slider right and left to force the actor to update
        """
        slider = self.slider
        val = slider.value()
        try:
            for i in [1, -1]:
                slider.setValue(val + i)
                slider.setValue(val)
        except ValueError:
            for i in [-1, 1]:
                slider.setValue(val + i)
                slider.setValue(val)


class QDoubleSpinBoxIgnoreLims(qw.QDoubleSpinBox):
    """ PyQt5.QtWidgets.QDoubleSpinBox with auto-updating range

    Values entered outside the current allowed range cause the max or min
    value to be shifted to accommodate.

    Parameters
    ----------
    *args : tuple, optional
        Arguments to `PyQt5.QtWidgets.QDoubleSpinBox`
    num_subdivisions : int, optional
        Number of subdivisions of the range, determining the step size.
    **kwargs : dict, optional
        Keyword arguments to `PyQt5.QtWidgets.QDoubleSpinBox`

    """

    def __init__(self, *args, num_subdivisions=1000, **kwargs):
        super().__init__(*args, **kwargs)
        self.num_subdivisions = num_subdivisions
        self.high_val = self.maximum()
        self.low_val = self.minimum()
        self.update_single_step()

    @property
    def extent(self):
        """ Difference between reference high and low values.

        Returns
        -------
        float
        """
        return self.high_val - self.low_val

    def set_high_val(self, value):
        """ Set reference high value.

        Sufficiently high value triggers increase in :attr:`maximum`.

        Parameters
        ----------
        value: float
        """

        self.high_val = value
        if value > 0.5 * (self.maximum() + self.minimum()):
            self.setMaximum(2 * self.maximum() - self.minimum())
        self.update_single_step()

    def set_low_val(self, value):
        """ Set reference low value.

        Sufficiently low value triggers decrease in :attr:`minimum`.

        Parameters
        ----------
        value: float
        """

        self.low_val = value
        if value < 0.5 * (self.minimum() + self.maximum()):
            self.setMinimum(2 * self.minimum() - self.maximum())
        self.update_single_step()

    def update_single_step(self):
        """ Change increment to reflect change in :attr:`extent`

        """
        self.setSingleStep(self.extent / self.num_subdivisions)

    def qslider_value(self):
        """ Slider position to match current value

        Returns
        -------
        int
        """

        if self.extent == 0:
            return 0
        else:
            return int(
                (self.value() - self.low_val)
                * self.num_subdivisions / self.extent
            )

    def set_value_from_qslider(self, qslider_value):
        """ Set value according to associated slider

        Parameters
        ----------
        qslider_value: int
        """

        self.setValue(
            self.low_val + qslider_value / self.num_subdivisions * self.extent
        )


class DropdownMenu(qw.QPushButton):
    """ Convenience class for creating dropdown menus from push-buttons.

    Parameters
    ----------
    width : float or None, optional
        Width of push-button

    """

    def __init__(self, width=None):
        super().__init__()
        if width is not None:
            self.setFixedWidth(int(width))
        self.menu = qw.QMenu()
        self.setMenu(self.menu)
        # noinspection PyUnresolvedReferences
        self.menu.aboutToHide.connect(self.set_button_text)

    def populate(self, label_callback_pairs):
        """ Fill menu with actions

        Parameters
        ----------
        label_callback_pairs: Iterable of tuple(str, callable)

        """
        self.menu.clear()
        for label, callback in label_callback_pairs:
            self.menu.addAction(label, callback)

    def set_button_text(self):
        """ Set text of button that triggers dropdown """
        active_action = self.menu.activeAction()
        if active_action is not None:
            self.setText(active_action.text())


class ColorDialogValid(qw.QColorDialog):
    """ `Qt.QtWidgets.QColorDialog` that resets to initial color if canceled.

   Parameters
   ----------
   callback : callable()
       Function of the chosen color
   initial_color : (float, float, float), str, or None
       Color to apply if the user presses "Cancel"
   parent : QWidget, optional
       Parent widget

   """

    def __init__(self, callback, initial_color, parent=None):
        self.callback = callback
        if initial_color is None:
            initial_color = (0., 0., 0.)
        self.initial_color = self.color_to_qcolor(initial_color)
        super().__init__(self.initial_color, parent)
        # noinspection PyUnresolvedReferences
        self.currentColorChanged.connect(self.color_dialog_callback)
        # noinspection PyUnresolvedReferences
        self.rejected.connect(self.reset_to_initial_color)
        self.open()

    def color_dialog_callback(self, chosen_qcolor):
        """ Send chosen color to :attr:`callback`

        Parameters
        ----------
        chosen_qcolor: PyQt5.QtGui.QColor
        """

        if chosen_qcolor.isValid():  # user pressed OK
            chosen_color = chosen_qcolor.name()
        else:
            chosen_color = self.initial_color
        self.callback(chosen_color)

    def reset_to_initial_color(self):
        """ Reset color to :attr:`initial_color` """
        self.color_dialog_callback(self.initial_color)

    @staticmethod
    def color_to_qcolor(color):
        """ Convert various color formats to :class:`~PyQt5.QtGui.QColor`

        Parameters
        ----------
        color: :class:`pyvista.plotting.colors.Color` | numpy.ndarray | tuple | str
            For `numpy.ndarray` or `tuple`, elements must be `int`s, or there
            can be exactly one element in the form of a hex color string
            beginning with `"#"`.
            For `str`, must be a hex color string beginning with `"#"` or a
            named color.

        Returns
        -------
        PyQt5.QtGui.QColor

        """
        if isinstance(color, pvColor):
            color = color.float_rgb
        if isinstance(color, (tuple, np.ndarray)):
            if (lambda c: c[0] == "#" or c.isalpha())(str(color[0])):
                color = ''.join(color)
            else:
                return QtGui.QColor.fromRgbF(*color)
        if isinstance(color, str):
            if color.startswith("#"):
                rgb = hex_to_rgb(color)
                # noinspection PyArgumentList
                return QtGui.QColor.fromRgb(*rgb)
            else:
                return QtGui.QColor(color)
        else:
            raise TypeError(
                "color must be tuple or numpy.ndarray of ints, or str, or "
                "pyvista.plotting.colors.Color; got "
                f"{type(color)}"
            )
