""" Routines to create and modify the in-GUI text representation of the
filters tree
"""

import qtpy.QtWidgets as qw
from qtpy import QtGui
from qtpy import QtCore as Qt
from open_viewmin.filter_tree_plot_Qt import settings_Qt


def fill_filters_tree_recursively(
    plotter, parent_filter_name, parent_tree_item
):
    # noinspection GrazieInspection
    """
    For each branch of filters tree, find all child meshes and their child
    meshes, etc., until reaching actors (leaves of tree)

    Parameters
    ----------
    plotter : :class:`~open_viewmin.filter_tree_plot_Qt.filter_tree_plot_Qt.FilterTreePlotQt`
    parent_filter_name : str
    parent_tree_item : qtpy.QtWidgets.QTreeWidgetItem
    """

    for child_name in (
        plotter.get_filter_formula(parent_filter_name).children
    ):
        # don't make duplicate entries
        if child_name in plotter.filters_tree_widget_items.keys():
            continue

        is_actor = plotter.get_filter_formula(child_name).has_actor
        child_tree_item = format_for_filters_tree(
            plotter,
            child_name,
            is_mesh=True,
            is_actor=is_actor
        )
        plotter.filters_tree_widget_items[child_name] = child_tree_item
        parent_tree_item.addChild(child_tree_item)
        if child_name != parent_filter_name:
            # if a child has the same name as its parent, assume this is an
            # actor i.e. a leaf of the tree,
            # so stop recursion; otherwise we would have infinite recursion
            fill_filters_tree_recursively(plotter, child_name, child_tree_item)


def add_filters_tree_top_item(plotter, mesh_name, is_mesh=True, is_actor=False):
    """ Add top-level (root) item to the filters tree GUI representation

    Parameters
    ----------
    plotter : :class:`~open_viewmin.filter_tree_plot_Qt.filter_tree_plot_Qt.FilterTreePlotQt`
    mesh_name : str
    is_mesh : bool or `None`
    is_actor : bool or `None`
    """
    tree_top_item = format_for_filters_tree(
        plotter, mesh_name, is_mesh=is_mesh, is_actor=is_actor
    )
    plotter.filters_tree_widget.addTopLevelItem(tree_top_item)


def format_for_filters_tree(
    plotter, label, is_mesh=None, is_actor=None
):
    """ Create a text item for the filters tree GUI representation

    The text is formatted according to whether the filter is associated with
    a mesh and/or actor.

    Parameters
    ----------
    plotter : :class:`~open_viewmin.filter_tree_plot_Qt.filter_tree_plot_Qt.FilterTreePlotQt`
    label : str
    is_mesh : bool or `None`
    is_actor : bool or None`

    Returns
    -------
    qtpy.QtWidgets.QTreeWidgetItem
    """
    if is_mesh is None:
        is_mesh = (label in plotter.mesh_names())
    if is_actor is None:
        is_actor = plotter.get_filter_formula(label).has_actor
    ret = qw.QTreeWidgetItem([label])
    font = QtGui.QFont()
    font.setBold(is_actor)
    font.setItalic(is_mesh)
    ret.setFont(0, font)
    return ret


# noinspection PyUnresolvedReferences
def create_filters_tree(plotter):
    """ Set up a logical tree of parent-child pairs of meshes and actors,
    to be displayed in the controls area """

    # noinspection PyArgumentList
    plotter.toolbars['filters tree'] = qw.QToolBar(
        'filters tree',
        orientation=Qt.Qt.Vertical
    )
    tree_toolbar = plotter.toolbars['filters tree']
    tree_title_toolbar = qw.QToolBar()
    # noinspection PyTypeChecker
    tree_title_toolbar.setOrientation(Qt.Qt.Horizontal)
    tree_title_toolbar.setIconSize(
        Qt.QSize(*((plotter.settings["icon_size"],) * 2))
    )
    tree_toolbar.addWidget(tree_title_toolbar)
    meshes_and_actors_label = qw.QLabel(
        f"{settings_Qt.mesh_label_style('meshes')} "
        f"and {settings_Qt.actor_label_style('actors')}"
    )
    tree_title_toolbar.addWidget(meshes_and_actors_label)
    tree_tooltip_text = (
        "double-click actor name to activate its control toolbar"
    )
    tree_title_toolbar.setToolTip(tree_tooltip_text)
    tree_toolbar.setToolTip(tree_tooltip_text)
    empty_label = qw.QLabel("")
    empty_label.setSizePolicy(*(qw.QSizePolicy.Expanding,) * 2)
    tree_title_toolbar.addWidget(empty_label)

    tree_title_toolbar.addAction(
        plotter.style().standardIcon(qw.QStyle.SP_BrowserReload),
        "",
        lambda: update_filters_tree(plotter)
    )

    plotter.filters_tree_widget = qw.QTreeWidget()
    plotter.filters_tree_widget.setColumnCount(1)
    plotter.filters_tree_widget.setWordWrap(True)
    tree_toolbar.resize(plotter.settings["controls_area_width"], 120)
    plotter.filters_tree_widget.resize(plotter.toolbars['filters tree'].size())
    add_filters_tree_top_item(plotter, plotter.default_mesh_name)
    plotter.toolbars['filters tree'].addWidget(plotter.filters_tree_widget)

    def filters_tree_callback(item):
        item_name = item.text(0)
        if item_name in plotter.toolbars.keys():
            toolbar = plotter.toolbars[item_name]
            if toolbar.isHidden():
                toolbar.show()

    plotter.filters_tree_widget.itemDoubleClicked.connect(filters_tree_callback)
    plotter.controls_dock_window.addToolBar(
        Qt.Qt.LeftToolBarArea, plotter.toolbars['filters tree']
    )

    # replace ellipsis policy with horizontal scrollbar
    plotter.filters_tree_widget.header().setSectionResizeMode(
        qw.QHeaderView.ResizeToContents
    )
    plotter.filters_tree_widget.header().setStretchLastSection(False)
    plotter.filters_tree_widget.setHeaderHidden(True)
    update_filters_tree(plotter)


def update_filters_tree(plotter):
    """ Update the GUI representation of the filters tree

    Parameters
    ----------
    plotter : :class:`~open_viewmin.filter_tree_plot_Qt.filter_tree_plot_Qt.FilterTreePlotQt`
    """
    if hasattr(plotter, "filters_tree_widget"):
        plotter.filters_tree_widget_items.clear()
        for i in range(plotter.filters_tree_widget.topLevelItemCount()):
            tree_top_item = plotter.filters_tree_widget.topLevelItem(i)
            filter_name = tree_top_item.text(0)
            if filter_name in plotter.mesh_names():
                tree_top_item.takeChildren()
                fill_filters_tree_recursively(
                    plotter, filter_name, tree_top_item
                )
        plotter.filters_tree_widget.expandAll()
