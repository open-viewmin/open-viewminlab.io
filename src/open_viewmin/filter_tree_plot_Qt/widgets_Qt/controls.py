""" Routines to create actor control toolbars in the Qt GUI """

from __future__ import annotations
import qtpy.QtWidgets as qw
import qtpy.QtCore as Qt
import numpy as np
from open_viewmin.filter_tree_plot_Qt.widgets_Qt import utilities_Qt
from open_viewmin.filter_tree_plot_Qt.menus import mesh_options, visualization_options
from open_viewmin.filter_tree_plot.widgets import plane_widgets


def setup_lighting_control(plotter):
    """ Slider to change lighting intensity

    Parameters
    ----------
    plotter: :class:`~open_viewmin.filter_tree_plot_Qt.filter_tree_plot_Qt.FilterTreePlotQt`
    """

    plotter.toolbars["lighting"] = utilities_Qt.ControlsSliderToolbar(
        lambda value: plotter.set_lights_intensity(
            value * plotter.settings["lighting_rescale_factor"]
        ),
        None,
        label_txt="☀︎",
        min_val=0,
        max_val=20 / plotter.settings["lighting_rescale_factor"],
        init_val_percent=8 / plotter.settings["lighting_rescale_factor"],
        name="lighting",
        spinbox=False,
    )
    lighting_toolbar = plotter.toolbars["lighting"]
    lighting_toolbar.slider.setFixedWidth(150)
    plotter.controls_dock_window.addToolBar(
        Qt.Qt.ToolBarArea.LeftToolBarArea,
        lighting_toolbar
    )
    lighting_toolbar.setOrientation(Qt.Qt.Orientation.Horizontal)
    lighting_toolbar.setIconSize(
        Qt.QSize(*((plotter.settings["icon_size"],) * 2))
    )
    utilities_Qt.add_close_button_to_toolbar(
        lighting_toolbar, add_padding=True
    )


class SliceControlsToolBar(qw.QToolBar):
    """ For a plane widget, add an expandable set of rows to its controls
        toolbar to change its normal and origin """
    # noinspection PyUnresolvedReferences
    def __init__(
            self,
            parent_toolbar,
            widget_name: Optional[str] = None,
            make_origin_slider: bool = True
    ):
        """

        Parameters
        ----------
        parent_toolbar: qw.QToolBar
        widget_name: str | `None`
        make_origin_slider: bool
        """
        # noinspection GrazieInspection
        # noinspection PyArgumentList
        super().__init__(parent_toolbar, orientation=Qt.Qt.Vertical)
        self.parent_toolbar = parent_toolbar
        self.plotter = self.parent_toolbar.plotter
        self.actor_name = self.parent_toolbar.actor_name
        self.QSliders = dict()
        self.input_boxes = dict()
        self.make_origin_slider = make_origin_slider

        if widget_name is None:
            widget_name = self.actor_name + "_widget"
            assert widget_name in self.plotter.widgets.keys()
        self.widget_name = widget_name
        self.widget = self.plotter.get_widget(self.widget_name)
        if self.widget is None:
            current_normal = [1, 0, 0]
        else:
            current_normal = self.widget.GetNormal()
        current_theta = np.arccos(current_normal[2])
        current_phi = np.arctan2(current_normal[1], current_normal[0])
        self.num_slider_divs = int(2 * np.pi * 1000)

        self.theta_slider, self.theta_input_box = self.setup_slice_control(
            "θ", 0, np.pi, current_theta,
            self.slice_theta_slider_formula,
            self.slice_theta_slider_inv_formula,
            self.slice_theta_slider_value_changed_callback,
        )
        self.phi_slider, self.phi_input_box = self.setup_slice_control(
            "φ", 0, 2 * np.pi, current_phi,
            self.slice_phi_slider_formula,
            self.slice_phi_slider_inv_formula,
            self.slice_phi_slider_value_changed_callback
        )

        max_slice_translate = int(
            np.sqrt(np.sum((np.array(self.plotter.dims()) / 2) ** 2))
        )
        if self.make_origin_slider:
            self.origin_slider, self.origin_input_box = self.setup_slice_control(
                "⟂", -max_slice_translate, max_slice_translate, 0,
                lambda val: val,
                lambda val: val,
                self.slice_origin_slider_value_changed_callback,
                use_ints=True,
                slider_minimum=-max_slice_translate,
                slider_maximum=max_slice_translate
            )

        self.parent_toolbar.slice_control_toolbar = self
        self.parent_toolbar.addWidget(self)

        self.setup_orient_along_axes_buttons()

        # self.expand_controls_button = qw.QToolButton()
        # self.expand_controls_button.setAutoRaise(True)
        self.symbol = "⊞"
        self.expand_controls_button = qw.QPushButton(self.symbol)
        self.expand_controls_button.setToolTip(
            "Click to expand/collapse slice controls"
        )
        self.expand_controls_button.released.connect(
            self.expand_or_collapse_control_toolbar
        )
        self.parent_toolbar.options_toolbar.addWidget(
            self.expand_controls_button
        )
        self.setFixedWidth(
            self.plotter.settings["controls_area_width"]
        )
        self.expanded = False
        self.collapse()  # initialize collapsed

    @property
    def widget_formula(self):
        """ Callback that updates the `slice` widget

        Returns
        -------
        :class:`~open_viewmin.filter_tree_plot.widget_formulas.WidgetFormula`

        """
        return self.plotter.get_widget_formula(self.widget_name)

    def slice_value_changed_callback(
            self,
            key: str,
            value_callable: callable(float) | callable(int)
    ) -> callable(float):
        """ Make callback executed when a `slice` widget is altered

        Parameters
        ----------
        key: str
        value_callable: callable

        Returns
        -------
        callable
        
        """
        def return_func(value):
            widget_formula = self.widget_formula
            if widget_formula is not None:
                return plane_widgets.alter_plane_widget(
                    widget_formula,
                    **{key: value_callable(value)}
                )
        return return_func

    def slice_theta_slider_formula(self, value: int) -> float:
        """ Conversion from slider position to slice $\\theta$ value

        Parameters
        ----------
        value: int
            slider value

        Returns
        -------
        float
            polar angle
        """
        return value * np.pi / self.num_slider_divs

    def slice_theta_slider_inv_formula(self, value: float) -> int:
        """ Conversion from slice $\\theta$ value to slider position

        Parameters
        ----------
        value: float
            polar angle

        Returns
        -------
        int
            slider value
        """
        return int(value / (np.pi / self.num_slider_divs))

    def slice_theta_slider_value_changed_callback(self, value):
        """ Recreate `slice` after change in polar angle

        Parameters
        ----------
        value: float
            polar angle

        Returns
        -------
        `None`
        """
        return self.slice_value_changed_callback(
            "theta", self.slice_theta_slider_formula
        )(value)

    def slice_phi_slider_formula(self, value: int) -> float:
        """ Conversion from slider position to slice $\\phi$ value

        Parameters
        ----------
        value: int
            slider value

        Returns
        -------
        `float`

        """
        return value * 2 * np.pi / self.num_slider_divs

    def slice_phi_slider_inv_formula(self, value: float) -> int:
        """ Conversion from slice $\\theta$ value to slider position

        Parameters
        ----------
        value: float

        Returns
        -------
        `int`

        """
        return int(value / (2 * np.pi / self.num_slider_divs))

    def slice_phi_slider_value_changed_callback(self, value):
        """ Recreate `slice` after change in azimuthal angle

        Parameters
        ----------
        value: float
            azimuthal angle

        Returns
        -------
        `None`

        """
        return self.slice_value_changed_callback(
            "phi", self.slice_phi_slider_formula
        )(value)

    def slice_origin_slider_value_changed_callback(self, value):
        """ Recreate `slice` after change in origin

        Parameters
        ----------
        value: float
            Shift along `slice` normal

        Returns
        -------
        `None`

        """
        return self.slice_value_changed_callback(
            "origin",
            lambda val: (
                0.5 * np.array(self.plotter.dims())
                + val * np.array(self.widget.GetNormal())
            )
        )(value)

    # noinspection PyUnresolvedReferences
    def setup_slice_control(
            self,
            label: str,
            minimum: float,
            maximum: float,
            init_val: float,
            slider_formula: callable,
            slider_inv_formula: callable,
            value_changed_callback: callable,
            use_ints: bool = False,
            slider_minimum: int = 0,
            slider_maximum: Optional[int] = None
    ) -> (qw.QSlider, qw.QSpinBox):
        """ Set up controls for `slice` widget

        Parameters
        ----------
        label: str
        minimum: float
        maximum: float
        init_val: float
        slider_formula: callable
        slider_inv_formula: callable
        value_changed_callback: callable
        use_ints: bool
        slider_minimum: int
        slider_maximum: int | `None`

        Returns
        -------
        `(qw.QSlider, qw.QSpinBox)`

        """
        if slider_maximum is None:
            slider_maximum = self.num_slider_divs
        # noinspection PyArgumentList
        slider = qw.QSlider(
            minimum=slider_minimum, maximum=slider_maximum,
            orientation=Qt.Qt.Horizontal
        )

        slider.valueChanged.connect(value_changed_callback)
        if use_ints:
            # noinspection PyArgumentList
            input_box = qw.QSpinBox(minimum=minimum, maximum=maximum)
        else:
            # noinspection PyArgumentList
            input_box = qw.QDoubleSpinBox(
                minimum=minimum, maximum=maximum, decimals=3
            )
        input_box.editingFinished.connect(
            lambda: slider.setValue(
                slider_inv_formula(input_box.value())
            )
        )

        slider.valueChanged.connect(
            lambda value: input_box.setValue(slider_formula(value))
        )

        input_box.setValue(init_val)
        slider.setValue(slider_inv_formula(input_box.value()))

        # noinspection PyArgumentList
        toolbar = qw.QToolBar(orientation=Qt.Qt.Horizontal)
        toolbar.addWidget(qw.QLabel(label))
        toolbar.addWidget(input_box)
        toolbar.addWidget(slider)

        self.addWidget(toolbar)
        return slider, input_box

    def create_orientation_button_callback(
            self,
            theta: float,
            phi: float
    ) -> callable():
        """ Callback to reset `slice` orientation

        Parameters
        ----------
        theta: polar angle
        phi: azimuthal angle
        """
        def return_func():
            self.theta_slider.setValue(
                self.slice_theta_slider_inv_formula(theta)
            )
            self.phi_slider.setValue(
                self.slice_phi_slider_inv_formula(phi)
            )

        return return_func

    def origin_reset_callback(self):
        """ Reset `slice` origin to center of volume """
        self.origin_slider.setValue(0)
        self.widget.SetOrigin(*(dim/2 for dim in self.plotter.dims()))

    # noinspection PyUnresolvedReferences
    def setup_orient_along_axes_buttons(self):
        """ Make buttons to orient `slice` normal along coordinate axes """
        toolbar = qw.QToolBar()
        button_width = 40
        pi = np.pi
        for button_label, button_theta, button_phi in (
                ("+x", pi / 2, 0),
                ("-x", pi / 2, pi),
                ("+y", pi / 2, pi / 2),
                ("-y", pi / 2, 3 * pi / 2),
                ("+z", 0, 0),
                ("-z", pi, 0)
        ):
            orientation_button = qw.QToolButton()
            orientation_button.setText(button_label)
            orientation_button.setMaximumWidth(button_width)
            orientation_button.clicked.connect(
                self.create_orientation_button_callback(
                    button_theta, button_phi
                )
            )
            toolbar.addWidget(orientation_button)

        if self.make_origin_slider:
            origin_reset_button = qw.QToolButton()
            origin_reset_button.setText("center")
            origin_reset_button.clicked.connect(self.origin_reset_callback)
            toolbar.addWidget(origin_reset_button)

        toolbar.setStyleSheet("QToolBar{spacing:0px;}")
        self.addWidget(toolbar)

    def expand(self):
        """ Increase control toolbar height, revealing `slice`-specific controls """
        height_setting = int(
            self.plotter.settings["slice_control_toolbar_height"]
            / self.plotter.settings.get("qt_scale_factor", 1)
        )
        self.setFixedHeight(height_setting)
        self.expanded = True

    def collapse(self):
        """ Increase control toolbar height, hiding `slice`-specific controls """
        self.setFixedHeight(0)
        self.expanded = False

    def expand_or_collapse_control_toolbar(self):
        """ Toggle show/hide `slice`-specific controls """
        if self.expanded:
            self.collapse()
        else:
            self.expand()


class ControlsCheckboxToolBar(utilities_Qt.ControlsToolbar):
    """ Toolbar containing only a checkbox and its label

    Parameters
    ----------
    plotter: :class:`~open_viewmin.filter_tree_plot_Qt.filter_tree_plot_Qt.FilterTreePlotQt`
    name: str | `None`
    label: str | `None`
    icon_size: int | `None`
    movable: bool
    floatable: bool
    items_dict_reference: dict | `None`
    init_state_callback: callable | `None`
    state_changed_callback: callable | `None`
    do_color_buttons: bool
    """
    # noinspection PyUnresolvedReferences
    def __init__(
        self, plotter, name=None, label=None, icon_size=None, movable=True,
        floatable=True, items_dict_reference=None,
        init_state_callback=None, state_changed_callback=None,
        do_color_buttons=False
    ):

        if icon_size is None:
            icon_size = plotter.settings.get("icon_size")
        if name is None:
            name = label
        self.name = name
        super().__init__(
            name=name, label=label, icon_size=icon_size, movable=movable,
            floatable=floatable, has_close_button=False
        )
        self.plotter = plotter
        self.plotter.toolbars[self.name] = self

        # set checkbox toolbar orientation to vertical
        self.options_toolbar.setOrientation(Qt.Qt.Vertical)

        add_toolbar_to_controls_dock_widget(self.plotter, self.name)
        self.items = dict()
        self.checkboxes = dict()
        self.checkbox_toolbars = dict()
        self.items_dict_reference = items_dict_reference
        self.init_state_callback = init_state_callback
        self.state_changed_callback = state_changed_callback
        self.color_button_enabled = do_color_buttons

        if self.items_dict_reference is not None:
            refresh_button = qw.QToolButton()
            refresh_button.setIcon(
                plotter.style().standardIcon(qw.QStyle.SP_BrowserReload)
            )
            refresh_button.setToolTip('Update list of items')
            refresh_button.clicked.connect(self.populate)
            self.label_toolbar.addWidget(refresh_button)
            self.populate()
        self.add_close_button()

    def add_item(
        self, item_name, init_state_callback, state_changed_callback
    ):
        """ Add checkbox to toolbar

        Parameters
        ----------
        item_name: str
        init_state_callback: callable | `None`
        state_changed_callback: callable | `None`
        """
        self.items[item_name] = (init_state_callback, state_changed_callback)
        self.add_checkbox(item_name)
        if self.color_button_enabled:
            self.make_label_a_color_button(item_name)
        self.refresh(item_name)

    def populate(self):
        """
        Recreate toolbar's checkbox
        """
        if self.items_dict_reference is not None:
            self.options_toolbar.clear()
            self.items.clear()
            for item_name in self.items_dict_reference.keys():
                obj = self.items_dict_reference[item_name]
                self.add_item(
                    item_name,
                    self.init_state_callback(obj),
                    self.state_changed_callback(obj)
                )

    def add_checkbox(self, item_name):
        """ Add checkbox to toolbar

        Parameters
        ----------
        item_name: str
        """
        checkboxes_toolbar = self.options_toolbar
        if item_name in self.items.keys():
            init_state_callback, state_changed_callback = self.items[item_name]
            this_checkbox_toolbar = qw.QToolBar()
            self.checkbox_toolbars[item_name] = this_checkbox_toolbar
            this_checkbox_toolbar.setOrientation(Qt.Qt.Orientation.Vertical)
            checkbox = self.checkboxes[item_name] = qw.QCheckBox(item_name)
            self.refresh(item_name=item_name)
            # noinspection PyUnresolvedReferences
            checkbox.stateChanged.connect(
                lambda: state_changed_callback(checkbox.isChecked())
            )
            this_checkbox_toolbar.addWidget(checkbox)
            this_checkbox_toolbar.setIconSize(
                Qt.QSize(*((self.plotter.settings["icon_size"],) * 2))
            )
            this_checkbox_toolbar.setOrientation(Qt.Qt.Orientation.Horizontal)
            checkboxes_toolbar.addWidget(this_checkbox_toolbar)

    # noinspection PyUnresolvedReferences
    def make_label_a_color_button(self, item_name):
        """ Replace label of checkbox with a separate button that activates
        a color picker dialog for the widget

        Parameters
        ----------
        item_name: str
        """
        if item_name in self.checkbox_toolbars.keys():
            if item_name in self.checkboxes.keys():
                self.checkboxes[item_name].setText("")

            max_len_display_name = 24
            if len(item_name) > max_len_display_name:
                display_name = item_name[:max_len_display_name] + " •••"
            else:
                display_name = item_name

            widget_formula = self.plotter.get_widget_formula(item_name)

            def open_widget_color_dialog():
                utilities_Qt.ColorDialogValid(
                    widget_formula.set_color,
                    widget_formula.color,
                    self.plotter.app_window
                )

            action = self.checkbox_toolbars[item_name].addAction(
                display_name,
                open_widget_color_dialog
            )
            action.setToolTip(f"Set color for {item_name}")

    def refresh(self, item_name=None):
        if item_name is None:
            self.refresh_all()
        else:
            init_state_callback, _ = self.items[item_name]
            self.checkboxes[item_name].setChecked(init_state_callback())

    def refresh_all(self):
        for item_name in self.items.keys():
            self.refresh(item_name=item_name)


def setup_widget_control(plotter):
    """
    Add toolbar to controls dock containing checkboxes for enabling/disabling
    PyVista widgets.

    Parameters
    ----------
    plotter: :class:`~open_viewmin.filter_tree_plot_Qt.filter_tree_plot_Qt.FilterTreePlotQt`

    """
    plotter.toolbars["widgets"] = ControlsCheckboxToolBar(
        plotter, label="widgets",
        items_dict_reference=plotter.widgets,
        init_state_callback=lambda widget: widget.GetEnabled,
        state_changed_callback=lambda widget: widget.SetEnabled,
        do_color_buttons=True
    )


def add_toolbar_to_controls_dock_widget(
    plotter, toolbar_name, orientation_int=2  # vertical by default
):
    """

    Parameters
    ----------
    plotter: :class:`~open_viewmin.filter_tree_plot_Qt.filter_tree_plot_Qt.FilterTreePlotQt`
    toolbar_name: str
    orientation_int: int
        Horizontal (`1`) or vertical (`2`, default) toolbar orientation
    """
    toolbar = plotter.toolbars[toolbar_name]
    toolbar.setOrientation(
        Qt.Qt.Orientation.Vertical
        if orientation_int == 2
        else Qt.Qt.Orientation.Horizontal
    )
    plotter.controls_dock_window.addToolBar(
        Qt.Qt.ToolBarArea.LeftToolBarArea, toolbar
    )


class ActorControlToolbar(utilities_Qt.ControlsToolbar):
    """ Toolbar with standard controls for an actor """
    def __init__(self, plotter, actor_name, label=None):
        """

        Parameters
        ----------
        plotter: :class:`~open_viewmin.filter_tree_plot_Qt.filter_tree_plot_Qt.FilterTreePlotQt`
        actor_name: str
        label: str | `None`
        """
        self.plotter = plotter
        self.actor_name = actor_name
        self.visibility_checkbox = qw.QCheckBox(
            self.plotter.settings["visible_symbol"]
        )

        super().__init__(
            name=self.actor_name,
            label=self.format_label_for_filters_tree(label),
            icon_size=self.plotter.settings['icon_size']
        )
        self.add_visibility_checkbox()
        self.visualization_options_button = (
            visualization_options.add_visualization_options_menu(self)
        )
        self.mesh_options_button = mesh_options.add_mesh_options_menu(self)
        self.QSlider = None
        self.slider_toolbar = None
        self.QSlider_target_type = ""
        self.slice_controls = None

    def set_label(self, text=None):
        """ Set toolbar's label

        Parameters
        ----------
        text: str | `None`

        """
        super().set_label(text=self.format_label_for_filters_tree(text))

    def format_label_for_filters_tree(self, label):
        """ By default, label toolbar with name of associated actor

        Parameters
        ----------
        label: str | `None`

        Returns
        -------
        str
        """
        if label is None:
            label = self.actor_name
        return label

    def add_to_plotter(self):
        """
        Add this toolbar to its parent `Plotter`
        """
        self.plotter.controls_dock_window.addToolBar(
            Qt.Qt.ToolBarArea.LeftToolBarArea, self
        )

    def add_visibility_checkbox(self):
        """ Add a checkbox that toggles an actor's visibility """
        self.assign_checkbox_to_actor_visibility()
        self.options_toolbar.addWidget(self.visibility_checkbox)

    # noinspection PyUnresolvedReferences
    def relink_visibility_checkbox(self):
        # noinspection GrazieInspection
        """ Make sure visibility checkbox state matches actor visibility,
                otherwise checkbox becomes unusable """
        actor = self.plotter.get_actor(self.actor_name)
        checkbox = self.visibility_checkbox
        try:
            checkbox.toggled.disconnect()
        except TypeError:  # raised if callback not set yet
            pass
        if actor is not None:
            checkbox.setChecked(actor.GetVisibility())

        def toggle_callback():
            self.plotter.set_actor_visibility(
                self.actor_name, checkbox.isChecked()
            )
        checkbox.toggled.connect(toggle_callback)

    def assign_checkbox_to_actor_visibility(self, actor_name=None):
        """ Couple toolbar's visibility checkbox to the visibility of the
        associated actor

        Parameters
        ----------
        actor_name: str | `None`
        """
        plotter = self.plotter
        checkbox = self.visibility_checkbox
        if actor_name is not None:
            self.actor_name = actor_name

        if self.actor_name in plotter.renderer.actors.keys():
            self.relink_visibility_checkbox()
            checkbox.setToolTip(
                'toggle visibility of \"' + self.actor_name + '\"'
            )

    def add_qslider(
        self, update_method, num_divs=100,
        init_val_percent=50, min_val=None, max_val=None, scalars=None,
        label_txt=""
    ):
        """ Add a slider control to the controls area

        Parameters
        ----------
        update_method: callable
        num_divs: int
        init_val_percent: int
        min_val: float | `None`
        max_val: float | `None`
        scalars: pv.pyvista_ndarray
        label_txt: str

        Returns
        -------
        :class:`~open_viewmin.filter_tree_plot_Qt.widgets_Qt.utilities_Qt.ControlsSliderToolbar`
        """
        actor = self.plotter.get_actor(self.actor_name)
        if label_txt is None:
            actor_info = self.plotter.get_filter_formula(self.actor_name)
            label_txt = actor_info.filter_kwargs['scalars']
            label_txt = label_txt[:3]
        cst = utilities_Qt.ControlsSliderToolbar(
            update_method,
            actor,
            num_divs=num_divs,
            init_val_percent=init_val_percent,
            min_val=min_val,
            max_val=max_val,
            scalars=scalars,
            label_txt=label_txt
        )
        self.addWidget(cst)
        self.slider_toolbar = cst
        self.QSlider = cst.slider

        return cst

    def add_qslider_type(self, type_string, **kwargs):
        """

        Parameters
        ----------
        type_string: str
            Must be `"isosurface"` or `"threshold"`
        **kwargs: dict, optional
            Keyword arguments to :meth:`add_isosurface_qslider` or :meth:`add_threshold_qslider`

        Returns
        -------
        :class:`~open_viewmin.filter_tree_plot_Qt.widgets_Qt.utilities_Qt.ControlsSliderToolbar`

        """
        if type_string == "isosurface":
            return self.add_isosurface_qslider(**kwargs)
        elif type_string == "threshold":
            return self.add_threshold_qslider(**kwargs)
        else:
            raise ValueError(
                f"Unrecognized QSlider target type string {type_string}"
            )

    def get_min_max_scalars(self, min_val=None, max_val=None):
        """ Get minimum and maximum values of a scalar dataset

        Parameters
        ----------
        min_val: float | `None`
        max_val: float | `None`

        Returns
        -------
        dict

        """
        plotter = self.plotter
        actor_name = self.actor_name
        actor_info = plotter.get_filter_formula(actor_name)
        dataset_name = actor_info.filter_kwargs['scalars']
        parent_mesh = plotter.get_mesh(actor_info.parent_mesh_name)
        dataset = parent_mesh[dataset_name]
        if min_val is None:
            min_val = np.min(dataset)
        if max_val is None:
            max_val = np.max(dataset)
        return dict(
            min_val=min_val,
            max_val=max_val,
            scalars=dataset_name
        )

    def isosurface_slider_callback(self, contour_value):
        """ Callback to update an `isosurface` filter

        Parameters
        ----------
        contour_value: float
        """
        self.plotter.update_filter(
            self.actor_name,
            update_actor=True,
            isosurfaces=[contour_value]
        )

    def add_isosurface_qslider(
        self, min_val=None, max_val=None, **kwargs
    ):
        """ Add slider control for value of `isosurface` filter

        Parameters
        ----------
        min_val: float | `None`
        max_val: float | `None`
        **kwargs: dict, optional
            Keyword arguments to :meth:`get_min_max_scalars`

        Returns
        -------
        :class:`~open_viewmin.filter_tree_plot_Qt.widgets_Qt.utilities_Qt.ControlsSliderToolbar`
        """
        self.QSlider_target_type = "isosurface"
        return self.add_qslider(
            self.isosurface_slider_callback,
            **self.get_min_max_scalars(
                min_val=min_val, max_val=max_val
            ),
            **kwargs
        )

    def threshold_slider_callback(self, value_lims):
        """ Callback to update a `threshold` filter

        Parameters
        ----------
        value_lims: tuple(float, float)
        """
        self.plotter.update_filter(
            self.actor_name,
            update_actor=True,
            value=value_lims
        )

    def add_threshold_qslider(
        self, min_val=None, max_val=None, **kwargs
    ):
        """ Add two-value slider control for min/max values of `threshold` filter

        Parameters
        ----------
        min_val: float | `None`
        max_val: float | `None`
        **kwargs: dict, optional
            Keyword arguments to :meth:`add_qslider`

        Returns
        -------
        :class:`~open_viewmin.filter_tree_plot_Qt.widgets_Qt.utilities_Qt.ControlsSliderToolbar`
        """
        self.QSlider_target_type = "threshold"
        return self.add_qslider(
            self.threshold_slider_callback,
            **self.get_min_max_scalars(
                min_val=min_val, max_val=max_val
            ),
            **kwargs
        )

    def wiggle_slider_to_update(self):
        """ For an actor with some feature controlled by a slider,
        move the slider right and left to force the actor to update
        """
        slider = self.QSlider
        if slider is not None:
            val = slider.value()
            try:
                for i in [1, -1]:
                    slider.setValue(val + i)
                    slider.setValue(val)
            except ValueError:
                for i in [-1, 1]:
                    slider.setValue(val + i)
                    slider.setValue(val)

    def make_special_settings_button(self, callback):
        """ Make button that launches a settings dialog

        Parameters
        ----------
        callback: callable
        """
        special_settings_button = qw.QPushButton("⚛︎")
        special_settings_button.setToolTip(
            f"Launch dialog for settings specific to mesh \"{self.actor_name}\""
        )
        # noinspection PyUnresolvedReferences
        special_settings_button.clicked.connect(
            lambda: callback(self.plotter, self.actor_name)
        )
        self.options_toolbar.addWidget(special_settings_button)

    def make_slice_slider_controls(
        self, widget_name=None, make_origin_slider=True
    ):
        """ Make sliders that control a `slice` widget's orientation and position

        Parameters
        ----------
        widget_name: str | `None`
        make_origin_slider: bool
        """
        self.slice_controls = SliceControlsToolBar(
            self,
            widget_name=widget_name,
            make_origin_slider=make_origin_slider
        )

    # noinspection PyUnresolvedReferences
    def add_threshold_text_controls(self, label=""):
        """ Add text box controls for min/max values of a `threshold` filter

        Parameters
        ----------
        label: str

        """
        plotter = self.plotter

        def threshold_set_val(callback_val_idx, callback_spinbox):
            def return_func():
                callback_filter_formula = plotter.get_filter_formula(
                    self.actor_name
                )
                callback_filter_kwargs = callback_filter_formula.filter_kwargs
                current_min, current_max = callback_filter_kwargs["value"]
                new_value = callback_spinbox.value()
                if (
                    (callback_val_idx == 1 and new_value > current_min)
                    or (callback_val_idx == 0 and new_value < current_max)
                ):
                    callback_filter_kwargs["value"][callback_val_idx] = (
                        callback_spinbox.value()
                    )
                    callback_filter_formula.update(update_actor=True)
                else:
                    # if bad value entered in spinbox, reset to previous value
                    callback_spinbox.setValue(
                        callback_filter_kwargs["value"][callback_val_idx]
                    )

            return return_func

        spinboxes_toolbar = qw.QToolBar()
        spinboxes_toolbar.addWidget(qw.QLabel(label + "  "))
        for val_idx in range(2):
            val_lbl = ["min", "max"][val_idx]
            spinboxes_toolbar.addWidget(qw.QLabel(val_lbl))
            spinbox = qw.QDoubleSpinBox()

            filter_formula = plotter.get_filter_formula(self.actor_name)
            filter_kwargs = filter_formula.filter_kwargs
            if "value" not in filter_kwargs.keys():
                filter_kwargs["value"] = [0, 1]
            spinbox.setValue(filter_kwargs["value"][val_idx])
            spinbox.editingFinished.connect(threshold_set_val(val_idx, spinbox))
            spinboxes_toolbar.addWidget(spinbox)
        self.addWidget(spinboxes_toolbar)

    def add_property_toggle_checkbox(self, property_name, label=None):
        """ Add checkbox that toggles a named property with boolean value

        Parameters
        ----------
        property_name: str
        label: str | `None`

        Returns
        -------
        `qw.QCheckBox`

        """
        if label is None:
            label = property_name
        checkbox = qw.QCheckBox(label)
        filter_formula = self.plotter.get_filter_formula(self.actor_name)
        filter_kwargs = filter_formula.filter_kwargs
        checkbox.setChecked(filter_kwargs.get(property_name, False))
        # noinspection PyUnresolvedReferences
        checkbox.stateChanged.connect(
            lambda: filter_formula.update(
                invert=checkbox.isChecked(),
                update_actor=True
            )
        )
        self.options_toolbar.addWidget(checkbox)
        return checkbox


def add_viz_options_toolbar(plotter, actor_name, label=None, mesh_type=None):
    # noinspection GrazieInspection
    """ Standard controls toolbar for each actor's rendering options

    Parameters
    ----------
    plotter: :class:`~open_viewmin.filter_tree_plot_Qt.filter_tree_plot_Qt.FilterTreePlotQt`
    actor_name: str
    label: str | `None`
    mesh_type: str | `None`
        Acceptable strings are: `"isosurface"`, `"threshold"`, `"slice"`,  `"line"`
    """
    toolbar = ActorControlToolbar(plotter, actor_name)
    filter_formula = plotter.get_filter_formula(actor_name)
    filter_kwargs = filter_formula.filter_kwargs
    filter_kwarg_keys = filter_kwargs.keys()
    if mesh_type in ("isosurface", "threshold"):
        toolbar.add_qslider_type(mesh_type, label=label)
    elif mesh_type == "slice":
        toolbar.make_slice_slider_controls()
    elif all([key in filter_kwarg_keys for key in ["pointa", "pointb"]]):
        toolbar.make_special_settings_button(
            visualization_options.line_settings_callback
        )
    elif "orient" in filter_kwarg_keys:
        toolbar.make_special_settings_button(
            visualization_options.glyph_settings_choice_callback
        )
    elif "surface_streamlines" in filter_kwarg_keys:
        toolbar.make_special_settings_button(
            visualization_options.streamlines_settings_callback
        )
    elif all([
        key in filter_kwarg_keys
        for key in ["radius", "n_samples", "use_ints"]
    ]):
        toolbar.make_special_settings_button(
            visualization_options.circuit_options_callback
        )
    elif "num_centroids" in filter_kwarg_keys:
        toolbar.make_special_settings_button(
            visualization_options.num_centroids_callback
        )
    elif filter_formula.ovm_info.get("tensor_ellipsoids"):
        toolbar.make_special_settings_button(
            visualization_options.tensor_ellipsoids_settings_callback
        )
    elif mesh_type is not None:
        raise ValueError(
            "Invalid value for keyword argument \'slider_for\'."
        )
    if (
        "invert" in filter_kwarg_keys
        or filter_formula.filter_callable == "threshold"
    ):
        toolbar.add_property_toggle_checkbox("invert")
    plotter.toolbars[actor_name] = toolbar
    toolbar.add_to_plotter()


def set_lights_intensity_from_slider(plotter):
    """ Set `Plotter`'s lights intensity from the `'lighting'` toolbar's slider

    Parameters
    ----------
    plotter: :class:`~open_viewmin.filter_tree_plot_Qt.filter_tree_plot_Qt.FilterTreePlotQt`
    """
    plotter.set_lights_intensity(
        plotter.toolbars['lighting'].slider.value()
        * plotter.settings['lighting_rescale_factor']
    )


# noinspection PyUnresolvedReferences
def setup_animation_buttons(plotter):
    """ Buttons for animations toolbar to change the frame number

    Parameters
    ----------
    plotter: :class:`~open_viewmin.filter_tree_plot_Qt.filter_tree_plot_Qt.FilterTreePlotQt`
    """
    btn_width = 20
    # noinspection PyArgumentList
    plotter.toolbars["animation"] = qw.QToolBar(
        "Animation",
        orientation=Qt.Qt.Vertical
    )
    tb = plotter.toolbars["animation"]
    tb.setStyleSheet("QToolBar{spacing:0px;}")
    # noinspection PyArgumentList
    toolbar_row = qw.QToolBar(orientation=Qt.Qt.Horizontal)
    utilities_Qt.add_close_button_to_toolbar(toolbar_row, closes=tb)
    toolbar_row.setStyleSheet("QToolBar{spacing:0px;}")

    btn = qw.QToolButton()
    btn.setAutoRaise(True)
    btn.setIcon(plotter.style().standardIcon(qw.QStyle.SP_MediaPlay))
    btn.released.connect(plotter.play)
    btn.released.connect(plotter.update_frame_spinbox)
    toolbar_row.addWidget(btn)
    # noinspection PyArgumentList
    sb = qw.QSpinBox(minimum=0)
    sb.setValue(plotter.frame_num)
    # noinspection PyTypeChecker
    # sb.setButtonSymbols(2)  # remove up/down arrows

    def frame_spinbox_callback():
        new_frame_num = sb.value()
        if new_frame_num != plotter.frame_num and (
            0 <= new_frame_num < plotter.num_frames()
        ):
            plotter.view_frame(frame_num=new_frame_num)

    sb.editingFinished.connect(frame_spinbox_callback)
    sb.setPrefix(' ')  # to make sure number is fully visible
    plotter.frame_spinbox = sb
    toolbar_row.addWidget(sb)

    plotter.anim_toolbar_nframes = qw.QLabel('')
    # filled in update_frame_spinbox()

    toolbar_row.addWidget(plotter.anim_toolbar_nframes)
    for icon_name, action in zip(
        [
            'SP_MediaSkipBackward', 'SP_MediaSeekBackward',
            'SP_MediaSeekForward', 'SP_MediaSkipForward'
        ],
        [
            plotter.first_frame, plotter.previous_frame,
            plotter.next_frame, plotter.last_frame
        ]
    ):
        btn = qw.QToolButton()
        btn.setAutoRaise(True)
        btn.setIcon(plotter.style().standardIcon(getattr(qw.QStyle, icon_name)))
        btn.released.connect(action)
        btn.released.connect(plotter.update_frame_spinbox)
        toolbar_row.addWidget(btn)
    toolbar_row.setIconSize(Qt.QSize(btn_width, btn_width))
    tb.addWidget(toolbar_row)
    plotter.controls_dock_window.addToolBar(Qt.Qt.LeftToolBarArea, tb)


def relink_visibility_checkbox(plotter, actor_name):
    # noinspection GrazieInspection
    """ Make sure visibility checkbox state matches actor visibility,
        otherwise checkbox becomes unusable

    Parameters
    ----------
    plotter: :class:`~open_viewmin.filter_tree_plot_Qt.filter_tree_plot_Qt.FilterTreePlotQt`
    actor_name: str
    """

    if actor_name not in plotter.actor_control_toolbars.keys():
        add_viz_options_toolbar(plotter, actor_name)
    toolbar = plotter.actor_control_toolbars[actor_name]
    toolbar.relink_visibility_checkbox()
