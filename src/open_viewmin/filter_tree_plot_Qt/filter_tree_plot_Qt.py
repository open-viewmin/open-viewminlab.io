"""
Extend `pyvistaqt.BackgroundPlotter` functionality to include auto-updating
filter trees and more complete UI.
"""

from qtpy import QtWidgets as qw
from qtpy import QtCore as Qt
from pyvistaqt import BackgroundPlotter
from ..filter_tree_plot.utilities import ovm_utilities, scalar_bars
from ..filter_tree_plot.filter_tree_plot import FilterTreePlot
from ..filter_tree_plot.utilities.ovm_utilities import sort_filenames_by_timestamp
from ..filter_tree_plot.widgets import plane_widgets
from . import settings_Qt
from .widgets_Qt import controls, filters_tree_widgets, utilities_Qt
from .menus import main_menus, menu_utilities, record, mesh_options, visualization_options


class FilterTreePlotQt(FilterTreePlot, BackgroundPlotter):
    """
    Implement some PyVista functionality, with filter trees, in the PyVistaQt
    GUI

    Parameters
    ----------
    filenames : list[str], str, or None, optional
        Files to import.
    user_settings : dict or None, optional
        Customizations to default plotter settings.
    theme : ['dark' | 'document' | 'paraview' | None], optional
        PyVista plot theme
    reader : callable()
        Function to import and process data files
    kwargs : dict, optional
        Keyword arguments to `open_viewmin.FilterTreePlot`

    """

    # noinspection PyArgumentList
    def __init__(
        self, filenames=None, user_settings=None, theme=None,
        reader=ovm_utilities.fast_import, **kwargs
    ):
        settings_Qt_dict = settings_Qt.settings_Qt_dict
        window_size = settings_Qt_dict.get("window_size", None)
        if window_size is not None:
            qt_scale_factor = settings_Qt_dict.get("qt_scale_factor", 1)
            window_size = tuple(
                [int(dim / qt_scale_factor) for dim in window_size]
            )

        self.finished_setup = False
        self.dock_widgets = dict()
        self.toolbars = dict()

        FilterTreePlot.__init__(
            self,
            filenames=filenames,
            user_settings=user_settings,
            theme=theme,
            reader=reader,
            window_size=window_size,
            **kwargs
        )

        ovm_utilities.copy_from_dict_recursively(
            settings_Qt.settings_Qt_dict, self.settings
        )

        # noinspection PyTypeChecker
        self.the_File_menu = self._get_menu(self.main_menu, 'File')

        if not self.off_screen:
            self._assign_keypress_events()

        self.app_window.setWindowTitle(self.title)
        self.frame_num = 0

        # noinspection PyTypeChecker
        self.the_View_menu = self._get_menu(self.main_menu, 'View')
        # noinspection PyTypeChecker
        self.the_Tools_menu = self._get_menu(self.main_menu, 'Tools')
        self.the_Add_menu = self.main_menu.addMenu("Add")
        self.theme_menu = self.main_menu.addAction(
            'Theme', lambda: main_menus.theme.open_edit_theme_widget(self)
        )
        self.calculate_menu = self.main_menu.addMenu("Calculate")
        self.the_refresh_toolbar = qw.QToolBar(
            orientation=Qt.Qt.Orientation.Horizontal
        )
        # noinspection PyUnresolvedReferences
        self.non_ovm_actors_toggle_menu = self.the_View_menu.addMenu("Toggle")
        # noinspection PyUnresolvedReferences
        self.the_View_menu.addAction("Hide all actors", self.hide_all_actors)
        main_menus.setup_main_menus(self)

        self.controls_dock_widget = qw.QDockWidget()
        self.controls_dock_window = qw.QMainWindow()
        self.controls_dock_scroll_area = qw.QScrollArea()
        self.app_window.addDockWidget(
            # Qt.Qt.DockWidgetArea.LeftDockWidgetArea,
            Qt.Qt.DockWidgetArea.RightDockWidgetArea,
            self.controls_dock_widget,
            Qt.Qt.Orientation.Vertical
        )

        controls.setup_animation_buttons(self)
        controls.setup_lighting_control(self)
        controls.setup_widget_control(self)

        self.filters_tree_widget_items = dict()
        filters_tree_widgets.create_filters_tree(self)
        self.filters_tree_widget.expandAll()

        self.controls_dock_scroll_area.setWidget(self.controls_dock_window)
        self.controls_dock_widget.setWidget(self.controls_dock_scroll_area)
        record.setup_record_toolbar(self)

        if len(self.filenames) > 0:
            self._init_procedures_after_data_import()
        if self.num_frames() < 2:
            self.toolbars["animation"].hide()

        self.toolbars["widgets"].populate()
        self.customize_controls_dock()

    @staticmethod
    def _get_menu(parent, title):
        """ Get a menu with a given title

        Parameters
        ----------
        parent : PyQt5.QtWidgets.QMenuBar
            Menu bar to search
        title : str
            Title of menu

        Returns
        -------
        PyQt5.QtWidgets.QMenu
            Menu

        """

        # noinspection PyUnresolvedReferences
        for item in parent.children():
            if isinstance(item, qw.QMenu):
                if item.title() == title:
                    return item
        raise KeyError(f"Cannot find menu named {title} in {parent}")

    def initialize_plotter(self, **kwargs):
        """ Override parent class's plotter initialization to use
        pyvistaqt.BackgroundPlotter instead of pyvista.Plotter

        Parameters
        ----------
        kwargs : dict, optional
            Keyword arguments to `pyvistaqt.BackgroundPlotter`

        """

        BackgroundPlotter.__init__(
            self,
            multi_samples=12,
            line_smoothing=True,
            point_smoothing=True,
            polygon_smoothing=True,
            lighting='light kit',  # | 'three lights',
            allow_quit_keypress=False,
            editor=False,
            **kwargs
        )

    def _assign_keypress_events(self):
        """ Assign actions to keyboard keys. """
        self.add_key_event('c', self.toggle_last_colorbar)
        self.add_key_event('o', self.toggle_orbit_visibility)
        self.add_key_event('O', self.do_orbit)
        self.add_key_event('F', self.toggle_floor)
        self.add_key_event('H', self.hide_all_actors)

    def update_filters_tree(self):
        filters_tree_widgets.update_filters_tree(self)

    def refresh(self):
        FilterTreePlot.refresh(self)
        self.update_filters_tree()
        toolbar = self.toolbars.get("widgets")
        if toolbar is not None:
            toolbar.populate()

    def set_actor_visibility(self, actor, visibility):
        if isinstance(actor, (tuple, list)):
            for _actor in actor:
                self.set_actor_visibility(_actor, visibility)
        else:
            FilterTreePlot.set_actor_visibility(self, actor, visibility)
            if isinstance(actor, str):
                actor_name = actor
            else:
                actor_name = self.get_actor_name(actor)
            if actor_name in self.toolbars.keys():
                self.toolbars[actor_name].relink_visibility_checkbox()

    def customize_controls_dock(self):
        """ Modify settings of controls area """

        self.controls_dock_widget.setFeatures(
            self.controls_dock_widget.DockWidgetFeature.DockWidgetFloatable
            | self.controls_dock_widget.DockWidgetFeature.DockWidgetMovable
        )
        self.controls_dock_scroll_area.setSizeAdjustPolicy(
            qw.QScrollArea.SizeAdjustPolicy.AdjustToContents
        )
        qsp = qw.QSizePolicy
        self.controls_dock_widget.setSizePolicy(
            qsp.Policy.MinimumExpanding,
            qsp.Policy.Expanding
        )
        self.controls_dock_scroll_area.setSizePolicy(
            qsp.Policy.Expanding,
            qsp.Policy.Expanding
        )
        self.controls_dock_window.setSizePolicy(
            qsp.Policy.Maximum,
            qsp.Policy.Fixed
        )

        qt_scale_factor = self.settings.get("qt_scale_factor", 1)
        self.controls_dock_window.setMinimumHeight(
            int(self.settings["controls_area_height"] / qt_scale_factor)
        )
        self.controls_dock_widget.setMinimumWidth(
            int(self.settings["controls_area_width"] / qt_scale_factor)
        )
        self.controls_dock_scroll_area.setWidgetResizable(True)

        # make vertical scroll bar invisible
        # noinspection PyTypeChecker
        self.controls_dock_scroll_area.setVerticalScrollBarPolicy(
            Qt.Qt.ScrollBarPolicy(1)
        )

    def show_editor(self):
        """ Show editor widget controlling actors' visibility """

        action = menu_utilities.get_menu_action(self.main_menu, 'Editor')
        action.activate(action.Trigger)

    # initialization routines

    # noinspection PyUnresolvedReferences
    def open_files_dialog(self, sort=True):
        """ Launch GUI widget for selecting files to import

        Parameters
        ----------
        sort : bool, optional
            Whether to sort filenames by timestamp

        """

        dlg = qw.QFileDialog(self)
        dlg.setFileMode(qw.QFileDialog.ExistingFiles)
        sort_func = sort_filenames_by_timestamp if sort else lambda x: x

        def accepted_callback():
            selected_files = dlg.selectedFiles()
            dlg.close()
            dlg.deleteLater()
            self.load(sort_func(selected_files))
        dlg.show()
        dlg.accepted.connect(accepted_callback)
        dlg.rejected.connect(dlg.deleteLater)

    @property
    def actor_control_toolbars(self):
        """ Get toolbars controlling individual actors

        Returns
        -------
        dict
            Dictionary of toolbars

        """

        ret = dict()
        for toolbar_name in self.toolbars.keys():
            toolbar = self.toolbars[toolbar_name]
            if isinstance(toolbar, controls.ActorControlToolbar):
                ret[toolbar_name] = toolbar
        return ret

    def _init_procedures_after_data_import(self):
        """ Initialization steps that can be carried out only after some data
        is imported """

        if not self.finished_setup:
            # bounding box
            self.add_mesh(
                self.fullmesh.outline(),
                color=self.settings['bounding_box_color'],
                name="outline"
            )
            self.finished_setup = True

    def add_filter_formula(self, name=None, **kwargs):
        FilterTreePlot.add_filter_formula(self, name=name, **kwargs)
        filter_formula = self.get_filter_formula(name)
        if filter_formula is not None:
            if filter_formula.has_actor:
                if name not in self.actor_control_toolbars.keys():
                    controls.add_viz_options_toolbar(self, name)
        filters_tree_widgets.update_filters_tree(self)

    def remove_mesh_completely(self, mesh_name, remove_children=True):
        FilterTreePlot.remove_mesh_completely(
            self, mesh_name, remove_children=remove_children
        )
        filters_tree_widgets.update_filters_tree(self)

    def rename_mesh(self, from_name, to_name, do_refresh=False):
        if from_name in self.actor_control_toolbars.keys():
            self.toolbars[to_name] = self.toolbars[from_name]
            del self.toolbars[from_name]
        FilterTreePlot.rename_mesh(self, from_name, to_name, do_refresh=False)
        if to_name in self.actor_control_toolbars.keys():
            toolbar = self.toolbars[to_name]
            toolbar.assign_checkbox_to_actor_visibility(actor_name=to_name)
            toolbar.set_label(to_name)
            visualization_options.populate_viz_options_menu(self, to_name)
            mesh_options.populate_mesh_options_menu(self, to_name)
        self.refresh()

    # frame handling / animation

    def load(self, filenames, **kwargs):
        old_num_frames = self.num_frames()
        super().load(filenames, **kwargs)
        new_num_frames = self.num_frames()

        # if we have changed from < 2 frames to >= 2 frames, make the animation
        # toolbar visible

        if all([
            new_num_frames >= 2,
            old_num_frames < 2,
            "animation" in self.toolbars.keys()
        ]):
            self.toolbars["animation"].show()

        if hasattr(self, "frame_spinbox"):
            self.update_frame_spinbox()

    def view_frame(self, frame_num=0, **kwargs):
        """ Use one of the imported data files as the source for all datasets """

        FilterTreePlot.view_frame(self, frame_num=frame_num, **kwargs)
        if hasattr(self, "frame_spinbox"):
            self.frame_spinbox.setValue(self.frame_num)
            self.update_frame_spinbox()
        for toolbar in self.toolbars.values():
            if hasattr(toolbar, "refresh"):
                toolbar.refresh()

    def update_frame_spinbox(self):
        """ Update the spinbox controlling the frame """
        # The seemingly circuitous route here to updating the frame-spinbox
        # with the current frame number is a hack to make sure the displayed
        # number updates cleanly.

        spinbox = self.frame_spinbox
        spinbox.setValue(self.frame_num)
        spinbox.setFocus()  # necessary to update displayed number
        spinbox.setPrefix('  ')  # hack to clear previous displayed number
        spinbox.clearFocus()  # release focus from spinbox so the above takes effect
        spinbox.setFocus()  # return focus to spinbox so that...
        spinbox.setPrefix(' ')  # ... we remove the extra spaces
        spinbox.clearFocus()  # release focus from spinbox so the above takes effect
        self.anim_toolbar_nframes.setText('/' + str(self.num_frames()))

    # lighting

    def enable_pyvista_lightkit(self, only_active=False):
        BackgroundPlotter.enable_lightkit(self, only_active=only_active)
        try:
            controls.set_lights_intensity_from_slider(self)
        except AttributeError:
            pass

    def enable_pyvista_3_lights(self, only_active=False):
        BackgroundPlotter.enable_3_lights(self, only_active=only_active)
        try:
            controls.set_lights_intensity_from_slider(self)
        except AttributeError:
            pass

    def update_filter(self, mesh_name, update_actor=False, **kwargs):
        FilterTreePlot.update_filter(
            self, mesh_name, update_actor=update_actor, **kwargs
        )
        if update_actor and mesh_name in self.toolbars.keys():
            self.toolbars[mesh_name].relink_visibility_checkbox()
        self.update_filters_tree()
        return self.get_mesh(mesh_name)

    def update_actor(
        self,
        actor_name,
        **kwargs
    ):
        FilterTreePlot.update_actor(
            self,
            actor_name,
            **kwargs
        )
        if actor_name in self.toolbars.keys():
            self.toolbars[actor_name].relink_visibility_checkbox()
        return self.get_actor(actor_name)

    # noinspection PyUnresolvedReferences,PyArgumentList
    def save_html(self, filename=None):
        """ Export plotter scene as interactive html widget

        Parameters
        ----------
        filename : str or `None`
            Filename to write to. Pass `None` to launch GUI file selector.

        """

        if filename is None:
            save_HTML_widget = utilities_Qt.FormDialog(title='Save HTML')
            HTML_filename_lineedit = qw.QLineEdit()
            pushbutton_HTML_filename = qw.QToolButton()
            pushbutton_HTML_filename.setIcon(
                self.style().standardIcon(
                    qw.QStyle.SP_DirIcon
                )
            )

            # noinspection PyArgumentList
            def html_filename_dialog():
                dlg = qw.QFileDialog(
                    parent=self.app_window,
                    filter='HTML files (*.html)'
                )
                HTML_filename = dlg.getSaveFileName()[0]
                if '.html' in HTML_filename:
                    HTML_filename = HTML_filename.split('.html')[0]
                HTML_filename_lineedit.setText(HTML_filename)

            pushbutton_HTML_filename.released.connect(html_filename_dialog)
            sublayout = qw.QHBoxLayout()
            sublayout.addWidget(pushbutton_HTML_filename)
            sublayout.addWidget(HTML_filename_lineedit)
            sublayout.addWidget(qw.QLabel('.html'))
            save_HTML_widget.add_row('Save visualization to:', sublayout)

            def ok_btn_callback():
                save_HTML_widget.close()
                self.export_html(
                    HTML_filename_lineedit.text() + '.html',
                )

            save_HTML_widget.set_accepted_callback(ok_btn_callback)
            save_HTML_widget.show()
        else:
            self.export_html(filename)

    # noinspection PyUnresolvedReferences,PyArgumentList
    def save_mesh(self, mesh_name, mesh_filename=None):
        """ Export a mesh.

        Note that the allowed file types depend on the type of mesh.

        Parameters
        ----------
        mesh_name : str
            Mesh name
        mesh_filename : str or `None`
            File to write to.
            Pass `None` to launch GUI file selector.

        """

        mesh = self.get_mesh(mesh_name)

        def save_callback(use_this_filename):
            try:
                mesh.save(use_this_filename)
            except ValueError as err:
                print(err)
            else:
                print(f"Mesh \'{mesh_name}\' saved to {use_this_filename}.")

        if mesh_filename is None:
            save_mesh_widget = utilities_Qt.FormDialog(
                title=f"Save mesh \"{mesh_name}\""
            )
            mesh_filename_lineedit = qw.QLineEdit()
            mesh_filename_lineedit.setText(mesh_name + ".vtk")

            pushbutton_mesh_filename = qw.QToolButton()
            pushbutton_mesh_filename.setIcon(
                self.style().standardIcon(
                    qw.QStyle.SP_DirIcon
                )
            )

            # noinspection PyArgumentList
            def mesh_filename_dialog():
                dlg = qw.QFileDialog(
                    parent=self.app_window,
                    filter='mesh files (*.ply *.stl *.vtk)'
                )
                dlg_mesh_filename = dlg.getSaveFileName(
                    directory=mesh_filename_lineedit.text()
                )[0]
                mesh_filename_lineedit.setText(dlg_mesh_filename)

            pushbutton_mesh_filename.released.connect(mesh_filename_dialog)

            sublayout = qw.QHBoxLayout()
            sublayout.addWidget(pushbutton_mesh_filename)
            sublayout.addWidget(mesh_filename_lineedit)
            save_mesh_widget.add_row('Save as:', sublayout)

            def ok_btn_callback():
                save_mesh_widget.close()
                lineedit_mesh_filename = mesh_filename_lineedit.text()
                save_callback(lineedit_mesh_filename)

            save_mesh_widget.set_accepted_callback(ok_btn_callback)
            save_mesh_widget.show()
        else:
            save_callback(mesh_filename)

    # noinspection PyUnresolvedReferences,PyArgumentList
    def save_meshes_launch_dialog(self, file_prefix=None):
        if file_prefix is None:
            file_prefix_widget = utilities_Qt.FormDialog(
                title="Save all meshes"
            )
            file_prefix_lineedit = qw.QLineEdit()
            file_prefix_default = ".".join(self.filenames[0].split(".")[:-1])
            file_prefix_lineedit.setText(file_prefix_default)
            file_prefix_pushbutton = qw.QToolButton()
            file_prefix_pushbutton.setIcon(
                self.style().standardIcon(qw.QStyle.SP_DirIcon)
            )

            def file_prefix_dialog():
                dlg = qw.QFileDialog(
                    parent=self.app_window
                )
                dlg_file_prefix = dlg.getSaveFileName(
                    directory=file_prefix_lineedit.text()
                )[0]
                file_prefix_lineedit.setText(dlg_file_prefix)

            file_prefix_pushbutton.released.connect(file_prefix_dialog)

            sublayout = qw.QHBoxLayout()
            sublayout.addWidget(file_prefix_pushbutton)
            sublayout.addWidget(file_prefix_lineedit)
            sublayout.addWidget(qw.QLabel("_[mesh_name].vtk"))
            file_prefix_widget.add_row("Save as", sublayout)

            def ok_btn_callback():
                file_prefix_widget.close()
                _file_prefix = file_prefix_lineedit.text()
                self.save_meshes(_file_prefix)

            file_prefix_widget.set_accepted_callback(ok_btn_callback)
            file_prefix_widget.show()

        else:
            self.save_meshes(file_prefix)

    def remove_actor_completely(self, actor_name):
        FilterTreePlot.remove_mesh_completely(self, actor_name)
        if actor_name in self.toolbars.keys():
            self.app_window.removeToolBar(self.toolbars[actor_name])
            del self.toolbars[actor_name]
        filters_tree_widgets.update_filters_tree(self)

    def add_slice_with_controls(
            self, scalars_name, mesh_to_slice_name="fullmesh"
    ):
        # noinspection GrazieInspection
        """ Create a callable that adds a slice filter formula
                controlled by sliders and buttons in the controls area.

                Parameters
                ----------
                scalars_name : str or `None`
                    Name of scalar dataset to use for coloring the slice.
                    Pass `None` to use a solid color.

                mesh_to_slice_name : str, optional
                    Name of parent mesh to slice

                Returns
                -------
                callable()

                """

        def return_func():
            slice_name, widget_name = plane_widgets.add_slice(
                self,
                scalars_name=scalars_name,
                mesh_to_slice_name=mesh_to_slice_name
            )
            if scalars_name is None:
                self.update_actor(slice_name, color="auto")
            toolbar = self.actor_control_toolbars[slice_name]
            toolbar.make_slice_slider_controls(widget_name=widget_name)

            filter_formula = self.get_filter_formula(slice_name)
            colorbar = filter_formula.colorbar
            if colorbar is not None:
                scalar_bars.standardize_scalar_bar(self, colorbar)
                colorbar.SetTitle(scalars_name)
        return return_func

    def add_widget_formula(self, name=None, **kwargs):
        FilterTreePlot.add_widget_formula(self, name=name, **kwargs)
        self.toolbars['widgets'].populate()
        self.toolbars['widgets'].refresh()

    def set_orbit_dlg(self, **kwargs):
        self.set_orbit_kwargs(self, **kwargs)
        orbit_kwargs = self.orbit_kwargs
        orbit_dlg = utilities_Qt.FormDialog(title='Set orbit path')
        old_orbit_kwargs = self.orbit_kwargs.copy()

        def set_factor():
            orbit_kwargs['factor'] = sb_factor.value()

        # noinspection PyArgumentList
        sb_factor = qw.QDoubleSpinBox(minimum=0)
        sb_factor.setValue(orbit_kwargs['factor'])
        # noinspection PyUnresolvedReferences
        sb_factor.editingFinished.connect(set_factor)
        orbit_dlg.add_row('factor: ', sb_factor)

        def set_n_points():
            orbit_kwargs['n_points'] = sb_n_points.value()

        # noinspection PyArgumentList
        sb_n_points = qw.QSpinBox(minimum=1, maximum=3000)
        sb_n_points.setValue(orbit_kwargs['n_points'])
        # noinspection PyUnresolvedReferences
        sb_n_points.editingFinished.connect(set_n_points)
        orbit_dlg.add_row('n_points: ', sb_n_points)
        le_viewup = qw.QLineEdit()
        le_viewup.setText(str(orbit_kwargs['viewup']))

        def set_viewup():
            txt = le_viewup.text()
            try:
                orbit_kwargs['viewup'] = list(eval(txt))
            except (TypeError, ValueError):  # reject invalid entry, revert
                le_viewup.setText(orbit_kwargs['viewup'])

        # noinspection PyUnresolvedReferences
        le_viewup.editingFinished.connect(set_viewup)
        orbit_dlg.add_row('viewup: ', le_viewup)
        # noinspection PyArgumentList
        sb_shift = qw.QDoubleSpinBox(
            minimum=-max(self.dims()),
            maximum=max(self.dims())
        )
        sb_shift.setValue(orbit_kwargs['shift'])

        def set_shift():
            orbit_kwargs['shift'] = sb_shift.value()

        # noinspection PyUnresolvedReferences
        sb_shift.editingFinished.connect(set_shift)
        orbit_dlg.add_row('shift: ', sb_shift)

        # noinspection PyArgumentList
        sb_fps = qw.QDoubleSpinBox(minimum=0.1, maximum=120)
        sb_fps.setValue(1. / orbit_kwargs['step'])

        def set_fps():
            orbit_kwargs['step'] = 1. / sb_fps.value()

        # noinspection PyUnresolvedReferences
        sb_fps.editingFinished.connect(set_fps)
        orbit_dlg.add_row('frames per second: ', sb_fps)

        def reject_callback():
            for key in old_orbit_kwargs.keys():
                self.orbit_kwargs[key] = old_orbit_kwargs[key]

        # noinspection PyUnresolvedReferences
        orbit_dlg.set_rejected_callback(reject_callback)
        # noinspection PyUnresolvedReferences
        orbit_dlg.set_accepted_callback(lambda: self.set_orbit(**self.orbit_kwargs))
        orbit_dlg.show()
