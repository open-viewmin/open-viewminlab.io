""" Jones matrix calculations for transmission of polarized light through the
nematic liquid crystal.

Adapted from code originally written by Sophie Ettinger
and adapted for open-Qmin data by Yvonne Zagzag.
"""

import numpy as np
import matplotlib as mpl
import matplotlib.pyplot as plt
import pyvista as pv
import qtpy.QtWidgets as qw
import qtpy.QtCore as Qt
from ..filter_tree_plot_Qt.widgets_Qt import utilities_Qt
from scipy.ndimage import gaussian_filter
mpl.use('agg')


def n_and_site_types_from_open_qmin(filename):
    """ Import Q-tensor data in the open-Qmin data_format

    Parameters
    ----------
    filename : str
        Filename of open-Qmin-style data.

    Returns
    -------
    numpy.ndarray, numpy.ndarray
        director, site_types arrays
    """

    data = np.loadtxt(filename, delimiter='\t')
    Qxx, Qxy, Qxz, Qyy, Qyz, site_types = data.T[3:9]

    # take system dimensions from coordinates on last line
    dims = tuple(np.asarray(data[-1][:3] + 1, dtype=int))

    # form traceless, symmetric matrix from indep. Q-tensor components
    q_matrix = np.moveaxis(
        np.array([
            [Qxx, Qxy, Qxz],
            [Qxy, Qyy, Qyz],
            [Qxz, Qyz, -Qxx - Qyy]
        ]),
        -1, 0
    )

    # Calculate director as eigenvector with leading eigenvalue
    _, eigvecs = np.linalg.eigh(q_matrix)
    n = eigvecs[:, :, 2]

    # Reshape 1D arrays into system (Lx, Ly, Lz) dimensions
    n = n.reshape(dims + (3,), order='F')
    site_types = site_types.reshape(dims, order='F')

    return n, site_types


def rotation_matrix_2d(angle):
    """ Calculate 2x2 rotation matrix

    Parameters
    ----------
    angle : float or numpy.ndarray
        rotation angle or array thereof

    Returns
    -------
    numpy.ndarray
        rotation matrix or array thereof

    """

    c = np.cos(angle)
    s = np.sin(angle)
    rot_mat = np.array([
        [c, -s],
        [s,  c]
    ], dtype=complex)

    # move newly created axes to last indices
    if isinstance(angle, np.ndarray):
        if len(angle.shape) > 1:
            rot_mat = np.moveaxis(rot_mat, (0, 1), (-2, -1))

    return rot_mat


def rotation_transform(mat, rot_mat):
    """  Calculate M' = R M R^T for given matrix M and rotation matrix R.

    Parameters
    ----------
    mat : numpy.ndarray
        Array of matrices *M*.

    rot_mat : numpy.ndarray
        Rotation matrix *R*.

    Returns
    -------
    numpy.ndarray
        Array of rotated matrices

    """
    rot_mat_transposed = np.swapaxes(rot_mat, -1, -2)
    return rot_mat @ mat @ rot_mat_transposed


def jones_matrix(n_phi, phi_ext, phi_ord):
    """ Calculate Jones matrix array for one slice at constant z.

    Parameters
    ----------
    n_phi : numpy.ndarray
        azimuthal angle of director in plane transverse to light propagation
    phi_ext : numpy.ndarray
        array of local-frame complex Jones matrices for effective
        extraordinary index of refraction
    phi_ord : numpy.ndarray
        local-frame complex Jones matrix for ordinary index of refraction

    Returns
    -------
    numpy.ndarray
        array of lab-frame complex Jones matrices

    """

    # 2D array of 2x2 rotation matrices
    rot_mat = rotation_matrix_2d(n_phi)

    # Jones matrix in local coordinate system where director is along x.
    jones = np.zeros_like(rot_mat)
    jones[..., 0, 0] = np.exp(1j * phi_ext)
    jones[..., 1, 1] = np.exp(1j * phi_ord)

    # rotation transform of Jones matrix into the lab frame
    jones[:] = rotation_transform(jones, rot_mat)

    return jones


def jones_light_propagation_through_sample(
    director,
    site_types,
    wavelength=660e-9,
    res=4.5e-8,  # 10x greater than typical LdG modeling lattice spacing
    n_ord=1.55,  # ordinary index of refraction (default value from 5CB)
    n_ext=1.70,  # extraordinary index of refraction (default value from 5CB)
    optical_axis='z',
    n_bdy=1.333,  # index of refraction inside boundary
):
    """ Calculate Jones matrix product for light propagation through nematic
    sample.

    Parameters
    ----------
    director : numpy.ndarray
        nematic director field
    site_types : numpy.ndarray
        array of site types (bulk, boundary, or surface) for the lattice
    wavelength : float
        wavelength of incident light, in meters
    res : float
        lattice resolution, in meters
    n_ord : float
        ordinary index of refraction
    n_ext : float
        extraordinary index of refraction
    optical_axis : 'x', 'y', or 'z', optional
        light propagation direction
    n_bdy : float or sequence of floats
        isotropic index of refraction inside boundary

    Returns
    -------
    numpy.ndarray
        Jones matrix for light propagation through the entire simulation box
    """

    director = permute_axes(director, optical_axis)
    site_types = permute_axes(site_types, optical_axis)

    dim_x, dim_y, dim_z = director.shape[:3]

    res_angle = 2 * np.pi * res / wavelength
    phi_ord = n_ord * res_angle

    # director components
    n_z = director[..., 2]
    n_phi = np.arctan2(director[..., 1], director[..., 0])

    if not hasattr(n_bdy, "len"):
        n_bdy = [n_bdy]
    n_bdy = np.array(n_bdy)
    num_missing_n_bdys = int(np.max(site_types) - len(n_bdy))
    if num_missing_n_bdys > 0:
        n_bdy = np.append(n_bdy, np.array([n_bdy[-1]] * num_missing_n_bdys))
    assert len(n_bdy) >= np.max(site_types)

    # effective extraordinary refractive index 3D array
    phi_ext_eff = np.where(
        site_types <= 0,
        n_ord * n_ext * res_angle / np.sqrt(
            (n_ext * n_z) ** 2 + n_ord ** 2 * (1 - n_z ** 2)
        ),  # nematic sites
        # don't calculate at boundary sites
        0
    )

    # 3D array of 2x2 Jones matrices
    jones_matrix_array = jones_matrix(n_phi, phi_ext_eff, phi_ord)
    # boundary sites
    for i_bdy in range(1, len(n_bdy) + 1):
        this_bdy_loc = (site_types == i_bdy)
        jones_matrix_array[this_bdy_loc, 0, 0] = np.exp(
            1j * res_angle * n_bdy[i_bdy-1]
        )
        jones_matrix_array[this_bdy_loc, 1, 1] = (
            jones_matrix_array[this_bdy_loc, 0, 0]
        )
        jones_matrix_array[this_bdy_loc, 1, 0] = 0
        jones_matrix_array[this_bdy_loc, 0, 1] = 0

    # iterate over z-values
    J = np.empty_like(jones_matrix_array[:, :, 0])
    J[:] = jones_matrix_array[:, :, 0]  # copy Jones values from 1st layer
    for k in range(1, dim_z):
        J[:] = jones_matrix_array[:, :, k] @ J

    return J


def polarizer_matrix_from_angle(angle):
    """ Calculate projection matrix along a polarizer's direction in the xy
    plane.

    Parameters
    ----------
    angle : float
        angle of polarizer with respect to x-axis (after permuting axes so
        that z is the light propagation direction)

    Returns
    -------
    numpy.ndarray
        polarizer projection matrix
    """
    rot_mat = rotation_matrix_2d(angle)
    x_projection = np.array([[1, 0], [0, 0]])
    mat = rotation_transform(x_projection, rot_mat)
    return mat


def get_quarter_wave_plate_jones(angle):
    """ Apply quarter-wave plate transformation to Jones matrix

    Parameters
    ----------
    angle: float

    Returns
    -------
    numpy.ndarray

    """
    return rotation_transform(
        np.array([
            [np.exp(1j * np.pi / 4), 0],
            [0, np.exp(-1j * np.pi / 4)]
        ]),
        rotation_matrix_2d(angle)
    )


def pom_image_one_wavelength(
    jones_mat_product, p_angle, a_angle, use_quarter_wave_plate=False
):
    """ Simulated polarized optical microscopy image using a single wavelength
    of light.

    Parameters
    ----------
    jones_mat_product : numpy.ndarray
        array of Jones matrix products for light propagation through the entire
        sample
    p_angle : float
        angle of polarizer with respect to x-axis (after permuting axes so that
        light propagates along z)
    a_angle : float
        angle of analyzer with respect to x-axis (after permuting axes so that
        light propagates along z)
    use_quarter_wave_plate: bool, optional
        whether to insert a quarter wave plate, oriented at a 45-degree
        counterclockwise rotation from the polarizer,  between the polarizer
        and the sample
    Returns
    -------
    numpy.ndarray
        2D array of transmitted intensities

    """

    analyzer = polarizer_matrix_from_angle(a_angle)

    # incoming linearly polarized E field
    E_0 = np.array([np.cos(p_angle), np.sin(p_angle)])

    if use_quarter_wave_plate:
        quarter_wave_plate_Jones = get_quarter_wave_plate_jones(
            p_angle + np.pi/4
        )
        E_0 = quarter_wave_plate_Jones @ E_0

    Exy = analyzer @ jones_mat_product @ E_0
    transmission = np.sum(np.abs(Exy) ** 2, axis=-1)

    return transmission


def pom_image_color(
    jones_mats,
    polarizer_angle=0.,
    analyzer_angle=None,
    brightness_factor=1,
    use_quarter_wave_plate=False
):
    """ Given Jones matrices for red, green, and blue light propagation,
    generate rgb array (float values in [0, 1]) for a simulated color polarized
    optical microscopy image.

    Parameters
    ----------
    jones_mats : Iterable of [numpy.ndarray]
        list of three Jones matrix products for propagation of red, green, and
        blue light (respectively) through the entire sample

    polarizer_angle : float
        angle of polarizer

    analyzer_angle : float
        angle of analyzer

    brightness_factor : float
        multiplier for intensity

    use_quarter_wave_plate : bool, optional
        whether to insert a quarter wave plate, oriented at a 45-degree
        counterclockwise rotation from the polarizer,  between the polarizer
        and the sample

    Returns
    -------
    numpy.ndarray
        2D array of rgb values for transmitted light

    """

    if analyzer_angle is None:
        analyzer_angle = polarizer_angle + np.pi / 2

    img_data = np.moveaxis(
        np.array(
            [
                pom_image_one_wavelength(
                    jms, polarizer_angle, analyzer_angle,
                    use_quarter_wave_plate=use_quarter_wave_plate
                )
                for jms in jones_mats
            ],
        ),
        0, -1
    )
    img_data *= brightness_factor
    img_data = np.minimum(img_data, 1)  # cut off values above 1
    return img_data


def permute_axes(arr, axis_name):
    """ Permute coordinate axes so that the optical axis corresponds to
    axis_name

    Parameters
    ----------
    arr : numpy.ndarray
    axis_name : 'x', 'y', or 'z'

    Returns
    -------
    numpy.ndarray

    """

    if axis_name == 'z':
        axes_permut = (0, 1, 2)  # x,y,z -> x,y,z; 0->0, 1->1, 2->2
    elif axis_name == 'x':
        axes_permut = (2, 0, 1)  # x,y,z -> y,z,x; 0->2, 1->0, 2->1
    elif axis_name == 'y':
        axes_permut = (1, 2, 0)  # x,y,z -> z,x,y; 0->1, 1->2, 2->0
    else:
        raise ValueError('axis_name must be \'x\', \'y\', or \'z\'')

    return np.moveaxis(arr, (0, 1, 2), axes_permut)


def jones_matrices_multiple_wavelengths(
    n, site_types,
    wavelengths=(638e-9, 532e-9, 465e-9),  # red, green, blue; from https://www.rp-photonics.com/rgb_sources.html
    **kwargs
):
    """ Calculate list of Jones matrices for a sequence of wavelengths.

    Parameters
    ----------
    n : numpy.ndarray
        nematic director field
    site_types : numpy.ndarray
        array of site types (bulk, boundary, surface)
    wavelengths : sequence of floats
        wavelengths in meters
    **kwargs :
        keyword arguments accepted by `jones_light_propagation_through_sample`

    Returns
    -------
    list(numpy.ndarray)
        list of Jones matrix arrays
    """

    jones_mats = [
        jones_light_propagation_through_sample(
            n, site_types, wavelength=wavelength, **kwargs
        )
        for wavelength in wavelengths
    ]
    return jones_mats


def list_to_xyz_mat(data, dims):
    """ Convert a flattened data array to shape `(Lx, Ly, Lz, ...)`.

    Parameters
    ----------
    data : numpy.ndarray
    dims : (int, int, int)

    Returns
    -------
    numpy.ndarray

    """
    new_shape = dims + data.shape[1:]
    data_reshaped = data.reshape(new_shape, order="F")
    return data_reshaped


def get_director_data_from_plotter(plotter, n, site_types):
    """ Use plotter's existing nematic director data for Jones matrix
    calculations

    Parameters
    ----------
    plotter : open_viewmin.NematicPlot or open_viewmin.NematicPlotNoQt
    n : numpy.ndarray or `None`
        nematic director field in flattened array; if `None`, data is taken
        from plotter
    site_types : numpy.ndarray, `"off"`, or `None`
        site types in flattened array; if `"off"`, all sites are treated as
        bulk; if `None`, data is taken from plotter

    Returns
    -------
    numpy.ndarray, numpy.ndarray
        reshaped arrays of nematic director and site types data

    """

    if n is None:
        n = plotter.default_mesh['director']
    if site_types is None:
        site_types = plotter.default_mesh['site_types']
    elif site_types == 'off':
        site_types = np.zeros(n.shape[0])
    n = list_to_xyz_mat(n, plotter.array_dims())
    site_types = list_to_xyz_mat(site_types, plotter.array_dims())
    return n, site_types


def pom_image_sequence_rotate_polarizers(
    ax, n, site_types,
    num_frames=10,
    brightness_factor=1,
    **kwargs
):
    """ Create image sequence of simulated polarized optical microscopy images
    with polarizer and analyzer rotating linearly in time through total angle
    pi/2.

    Parameters
    ----------
    ax : matplotlib.axes
        figure axes
    n : numpy.ndarray or `None`
        nematic director field in flattened array; if `None`, data is taken
        from plotter
    site_types : numpy.ndarray, `"off"`, or `None`
        site types in flattened array; if `"off"`, all sites are treated as
        bulk; if `None`, data is taken from plotter
    num_frames : int, optional
        number of subdivisions of the [0, pi/2] interval for the polarizer
        angle, and thus the number of frames in the animation
    brightness_factor : float, optional
        multiplier for intensity
    kwargs : dict, optional
        keyword arguments accepted by `jones_matrices_multiple_wavelengths`

    Returns
    -------
    list(matplotlib.axes.Axes.imshow)
        list of transmitted intensity images

    """

    jones_mats = jones_matrices_multiple_wavelengths(
        n, site_types, **kwargs
    )

    ims = []
    for i in range(num_frames):
        polarizer_angle = np.pi / 2 * i / num_frames
        analyzer_angle = polarizer_angle + np.pi / 2
        img_data = pom_image_color(
            jones_mats, polarizer_angle, analyzer_angle,
            brightness_factor=brightness_factor
        )
        # noinspection PyUnresolvedReferences
        img = ax.imshow(img_data, animated=(i != 0))
        ims.append([img])
    return ims


def pom_animation_rotate_polarizers(
    plotter, n=None, site_types='off', interval=50, filename=None, **kwargs
):
    """  Create video of simulated polarized optical microscopy images
    with polarizer and analyzer rotating linearly in time through total angle
    pi/2.

    Parameters
    ----------
    plotter : open_viewmin.NematicPlot or open_viewmin.NematicPlotNoQt
    n : numpy.ndarray or None, optional
        nematic director data; if `None`, this data is taken from the plotter
    site_types : numpy.ndarray, `"off"`, or `None`, optional
        site types data; if `"off"`, all sites are treated as bulk; if `None`,
        data is taken from plotter
    interval : int, optional
        number of milliseconds between frames of animation
    filename : str or `None`, optional
        filename for exported video; if `None`, no export occurs and the
        function returns the full HTML string for the video
    kwargs : dict, optional
        keyword arguments accepted by `pom_image_sequence_rotate_polarizers`

    Returns
    -------
    str
       filename of exported video or full HTML string defining the video

    """

    n, site_types = get_director_data_from_plotter(plotter, n, site_types)
    if "brightness_factor" not in kwargs.keys():
        kwargs["brightness_factor"] = (
            plotter.jones_settings["brightness_factor"]
        )
    mpl.pyplot.ioff()  # don't show frames as they're created

    # create figure with correct aspect ratio
    dim_x, dim_y = n.shape[:2]
    fig, ax = mpl.pyplot.subplots(figsize=(10 * dim_y / dim_x, 10))
    ax.axis('off')  # disable axes ticks

    # image sequence
    ims = pom_image_sequence_rotate_polarizers(ax, n, site_types, **kwargs)

    # animation
    anim = mpl.animation.ArtistAnimation(
        fig, ims, interval=interval, blit=True
    )
    mpl.pyplot.close()  # don't leave figure open

    vid_txt = anim.to_html5_video()

    if filename is None:
        return vid_txt
    else:
        with open(filename, 'w') as f:
            f.write(vid_txt)
        return filename


def rgb_to_texture(rgb_data, sigma=1.):
    """  Transform 2D NumPy float rgb array to PyVista texture

    Parameters
    ----------
    rgb_data : numpy.ndarray
        2D array of (red, green, blue) intensities, each in the interval
        [0, 1]
    sigma : float, optional
        smoothing factor for Gaussian filter; set to 1. for no filter.

    Returns
    -------
    pyvista.Texture

    """
    if sigma != 1:
        for i in range(3):
            rgb_data[..., i] = gaussian_filter(rgb_data[..., i], sigma=sigma)
    return pv.numpy_to_texture(np.asarray(256 * rgb_data, dtype=np.uint8))


def add_jones_to_plotter(plotter, n=None, site_types=None):
    """ Add planes to back of box, colored by RGB Jones matrix transmission
    and normal to x, y, z.


    Parameters
    ----------
    plotter : :class:`~open_viewmin.nematic.nematic_plot.NematicPlot` or \
              :class:`~open_viewmin.nematic.nematic_plot.NematicPlotNoQt`
    n : numpy.ndarray or `None`, optional
        director data; if `None`, director will be taken from plotter
    site_types : numpy.ndarray, `"off"`, or `None`
        site types data; if `"off"`, all sites are taken to be bulk; if `None`,
        site types data will be taken from plotter
    """

    n, site_types = get_director_data_from_plotter(plotter, n, site_types)

    for i in range(3):
        z_dir_idx = (i + 2) % 3
        optical_axis = ['x', 'y', 'z'][z_dir_idx]
        jones_rgb = jones_matrices_multiple_wavelengths(
            n, site_types, optical_axis=optical_axis,
            **{
                key: plotter.jones_settings[key] for key in [
                    "wavelengths",
                    "n_ord",
                    "n_ext",
                    "n_bdy",
                    "res"
                ]
            }
        )
        rgb_data = pom_image_color(
            jones_rgb,
            **{
                key: plotter.jones_settings[key] for key in [
                    "polarizer_angle",
                    "brightness_factor",
                    "use_quarter_wave_plate"
                ]
            }
        )

        # corrections for the way imshow flips axes around
        if i in [0, 1]:
            rgb_data = np.moveaxis(rgb_data, 0, 1)
        rgb_data = np.flip(rgb_data, axis=0)

        # store and expose rgb data
        plotter.jones_data[optical_axis] = rgb_data

        center = np.array(n.shape[:3]) / 2
        center[z_dir_idx] = 0.01  # some small non-zero value needed
        normal = -np.identity(3)[z_dir_idx]

        texture = rgb_to_texture(rgb_data)

        pln1 = plotter.default_mesh.slice(normal=normal, origin=center)
        pln2 = plotter.default_mesh.slice(normal=-normal, origin=center)
        pln = pln1 + pln2
        pln.texture_map_to_plane(inplace=True)
        actor_name = f"Jones_{optical_axis}"
        plotter.remove_actor(actor_name)
        plotter.add_mesh(
            pln,
            texture=texture,
            name=actor_name,
            ambient=1,
            diffuse=0
        )
        _add_jones_arrows(plotter, i, center, actor_name)


def _double_headed_arrow(center, direction, **kwargs):
    """

    Parameters
    ----------
    center: Iterable of [float]
    direction: Iterable of [float]
    kwargs: dict, optional
        Keyword arguments to `pyvista.Arrow`

    Returns
    -------
    `(pyvista.Arrow, pyvista.Arrow)`

    """
    arrow_1, arrow_2 = [
        pv.Arrow(
            center,
            direction * i_sgn,
            **kwargs
        )
        for i_sgn in [1, -1]
    ]
    arrows = arrow_1 + arrow_2
    return arrows


def _add_jones_arrows(plotter, coord_index, center, actor_name):
    """ Add arrows for polarizer and analyzer directions and, if applicable,
    quarter-wave plate direction

    Parameters
    ----------
    plotter: :class:`~open_viewmin.nematic.nematic_plot.nematic_plot_no_qt.NematicPlotNoQt`
    coord_index: int
    center: Iterable of [float]
    actor_name: str
    """
    jones_settings = plotter.jones_settings
    polarizer_angle = jones_settings.get("polarizer_angle", 0)
    use_quarter_wave_plate = jones_settings.get(
        "use_quarter_wave_plate", False
    )
    jones_arrows_scale = plotter.settings["Jones_arrows_scale"]
    jones_arrows_placement_dist = (
        plotter.settings["Jones_arrows_placement_dist"]
    )
    p_arrows_color = plotter.settings["Jones_p_arrows_color"]
    a_arrows_color = plotter.settings["Jones_a_arrows_color"]

    jones_arrows_font_size = plotter.settings["Jones_arrows_font_size"]
    jones_arrows_label_separation = plotter.settings[
        "Jones_arrows_label_separation"
    ]

    x_idx = coord_index
    y_idx = (coord_index + 1) % 3
    x_dir = np.identity(3)[x_idx]
    y_dir = np.identity(3)[y_idx]

    arrows_center = (
            center
            + 0.5 * (
                plotter.dims()[x_idx] * x_dir + plotter.dims()[y_idx] * y_dir
            )
            + jones_arrows_placement_dist * (x_dir + y_dir) / np.sqrt(2)
    )

    analyzer_angle = polarizer_angle + np.pi / 2
    p_dir = x_dir * np.cos(polarizer_angle) + y_dir * np.sin(polarizer_angle)
    a_dir = x_dir * np.cos(analyzer_angle) + y_dir * np.sin(analyzer_angle)

    for arrow_dir, arrow_label_txt, arrow_color in zip(
        (p_dir, a_dir),
        ("P", "A"),
        (p_arrows_color, a_arrows_color)
    ):
        arrows = _double_headed_arrow(
            arrows_center,
            jones_arrows_scale * arrow_dir,
            scale="auto"
        )
        arrows_actor_name = f"{actor_name}_{arrow_label_txt}"
        plotter.remove_actor(arrows_actor_name)
        plotter.add_mesh(
            arrows, color=arrow_color, name=arrows_actor_name,
            ambient=1, diffuse=0
        )
        plotter.remove_actor(arrows_actor_name + "-labels")
        plotter.add_point_labels(
            [
                arrows_center
                + (
                    (jones_arrows_scale + jones_arrows_label_separation)
                    * arrow_dir * (-1 if arrow_dir[2] < 0 else 1)
                )
            ],
            [arrow_label_txt],
            show_points=False,
            shape=None,
            font_size=jones_arrows_font_size,
            text_color=arrow_color,
            name=arrows_actor_name,
            italic=False,
            bold=False,
        )

    lambda_arrow_actor_name = f"{actor_name}_lambda_arrow"
    plotter.remove_actor(lambda_arrow_actor_name)
    if use_quarter_wave_plate:
        _add_jones_lambda_arrow(
            plotter,
            p_dir,
            a_dir,
            jones_arrows_scale,
            arrows_center,
            lambda_arrow_actor_name
        )


def _add_jones_lambda_arrow(
    plotter,
    p_dir,
    a_dir,
    jones_arrows_scale,
    arrows_center,
    lambda_arrow_actor_name
):
    """ Add arrow indicating orientation of quarter-wave plate

    Parameters
    ----------
    plotter: :class:`~open_viewmin.nematic.nematic_plot.nematic_plot_no_qt.NematicPlotNoQt`
    p_dir: Iterable of [float]
    a_dir: Iterable of [float]
    jones_arrows_scale: float
    arrows_center: Iterable of [float]
    lambda_arrow_actor_name: str
    """
    arrow_dir = (p_dir + a_dir) / 2
    lambda_arrow_scale = np.sqrt(2) * jones_arrows_scale
    lambda_arrow = pv.Arrow(
        arrows_center - lambda_arrow_scale * arrow_dir,
        2 * lambda_arrow_scale * arrow_dir,
        scale="auto"
    )
    arrow_color = "pink"
    plotter.add_mesh(
        lambda_arrow, color=arrow_color,
        name=lambda_arrow_actor_name,
        ambient=1, diffuse=0
    )


def add_jones_toolbar_to_plotter(plotter, n=None, site_types=None):
    """ Add planes to back of box, colored by RGB Jones matrix transmission
    and normal to x, y, z, with polarizer/analyzer angles and brightness
    controlled by sliders.

    Parameters
    ----------
    plotter : open_viewmin.NematicPlot

    n : numpy.ndarray or None, optional
        director data; if `None`, director will be taken from plotter

    site_types : numpy.ndarray, `"off"`, or `None`
        site types data; if `"off"`, all sites are taken to be bulk; if `None`,
        site types data will be taken from plotter

    Returns
    -------
    callable()
        callback that refreshes Jones planes by re-applying the control
        slider widgets

    """

    plotter.toolbars["Jones"] = utilities_Qt.ControlsToolbar(
        name="Jones", label="Jones",
        icon_size=plotter.settings["icon_size"], has_refresh_button=True
    )
    jones_toolbar = plotter.toolbars["Jones"]
    jones_toolbar.options_toolbar.setOrientation(Qt.Qt.Orientation.Vertical)
    visibility_checkboxes = []

    def checkbox_callback(checkbox, jones_actor_name):
        """ update actor visibility according to checkbox

        Parameters
        ----------
        checkbox: qw.QCheckBox
        jones_actor_name: str
        """
        checked = checkbox.isChecked()
        for actor_name in plotter.renderer.actors.keys():
            if actor_name.startswith(jones_actor_name):
                actor = plotter.renderer.actors[actor_name]
                actor.SetVisibility(checked)

    def jones_callback(_):
        """ update Jones matrix visualization """
        kwargs = plotter.jones_settings
        try:
            kwargs["polarizer_angle"] = (
                polarizer_angle_toolbar.slider.value() / 100 * np.pi / 2
            )
        except NameError:  # ignore if toolbar doesn't exist yet
            return
        try:
            brightness_slider_factor = 50
            kwargs["brightness_factor"] = (
                jones_brightness_toolbar.slider.value()
                / brightness_slider_factor
            )
        except NameError:  # ignore if toolbar doesn't exist yet
            return
        try:
            kwargs["n_ord"] = n_ord_spinbox.value()
        except NameError:
            return
        try:
            kwargs["n_ext"] = n_ext_spinbox.value()
        except NameError:
            return
        try:
            kwargs["res"] = res_spinbox.value() * 1e-9
        except NameError:
            return
        add_jones_to_plotter(
            plotter, n=n, site_types=site_types
        )
        if len(visibility_checkboxes) == 3:
            for checkbox, norm_dir_lbl in zip(
                visibility_checkboxes, ['x', 'y', 'z']
            ):
                checkbox_callback(checkbox, f"Jones_{norm_dir_lbl}")

    @wraps(jones_callback)
    def refresh_callback():
        jones_callback(None)

    jones_toolbar.refresh_actions.append(refresh_callback)
    # refresh_callback()  # create the Jones planes

    polarizer_angle_toolbar = utilities_Qt.ControlsSliderToolbar(
        jones_callback,
        label_txt="P angle    ︎",
        min_val=0,
        max_val=100,
        init_val_percent=0,
        name="Jones_polarizer_angle",
        spinbox=False
    )
    polarizer_angle_toolbar.slider.setFixedWidth(150)
    jones_toolbar.options_toolbar.addWidget(polarizer_angle_toolbar)

    jones_brightness_toolbar = utilities_Qt.ControlsSliderToolbar(
        jones_callback,
        label_txt="brightness",
        min_val=0,
        max_val=100,
        init_val_percent=50,
        name="Jones brightness",
        spinbox=False
    )
    jones_brightness_toolbar.slider.setFixedWidth(150)
    jones_toolbar.options_toolbar.addWidget(jones_brightness_toolbar)

    jones_checkbox_toolbar = qw.QToolBar()

    # noinspection PyUnresolvedReferences
    def add_jones_checkbox(callback_norm_dir):
        """ Add visibility control checkbox for a Jones matrix plane

        Parameters
        ----------
        callback_norm_dir: str
            Must be one of 'x', 'y', 'z'.
        """

        checkbox = qw.QCheckBox()
        checkbox.setText(callback_norm_dir)
        actor_name = "Jones_" + callback_norm_dir
        actor = plotter.renderer.actors.get(actor_name)
        if actor is not None:
            checkbox.setChecked(actor.GetVisibility())
        else:
            checkbox.setChecked(True)

        checkbox.stateChanged.connect(
            lambda: checkbox_callback(checkbox, actor_name)
        )
        jones_checkbox_toolbar.addWidget(checkbox)
        visibility_checkboxes.append(checkbox)

    for norm_dir in ['x', 'y', 'z']:
        add_jones_checkbox(norm_dir)

    quarter_wave_plate_checkbox = qw.QCheckBox("1/4-waveplate")
    quarter_wave_plate_checkbox.setChecked(
        plotter.jones_settings["use_quarter_wave_plate"]
    )

    def quarter_wave_plate_checkbox_callback():
        """ Toggle use of quarter-wave plate in Jones calculation according to
        checkbox.
        """
        checkbox = quarter_wave_plate_checkbox
        plotter.jones_settings["use_quarter_wave_plate"] = checkbox.isChecked()
        refresh_callback()

    # noinspection PyUnresolvedReferences
    quarter_wave_plate_checkbox.stateChanged.connect(
        quarter_wave_plate_checkbox_callback
    )
    jones_checkbox_toolbar.addWidget(quarter_wave_plate_checkbox)
    jones_toolbar.options_toolbar.addWidget(jones_checkbox_toolbar)

    materials_constants_toolbar = qw.QToolBar()

    # noinspection PyArgumentList
    n_ord_spinbox = qw.QDoubleSpinBox(minimum=1, maximum=10, value=1.55)
    n_ord_spinbox.setSingleStep(0.05)
    # noinspection PyUnresolvedReferences
    n_ord_spinbox.editingFinished.connect(refresh_callback)
    n_ord_spinbox.setToolTip("Set ordinary index of refraction.")
    materials_constants_toolbar.addWidget(qw.QLabel("n_ord:"))
    materials_constants_toolbar.addWidget(n_ord_spinbox)

    # noinspection PyArgumentList
    n_ext_spinbox = qw.QDoubleSpinBox(minimum=1, maximum=10, value=1.70)
    n_ext_spinbox.setSingleStep(0.05)
    # noinspection PyUnresolvedReferences
    n_ext_spinbox.editingFinished.connect(refresh_callback)
    n_ext_spinbox.setToolTip("Set extraordinary index of refraction.")
    materials_constants_toolbar.addWidget(qw.QLabel("n_ext:"))
    materials_constants_toolbar.addWidget(n_ext_spinbox)

    # noinspection PyArgumentList
    res_spinbox = qw.QDoubleSpinBox(minimum=0, maximum=1000, value=45)
    # noinspection PyUnresolvedReferences
    res_spinbox.editingFinished.connect(refresh_callback)
    res_spinbox.setToolTip("Set grid resolution in nanometers.")
    materials_constants_toolbar.addWidget(qw.QLabel("res:"))
    materials_constants_toolbar.addWidget(res_spinbox)
    res_spinbox.setMaximumWidth(50)
    for spinbox in (n_ord_spinbox, n_ext_spinbox, res_spinbox):
        spinbox.setButtonSymbols(spinbox.ButtonSymbols.NoButtons)

    jones_toolbar.options_toolbar.addWidget(materials_constants_toolbar)

    jones_save_toolbar = qw.QToolBar(orientation=Qt.Qt.Horizontal)
    save_buttons_width = 90
    save_jones_images_button = qw.QPushButton()
    save_jones_images_button.setText('save imgs')
    save_jones_images_button.setToolTip('save Jones images')
    save_jones_images_button.setFixedWidth(save_buttons_width)
    save_jones_images_button.clicked.connect(
        lambda: save_jones_images_gui(plotter)
    )
    jones_save_toolbar.addWidget(save_jones_images_button)

    save_jones_data_button = qw.QPushButton()
    save_jones_data_button.setText('save data')
    save_jones_data_button.setToolTip('sae Jones data')
    save_jones_data_button.setFixedWidth(save_buttons_width)
    save_jones_data_button.clicked.connect(
        lambda: save_jones_data_gui(plotter)
    )
    jones_save_toolbar.addWidget(save_jones_data_button)
    jones_toolbar.options_toolbar.addWidget(jones_save_toolbar)

    plotter.controls_dock_window.addToolBar(
        Qt.Qt.ToolBarArea.LeftToolBarArea, jones_toolbar
    )

    refresh_callback()
    return refresh_callback


def save_jones_data(plotter, filename: str):
    """ Saves numerical Jones matrix data to files

    Parameters
    ----------
    plotter: :class:`~open_viewmin.nematic.nematic_plot.nematic_plot_no_qt.NematicPlotNoQt`
    filename: str
    """
    for optical_axis in ["x", "y", "z"]:
        for i, color in enumerate(["r", "g", "b"]):
            data_filename = f"{filename}_{optical_axis}_{color}.dat"
            np.savetxt(data_filename, plotter.jones_data[optical_axis][..., i])
            print(
                f"{plotter.__class__.__name__}.jones_data['{optical_axis}'][..., {i}] "
                f"-> {data_filename}"
            )


def save_jones_data_gui(plotter, filename=None):
    """ Launch dialog to save numerical Jones matrix data to files

    Parameters
    ----------
    plotter: :class:`~open_viewmin.nematic.nematic_plot.nematic_plot.NematicPlot`
    filename: str | `None`
    """
    if filename is None:
        save_jones_data_widget = utilities_Qt.FormDialog(
            title='Save Jones data'
        )
        filename_lineedit = qw.QLineEdit()
        pushbutton_filename = qw.QToolButton()
        pushbutton_filename.setIcon(
            plotter.style().standardIcon(
                qw.QStyle.SP_DirIcon
            )
        )

        # noinspection PyArgumentList
        def filename_dialog():
            """ Dialog to choose file name """
            dlg = qw.QFileDialog(
                parent=plotter.app_window
            )
            _filename = dlg.getSaveFileName(
                directory="results"
            )[0]
            filename_lineedit.setText(_filename)

        pushbutton_filename.released.connect(filename_dialog)
        sublayout = qw.QHBoxLayout()
        sublayout.addWidget(pushbutton_filename)
        sublayout.addWidget(filename_lineedit)
        sublayout.addWidget(qw.QLabel("_[x,y,z].png"))
        save_jones_data_widget.add_row('Save Jones data to:', sublayout)

        def ok_btn_callback():
            """ Save Jones data """
            _filename = filename_lineedit.text()
            save_jones_data_widget.close()
            save_jones_data(plotter, _filename)

        save_jones_data_widget.set_accepted_callback(ok_btn_callback)
        save_jones_data_widget.show()

    else:
        save_jones_data(plotter, filename)


def save_jones_images(plotter, filename: str):
    """ Save Jones matrix images to files

    Parameters
    ----------
    plotter: :class:`~open_viewmin.nematic.nematic_plot.nematic_plot_no_qt.NematicPlotNoQt`
    filename: str
    """
    for optical_axis in ["x", "y", "z"]:
        fig = plt.figure()
        plt.imshow(plotter.jones_data[optical_axis])
        fig_filename = f"{filename}_{optical_axis}.png"
        fig.savefig(fig_filename)
        print(
            f"{plotter.__class__.__name__}.jones_data['{optical_axis}'] "
            f"-> {fig_filename}"
        )


def save_jones_images_gui(plotter, filename=None):
    """ Launch dialog to save Jones matrix images to files

    Parameters
    ----------
    plotter: :class:`~open_viewmin.nematic.nematic_plot.nematic_plot.NematicPlot`
    filename: str | `None`
    """
    if filename is None:
        save_jones_images_widget = utilities_Qt.FormDialog(
            title='Save Jones images'
        )
        filename_lineedit = qw.QLineEdit()
        pushbutton_filename = qw.QToolButton()
        pushbutton_filename.setIcon(
            plotter.style().standardIcon(
                qw.QStyle.SP_DirIcon
            )
        )

        # noinspection PyArgumentList
        def filename_dialog():
            """ Dialog to choose file name """
            dlg = qw.QFileDialog(
                parent=plotter.app_window
            )
            _filename = dlg.getSaveFileName(
                directory="results/images"
            )[0]
            filename_lineedit.setText(_filename)

        pushbutton_filename.released.connect(filename_dialog)
        sublayout = qw.QHBoxLayout()
        sublayout.addWidget(pushbutton_filename)
        sublayout.addWidget(filename_lineedit)
        sublayout.addWidget(qw.QLabel("_[x,y,z].png"))
        save_jones_images_widget.add_row('Save Jones images to:', sublayout)

        def ok_btn_callback():
            """ Save Jones images """
            _filename = filename_lineedit.text()
            save_jones_images_widget.close()
            save_jones_images(plotter, _filename)

        save_jones_images_widget.set_accepted_callback(ok_btn_callback)
        save_jones_images_widget.show()

    else:
        save_jones_images(plotter, filename)
