"""
Define plotter class for nematic liquid crystals visualization, with the
filters tree, in a Qt window.
"""

from functools import wraps
import numpy as np
import qtpy.QtWidgets as qw
from .. import jones
from .nematic_plot_no_qt import NematicPlotNoQt
from open_viewmin.filter_tree_plot.filter_tree_plot import FilterTreePlot
from open_viewmin.filter_tree_plot_Qt.filter_tree_plot_Qt import FilterTreePlotQt
from open_viewmin.filter_tree_plot_Qt.widgets_Qt.utilities_Qt import FormDialog
from open_viewmin.filter_tree_plot.widgets import plane_widgets
from open_viewmin.filter_tree_plot.utilities import calculations, ovm_utilities
from open_viewmin.filter_tree_plot.utilities.calculations import levi_civita, safe_inverse
from open_viewmin.filter_tree_plot.filters import isosurfaces, glyphs, sample


class NematicPlot(FilterTreePlotQt, NematicPlotNoQt):
    # noinspection GrazieInspection
    """ Plotter class for nematic liquid crystals visualization,
        with the filters tree, in a Qt window.

        Parameters
        ----------
        filenames : list[str], str, or None, optional
            Files to import.
        q0 : float, optional
            Chiral wavenumber for cholesteric (chiral nematic) ground state, used
            in free energy calculations.
        user_settings : dict or None, optional
            Customizations to default plotter settings.
        data_format : ['open-Qmin' | 'Qmin' | 'director'], optional
            Formatting style of data text files to import; used only if parameter
            `reader` has value `None`.
        sep : str, optional
            Column delimiter for imported text file(s)
        reader : callable() or None, optional
            Function for reading data from text files; set to `None` to use the
            `data_format` (and, optionally, `sep`) parameters to determine data import
            behavior.
        theme : ['dark' | 'document' | 'paraview' | None], optional
            PyVista plot theme
        dims : tuple(int) or None
            System size; used only if `filenames` is `None`.
        data_stride : int, optional
            Data spacing; used only if `filenames` is `None`.
        kwargs : dict, optional
            Keyword arguments to :class:`~open_viewmin.filter_tree_plot.filter_tree_plot.FilterTreePlot`

        """
    def __init__(
        self, filenames=None, q0=0., user_settings=None,
        data_format="open-Qmin", sep="\t", reader=None,
        theme=None, dims=None, data_stride=1,
        **kwargs
    ):

        reader = self._pre_init(q0, data_format, reader, sep)
        FilterTreePlotQt.__init__(
            self, filenames=filenames, user_settings=user_settings,
            theme=theme, reader=reader,
            dims=dims, data_stride=data_stride,
            **kwargs
        )
        NematicPlotNoQt.post_init(self)
        self.toolbars["widgets"].populate()
        self.the_Add_menu.addSeparator()
        self.calculate_menu.addAction(
            "Defect tangent vectors", self.calculate_defect_tangent
        )
        self.the_Add_menu.addAction(
            "Defect tangents glyphs", self.calculate_and_show_defect_tangent
        )

        self.the_Add_menu.addAction(
            "Jones matrix planes", self.show_jones
        )
        self.the_Add_menu.addAction(
            "Disclination analysis", self.analyze_disclinations
        )

        # if filenames is None and dims is not None:
        #     self.Lx, self.Ly, self.Lz = dims
        #     self.data_stride = data_stride
        #     self.create_fullmesh()

    def finish_setup_calculate_menu(self):
        """ Add nematic-specific items to the "Calculate" menu """
        for label, action in (
            ("Frank energy",   self.calculate_frank_energy_comps),
            ("D_tensor",       self.calculate_D_tensor),
            ("Chi_tensor",     self.calculate_chi_tensor),
            ("Westin metrics", self.calculate_Westin_metrics),
            ("rotation_vector_n", self.calculate_rotation_vector_n)
        ):
            self.calculate_menu.addAction(label, action)
        self.calculate_menu.addSeparator()

        def q0_callback():
            """ launch dialog to set reference chiral wavenumber $q_0$ """
            dlg = FormDialog(
                title="set q0",
                message="Energetically preferred chiral wavenumber"
            )
            # noinspection PyArgumentList
            q0_spinbox = qw.QDoubleSpinBox(
                minimum=-np.inf,
                maximum=np.inf,
                value=self.q0,
                decimals=4
            )
            q0_spinbox.setMinimumWidth(75)
            dlg.add_row("q0:", q0_spinbox)
            dlg.set_accepted_callback(lambda: self.set_q0(q0_spinbox.value()))
            dlg.show()

        self.calculate_menu.addAction("Set q0", q0_callback)

    @wraps(NematicPlotNoQt.auto_setup)
    def auto_setup(self, notebook=False):
        NematicPlotNoQt.auto_setup(self, notebook=notebook)
        self.finish_setup_calculate_menu()
        self.filters_tree_widget.expandAll()
        # orbit.set_orbit(self, do_dlg=False, factor=200)

    def setup_defects(self):
        """ Automatic setup for defects visualization """
        NematicPlotNoQt.setup_defects(self)
        defects_S_value = self.settings["defects_S"]
        max_S_value = np.max(self.fullmesh["order_uniaxial"])
        toolbar = self.actor_control_toolbars.get("defects")
        if toolbar is not None:
            toolbar.add_isosurface_qslider(
                min_val=0, max_val=max_S_value, label_txt="S = ",
                init_val_percent=int(defects_S_value / max_S_value * 100),
            )

    def setup_director(self):
        """ Automatic setup for director visualization """
        slice_name, _, widget_name = NematicPlotNoQt.setup_director(self)
        self.actor_control_toolbars[slice_name].make_slice_slider_controls(
            widget_name=widget_name
        )

    def show_jones(self, **kwargs):
        """ Display optical transmission, calculated from Jones matrices,
        on three planes.

        Parameters
        ----------
        kwargs : dict, optional

        Returns
        -------
        callable()
            callback that refreshes Jones planes by re-applying the control
            slider widgets

        """

        return jones.add_jones_toolbar_to_plotter(self, **kwargs)
