"""
Define `NematicPlotNoQt` Plotter class for nematic liquid crystals
visualization, with the filters tree but without Qt.
"""

from __future__ import annotations
from functools import wraps
import numpy as np
import qtpy.QtWidgets as qw
from .. import jones
from open_viewmin.filter_tree_plot.filter_tree_plot import FilterTreePlot
from open_viewmin.filter_tree_plot.filter_formulas import FilterFormula
from open_viewmin.filter_tree_plot_Qt.filter_tree_plot_Qt import FilterTreePlotQt
from open_viewmin.filter_tree_plot_Qt.widgets_Qt.utilities_Qt import FormDialog
from open_viewmin.filter_tree_plot.widgets import plane_widgets
from open_viewmin.filter_tree_plot.utilities import calculations, ovm_utilities
from open_viewmin.filter_tree_plot.utilities.calculations import levi_civita, safe_inverse
from open_viewmin.filter_tree_plot.filters import isosurfaces, glyphs, sample


class NematicPlotNoQt(FilterTreePlot):
    # noinspection GrazieInspection
    """ Plotter class for nematic liquid crystals visualization, with the
        filters tree but without Qt.

    Parameters
    ----------
    filenames : list[str], str, or `None`, optional
        Files to import.
    q0 : float, optional
        Chiral wavenumber for cholesteric (chiral nematic) ground state, used
        in free energy calculations.
    user_settings : dict or `None`, optional
        Customizations to default plotter settings.
    data_format : ['open-Qmin' | 'Qmin' | 'director'], optional
        Formatting style of data text files to import; used only if parameter
        `reader` has value `None`.
    sep : str, optional
        Column delimiter for imported text file(s)
    reader : callable() or `None`, optional
        Function for reading data from text files; set to `None` to use the
        `data_format` (and, optionally, `sep`) parameters to determine data import
        behavior.
    theme : ['dark' | 'document' | 'paraview' | `None`], optional
        PyVista plot theme
    off_screen : bool, optional
        Whether to render off-screen
    notebook : bool, optional
        Whether to display inside of a Jupyter notebook
    **kwargs :
        Keyword arguments to :class:`~open_viewmin.filter_tree_plot.filter_tree_plot.FilterTreePlot`
    """

    def __init__(
            self, filenames=None, q0=0., user_settings=None,
            data_format="open-Qmin", sep="\t", reader=None,
            theme=None, off_screen=False, notebook=True, **kwargs
    ):

        if filenames is None:
            filenames = []
        else:
            filenames = self.organize_filenames(filenames)
        print(filenames)
        self.q0 = q0
        self.do_auto_setup = False
        self.xyz_arrays = dict()
        reader = self._pre_init(q0, data_format, reader, sep)

        # noinspection PyTypeChecker
        super().__init__(
            filenames, user_settings, theme, reader=reader,
            off_screen=off_screen, notebook=notebook,
            **kwargs
        )
        self.post_init()

    @wraps(FilterTreePlot.load)
    def load(self, filenames, **kwargs):
        successful_load = super().load(filenames, **kwargs)
        if self.do_auto_setup and successful_load:
            self.auto_setup()

    def _pre_init(self, q0, data_format, reader, sep):
        """ Portion of initialization to run before
        `open_viewmin.FilterTreePlot.__init__()`

        Parameters
        ----------
        q0 : float
            Chiral wavenumber
        data_format : ['open-Qmin' | 'Qmin' | 'director'], optional
            Formatting style of data text files to import; used only if parameter
            `reader` has value `None`.
        reader : callable() or None, optional
            Function for reading data from text files; set to `None` to use the
            `data_format` (and, optionally, `sep`) parameters to determine data import
            behavior.
        sep : str, optional
            Column delimiter for imported text file(s)

        Returns
        -------
        callable()
            Reader

        """
        self.q0 = q0
        self.jones_settings = dict(
            polarizer_angle=0,
            brightness_factor=1,
            use_quarter_wave_plate=False,
            n_ord=1.55,
            n_ext=1.70,
            n_bdy=1.333,
            res=4.5,
            wavelengths=[638e-9, 532e-9, 465e-9]
        )
        self.jones_data = dict(x=None, y=None, z=None)
        self.do_auto_setup = False
        self.xyz_arrays = dict()
        if reader is None:
            if data_format == "open-Qmin":
                reader = ovm_utilities.fast_import
            elif data_format == "Qmin":
                reader = self._legacy_qmin_import
            elif data_format == "director":
                def reader_callback(*args, **kwargs):
                    """ callback to import director data """
                    return self.director_data_import(*args, sep=sep, **kwargs)

                reader = reader_callback
        return reader

    def post_init(self):
        """
        Portion of initialization to run after
        `open_viewmin.FilterTreePlot.__init__()`

        """

        self.settings = {
            **self.settings,
            **dict(
                director_color=self.settings["actor_color"],
                boundaries_color=self.oVM_theme["darkblue"],
                scale_mesh_name="nematic_sites",
                defects_S=0.3,
                Jones_arrows_scale=5,
                Jones_arrows_placement_dist=6,
                Jones_p_arrows_color="white",
                Jones_a_arrows_color="yellow",
                Jones_arrows_font_size=16,
                Jones_arrows_label_separation=1
            )
        }
        self.do_auto_setup = True
        if len(self.filenames) > 0:
            self.auto_setup()
        self.reset_camera()
        self.orbit_path = self.set_orbit()

    def _legacy_qmin_import(self, qtensor_filename):
        """ Reader for output from older Qmin (a.k.a. nematicvXX) code.

        Note that while only the `"Qtensor_..."` files are supplied directly,
        this routine expects the associated `"Qmatrix_..."` files to be in the
        same directory.

        Parameters
        ----------
        qtensor_filename : str
            Filenames of `"Qtensor_..."`

        Returns
        -------
        list[np.ndarray]
            Imported data

        """

        qtensor = ovm_utilities.fast_import(qtensor_filename, sep=" ")
        qmatrix = ovm_utilities.fast_import(
            qtensor_filename.replace('Qtensor', 'Qmatrix'),
            sep=" "
        )
        # calculate nematic order here, not using file, because that info isn't
        # saved for animations
        S = np.linalg.eigvalsh(self.Q33_from_Q5(qmatrix))[:, -1]

        site_types = qtensor[:, -1]

        # infer Lx, Ly, Lz from filename
        self.Lx, self.Ly, self.Lz = np.asarray(
            (
                ' '.join(
                    qtensor_filename.split('/')[-1].split(
                        '.dat'
                    )[0].split('x')
                )
            ).split('_')[1].split(),
            dtype=int
        )
        self.data_stride = int(
            ((self.Lx * self.Ly * self.Lz) / len(qmatrix)) ** (1 / 3)
        )
        Z, Y, X = np.meshgrid(
            range(0, self.Lz, self.data_stride),
            range(0, self.Ly, self.data_stride),
            range(0, self.Lx, self.data_stride)
        )
        return np.concatenate(
            (
                np.array([X.flatten(), Y.flatten(), Z.flatten()]).T,
                qmatrix,
                np.array((site_types, S)).T
            ),
            axis=-1
        )

    # default visualizations

    def setup_defects(self):
        """ Add single-value isosurface of uniaxial order to visualize defects
        """
        max_S_value = np.max(self.fullmesh["order_uniaxial"])
        min_S_value = np.min(self.fullmesh["order_uniaxial"])

        # plot defects, unless range of S is very small
        show_defects = ((max_S_value - min_S_value) / max_S_value > 1e-4)
        self.add_filter_formula(
            "defects",
            filter_callable="contour",
            mesh_kwargs=self.settings["isosurface_kwargs"].copy(),
            scalars="order_uniaxial",
            isosurfaces=[(min_S_value + max_S_value) / 2],
        )
        self.set_actor_visibility("defects", bool(show_defects))

    def setup_boundaries(self, separate_meshes=False):
        """ Add isosurface or isosurfaces for boundaries.

        Parameters
        ----------
        separate_meshes : bool, optional
            Whether to create a separate isosurface mesh for each boundary.

        """

        boundary_vis_kwargs = dict()
        ovm_utilities.copy_from_dict_recursively(
            self.settings['isosurface_kwargs'],
            boundary_vis_kwargs
        )
        boundary_vis_kwargs["color"] = self.settings["boundaries_color"]

        if separate_meshes:
            for i in range(1, self.num_boundaries() + 1):
                actor_name = f"boundary_{i}"
                isosurfaces.update_isosurface(
                    self, actor_name, dataset_name=actor_name,
                    contour_values=[0.5], **boundary_vis_kwargs
                )
        else:
            actor_name = "boundaries"
            try:
                isosurfaces.update_isosurface(
                    self,
                    actor_name, dataset_name="nematic_sites",
                    contour_values=[0.5], **boundary_vis_kwargs
                )
            except (KeyError, ValueError):
                pass

    def setup_director(self):
        """ Add glyphs to visualize nematic director on a plane controlled by
        a plane widget

        """

        slice_name = "director_plane"
        widget_name = slice_name + "_widget"
        plane_widgets.setup_slice_widget(
            self,
            widget_name=widget_name,
            slice_name=slice_name,
            mesh_to_slice_name="fullmesh"
        )
        self[slice_name].set(opacity=0.1, color=self.settings['slice_color'])
        glyph_viz_kwargs = dict()
        ovm_utilities.copy_from_dict_recursively(
            self.settings["default_mesh_kwargs"],
            glyph_viz_kwargs
        )
        glyphs_name = "director"
        glyphs.add_glyphs_to_mesh(
            self,
            glyphs_name,
            mesh_name=slice_name,
            glyph_stride=2,
            orient="director",
            scale="nematic_sites",
            **glyph_viz_kwargs
        )
        return slice_name, glyphs_name, widget_name

    def set_director(self, director_data, mesh_name=None):
        """ Set director orientation data directly from an array.

        Parameters
        ----------
        director_data : np.ndarray
        mesh_name : str or None, optional
            Name of mesh on which to define director

        """
        mesh = self.get_mesh(mesh_name)
        mesh["director"] = director_data

    def set_qtensor(self, q_data, mesh):
        """ Set Q-tensor data directly from an array.

        Parameters
        ----------
        q_data : np.ndarray
            Q-tensor data with each row formatted as
            `[Qxx, Qxy, Qxz, Qyy, Qyz]`
        mesh : pyvista.DataSet


        Returns
        -------
        np.ndarray
            Q-tensor data reformatted such that each row contains the
            nine elements of the symmetric Q-tensor matrix, flattened.

        """

        Q33 = self.Q33_from_Q5(q_data)  # 3x3 Q matrix
        mesh["Q"] = Q33.reshape(-1, 9)
        mesh["director"] = self.n_from_Q(Q33)
        return Q33

    @wraps(FilterTreePlot.create_fullmesh)
    def create_fullmesh(self, dat=None, mesh_name="fullmesh", frame_num=None):
        frame_num, root_mesh = super().create_fullmesh(dat, mesh_name)
        if dat is not None and root_mesh is not None:
            self.nematic_calculations(dat, root_mesh)

        return frame_num, root_mesh

    def num_boundaries(self):
        """ Number of boundary objects

        Returns
        -------
        int
            Number of boundary objects
        """

        site_types = self.default_mesh["site_types"]
        return int(np.max(site_types))

    def nematic_calculations(self, dat, mesh):
        """ Assign Q-tensor and director field data to a mesh, and conduct
        calculations based on them.

        Parameters
        ----------
        dat : np.ndarray
            Data array of floats in the open-Qmin data_format:
            `x y z Qxx Qxy Qxz Qyy Qyz site_type S`
        mesh : pyvista.DataSet

        """

        try:
            mesh["site_types"] = dat[:, 8]
        except ValueError as err:
            print(err)
            return

        site_types = mesh["site_types"]

        Q_data = dat[:, 3:8]

        # Don't bother calculating eigenvectors inside objects
        Q_data[site_types > 0] = 0.

        Q33 = self.set_qtensor(Q_data, mesh)

        # boundaries:

        mesh["nematic_sites"] = 1 * (site_types <= 0)
        nematic_sites = mesh["nematic_sites"]
        nematic_sites3 = np.stack((nematic_sites,) * 3, axis=-1)
        self.xyz_arrays["Qij"] = calculations.list_to_xyzmat(
            Q33, self.array_dims()
        )

        self.xyz_arrays["diQjk"] = calculations.di(
            self.xyz_arrays["Qij"],
            self.array_dims(),
            data_stride=self.data_stride
        )
        diQjk = self.xyz_arrays["diQjk"]

        # returns eigenvalues in ascending order at each site
        Q_eigenvalues = np.linalg.eigvalsh(Q33)

        mesh["order_uniaxial"] = Q_eigenvalues[..., 2]

        site_types = mesh["site_types"]
        mesh["order_uniaxial"][site_types > 0] = np.max(
            mesh["order_uniaxial"]
        )  # fake values to avoid plotting defects behind boundaries

        mesh["inverse_order"] = (
                safe_inverse(mesh["order_uniaxial"]) * nematic_sites
        )

        mesh["order_biaxial"] = np.abs(
            Q_eigenvalues[..., 1] - Q_eigenvalues[..., 0]
        ) * nematic_sites
        self.xyz_arrays["n"] = calculations.list_to_xyzmat(
            mesh["director"], self.array_dims()
        )

        self.xyz_arrays["dinj"] = np.empty(self.xyz_arrays["n"].shape + (3,))

        def dot_prod_sgn(arr1: np.ndarray, arr2: np.ndarray) -> np.ndarray:
            """ Sign of dot product """
            return np.sign(np.sum(arr1 * arr2, axis=-1))

        def dot_prod_sgn_correct(
                arr_to_correct: np.ndarray,
                ref_arr: np.ndarray
        ):
            """ Flip sign of a vector array to ensure point-wise positive dot
            product with a reference array
            """
            dps = dot_prod_sgn(arr_to_correct, ref_arr)
            for ii in range(3):
                arr_to_correct[..., ii] *= dps

        for i in range(3):
            arr_up = np.roll(self.xyz_arrays["n"], -1, axis=i)
            dot_prod_sgn_correct(arr_up, self.xyz_arrays["n"])
            arr_dn = np.roll(self.xyz_arrays["n"], 1, axis=i)
            dot_prod_sgn_correct(arr_dn, self.xyz_arrays["n"])
            self.xyz_arrays["dinj"][..., i, :] = (
                    0.5 / self.data_stride * (arr_up - arr_dn)
            )

        S_inv = mesh["inverse_order"]

        mesh["active_force_Q"] = (
                -calculations.einstein_sum("iij", diQjk) * nematic_sites3
        )

        # energy:
        mesh["LdG_L1"] = nematic_sites * calculations.einstein_sum(
            "ijk, ijk", diQjk, diQjk
        )
        mesh["LdG_L2"] = nematic_sites * calculations.einstein_sum(
            "jij, kik", diQjk, diQjk
        )
        mesh["LdG_L3"] = nematic_sites * calculations.einstein_sum(
            "jik, kij", diQjk, diQjk
        )
        mesh["LdG_L6"] = nematic_sites * calculations.einstein_sum(
            "ij,ikl,jkl", self.xyz_arrays["Qij"], diQjk, diQjk
        )
        mesh["LdG_L4"] = nematic_sites * calculations.einstein_sum(
            "lik, lj, kij", levi_civita, self.xyz_arrays["Qij"], diQjk
        )
        L1 = mesh["LdG_L1"]
        L2 = mesh["LdG_L2"]
        L3 = mesh["LdG_L3"]
        L4 = mesh["LdG_L4"]
        L6 = mesh["LdG_L6"]

        mesh["LdG_L24"] = L2 - L3
        # mesh["LdG_L24"] = -nematic_sites * (
        #       calculations.einstein_sum("ijk, kij", diQjk, diQjk)
        #     - calculations.einstein_sum("iij, kjk", diQjk, diQjk)
        # )

        prefactor = (2 / 9) * S_inv ** 2
        tmp = 2 / 3 * S_inv * L6
        mesh["LdG_K1"] = prefactor * (-L1 / 3 + 2 * L2 - tmp)
        mesh["twist_Q"] = -2 * prefactor * L4
        self.set_q0(self.q0)  # <- calculate LdG_K2 here
        mesh["LdG_K3"] = prefactor * (L1 / 3 + tmp)
        mesh["saddle-splay_Q"] = 2 * prefactor * mesh["LdG_L24"]
        mesh["cholestericity"] = (
                mesh["twist_Q"] ** 2 - 2 * mesh["saddle-splay_Q"]
        )
        mesh["saddle-splay_vector_Q"] = nematic_sites3 * (
            calculations.einstein_sum(
                "ik, ijk", self.xyz_arrays["Qij"], diQjk
            )
            - calculations.einstein_sum(
                "jk, iik", self.xyz_arrays["Qij"], diQjk
            )
        )

        self.xyz_arrays["dijQkl"] = calculations.dij(
            self.xyz_arrays["Qij"],
            self.array_dims(),
            data_stride=self.data_stride
        )
        mesh["splay-bend"] = (
                calculations.einstein_sum("ijij", self.xyz_arrays["dijQkl"])
                * nematic_sites
        )

    def calculate_defect_tangent(
            self, threshold=0.4, mesh_name='defects', field_name=None
    ):
        """ Calculate the unit tangent field to defect lines.

        Parameters
        ----------
        threshold : float or None, optional
            Minimum value of 1/S for deciding which points to include
            in tangent calculation.
        mesh_name : str, optional
            Name of defects mesh
        field_name : str or None, optional
            Proposed name for new tangent vector field array

        Returns
        -------
        str
            Name of new tangent vector field array

        """

        defects_formula = self.get_filter_formula(mesh_name)
        if field_name is None:
            field_name = mesh_name + "_tangent"

        def filter_formula_calculate_tangent(filter_formula: FilterFormula):
            """ Callback to calculate tangent of mesh """
            this_mesh = filter_formula.mesh
            glyphs.calculate_tangent(
                filter_formula.plotter.dims(),
                this_mesh, this_mesh['inverse_order'],
                threshold=None if threshold is None else 1 / threshold,
                field_name=field_name
            )

        defects_formula.do_after_update.append(
            filter_formula_calculate_tangent
        )
        defects_formula.update()
        return field_name

    def calculate_and_show_defect_tangent(
            self, glyph_stride=2, field_name=None, scale="nematic_sites",
            mesh_name="defects", threshold=0.4
    ):
        """ Display disclination line tangent vectors as glyphs

        Parameters
        ----------
        glyph_stride : int, optional
            Typical spacing between glyphs
        field_name : str or None, optional
            Proposed name for new tangent vector field array
        scale : str, optional
            Name of scalar array to use for glyph size scale.
        mesh_name : str, optional
            Name of defects mesh
        threshold : float or None, optional
            Minimum value of 1/S for deciding which points to include
            in tangent calculation.

        Returns
        -------
        str
            Name of new glyphs mesh

        """
        field_name = self.calculate_defect_tangent(
            field_name=field_name, mesh_name=mesh_name, threshold=threshold
        )
        glyphs_name = field_name + "_glyph"
        glyphs.add_glyphs_to_mesh(
            self,
            glyphs_name,
            mesh_name=mesh_name,
            glyph_stride=glyph_stride,
            orient=field_name,
            scale=scale,
            **self.settings["default_mesh_kwargs"]
        )
        return glyphs_name

    def set_q0(self, q0):
        """ Set energetically preferred cholesteric wavenumber.

        (Re)calculates arrays that depend on q0.

        Parameters
        ----------
        q0 : float
            Chiral wavenumber

        Returns
        -------

        """
        self.q0 = q0
        self.fullmesh["LdG_K2"] = (
                self.fullmesh["nematic_sites"]
                * (self.fullmesh["twist_Q"] + self.q0) ** 2
        )

    def calculate_n_gradients(self, refresh=True):
        """ Calculate gradients of director field """
        nematic_sites = self.fullmesh["nematic_sites"]
        nematic_sites3 = np.stack((nematic_sites,) * 3, axis=-1)

        n = self.xyz_arrays["n"]
        dinj = self.xyz_arrays["dinj"]

        self.fullmesh["splay"] = (
                calculations.einstein_sum("ii", dinj) * nematic_sites
        )
        self.fullmesh["splay_vec"] = calculations.einstein_sum(
            "i, jj", n, dinj
        ) * nematic_sites3

        self.fullmesh["curl_n"] = calculations.einstein_sum(
            "ijk,jk", levi_civita, dinj
        ) * nematic_sites3
        self.fullmesh["twist_n"] = calculations.einstein_sum(
            "i,ijk,jk", n, levi_civita, dinj
        ) * nematic_sites
        self.fullmesh["bend"] = (
                calculations.einstein_sum("i, ij", n, dinj) * nematic_sites3
        )
        self.fullmesh["saddle-splay_vector_n"] = (
                self.fullmesh["bend"] - self.fullmesh["splay_vec"]
        )

    def calculate_frank_energy_comps(self):
        """ Calculate splay, twist, and bend Frank free energy densities
        and related quantities.

        """
        self.calculate_n_gradients(refresh=False)
        nematic_sites = self.fullmesh["nematic_sites"]
        dinj = self.xyz_arrays["dinj"]

        self.fullmesh["Frank_K1"] = self.fullmesh["splay"] ** 2 * nematic_sites
        self.fullmesh["Frank_K2"] = (
                                            (self.fullmesh["twist_n"] + self.q0) ** 2
                                    ) * nematic_sites
        self.fullmesh["Frank_K3"] = (
                np.sum(self.fullmesh["bend"] ** 2, axis=-1)
                * nematic_sites
        )

        self.fullmesh["Frank_K24"] = (
                                             calculations.einstein_sum("ij,ji", dinj, dinj)
                                             - self.fullmesh["Frank_K3"]
                                     ) * nematic_sites
        self.fullmesh["Frank_oneconst"] = (
                self.fullmesh["Frank_K1"]
                + self.fullmesh["Frank_K2"]
                + self.fullmesh["Frank_K3"]
        )
        self.refresh()

    def calculate_rotation_vector_n(self):
        """ Calculate a combination of derivatives on the director field that,
        for an ideal disclination, gives the rotation vector (a.k.a. Frank vector)
        up to sign. """
        if any(
                key not in self.fullmesh.array_names
                for key in ("curl_n", "twist_n")
        ):
            self.calculate_n_gradients(refresh=False)
        self.fullmesh["rotation_vector_n"] = (
            self.fullmesh["curl_n"]
            - self.fullmesh["director"] * np.stack(
                (self.fullmesh["twist_n"],) * 3, axis=-1
            )
        )
        tmp = np.sqrt(
            safe_inverse(
                np.sum(self.fullmesh["rotation_vector_n"] ** 2, axis=-1)
            )
        )
        self.fullmesh["rotation_vector_n"] *= np.stack((tmp,) * 3, axis=-1)
        self.refresh()

    @staticmethod
    def max_re_eigval(a):
        """
        Maximum real part of eigenvalue for eigenvalues in an array of 3x3 matrices

        Parameters
        ----------
        a: np.ndarray | pyvista.pyvista_ndarray
            Array of 3x3 matrices

        Returns
        -------
        np.ndarray
            Maximum real part of eigenvalues
        """

        return np.max(np.real(np.linalg.eigvals(a.reshape(-1, 3, 3))), axis=-1)

    def calculate_chi_tensor(self, refresh=True):
        """ Calculate Efrati-Irvine handedness pseudotensor.

        Creates the tensor array "Chi",
        the symmetric tensor arrays "Chi^T Chi" and "Chi Chi^T",
        and the vector array "Omega from Chi".
        Note that this calculation extends Efrati and Irvine's
        construction to the Q-tensor rather than relying on the director
        field.
        """
        chi_Q = calculations.einstein_sum(
            "lm, jlk, ikm",
            self.xyz_arrays["Qij"],
            levi_civita,
            self.xyz_arrays["diQjk"],
            reshape=False
        )
        self.fullmesh["Chi"] = calculations.einstein_sum("ij", chi_Q)
        chi_Q_flat = calculations.xyzmat_to_list(chi_Q)
        self.fullmesh["Chi^T Chi"] = (
            calculations.transpose_times_matrix(chi_Q_flat).reshape(-1, 9)
        )
        self.fullmesh["Chi Chi^T"] = (
            calculations.matrix_times_transpose(chi_Q_flat).reshape(-1, 9)
        )
        self.fullmesh["Omega from Chi"] = np.linalg.eigh(
            self.fullmesh["Chi^T Chi"].reshape(-1, 3, 3)
        )[1][:, :, -1]
        self.fullmesh["Chi_max_re_eigval"] = self.max_re_eigval(
            self.fullmesh["Chi"]
        )

        if refresh:
            self.refresh()

    # noinspection PyPep8Naming
    def calculate_D_tensor(self, refresh=True):
        """ Calculate Schimming-Viñals tensor.

        Creates the tensor array "D_tensor",
        and the symmetric tensor arrays "D^T D" and "D D^T".

        """

        diQjk = self.xyz_arrays["diQjk"]
        self.xyz_arrays["D_tensor"] = np.zeros((diQjk.shape[:-1]))
        D_tensor = self.xyz_arrays["D_tensor"]
        for g in range(3):
            for i in range(3):
                for m in range(3):
                    for n in range(3):
                        lc1 = levi_civita[g, m, n]
                        if lc1 != 0:
                            for ell in range(3):
                                for k in range(3):
                                    lc2 = levi_civita[i, ell, k]
                                    if lc2 != 0:
                                        D_tensor[..., g, i] += (
                                            lc1 * lc2
                                            * np.sum(
                                                diQjk[..., ell, m, :]
                                                * diQjk[..., k, n, :],
                                                axis=-1
                                            )
                                        )

        self.fullmesh["D_tensor"] = calculations.einstein_sum(
            "ij", D_tensor
        )
        D_tensor = calculations.xyzmat_to_list(D_tensor)
        self.fullmesh["D^T D"] = calculations.transpose_times_matrix(D_tensor)
        self.fullmesh["D^T D"] = self.fullmesh["D^T D"].reshape(-1, 9)
        self.fullmesh["D D^T"] = calculations.matrix_times_transpose(D_tensor)
        self.fullmesh["D D^T"] = self.fullmesh["D D^T"].reshape(-1, 9)
        self.fullmesh["D_max_re_eigval"] = self.max_re_eigval(
            self.fullmesh["D_tensor"]
        )
        self.fullmesh["Omega from D"] = np.linalg.eigh(
            self.fullmesh["D D^T"].reshape(-1, 3, 3)
        )[1][:, :, -1]
        if refresh:
            self.refresh()

    # noinspection PyPep8Naming
    def calculate_Westin_metrics(self):
        """ Calculate Westin scalars measures from the Q-tensor.

        Creates the scalar arrays "Westin_l", "Westin_p", "Westin_s".

        """

        nematic_sites = self.fullmesh["nematic_sites"]
        self.fullmesh["Westin_l"] = (
                                            self.fullmesh["order_uniaxial"] - self.fullmesh["order_biaxial"]
                                    ) * nematic_sites
        self.fullmesh["Westin p"] = (
                4 * self.fullmesh["order_biaxial"] * nematic_sites
        )
        self.fullmesh["Westin_s"] = (
                                            1 - self.fullmesh["order_uniaxial"]
                                            - 3 * self.fullmesh["order_biaxial"]
                                    ) * nematic_sites

    # noinspection PyPep8Naming
    @staticmethod
    def Q33_from_Q5(q5):
        """ Convert 5-component Q-tensor information to 3x3 matrix """
        (Qxx, Qxy, Qxz, Qyy, Qyz) = q5.T
        q_matrix = np.moveaxis(np.array([
            [Qxx, Qxy, Qxz],
            [Qxy, Qyy, Qyz],
            [Qxz, Qyz, -Qxx - Qyy]
        ]), -1, 0)
        return q_matrix

    # noinspection PyPep8Naming
    @staticmethod
    def n_from_Q(q_matrix):
        """Get director from 3x3-matrix Q-tensor data"""
        _, eigvecs = np.linalg.eigh(q_matrix)
        return eigvecs[..., -1]

    # noinspection PyPep8Naming
    @staticmethod
    def Q_from_n(director_data):
        """ Create Q-tensor array from director data assuming S_0=1 """
        return 3 / 2 * (
                np.einsum("...i,...j->...ij", director_data, director_data)
                - np.eye(3) / 3
        )

    @staticmethod
    def director_data_import(filename, sep="\t"):
        """ Import nematic director data and convert it into
        Q-tensor data in the open-Qmin data_format.

        Parameters
        ----------
        filename : str
        sep : str, optional

        Returns
        -------
        np.ndarray
            Q-tensor data in the open-Qmin data_format.

        """

        director_data = ovm_utilities.fast_import(filename, sep=sep)
        x, y, z, nx, ny, nz = director_data.T
        x -= np.min(x)
        y -= np.min(y)
        z -= np.min(z)
        Qxx = 3 / 2 * (nx * nx - 1 / 3)
        Qxy = 3 / 2 * nx * ny
        Qxz = 3 / 2 * nx * nz
        Qyy = 3 / 2 * (ny * ny - 1 / 3)
        Qyz = 3 / 2 * ny * nz
        open_Qmin_style_data = np.array([
            x, y, z,
            Qxx, Qxy, Qxz, Qyy, Qyz,
            np.zeros_like(x), np.ones_like(x)
        ]).T
        return open_Qmin_style_data

    def auto_setup(self, notebook=True):
        """ Routines to run after first importing data. """
        self.view_frame(self.frame_num)
        self.setup_boundaries()
        self.setup_defects()
        self.setup_director()
        self.reset_camera()
        # for in-notebook plots, add widgets to control defects isosurface,
        # and director plane
        if notebook:
            self.add_isosurface_slider("defects")
            self.add_bounding_box()
        self.do_auto_setup = False

    def analyze_disclinations(
            self, defects_mesh_name="defects", n_centroids=50,
            tangents_glyph_name=None,
            **kwargs
    ):
        """ For several "centroid" points along disclination lines, show
        the director winding character.

        Parameters
        ----------
        defects_mesh_name: str
        n_centroids: int
        tangents_glyph_name: str, optional
        **kwargs: dict, optional
            Keyword arguments to :meth:`analyze_disclinations_at_points`
        """
        defects_formula = self.get_filter_formula(defects_mesh_name)

        self.set_actor_visibility(tangents_glyph_name, False)

        centroids_name = defects_formula.get_centroids(
            num_centroids=n_centroids
        )

        self.set_actor_visibility(centroids_name, False)
        self.analyze_disclinations_at_points(
            centroids_name, **kwargs
        )

    def analyze_disclinations_at_points(
            self, points_mesh_name, radius=3, n_samples=16, use_ints=False
    ):
        """

        Parameters
        ----------
        points_mesh_name: str
        radius: int
        n_samples: int
        use_ints: bool

        Returns
        -------
        :class:`~open_viewmin.filter_tree_plot.filter_formulas.FilterFormula`

        """
        # Calculate defect tangent at the centroids
        normals_name = self.calculate_defect_tangent(
            mesh_name=points_mesh_name
        )

        circuits_formula = self.add_circuits_around_points(
            points_mesh_name, normals_name, radius=radius,
            n_samples=n_samples, use_ints=use_ints
        )
        return self.plot_director_glyphs_on_circuits(
            points_mesh_name, circuits_formula
        )

    def analyze_disclination_at_picked_point(
            self,
            **kwargs
    ):
        """ Show director winding character at a picked point on a disclination

        Parameters
        ----------
        **kwargs: dict, optional
            Keyword arguments to :meth:`analyze_disclinations_at_points`
        """

        point_mesh_name = self.probe_at_picked_surface_point()
        self.analyze_disclinations_at_points(point_mesh_name, **kwargs)

    def pick_point_analyze_disclination(self, **kwargs):
        """ Enable surface picking mode and analyze director winding about the
        picked point, assumed to be on a disclination.

        Parameters
        ----------
        **kwargs: dict, optional
            Keyword arguments to :meth:`analyze_disclinations_at_points`

        """
        self.enable_surface_picking(
            lambda _: self.analyze_disclination_at_picked_point(**kwargs)
        )

    def plot_director_glyphs_on_circuits(
            self, points_mesh_name, circuits_formula,
            result_name='director_on_circuits', defects_mesh_name='defects'
    ):
        """ Show director glyphs on small, closed-loop measuring circuits
        around disclinations at given points.

        Parameters
        ----------
        points_mesh_name: str
        circuits_formula: :class:`~open_viewmin.filter_tree.filter_formulas.FilterFormula`
        result_name: str
        defects_mesh_name: str

        Returns
        -------
        :class:`~open_viewmin.filter_tree_plot.filter_formulas.FilterFormula`

        """
        result_name = self.name_mesh_without_overwriting(result_name)
        self[result_name] = circuits_formula.add_glyphs(
            orient="local_director", factor=2, scale="ones",
            glyph_shape="cylinder", glyph_stride=1
        )
        self[result_name].set(
            scalars="LdG_L24", clim=[-0.02, 0.02], cmap="turbo_r"
        )
        points_mesh_formula = self.get_filter_formula(points_mesh_name)
        points_mesh_formula.do_after_update.append(
            lambda filter_formula: filter_formula.get_eigenvec(
                "Q", evec_idx=0, new_dataset_name="omega"
            )
        )
        points_mesh_formula.update()
        omega_name = self.name_mesh_without_overwriting("Omega")
        self[omega_name] = points_mesh_formula.add_filter(
            "glyph", orient="omega", factor=4, scale="ones"
        )
        omega_formula = self.get_filter_formula(omega_name)
        omega_formula.set_glyph_shape("dbl_headed_arrow")
        # omega_formula.update()
        #
        # def calculate_beta(filter_formula):
        #     dot_prod = np.dot(
        #             filter_formula["omega"], filter_formula["defects_tangent"]
        #         )
        #     dot_prod *= -np.sign(
        #         dot_prod * filter_formula["LdG_L24"]
        #     )
        #     filter_formula.mesh["beta"] = np.arccos(dot_prod)
        #     filter_formula.update(update_actor=True)
        #
        # omega_formula.do_after_update.append(calculate_beta)
        # omega_formula.update()
        omega_formula.set(
            scalars="LdG_L24", clim=[-0.1, 0.1], cmap="turbo_r"
        )

        self[result_name].update()
        omega_formula.update()

        self[defects_mesh_name].set(opacity=0.2)

        return self[defects_mesh_name]

    def add_circuits_around_points(
            self, points_mesh_name, normals_name, radius=3, n_samples=16,
            use_ints=False
    ):
        """ Create small, closed-loop meshes around a set of points, for use in
        plotting director winding near disclinations.

        Parameters
        ----------
        points_mesh_name: str
        normals_name: str
        radius: float
        n_samples: int
        use_ints: bool

        Returns
        -------
        :class:`~open_viewmin.filter_tree_plot.filter_formulas.FilterFormula`

        """
        circuits_name = sample.add_circuit_probe_filter_formula(
            self,
            points_mesh_name,
            mesh_to_probe_name="fullmesh",
            normals_name=normals_name,
            radius=radius,
            n_samples=n_samples,
            use_ints=use_ints
        )
        circuits_formula = self.get_filter_formula(circuits_name)
        circuits_formula.do_after_update.append(
            lambda filter_formula: filter_formula.get_eigenvec(
                "Q", new_dataset_name="local_director"
            )
        )
        self.set_actor_visibility(circuits_name, False)
        circuits_formula.update()
        return circuits_formula

    def show_jones(self, **kwargs):
        """ Display optical transmission, calculated from Jones matrices,
        on three planes.

        Parameters
        ----------
        **kwargs : dict, optional
        """
        jones.add_jones_to_plotter(self, **kwargs)
