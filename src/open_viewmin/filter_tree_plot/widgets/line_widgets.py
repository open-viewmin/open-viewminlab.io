"""
Utilities for working with PyVista line widgets
"""

from pyvista import Line


def setup_line_widget(
    plotter, widget_name=None, new_mesh_name="line_mesh", parent_mesh_name=None
):
    """ Set up a line widget

    Parameters
    ----------
    plotter : :class:`~open_viewmin.filter_tree_plot.filter_tree_plot.FilterTreePlot`
    widget_name : str or `None`
    new_mesh_name : str
    parent_mesh_name : str or `None`
    """

    new_mesh_name = plotter.name_mesh_without_overwriting(new_mesh_name)
    if widget_name is None:
        widget_name = new_mesh_name + "_widget"
        widget_name = plotter.name_widget_without_overwriting(widget_name)

    def configure_widget(widget):
        """

        Parameters
        ----------
        widget : vtkmodules.vtkInteractionWidgets.vtkLineWidget

        Returns
        -------
        bool, tuple
        """

        if widget is None:
            enabled = True
        else:
            enabled = widget.GetEnabled()
        return enabled, ()

    plotter.add_filter_formula(
        name=new_mesh_name,
        parent_mesh_name=parent_mesh_name,
        filter_callable="sample_over_line",
        filter_kwargs=dict(
            pointa=(0, plotter.Ly / 2, plotter.Lz / 2),
            pointb=(plotter.Lx, plotter.Ly / 2, plotter.Lz / 2)
        )
    )

    def generate_line_widget_callback(callback_plotter, mesh_name):
        """ Create callable to update
        :class:`~open_viewmin.filter_tree_plot.filter_formulas.FilterFormula`
        based on Line Widget

        Parameters
        ----------
        callback_plotter :class:`~open_viewmin.filter_tree_plot.filter_tree_plot.FilterTreePlot`
        mesh_name : str

        Returns
        -------
        callable

        """
        def line_widget_callback(point_a, point_b):
            callback_plotter.get_filter_formula(mesh_name).update(
                # points=Line(pointa=point_a, pointb=point_b)
                pointa=point_a, pointb=point_b
            )

        return line_widget_callback

    if not plotter.off_screen:
        plotter.add_widget_formula(
            name=widget_name,
            mesh_name=new_mesh_name,
            callback=generate_line_widget_callback,
            creator="add_line_widget",
            configure=configure_widget,
            factor=plotter.settings["widget_factor"],
            color=plotter.settings["widget_outline_color"],
            use_vertices=True
        )
