""" Utilities for working with PyVista plane widgets
"""

import numpy as np
from open_viewmin.filter_tree_plot.filters import isosurfaces
from open_viewmin.filter_tree_plot.utilities import ovm_utilities


def new_slice_mesh(
    plotter, slice_name=None, mesh_to_slice_name="fullmesh",
    normal=(1.0, 0.0, 0.0), origin=None, **mesh_kwargs
):
    """ Add a new `FilterFormula` with a `slice` filter

    Parameters
    ----------
    plotter : :class:`~open_viewmin.filter_tree_plot.filter_tree_plot.FilterTreePlot`
    slice_name: str or `None`
    mesh_to_slice_name: str
    normal: (float, float, float)
    origin: (float, float, float) or `None`
    **mesh_kwargs
        Keyword arguments to `FilterTreePlot.add_filter_formula`

    Returns
    -------
    pyvista.dataset or `None`
        New child mesh created by filter formula, or `None` if no filter
        formula was created.

    """
    if origin is None:
        origin = [dim / 2 for dim in plotter.dims()]
    if slice_name is None:
        slice_name = mesh_to_slice_name + "_slice"
    use_mesh_kwargs = ovm_utilities.copy_from_dict_recursively(
        plotter.settings['default_slice_kwargs']
    )
    ovm_utilities.copy_from_dict_recursively(mesh_kwargs, use_mesh_kwargs)
    slice_mesh = plotter.add_filter_formula(
        name=slice_name,
        parent_mesh_name=mesh_to_slice_name,
        filter_callable="slice",
        filter_kwargs=dict(normal=normal, origin=origin),
        mesh_kwargs=use_mesh_kwargs
    )
    return slice_mesh


def setup_slice_widget(
    plotter, widget_name=None, slice_name=None, mesh_to_slice_name="fullmesh",
    **mesh_kwargs
):
    """ Set up a slice widget

    Parameters
    ----------
    plotter : :class:`~open_viewmin.filter_tree_plot.filter_tree_plot.FilterTreePlot`
    widget_name : str or `None`
    slice_name : str or `None`
    mesh_to_slice_name : str
    **mesh_kwargs
        Keyword arguments to :func:`new_slice_mesh`
    """
    if slice_name is None and widget_name is not None:
        slice_name = widget_name.replace('_widget', '')
    if widget_name is None:
        widget_name = slice_name + "_widget"
    normal, origin = default_slice_kwargs(plotter).values()
    if slice_name not in plotter.actor_names():
        new_slice_mesh(
            plotter,
            slice_name=slice_name,
            mesh_to_slice_name=mesh_to_slice_name,
            normal=normal,
            origin=origin,
            **mesh_kwargs
        )
    add_slice_widget_to_slice_mesh(
        plotter,
        slice_name=slice_name,
        widget_name=widget_name
    )


def configure_plane_widget(plotter):
    """ Create callable used to configure plane widget

    Parameters
    ----------
    plotter : :class:`~open_viewmin.filter_tree_plot.filter_tree_plot.FilterTreePlot`

    Returns
    -------
    callable

    """
    def return_func(widget):
        if widget is None:
            normal, origin = default_slice_kwargs(plotter).values()
            enabled = True
        else:
            normal = widget.GetNormal()
            origin = widget.GetOrigin()
            enabled = widget.GetEnabled()
        return enabled, (normal, origin)
    return return_func


def default_slice_kwargs(plotter):
    """ Default normal and origin for `slice` filter

    Parameters
    ----------
    plotter : :class:`~open_viewmin.filter_tree_plot.filter_tree_plot.FilterTreePlot`

    Returns
    -------
    dict

    """
    return dict(
        normal=(1.0, 0.0, 0.0),
        origin=[dim / 2 for dim in plotter.dims()]
    )


def add_slice_aux(plotter, scalars_name=None):
    """ Create callable that adds a slice widget

    Parameters
    ----------
    plotter : :class:`~open_viewmin.filter_tree_plot.filter_tree_plot.FilterTreePlot`
    scalars_name : str or `None`

    Returns
    -------
    callable

    """
    def return_function():
        add_slice(plotter, scalars_name=scalars_name)
    return return_function


def add_slice(
    plotter, scalars_name=None, slice_name=None, widget_name=None,
    mesh_to_slice_name="fullmesh"
):
    """ Add slice widget

    Parameters
    ----------
    plotter : :class:`~open_viewmin.filter_tree_plot.filter_tree_plot.FilterTreePlot`
    scalars_name : str or `None`
    slice_name : str or `None`
    widget_name : str or `None`
    mesh_to_slice_name : str

    Returns
    -------
    (str, str)
        Slice name, widget name

    """
    if scalars_name is None:
        color = plotter.settings["slice_color"]
        slice_name = "new_slice"
    else:
        color = None
        if slice_name is None:
            slice_name = plotter.name_mesh_without_overwriting(
                scalars_name + "_slice"
            )
    if widget_name is None:
        widget_name = ovm_utilities.name_without_overwriting(
            slice_name + "_widget",
            plotter.widgets
        )
    mesh_kwargs = ovm_utilities.copy_from_dict_recursively(
        plotter.settings["default_mesh_kwargs"]
    )
    mesh_kwargs["color"] = color
    mesh_kwargs["scalars"] = scalars_name

    setup_slice_widget(
        plotter,
        widget_name,
        slice_name=slice_name,
        mesh_to_slice_name=mesh_to_slice_name,
        **mesh_kwargs
    )
    plotter.set_actor_visibility(slice_name, 1)

    return slice_name, widget_name


def slice_widget_callback(plotter, slice_name):
    """ Create callback for updated slice widget

    Parameters
    ----------
    plotter : :class:`~open_viewmin.filter_tree_plot.filter_tree_plot.FilterTreePlot`
    slice_name : str

    Returns
    -------
    callable
    """

    def return_func(widget_normal, widget_origin):
        plotter.update_filter(
            slice_name,
            update_actor=True,
            normal=widget_normal,
            origin=widget_origin
        )
    return return_func


def add_slice_widget_to_slice_mesh(
    plotter, slice_name=None, widget_name=None
):
    if slice_name in plotter.mesh_names():
        if widget_name is None:
            widget_name = slice_name + "_widget"
        normal, origin = default_slice_kwargs(plotter).values()
        bounds_min_xyz = np.min(plotter.get_points(), axis=0)
        bounds_max_xyz = np.max(plotter.get_points(), axis=0)
        bounds = np.array([bounds_min_xyz, bounds_max_xyz]).T.flatten()
        if not plotter.off_screen:
            plotter.add_widget_formula(
                name=widget_name,
                mesh_name=slice_name,
                creator="add_plane_widget",
                callback=slice_widget_callback,
                configure=configure_plane_widget(plotter),
                factor=plotter.settings["widget_factor"],
                color=plotter.settings["widget_color"],
                tubing=True,
                bounds=bounds
            )
        else:
            slice_widget_callback(plotter, slice_name)(normal, origin)


def alter_plane_widget(
    widget_formula, theta=None, phi=None, origin=None, dtheta=0., dphi=0.,
    d_origin=(0., 0., 0.)
):
    """ Change normal and/or origin of a plane widget,
    and update associated mesh (and possibly actor(s)) accordingly

    Parameters
    ----------
    widget_formula : :class:`~open_viewmin.filter_tree_plot.widget_formulas.WidgetFormula`
    theta : float or `None`
    phi : float or `None`
    origin : (float, float, float) or `None`
    dtheta : float
    dphi : float
    d_origin : (float, float, float)
    """

    widget = widget_formula.widget
    if widget is None:
        return
    normal_x, normal_y, normal_z = widget.GetNormal()
    slice_origin = widget.GetOrigin()
    normal_theta = np.arccos(normal_z)
    normal_phi = np.arctan2(normal_y, normal_x)
    if theta is not None:
        normal_theta = theta
    if phi is not None:
        normal_phi = phi
    if origin is not None:
        slice_origin = origin
    normal_theta += dtheta
    normal_phi += dphi
    slice_origin = np.array(slice_origin) + np.array(d_origin)
    widget.SetNormal(
        np.sin(normal_theta) * np.cos(normal_phi),
        np.sin(normal_theta) * np.sin(normal_phi),
        np.cos(normal_theta)
    )
    widget.SetOrigin(slice_origin)
    widget_formula.update()


def add_filter_widget_clip_plane(plotter, mesh_name):
    """ Add plane widget for `clip` filter

    Parameters
    ----------
    plotter : :class:`~open_viewmin.filter_tree_plot.filter_tree_plot.FilterTreePlot`
    mesh_name : str

    """
    if mesh_name in plotter.mesh_names():
        new_mesh_name = plotter.name_mesh_without_overwriting(
            mesh_name + "_plane_clipped"
        )

        def generate_clip_plane_callback(callback_plotter, callback_mesh_name):
            def clip_plane_callback(normal, origin):
                filter_info = callback_plotter.get_filter_formula(
                    callback_mesh_name
                )
                scalars = filter_info.mesh_kwargs.get("scalars")
                if scalars is None:
                    color = filter_info.mesh_kwargs.get(
                        "color", callback_plotter.settings['slice_color']
                    )
                else:
                    color = None
                mesh_kwargs = dict(color=color, scalars=scalars)
                callback_plotter.add_filter_formula(
                    name=new_mesh_name,
                    parent_mesh_name=callback_mesh_name,
                    filter_callable="clip",
                    filter_kwargs=dict(normal=normal, origin=origin),
                    mesh_kwargs=mesh_kwargs
                )
            return clip_plane_callback
        widget_name = plotter.name_widget_without_overwriting(
            mesh_name+'_plane_widget'
        )
        plotter.add_widget_formula(
            name=widget_name,
            mesh_name=mesh_name,
            creator="add_plane_widget",
            callback=generate_clip_plane_callback,
            configure=configure_plane_widget(plotter),
            tubing=True,
            factor=plotter.settings['widget_factor'],
            color=plotter.settings['widget_outline_color']
        )
        # make unclipped mesh invisible immediately
        if mesh_name in plotter.actor_names():
            plotter.set_actor_visibility(mesh_name, 0)

        plotter.refresh()


def add_pontryagin_thom_widget(plotter, vectors_name, apolar=True, name=None):
    """ Add a Pontryagin-Thom surface.

    Finds the surface(s) of points where the orientation lies in the plane
    of directions
    perpendicular to `normal_dir`, and colors the surface by angle in this
    plane. For uses of the Pontryagin-Thom construction in nematic liquid
    crystals, see:
    Chen, Ackerman, Alexander, Kamien, and Smalyukh (2013),
    Generating the Hopf fibration experimentally in nematic liquid crystals.
    Phys. Rev. Lett. 110, 237801.
    https://doi.org/10.1103/PhysRevLett.110.237801

    Čopar, Porenta and Žumer (2013),
    Visualisation methods for complex nematic fields,
    Liquid Crystals, 40:12, 1759-1768.
    https://doi.org/110.1080/02678292.2013.853109.

    Machon and Alexander (2014),
    Knotted Defects in Nematic Liquid Crystals,
    Phys. Rev. Lett. 113, 027801.
    https://doi.org/110.1103/PhysRevLett.113.027801

    """
    actor_name = name
    if actor_name is None:
        actor_name = plotter.name_mesh_without_overwriting(
            vectors_name + "_PT"
        )

    mesh_kwargs = {
        **plotter.settings["isosurface_kwargs"].copy(),
        **dict(
            cmap="hsv",
            preference="cell",
            interpolate_before_map=False  # avoid color jumps at branch cuts
        )
    }

    plotter.add_filter_formula(
        actor_name,
        filter_callable="contour",
        mesh_kwargs=mesh_kwargs,
        ovm_info=dict(apolar=apolar),
        apply_upon_creation=False
    )

    def callback_generator(callback_plotter, pontryagin_thom_surface_name):

        def new_normal_callback(normal, _):
            filter_formula = callback_plotter.get_filter_formula(
                pontryagin_thom_surface_name
            )
            parent_mesh_name = filter_formula.parent_mesh_name
            theta_key, phi_key, apolar_phi_key = (
                isosurfaces.calculate_pontryagin_thom_surface(
                    callback_plotter.get_mesh(parent_mesh_name),
                    vectors_name,
                    normal_dir=normal
                )
            )

            filter_formula.update(scalars=theta_key, update_actor=False)
            current_apolar = filter_formula.ovm_info.get("apolar", False)
            filter_formula.update_actor(
                scalars=apolar_phi_key if current_apolar else phi_key,
                clim=[0, (1 if current_apolar else 2) * np.pi]
            )

        return new_normal_callback

    widget_name = ovm_utilities.name_without_overwriting(
        actor_name + "_normal", plotter.widgets
    )

    plotter.add_widget_formula(
        name=widget_name,
        mesh_name=actor_name,
        creator="add_plane_widget",
        callback=callback_generator,
        configure=configure_plane_widget(plotter),
        color=plotter.settings["widget_color"],
        tubing=True,
        factor=plotter.settings["widget_factor"]
    )
    return actor_name
