""" Utilities for working with PyVista box widgets """

import numpy as np


def get_box_faces_info(box):
    """ Helper function for clipping by a box """
    faces_vertices = box.points[(box.faces.reshape(-1, 5)[:, 1:],)]
    normals = ()
    origins = ()
    for face_vertices in faces_vertices:
        origins += (np.average(face_vertices, axis=0),)
        normal = np.cross(
            face_vertices[1] - face_vertices[0],
            face_vertices[2] - face_vertices[1]
        )
        normal /= np.sqrt(np.sum(normal ** 2, axis=-1))
        normals += (normal,)
    return normals, origins


def configure_box_widget(widget):
    if widget is None:
        return True, ()
    else:
        return widget.GetEnabled(), ()


def add_filter_widget_clip_box(plotter, mesh_name):
    if mesh_name in plotter.mesh_names():
        new_mesh_name = plotter.name_mesh_without_overwriting(
            mesh_name + "_box_clipped"
        )

        def generate_clip_box_callback(callback_plotter, callback_mesh_name):

            def clip_box_callback(box):
                filter_formula = callback_plotter.filter_formulas[
                    callback_mesh_name
                ]
                scalars = filter_formula.mesh_kwargs.get('scalars')
                if scalars is None:
                    color = filter_formula.mesh_kwargs.get(
                        'color', callback_plotter.settings['actor_color']
                    )
                else:
                    color = None
                normals, origins = get_box_faces_info(box)

                def mesh_creator(mesh):
                    def return_func():
                        clipped_mesh = mesh
                        for normal, origin in zip(normals, origins):
                            clipped_mesh = clipped_mesh.clip(
                                normal=normal, origin=origin
                            )
                        return clipped_mesh
                    return return_func

                callback_plotter.add_filter_formula(
                    name=new_mesh_name,
                    parent_mesh_name=callback_mesh_name,
                    filter_callable=mesh_creator,
                    has_actor=True,
                    mesh_kwargs=dict(color=color, scalars=scalars)
                )
            return clip_box_callback

        widget_name = plotter.name_widget_without_overwriting(
            mesh_name+'_box_widget'
        )
        plotter.add_widget_formula(
            name=widget_name,
            mesh_name=mesh_name,
            creator="add_box_widget",
            callback=generate_clip_box_callback,
            configure=configure_box_widget,
            color=plotter.settings['widget_outline_color'],
            factor=plotter.settings['widget_factor']
        )
        # make unclipped mesh invisible immediately
        if mesh_name in plotter.actor_names():
            plotter.set_actor_visibility(mesh_name, 0)

        plotter.refresh()
