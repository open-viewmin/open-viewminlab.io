""" Define class extending `pyvista.Plotter` with filter formulas and a
filters tree """

import pyvista as pv
import numpy as np
from time import sleep
import glob
from functools import wraps
from . import settings
from .utilities import mpi_stitch, ovm_utilities
from .filters import glyphs
from .filters.sample import probe
from .filter_formulas import FilterFormula, WidgetFormula
from .widgets import plane_widgets


# noinspection PyMissingConstructor
class FilterTreePlot(pv.Plotter):
    """
    PyVista `Plotter` with meshes organized into a "filter tree".

    Updates to any mesh are automatically followed by updates
    to its child meshes and actors, recursively.

    Parameters
    ----------
    filenames : list[str], str, or None, optional
        Files to import.
    user_settings : dict or None, optional
        Customizations to default plotter settings.
    theme : ['dark' | 'document' | 'paraview' | None], optional
        PyVista plot theme
    reader : callable()
        Function to import and process data files
    dims : tuple of ints or None, optional
        System dimensions used if no filenames are given.
    data_stride : int, optional
        Spacing between grid sites used if no filenames are given.
    kwargs : dict, optional
        Keyword arguments to :meth:`pyvista.Plotter`

    """

    def __init__(
        self, filenames=None, user_settings=None, theme=None,
        reader=ovm_utilities.fast_import, dims=None, data_stride=1, **kwargs
    ):

        self.array_dims_defined = False
        self.axes = None
        self.filenames = []
        self.frame_num = -1  # so loading 0th frame counts as an update
        self.frame_data = []
        self.orbit_kwargs = dict()
        if user_settings is None:
            user_settings = {}
        if kwargs.get('off_screen') is not None:
            pv.start_xvfb()

        if theme in ['paraview', 'document', 'dark']:
            pv.set_plot_theme(theme)
        else:
            pv.set_plot_theme('dark')
        # Themes: use ['dark' | 'document' | 'paraview' ]
        # for [black, white, gray] background

        self.initialize_plotter(**kwargs)
        self.is_enabled_eye_dome_lighting = kwargs.get(
            "enable_eye_dome_lighting", False
        )

        self._set_pyvista_plotter_properties()
        self.oVM_theme = settings.Hopkins_theme

        # convenience objects and methods
        self.data = []
        self.meshes_dict = dict()
        self.filter_formulas = dict()
        self.widgets = dict()
        self.widget_formulas = dict()

        self.data_stride = data_stride

        self.settings = settings.make_settings_dict(
            user_settings, self.oVM_theme, self.off_screen
        )
        self.title = self.settings.get("title", "")
        self.theme.title = self.title
        self.default_mesh_name = self.settings.get(
            "default_mesh_name", "fullmesh"
        )
        self.actors_color_cycle = self.settings["actors_color_cycle"].copy()
        self.set_background()

        if "text_color" in self.settings.keys():
            self.theme['font']['color'] = self.settings["text_color"]

        # self.add_root_mesh()
        if filenames not in [None, []]:
            self.load(filenames, reader=reader)
        elif dims is not None:
            self.Lx, self.Ly, self.Lz = dims
            self.array_dims_defined = True
            self.create_fullmesh()
        # prepare glyph shapes
        self.geometric_objects = glyphs.make_geometric_objects_dict(
            self.settings
        )
        # if self.num_frames > 0:
        #     self.last_frame()  # display frame from last file imported
        self.orbit_path = self.set_orbit()
        self.set_lights_intensity(4)

        # it's unclear why this is necessary:
        self.off_screen = kwargs.get("off_screen", False)

    def set_background(self, *args, **kwargs):
        """ Set plotter's background color(s)

        If called with no arguments, retrieves color from
        `FilterTreePlot.settings["background_color"]`, defaulting to black.
        Otherwise, `args` and `kwargs` are sent to
        `pyvista.Plotter.set_background`.

        Parameters
        ----------
        args
        kwargs
        """

        if len(args) == 0:
            background_color = self.settings.get("background_color", "black")
            background_top_color = self.settings.get(
                "background_top_color", "black"
            )
            super().set_background(background_color, top=background_top_color)

        else:
            super().set_background(*args, **kwargs)

    @property
    def default_mesh(self):
        return self.get_mesh(self.default_mesh_name)

    def _set_pyvista_plotter_properties(self):
        """ Initialization choices for PyVista Plotter """

        self.theme.anti_aliasing = 'msaa'
        self.theme.multi_samples = 12

        self.theme.smooth_shading = True
        self.theme.render_points_as_spheres = True

        self.shadowsEnabled = False

        # prevent warnings about empty mesh e.g. if there are no boundaries
        self.theme.allow_empty_mesh = True

        self.fly_to_right_click_enabled = False

        # default to anaglyph for 3D stereo
        self.ren_win.SetStereoTypeToAnaglyph()

        # don't print VTK warnings
        self.ren_win.SetGlobalWarningDisplay(0)

        self.add_axes()

    def add_axes(self):
        """ Add x,y,z axes widget. """

        # xyz axes arrows
        if not self.off_screen:
            self.axes = self.renderer.add_axes(
                interactive=(not self.off_screen),
                color=self.theme['font']['color']
            )

    def initialize_plotter(self, **kwargs):
        """ Run `pyvista.Plotter` initialization.

        Note: This is removed from `__init__()` for purposes of multiple
        inheritance of :class:`~open_viewmin.nematic.nematic.NematicPlot`.

        """

        super().__init__(
            line_smoothing=True,
            point_smoothing=True,
            polygon_smoothing=True,
            lighting='light kit',
            **kwargs
        )

    def __getitem__(self, mesh_name):
        """  Mimic dictionary key-value behavior for mesh names and meshes.

        Parameters
        ----------
        mesh_name : str

        Returns
        -------
        :class:`~open_viewmin.filter_tree_plot.filter_formulas.FilterFormula`

        """

        return self.filter_formulas[mesh_name]

    def __setitem__(self, mesh_name, filter_formula_lambda):
        """  Mimic dictionary key-value behavior for mesh names and meshes.

        Parameters
        ----------
        mesh_name : str

        filter_formula_lambda : callable()
            Callable that creates filter formula

        Returns
        -------
        :class:`~open_viewmin.filter_tree_plot.filter_formulas.FilterFormula`

        """

        return filter_formula_lambda(mesh_name)

    def add_filter_formula(self, name=None, **kwargs):
        """ Add a filter formula to the plotter.

        Parameters
        ----------
        name : str or `None`, optional
            Name of new mesh. Pass `None` to do nothing.

        **kwargs :
            Keyword arguments to :class:`~open_viewmin.filter_tree_plot.filter_formulas.FilterFormula`

        Returns
        -------
        pyvista.dataset or `None`
            New child mesh created by filter formula, or `None` if no filter
            formula was created.

        """

        if name is not None:
            self.filter_formulas[name] = FilterFormula(
                self, name=name, **kwargs
            )
        return self.get_mesh(name)

    def add_widget_formula(self, name=None, **kwargs):
        """ Add a widget formula to the plotter.

        Parameters
        ----------
        name : str or `None`, optional
            Name of new widget. Pass `None` to do nothing.

        **kwargs : dict, optional
            Keyword arguments to :class:`~open_viewmin.filter_tree_plot.widget_formulas.WidgetFormula`

        Returns
        -------
        :class:`~open_viewmin.filter_tree_plot.widget_formulas.WidgetFormula` or `None`
            The new widget formula, or `None` if none was created.

        """

        if name is not None:
            self.widget_formulas[name] = WidgetFormula(
                self, name=name, **kwargs
            )
        return self.get_widget_formula(name)

    def __repr__(self):
        """
        Represent plotter as a textual representation of its filters tree.

        Returns
        -------
        str
            Textual representation of filters tree.

        """

        def _mesh_type_string(mesh_name):
            return self.meshes_dict[mesh_name].__class__.__name__

        def _get_filter_tree_string_component(mesh_name):
            if mesh_name in self.mesh_names():
                ans = f"  {mesh_name} ({_mesh_type_string(mesh_name)})\n"
                for child in self.filter_formulas[mesh_name].children:
                    if child != mesh_name:
                        tree_component = _get_filter_tree_string_component(child)
                        lines = tree_component.splitlines()
                        ans += "\n".join(["  " + line for line in lines]) + "\n"
                return ans

        ret = self.__class__.__name__ + "\n"
        for root in self.filter_tree_roots():
            ret += _get_filter_tree_string_component(root) + "\n"
        return ret

    # file handling

    @staticmethod
    def organize_filenames(filenames, mpi_group=True):
        """ For data to be imported, sort filenames alphabetically after
        grouping MPI files appropriately.

        Parameters
        ----------
        filenames : list[str] | str
        mpi_group : bool

        Returns
        -------
        list[str]

        """

        if isinstance(filenames, str):
            filenames = sorted(glob.glob(filenames))
        if mpi_group:
            filenames = mpi_stitch.mpi_group(filenames)
        return filenames

    def load(
        self, filenames, reader=ovm_utilities.fast_import, mpi_group=True,
        do_load_frame=True
    ):
        """ Import a file or files.

        Parameters
        ----------
        filenames : str or list[str]
            Files to load.

        reader : callable, optional
            Function to read each file.

        mpi_group : bool, optional
            Whether to stitch files together as separate outputs
            from the same timestep from an MPI run.

        do_load_frame : bool, optional
            Whether to view the frame containing the loaded data.

        """

        filenames_organized = self.organize_filenames(
            filenames, mpi_group=mpi_group
        )

        if len(filenames_organized) == 0:
            print(f"Error: Found no files matching \"{filenames}\". Skipping.")

        result = True
        for filename in filenames_organized:
            try:
                file_data = reader(filename)
            except ValueError:
                print(
                    f"Filename {filename} is improperly formatted for Reader "
                    f"{reader}"
                )
                continue
            except IndexError:
                continue
            # infer data_stride from x-values of first two lines
            data_stride = int(file_data[1, 0]) - int(file_data[0, 0])
            # infer data dimensions from last line
            data_dims = [
                int(item + data_stride) for item in file_data[-1, :3]
            ]
            if not self.array_dims_defined:
                # use this data to set plot's dimensions and stride
                self.data_stride = data_stride
                self.Lx, self.Ly, self.Lz = data_dims
                self.array_dims_defined = True
            else:
                # check that new data is compatible with plot's dimensions and stride
                if len(file_data) != np.prod(np.array(self.array_dims())):
                    print(
                        f"Error in FilterTreePlot.load(): file data of length "
                        f"{len(file_data)} is incompatible with existing "
                        f"plotter array dimensions {self.array_dims()}."
                    )
                    return False
            result = result and self.assign_file_data_to_frame(
                file_data, filename, do_load_frame=do_load_frame
            )
        return result

    def assign_file_data_to_frame(
        self, file_data, filename, do_load_frame=True
    ):
        """ Store data from a file as a new frame in the plotter's timeseries
        data.

        Parameters
        ----------
        file_data : np.ndarray
        filename : str
        do_load_frame : bool, optional
            Whether to view the frame

        """

        self.filenames.append(filename)
        self.data.append(file_data)
        frame_num, _ = self.create_fullmesh(file_data)
        print(f"Imported {filename} as frame # {frame_num}.")
        if do_load_frame:
            self.view_frame(frame_num)
        return True

    def add_root_mesh(self, mesh, mesh_name="fullmesh"):
        """  Add a filter formula as a new root of the filter tree.

        Parameters
        ----------
        mesh : pyvista.DataSet
            PyVista mesh
        mesh_name : str, optional
            Name of the new root mesh.

        Returns
        -------
        str
            Name of the new root mesh (possibly altered to avoid overwriting
            existing mesh names).

        """

        mesh_name = self.name_mesh_without_overwriting(mesh_name)
        self.meshes_dict[mesh_name] = mesh
        self.add_filter_formula(
            name=mesh_name,
            parent_mesh_name=mesh_name,
            has_actor=False,
            apply_upon_creation=False
        )
        return mesh_name

    @property
    def fullmesh(self):
        return self.frame_data[self.frame_num]["fullmesh"]

    def create_fullmesh(
        self, dat=None, mesh_name="fullmesh", frame_num=None
    ):
        # noinspection GrazieInspection
        """ Create a uniform grid mesh as a root mesh for the filter tree.

        Parameters
        ----------
        dat : np.ndarray, optional
            Data array, with first three columns holding integer coordinates
            X, Y, Z.

        mesh_name : str, optional
            Name of root mesh.

        frame_num : int or `None`, optional
            Frame number for this root mesh. Pass `None` to append as a new
            frame.

        Returns
        -------
        int, pyvista.dataset
            Frame number and new root mesh.

        """

        if dat is not None and self.array_dims_defined:
            if len(dat) != np.prod(np.array(self.array_dims())):
                print(
                    f"Cannot create fullmesh: shape of parameter `dat` is "
                    f"{dat.shape}, which is inconsistent with "
                    f"self.array_dims={self.array_dims()}"
                )
                return len(self.frame_data) - 1, None

        if frame_num is None:
            self.frame_data.append(dict())  # add new frame
            frame_num = len(self.frame_data) - 1

        this_frame_data = self.frame_data[frame_num]

        root_mesh = pv.ImageData(
            dimensions=self.array_dims(),
            spacing=(self.data_stride,)*3
        )
        this_frame_data[mesh_name] = root_mesh
        self.add_root_mesh(root_mesh, mesh_name)
        root_mesh["ones"] = np.ones(len(root_mesh.points))
        return frame_num, root_mesh

    def update_filter(self, mesh_name, update_actor=False, **kwargs):
        """ Update a filter formula.

        Shorthand for `self.get_filter_formula(parent_mesh_name).update()`.

        Parameters
        ----------
        mesh_name : str
            Name of filter formula

        update_actor : bool, optional
            Whether to update an actor immediately after updating its mesh.

        **kwargs : dict, optional
            Keyword arguments to :meth:`open_viewmin.filter_tree_plot.filter_formulas.FilterFormula.update`

        """

        filter_formula = self.filter_formulas.get(mesh_name)
        if filter_formula is not None:
            filter_formula.update(update_actor=update_actor, **kwargs)

    def refresh(self):
        """ Re-apply all filter formulas to refresh meshes """
        for mesh_name in self.filter_tree_roots():
            self.update_filter(mesh_name)

    def update_actor(self, actor_name, **kwargs):
        """ Update a filter formula's mesh visualization options.

        Shorthand for `self.get_filter_formula(ellipsoid_mesh_name).set()`

        Parameters
        ----------
        actor_name : str
            Name of filter formula

        **kwargs
            Keyword arguments to :meth:`open_viewmin.filter_tree_plot.filter_formulas.FilterFormula.set`

        """
        filter_formula = self.filter_formulas.get(actor_name)
        if filter_formula is not None:
            if filter_formula.has_actor:
                filter_formula.set(**kwargs)

    # frame handling / animation

    def view_frame(self, frame_num=0, keep_camera_dist=True):
        """ Use one of the (already imported) sets of data as the source for
        all PyVista meshes, e.g. for generating a frame of an animation from
        timeseries data.

        Parameters
        ----------
        frame_num : int, optional
            Index in `self.data` of the timeframe to display.

        keep_camera_dist : bool, optional
            Whether to force camera to maintain position after frame load.

        """

        camera_dist = self.camera.GetDistance()

        if not 0 <= frame_num < self.num_frames():
            return  # keep frame_num in the interval [0, self.num-frames)

        # update self.frame_num
        self.frame_num = frame_num

        # get root mesh(es) from self.frame_data
        this_frame_data = self.frame_data[self.frame_num]

        # reassign self.meshes values to (presumably only root) meshes
        # defined in this frame's data
        for mesh_name in this_frame_data.keys():
            self.meshes_dict[mesh_name] = this_frame_data[mesh_name]

        # apply filter/widget formulas to re-generate child meshes
        for mesh_name in this_frame_data.keys():
            self.get_filter_formula(mesh_name).update()
        for widget_formula in self.widget_formulas.values():
            widget_formula.update()

        # avoid jumps in view from camera auto-updates
        if keep_camera_dist:
            self.camera.SetDistance(camera_dist)

    def next_frame(self):
        """ View next frame in data timeseries. """
        self.view_frame(self.frame_num + 1)

    def previous_frame(self):
        """ View previous frame in data timeseries. """
        self.view_frame(self.frame_num - 1)

    def first_frame(self):
        """ View first frame in data timeseries. """
        self.view_frame(0)

    def last_frame(self):
        """ View last frame in data timeseries. """
        self.view_frame(self.num_frames() - 1)

    def num_frames(self):
        """ Number of frames in timeseries data. """
        return len(self.frame_data)

    def play(self, pause_time=0.5):
        """ In-GUI animation; will be slow because it loads and renders on the fly """
        while self.frame_num < self.num_frames() - 1:
            self.next_frame()
            self.update_frame_spinbox()
            self.repaint()
            sleep(pause_time)

    # general utilities

    def colorbars(self):
        """ Get list of all existing colorbars (scalar bars) for filter tree
        actors.

        Returns
        -------
        list of VTK scalar bar actors

        """
        colorbars = dict()
        for mesh_name in self.mesh_names():
            filter_formula = self.get_filter_formula(mesh_name)
            cbar = filter_formula.colorbar
            if cbar is not None:
                colorbars[mesh_name] = cbar
        return colorbars

    def colorbar_titles(self):
        """
        Get titles of all scalar bars

        Returns
        -------
        list[str]
            List of scalar bar titles
        """

        return [
            scalar_bar.GetTitle()
            for scalar_bar in self.colorbars().values()
        ]

    def get_points(self):
        """ Get array of coordinates belonging to root mesh "fullmesh"

        Returns
        -------
        np.ndarray
            Array of point coordinates.
        """

        return self.fullmesh.points

    def unique_array_names(self, mesh=None):
        """
        Get array names for datasets attached to a mesh, deleting duplicates.

        Parameters
        ----------
        mesh : str or pyvista.DataSet
            PyVista mesh or name of mesh

        Returns
        -------
        (pyvista.DataSet, list[str])
            Mesh along with the list of unique array names.

        """

        if mesh is None:
            roots = self.filter_tree_roots()
            if len(roots) > 0:
                mesh_name = roots[0]
                mesh = self.get_mesh(mesh_name)
        elif isinstance(mesh, str):
            mesh_name = mesh
            mesh = self.get_mesh(mesh_name)
        if mesh is None:
            return None, []

        # get unique array names
        arr_names = mesh.array_names
        unique_names = list(set(arr_names))  # delete duplicates

        # if we've deleted any duplicates
        if len(unique_names) < len(arr_names):
            for arr_name in mesh.cell_data.keys():
                # check if any cell array has the same name as any point array
                # and rename it if so
                if arr_name in mesh.point_data.keys():
                    mesh.rename_array(
                        arr_name,
                        arr_name + " (cells)",
                        preference="Cells"
                    )
        return mesh, mesh.array_names

    # array/dataset utilities

    def scalar_fields(self, mesh_name="fullmesh"):
        """
        List names of datasets with one value at each site.

        Parameters
        ----------
        mesh_name : str or pyvista.DataSet
            PyVista mesh or its name.

        Returns
        -------
        list[str]
            List of array names.

        """

        mesh_name, array_names = self.unique_array_names(mesh=mesh_name)
        ret = []
        for array_name in array_names:
            array_shape = mesh_name[array_name].shape
            if len(array_shape) == 1:
                ret.append(array_name)
        return sorted(ret)

    def vector_fields(self, mesh="fullmesh"):
        """
        List names of datasets with three values at each site.

        Parameters
        ----------
        mesh : str or pyvista.DataSet
            PyVista mesh or its name.

        Returns
        -------
        list[str]
            List of array names.

        """

        mesh, array_names = self.unique_array_names(mesh=mesh)
        ret = []
        for array_name in array_names:
            array_shape = mesh[array_name].shape
            if len(array_shape) > 1:
                if array_shape[1] == 3:
                    ret.append(array_name)
        return sorted(ret)

    def tensor_fields(self, mesh_name="fullmesh"):
        """
        List names of datasets with nine values at each site.

        Parameters
        ----------
        mesh_name : str or pyvista.DataSet
            PyVista mesh or its name.

        Returns
        -------
        list[str]
            List of array names.

        """
        mesh = self.get_mesh(mesh_name)
        _, array_names = self.unique_array_names(mesh=mesh)
        ret = []
        for array_name in array_names:
            array_shape = mesh[array_name].shape
            if len(array_shape) > 1:
                if array_shape[1] == 9:
                    ret.append(array_name)
        return sorted(ret)

    def symmetric_tensor_fields(self, mesh_name="fullmesh"):
        """
        List names of datasets with nine values at each site, whose corresponding
        3x3 matrix is everywhere symmetric.

        Parameters
        ----------
        mesh_name : str or pyvista.DataSet
            PyVista mesh or its name.

        Returns
        -------
        list[str]
            List of array names.

        """
        ret = []
        for field_name in self.tensor_fields(mesh_name=mesh_name):
            field = self.get_mesh(mesh_name)[field_name]
            tmp = field.reshape(-1, 3, 3)
            if np.array_equal(np.swapaxes(tmp, -1, -2), tmp):
                ret.append(field_name)
        return ret

    def dims(self):
        """ System linear dimensions.

        Returns
        -------
        tuple(int)
            `(Lx, Ly, Lz)`
        """
        return self.Lx, self.Ly, self.Lz

    def array_dims(self):
        """ System array-size dimensions.

        Returns
        -------
        tuple(int)
            `(array_Lx, array_Ly, array_Lz)`

        """
        return tuple([
            int(item/self.data_stride)
            for item in self.dims()
        ])

    # utilities for working with actors and meshes

    def mesh_names(self):
        """
        Names of all meshes in filters tree.

        Returns
        -------
        list[str]
            List of mesh names.
        """
        return list(self.meshes_dict.keys())

    def actor_names(self):
        """
        Names of all actors in filters tree.

        Returns
        -------
        list[str]
            List of actor names.

        """

        return [
            mesh_name for mesh_name in self.mesh_names()
            if self.filter_formulas[mesh_name].has_actor
        ]

    def non_filter_tree_actor_names(self):
        """
        List actors that don't belong to open_viewmin's filter tree.

        Returns
        -------
        list[str]
            List of actor names.

        """
        ovm_actor_names = self.actor_names()
        return [
            key for key in self.renderer.actors.keys()
            if key not in ovm_actor_names
        ]

    def get_mesh_name(self, mesh):
        """
        Get the name of a given mesh.

        Parameters
        ----------
        mesh : pyvista.DataSet

        Returns
        -------
        str
            name of the mesh

        """

        ans_list = [
            mesh_name
            for (mesh_name, mesh_b) in [
                (mesh_name, self.get_mesh(mesh_name))
                for mesh_name in self.mesh_names()
            ]
            if mesh_b is mesh
        ]
        if len(ans_list) > 0:
            return ans_list[0]
        else:
            raise KeyError(f"Cannot find mesh in filter tree: {mesh}")

    def get_grandparent_mesh_name(self, actor_or_mesh_name):
        """
        Get the name of the parent of the parent of a given actor or mesh.

        Parameters
        ----------
        actor_or_mesh_name : str
            Name of actor of mesh

        Returns
        -------
        str
            Name of grandparent mesh.

        """

        filter_formula = self.get_filter_formula(actor_or_mesh_name)
        parent_mesh_name = filter_formula.parent_mesh_name
        parent_mesh_formula = self.get_filter_formula(parent_mesh_name)
        grandparent_mesh_name = parent_mesh_formula.parent_mesh_name

        return grandparent_mesh_name

    def get_actor_name(self, actor):
        """
        Given an actor, return its name.

        Parameters
        ----------
        actor : vtkOpenGLActor
            PyVista actor.

        Returns
        -------
        str
            Name of actor.

        """

        for key in self.renderer.actors.keys():
            if self.renderer.actors[key] is actor:
                return key

    def get_mesh(self, mesh_name):
        """
        Return mesh with a given name, if it exists.
        For non-mesh actors, return parent mesh.

        Parameters
        ----------
        mesh_name : str

        Returns
        -------
        pyvista.DataSet
            PyVista mesh

        """

        if mesh_name is None:
            mesh_name = self.default_mesh_name
        elif isinstance(mesh_name, pv.DataSet):
            # in case the mesh itself was passed, just return that
            return mesh_name
        return self.meshes_dict.get(mesh_name)

    def get_actor(self, actor_name):
        """
        Return actor with a given name, if it exists.

        Parameters
        ----------
        actor_name : str
            Name of actor.

        Returns
        -------
        vtkmodules.vtkRenderingOpenGL2.vtkOpenGLActor
            PyVista actor.

        """

        if isinstance(actor_name, str):
            try:
                return self.renderer.actors[actor_name]
            except KeyError:
                # do nothing if passed an invalid name
                return
        else:
            # if passed an actor rather than its name, return the actor
            return actor_name

    def get_filter_formula(self, actor_name):
        """
        Get the formula needed to re-create an actor from its parent mesh.

        Parameters
        ----------
        actor_name : str
            Name of the actor.

        Returns
        -------
        types.SimpleNamespace
            Actor recipe.
        """
        if actor_name is None:
            actor_name = self.default_mesh_name
        return self.filter_formulas.get(actor_name)

    def get_widget(self, widget_name):
        """ Get widget by name

        Parameters
        ----------
        widget_name : str

        Returns
        -------
        vtkmodules.vtkInteractionWidgets.vtk3DWidget

        """

        return self.widgets.get(widget_name)

    def get_widget_formula(self, widget_name):
        """ Get :class:`~open_viewmin.filter_tree_plot.widget_formulas.WidgetFormula` by name

        Parameters
        ----------
        widget_name : str

        Returns
        -------
        :class:`~open_viewmin.filter_tree_plot.widget_formulas.WidgetFormula`
        """

        return self.widget_formulas.get(widget_name)

    def copy_all_actors(self, to_plotter):
        """
        Copy actors to another plotter,
        such as for displaying in Jupyter notebooks.

        Parameters
        ----------
        to_plotter : pyvista.Plotter

        Returns
        -------
        None

        """

        for actor_name in self.renderer.actors.keys():
            actor_is_visible = self.renderer.actors[actor_name].GetVisibility()
            if actor_is_visible:
                if actor_name in self.mesh_names():
                    filter_formula = self.get_filter_formula(actor_name)
                    parent_mesh = self.get_mesh(filter_formula.parent_mesh_name)
                    pvfilter = filter_formula.filter_callable(parent_mesh)
                    filter_kwargs = filter_formula.filter_kwargs
                    mesh_kwargs = filter_formula.mesh_kwargs
                    if to_plotter.theme.jupyter_backend == 'panel':
                        mesh_kwargs['diffuse'] = 1
                    actor = pvfilter(**filter_kwargs)
                    to_plotter.add_mesh(actor, **mesh_kwargs)
                else:
                    actor = self.renderer.actors[actor_name]
                    to_plotter.add_actor(actor)

    def extract_actors(self, jupyter_backend='trame'):
        """
        Set up a new PyVista plotter with a copy of all this plotter's actors.

        Parameters
        ----------
        jupyter_backend : str, optional
            PyVista plotting backend. See
            https://docs.pyvista.org/version/stable/user-guide/themes.

        Returns
        -------
        pyvista.Plotter
            New PyVista `Plotter` containing this `Plotter`'s actors.

        """
        to_plotter = pv.Plotter()
        to_plotter.theme = self.theme
        to_plotter.set_background(self.background_color)
        to_plotter.theme.jupyter_backend = jupyter_backend
        self.copy_all_actors(to_plotter)
        return to_plotter

    # operations on meshes and actors

    def rename_mesh(self, from_name, to_name, do_refresh=True):
        # noinspection GrazieInspection
        """
        Copy a mesh in the filters tree by copying its recipe.

        Parameters
        ----------
        from_name : str
            Name of mesh to copy.
        to_name : str
            Name of new mesh.
        do_refresh : bool, optional
            Whether to create the mesh anew from its filter after renaming.

        Returns
        -------
        None

        """

        # if mesh has actor currently, remove it
        if from_name in self.actor_names():
            self.remove_actor(from_name)

        filter_formula = self.get_filter_formula(from_name)

        # inform children about change of name
        for child_name in filter_formula.children:
            child_filter_formula = self.get_filter_formula(child_name)
            if child_filter_formula.parent_mesh_name == from_name:
                child_filter_formula.parent_mesh_name = to_name

        # inform parents about change of name
        filter_formula.remove_child_relationship_to_all_parents()

        # rename filter formula
        filter_formula.name = to_name
        self.filter_formulas[to_name] = filter_formula
        del self.filter_formulas[from_name]

        # rename mesh
        self.meshes_dict[to_name] = self.meshes_dict[from_name]
        del self.meshes_dict[from_name]

        # rename widget if applicable
        old_widget_name = filter_formula.widget_name
        if old_widget_name is not None:
            widget = self.get_widget(old_widget_name)

            # remember widget state (enabled/disabled)
            widget_enabled = widget.GetEnabled()

            # disable the widget
            widget.SetEnabled(0)

            widget_formula = self.get_widget_formula(old_widget_name)

            # rename widget formula
            new_widget_name = to_name + "_widget"
            self.widget_formulas[new_widget_name] = widget_formula
            widget_formula.name = new_widget_name
            del self.widget_formulas[old_widget_name]

            # delete old widget from widgets dictionary
            del self.widgets[old_widget_name]

            # widget formula's mesh name should match new mesh name
            widget_formula.mesh_name = to_name

            # re-apply widget formula
            widget_formula.apply()

            # inform filter formula about new widget name
            filter_formula.widget_name = new_widget_name

            # set widget to previous enabled/disabled state
            self.get_widget(new_widget_name).SetEnabled(widget_enabled)

        # make sure change of actor name is recorded in self.renderer.actors
        # if applicable
        if from_name in self.renderer.actors.keys():
            self.renderer.actors[to_name] = self.renderer.actors[from_name]
            del self.renderer.actors[from_name]

        filter_formula.make_sure_parent_knows_about_me()

        if do_refresh:
            self.refresh()

    def remove_mesh_completely(self, mesh_name, remove_children=True):
        """
        Remove a mesh (filter or actor) from the plotter and from the filters
        tree.

        Parameters
        ----------
        mesh_name : str
            Name of mesh to remove.

        remove_children : bool, optional
            Whether to recursively remove all meshes descended from this mesh
            in the filters tree.

        Returns
        -------
        None

        """

        filter_formula = self.filter_formulas[mesh_name]
        colorbar = filter_formula.colorbar
        children = filter_formula.children.copy()
        if colorbar is not None:
            self.remove_scalar_bar(colorbar.GetTitle())
        if mesh_name in self.actor_names():
            self.remove_actor(mesh_name)
        filter_formula.remove_child_relationship_to_all_parents()
        del self.filter_formulas[mesh_name]
        try:
            del self.meshes_dict[mesh_name]
        except KeyError:
            pass
        if mesh_name in self.toolbars.keys():
            self.toolbars[mesh_name].hide()
            del self.toolbars[mesh_name]
        if remove_children:
            for child_name in children:
                self.remove_mesh_completely(child_name, remove_children=True)

    # actor/filter creation / update
    #
    # def add_filter(
    #     self, parent_mesh_name="fullmesh", child_name=None, creator_func=None,
    #     filter_kwargs=None, actor=True, **mesh_kwargs
    # ):
    #     """
    #     Add a new filter to the filters tree.
    #
    #     Parameters
    #     ----------
    #     parent_mesh_name : str, optional
    #         Name of mesh to which to apply the filter.
    #     child_name : str, optional
    #         Name of mesh to be created by filter.
    #     creator_func : callable() or str, optional
    #         Function mapping a parent mesh to its filter callable;
    #         or the filter name as a string (for filters defined by
    #         PyVista). Defaults to the identity mapping.
    #     filter_kwargs : dict or None, optional
    #         Keyword arguments to the filter defined by `creator_func`
    #     actor : bool, optional
    #         Whether to treat the child mesh as an actor with visualization
    #         properties.
    #     mesh_kwargs : dict or None, optional
    #         Keyword arguments to `pyvista.Plotter.add_mesh` for controlling
    #         visualization properties. Used only if `actor==True`.
    #
    #     Returns
    #     -------
    #     pyvista.Dataset or vtkOpenGLActor
    #         Mesh or actor object.
    #
    #     """
    #
    #     if filter_kwargs is None:
    #         filter_kwargs = dict()
    #     if type(creator_func) is str:
    #         creator_string = creator_func
    #         creator_func = (lambda mesh: getattr(mesh, creator_string))
    #     else:
    #         creator_string = "child"
    #     if child_name is None:
    #         child_name = parent_mesh_name + "_" + creator_string
    #     if creator_func is None:
    #         def creator_func(mesh):
    #             return lambda: mesh
    #
    #     self.add_filter_formula(
    #         name=child_name,
    #         parent_mesh_name=parent_mesh_name,
    #         filter_callable=creator_func,
    #         filter_kwargs=filter_kwargs,
    #         mesh_kwargs=mesh_kwargs,
    #         has_actor=actor
    #     )

    def filter_tree_roots(self):
        """ Names of meshes whose generating :class:`~open_viewmin.filter_tree_plot.filter_formulas.FilterFormula` 's
        are roots of the filter tree

        Returns
        -------
        list[str]

        """

        return [
            mesh_name for mesh_name in self.mesh_names()
            if self.get_filter_formula(mesh_name).is_filter_tree_root
        ]

    def name_mesh_without_overwriting(self, mesh_name):
        """ Modify a proposed new mesh name to be distinct from existing mesh
        names

        Parameters
        ----------
        mesh_name : str
            Proposed mesh name

        Returns
        -------
        str
            Distinct mesh name to use

        """

        return ovm_utilities.name_without_overwriting(mesh_name, self.meshes_dict)

    def name_widget_without_overwriting(self, widget_name):
        """ Modify a proposed new widget name to be distinct from existing
        widget names

       Parameters
       ----------
       widget_name : str
           Proposed widget name

       Returns
       -------
       str
           Distinct widget name to use

       """
        return ovm_utilities.name_without_overwriting(
            widget_name, self.widgets
        )

    def set_actor_visibility(self, actor, visibility: bool):
        """ Set visibility of a mesh or meshes

        Parameters
        ----------
        actor : str, sequence of str, VTK actor, or sequence of VTK actors

        visibility : bool
            Whether mesh(es) are to be visible.

        """

        if isinstance(actor, list) or isinstance(actor, tuple):
            for item in actor:
                self.set_actor_visibility(item, visibility)
        if isinstance(actor, str):
            actor_name = actor
            actor = self.get_actor(actor_name)
        else:
            actor_name = self.get_actor_name(actor)
        try:
            # noinspection PyUnresolvedReferences
            actor.SetVisibility(visibility)
        except AttributeError:
            pass
        else:
            filter_formula = self.get_filter_formula(actor_name)
            if filter_formula is not None:
                colorbar = filter_formula.colorbar
                if colorbar is not None:
                    if not visibility:
                        colorbar.SetVisibility(0)
                    else:
                        mesh_kwargs = filter_formula.mesh_kwargs
                        show_scalar_bar = mesh_kwargs['show_scalar_bar']
                        colorbar.SetVisibility(int(show_scalar_bar))

    def _visibility_callback(self, actor_name):
        def return_func(flag):
            self.set_actor_visibility(actor_name, flag)
        return return_func

    def toggle_filter_tree_actor_visibility(self, actor_name):
        """ Toggle visibility of a mesh in the filter tree.

        Parameters
        ----------
        actor_name : str
            Name of mesh

        """

        actor = self.get_actor(actor_name)
        # noinspection PyUnresolvedReferences
        current_visibility = actor.GetVisibility()
        new_visibility = 1 - current_visibility
        self.set_actor_visibility(actor_name, bool(new_visibility))

    def toggle_eye_dome_lighting(self):
        """ Switch eye dome lighting on/off """

        if self.is_enabled_eye_dome_lighting:
            self.disable_eye_dome_lighting()
        else:
            self.enable_eye_dome_lighting()

    @wraps(pv.Plotter.enable_eye_dome_lighting)
    def enable_eye_dome_lighting(self):
        self.is_enabled_eye_dome_lighting = True
        super().enable_eye_dome_lighting()

    @wraps(pv.Plotter.disable_eye_dome_lighting)
    def disable_eye_dome_lighting(self):
        self.is_enabled_eye_dome_lighting = False
        super().disable_eye_dome_lighting()

    def do_orbit(self):
        """ Move camera along orbit path

        Removes actor named "orbit", if it exists, before starting the motion.
        """

        self.remove_actor("orbit")
        self.orbit_on_path(
            path=self.orbit_path, threaded=True, step=self.orbit_kwargs['step']
        )

    def show_orbit(self):
        """ Add a mesh of points visualizing the camera orbit. """
        self.add_mesh(self.orbit_path.points, name='orbit')

    def hide_orbit(self):
        """ Remove the mesh visualizing the camera orbit. """
        self.remove_actor("orbit")

    def toggle_orbit_visibility(self, toggle=True):
        """ Toggle visibility of mesh visualizing camera orbit. """
        if "orbit" in self.renderer.actors.keys():
            vis = self.renderer.actors["orbit"].GetVisibility()
        else:
            vis = False
        if toggle:
            vis = not vis
        if vis:
            self.show_orbit()
        else:
            self.hide_orbit()

    def toggle_last_colorbar(self):
        """ Toggle visibility of most recently modified colorbar. """
        colorbars = list(self.colorbars().values())
        if len(colorbars) > 0:
            colorbar = colorbars[-1]
            colorbar.SetVisibility(1-colorbar.GetVisibility())

    def set_lights_intensity(self, intensity):
        """  Rescale intensity of all lights.

        Parameters
        ----------
        intensity : float

        """

        for light in self.renderer.lights:
            light.SetIntensity(intensity)

    def toggle_floor(self):
        """ Toggle visibility of floor (-z) plane. """
        key = 'Floor(-z)'
        if key in self.renderer.actors.keys():
            floor_actor = self.renderer.actors[key]
            floor_actor.SetVisibility(1-floor_actor.GetVisibility())
        else:
            self.add_floor()

    def toggle_stereo_render(self):
        """ Turn anaglyph stereo 3D effect on/off """
        self.ren_win.SetStereoRender(1-self.ren_win.GetStereoRender())

    @wraps(pv.Plotter.enable_shadows)
    def enable_shadows(self):
        super().enable_shadows()
        self.shadowsEnabled = True

    @wraps(pv.Plotter.disable_shadows)
    def disable_shadows(self):
        super().disable_shadows()
        self.shadowsEnabled = False

    def toggle_shadows(self):
        """ Turn shadow rendering on/off """
        if self.shadowsEnabled:
            self.disable_shadows()
        else:
            self.enable_shadows()

    def hide_all_meshes(self):
        """ Make actors of all meshes invisible """
        for mesh_name in self.mesh_names():
            filter_formula = self.get_filter_formula(mesh_name)
            if filter_formula.has_actor:
                self.set_actor_visibility(mesh_name, False)
        self.refresh()

    def hide_all_actors(self):
        """ Make all actors invisible """
        all_actors = self.renderer.actors
        for actor_name in all_actors.keys():
            if actor_name in self.actor_names():
                self.set_actor_visibility(actor_name, False)
            else:
                all_actors[actor_name].SetVisibility(0)
        self.refresh()

    def find_actor_for_scalar_bar(self, scalar_bar):
        """ Get the name of the filter tree mesh corresponding to a given
        scalar bar.

        Parameters
        ----------
        scalar_bar : VTK scalar bar actor

        Returns
        -------
        str
            Mesh name
        """
        for actor_name in self.actor_names:
            filter_formula = self.get_filter_formula(actor_name)
            if filter_formula.colorbar is scalar_bar:
                return actor_name

    def toggle_scalar_bar_visibility(self, scalar_bar_actor):
        """ Toggle visibility of a scalar bar

        Parameters
        ----------
        scalar_bar_actor : VTK scalar bar actor

        """

        new_visibility = self.toggle_actor_visibility(scalar_bar_actor)
        parent_actor_name = self.find_actor_for_scalar_bar(scalar_bar_actor)
        filter_formula = self.get_filter_formula(parent_actor_name)
        mesh_kwargs = filter_formula.mesh_kwargs
        mesh_kwargs['show_scalar_bar'] = new_visibility

    @staticmethod
    def toggle_actor_visibility(actor):
        """ Toggle visibility of an actor

        Parameters
        ----------
        actor : VTK actor

        Returns
        -------
        new_visibility: bool

        """

        current_visibility = actor.GetVisibility()
        new_visibility = 1 - current_visibility
        actor.SetVisibility(new_visibility)
        return new_visibility

    def set_orbit_kwargs(
        self, factor=3.0, n_points=20, viewup=None, shift=0.0, step=0.1
    ):
        """ Modify camera orbit

        Parameters
        ----------
        factor : float
            orbit radius relative to data extent
        n_points : int
            number of points subdividing circle at which to generate images
        viewup : iterable[float], optional
            orientation vector for camera's up direction
        shift : float
            translation of camera location along `viewup`
        step : float
            orbit speed in seconds per frame

        Returns
        -------
        vis : bool
            Visibility of orbit points actor

        """

        if viewup is None:
            viewup = [0, 0, 1]
        if "orbit" in self.renderer.actors.keys():
            old_orbit_actor = self.renderer.actors["orbit"]
            vis = old_orbit_actor.GetVisibility()
            self.remove_actor("orbit")
        else:
            vis = False
        ovm_utilities.add_if_dict_lacks(
            self.orbit_kwargs,
            dict(
                factor=factor, n_points=n_points, viewup=viewup, shift=shift,
                step=step
            )
        )
        return vis

    # noinspection PyUnresolvedReferences,PyArgumentList
    def set_orbit(self, **kwargs):
        """ Set orbit properties and get its camera locations

        Parameters
        ----------
        kwargs
            keyword arguments to `set_orbit_kwargs()`

        Returns
        -------
        pyvista.core.pointset.PolyData
            Orbit points

        """

        vis = self.set_orbit_kwargs(**kwargs)
        orbit_kwargs = self.orbit_kwargs
        orbit_path = self.generate_orbital_path(
            factor=orbit_kwargs['factor'],
            n_points=orbit_kwargs['n_points'],
            viewup=orbit_kwargs['viewup'],
            shift=orbit_kwargs['shift']
        )
        if hasattr(self, "orbit_path"):
            self.orbit_path = orbit_path
            if vis:
                self.show_orbit()
        return orbit_path

    def probe_at_picked_surface_point(self, result_name=None):
        """ Create a `FilterFormula` with the `sample` filter probing
        a picked mesh at a picked point

        Parameters
        ----------
        result_name : str, optional
            Name of new `FilterFormula`; defaults to "_probed" appended to
            name of picked mesh

        Returns
        -------
        result_name : str
            Name of new `FilterFormula`
        """

        if self.picked_mesh is not None and self.picked_point is not None:
            picked_mesh_name = self.get_mesh_name(self.picked_mesh)
            if result_name is None:
                result_name = self.name_mesh_without_overwriting(
                    picked_mesh_name + "_probed"
                )

            def filter_callable(mesh):
                def return_func(points, **kwargs):
                    return probe(mesh, points, **kwargs)
                return return_func

            self.add_filter_formula(
                result_name,
                parent_mesh_name=picked_mesh_name,
                filter_callable=filter_callable,
                filter_kwargs=dict(
                    points=np.array([self.picked_point])
                )
            )
        return result_name

    def add_plane_widget(self, *args, **kwargs):
        """ Overload add_plane_widget to take a string argument as name of mesh
        controlled by widget.

        Parameters
        ----------
        *args : tuple, optional
        **kwargs : dict, optional

        Returns
        -------
        vtkmodules.vtkInteractionWidgets.vtkImplicitPlaneWidget

        """

        if isinstance(args[0], str):
            widget_name = args[1] if len(args) > 1 else None
            return plane_widgets.add_slice_widget_to_slice_mesh(
                self, slice_name=args[0], widget_name=widget_name
            )
        else:
            return super().add_plane_widget(*args, **kwargs)

    def _add_inplot_slider(
        self, callback, mesh_name, current_val=None,
        min_val=None, max_val=None, title=None,
        title_height=0.03, style="classic", label_height=0.03,
        pointa=None, pointb=None, loc="top right",
        **kwargs
    ):
        """

        Parameters
        ----------
        callback : callable
        mesh_name : str
        current_val : float, optional
        min_val : float, optional
        max_val : float, optional
        title : str, optional
        title_height : float, optional
        style : str, optional
            Options in pyvista.global_theme.slider_styles
        label_height : float
        pointa : tuple(float), optional
        pointb : tuple(float), optional
        loc : str, optional
            Slider location by keyword; options are
            "top left", "bottom left", "bottom right". If any other string or
            `None` is given, slider is positioned  in top right.

        kwargs
            keyword arguments to :meth:`pyvista.Plotter.add_slider_widget`

        Returns
        -------
        vtk.vtkSliderWidget

        """
        filter_formula = self.get_filter_formula(mesh_name)
        scalars_name = filter_formula.filter_kwargs.get('scalars')
        if scalars_name is None:
            return
        if max_val is None:
            max_val = max(self.default_mesh[scalars_name])
        if min_val is None:
            min_val = min(self.default_mesh[scalars_name])
        if title is None:
            title = f"{scalars_name} ({mesh_name})"[:30]
        if pointa is None or pointb is None:
            if loc == "top left":
                pointa = (0.05, 0.9)
                pointb = (0.45, 0.9)
            elif loc == "bottom left":
                pointa = (0.05, 0.1)
                pointb = (0.45, 0.1)
            elif loc == "bottom right":
                pointa = (0.55, 0.1)
                pointb = (0.95, 0.1)
            else:
                pointa = (0.55, 0.9)
                pointb = (0.95, 0.9)

        widget = self.add_slider_widget(
            callback,
            (min_val, max_val),
            value=current_val,
            title=title,
            color=filter_formula.mesh_kwargs.get(
                'color', self.theme.font.color
            ),
            title_height=title_height,
            style=style,
            pointa=pointa,
            pointb=pointb,
            **kwargs
        )
        widget.GetRepresentation().SetLabelHeight(label_height)

        return widget

    def add_isosurface_slider(
        self, mesh_name, min_val=None, max_val=None, title=None,
        title_height=0.02, style="classic", label_height=0.02,
        pointa=None, pointb=None, loc="top right",
        **kwargs
    ):
        """ Add isosurface slider

        Parameters
        ----------
        mesh_name : str
        min_val : float, optional
        max_val : float, optional
        title : str, optional
        title_height : float
        style : str, optional
            Options in pyvista.global_theme.slider_styles
        label_height : float, optional
        pointa : tuple(float), optional
        pointb : tuple(float), optional
        loc : str, optional
            Slider location by keyword; options are
            "top left", "bottom left", "bottom right". If any other string or
            `None` is given, slider is positioned  in top right.
        kwargs
            keyword arguments to :meth:`_add_inplot_slider`

        Returns
        -------
        vtk.vtkSliderWidget

        """

        def get_current_val():
            filter_formula = self.get_filter_formula(mesh_name)
            return filter_formula.filter_kwargs.get('isosurfaces')[0]

        def slider_callback(val):
            self.set_actor_visibility(mesh_name, True)
            self[mesh_name].update(isosurfaces=[val])

        return self._add_inplot_slider(
            slider_callback, mesh_name,
            current_val=get_current_val(),
            min_val=min_val, max_val=max_val, title=title,
            title_height=title_height, style=style, label_height=label_height,
            pointa=pointa, pointb=pointb, loc=loc,
            **kwargs
        )

    def add_threshold_slider(
        self, mesh_name, min_val=None, max_val=None, title=None,
        title_height=0.02, style="classic", label_height=0.02,
        pointa=None, pointb=None, loc="top right",
        **kwargs
    ):
        """ Add slider for threshold

        Parameters
        ----------
        mesh_name : str
        min_val : float, optional
        max_val : float, optional
        title : str, optional
        title_height : float, optional
        style : str, optional
            Options in pyvista.global_theme.slider_styles
        label_height : float, optional
        pointa : tuple(float), optional
        pointb : tuple(float), optional
        loc : str, optional
            Slider location by keyword; options are
            "top left", "bottom left", "bottom right". If any other string or
            `None` is given, slider is positioned  in top right.
        kwargs
            keyword arguments to :meth:`_add_inplot_slider`

        """

        def get_current_vals():
            filter_formula = self.get_filter_formula(mesh_name)
            return filter_formula.filter_kwargs.get('value')

        def slider_callback(val, slider_idx):
            self.set_actor_visibility(mesh_name, True)
            current_vals = get_current_vals()
            if slider_idx == 0:
                self[mesh_name].update(value=[val, current_vals[1]])
            else:
                self[mesh_name].update(value=[current_vals[0], val])
        for idx in [0, 1]:
            self._add_inplot_slider(
                lambda val: slider_callback(val, idx),
                mesh_name,
                current_val=get_current_vals()[idx],
                min_val=min_val, max_val=max_val, title=title,
                title_height=title_height, style=style, label_height=label_height,
                pointa=pointa, pointb=pointb, loc=loc,
                **kwargs
            )

    def _add_checkbox_generic(
        self, callback, current_val, size=50, position=None,
        color_on=None, **kwargs
    ):
        """ Add checkbox

        Parameters
        ----------
        callback : callable
        current_val : bool
        size : int
        position : tuple(float), optional
        color_on : pyvista.plotting.colors.Color, optional
        kwargs
            keyword arguments to :meth:pyvista.Plotter.add_checkbox_button_widget

        """
        if color_on is None:
            color_on = self.theme.font.color
        if position is None:
            position = (
                int((len(self.button_widgets) + 0.5) * 1.25 * size),
                int(size/2)
            )
        self.add_checkbox_button_widget(
            callback,
            value=current_val,
            color_on=color_on,
            color_off=self.theme.background,
            position=position,
            **kwargs
        )

    def add_visibility_checkbox(
        self, actor_name, size=50, position=None, **kwargs
    ):
        """ Add checkbox to toggle an actor's visibility

        Parameters
        ----------
        actor_name : str
        size : int
        position : tuple(float), optional
        kwargs
            Keyword arguments to :meth:`_add_checkbox_generic`
        """
        # noinspection PyUnresolvedReferences
        self._add_checkbox_generic(
            self._visibility_callback(actor_name),
            self.get_actor(actor_name).GetVisibility(),
            color_on=self.get_filter_formula(actor_name).mesh_kwargs.get(
                "color"
            ),
            size=size,
            position=position,
            **kwargs
        )

    def add_widget_checkbox(
        self, widget_name, size=50, position=None, background_color="red",
        **kwargs
    ):
        """ Add checkbox that activates/deactivates a widget

        Parameters
        ----------
        widget_name : str
        size : int
        position : tuple(float), optional
        background_color : str
        kwargs
            Keyword arguments to :meth:`_add_checkbox_generic`

        """
        widget = self.widgets.get(widget_name)
        if widget is None:
            return

        def get_color():
            widget_formula = self.get_widget_formula(widget_name)
            return widget_formula.widget_kwargs.get("color")

        self._add_checkbox_generic(
            widget.SetEnabled,
            widget.GetEnabled(),
            color_on=get_color(),
            size=size,
            position=position,
            background_color=background_color,
            **kwargs
        )

    def save_meshes(self, file_prefix):
        """ Save all meshes to .vtk files

        Parameters
        ----------
        file_prefix : str
            File location will be `[mesh_name].vtk` appended to this.
        """

        for mesh_name in self.mesh_names():
            mesh = self.get_mesh(mesh_name)
            mesh_filename = file_prefix + "_" + mesh_name + ".vtk"
            mesh.save(mesh_filename)
            print(f"Mesh \"{mesh_name}\" saved to {mesh_filename}")


