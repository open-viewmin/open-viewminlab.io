import numpy as np
import matplotlib.colors
from open_viewmin.filter_tree_plot.utilities import ovm_utilities


class WidgetFormula:
    """ Class for creating re-usable widgets

    Parameters
    ----------
    plotter : :class:`~open_viewmin.filter_tree_plot.filter_tree_plot.FilterTreePlot`
    name : str or `None`
        Name of widget. Pass `None` to default to `"_widget"` appended to
        `parent_mesh_name`.
    mesh_name : str or `None`
        Name of mesh controlled by widget. Pass `None` to default to
        plotter's default mesh name.
    enabled : bool, optional
        Whether to initialize widget as enabled.
    callback : callable()
        Function mapping (plotter, parent_mesh_name) to a callable that applies the
        widget to update its mesh.
    creator : callable()
        Function creating the widget. First argument must be a function of
        the kind created by callback(plotter, parent_mesh_name). Must also accept
        all arguments in `widget_args` and keyword arguments in `kwargs`.
    configure : callable() or `None`, optional
        Function configuring a widget based on previously existing widget,
        mapping the widget to a two-element tuple
        `(enabled: bool, widget_args: tuple)`. Pass `None` to ignore.
    widget_args : tuple or `None`
        Positional arguments, besides `callback`, to `creator`.
        `None` is equivalent to empty tuple.
    kwargs : dict, optional
        Keyword arguments to `creator`.
    """

    def __init__(
        self, plotter, name=None, mesh_name=None, enabled=True, callback=None,
        creator=None, configure=None, widget_args=None, **kwargs
    ):

        self.plotter = plotter
        self.color = None
        if mesh_name is None:
            self.mesh_name = self.plotter.default_mesh_name
        else:
            self.mesh_name = mesh_name
        if widget_args is None:
            self.widget_args = ()
        else:
            self.widget_args = widget_args
        if name is None:
            self.name = self.mesh_name + "_widget"
        else:
            self.name = name
        self.enabled = enabled

        # Function mapping (plotter, parent_mesh_name) to
        # function(*widget_args, **widget_kwargs)
        # that updates mesh controlled by widget.
        self.callback = callback

        # name of method of self.plotter that creates the widget
        self.creator = creator

        # function that returns arguments of existing widget to use in updated
        # widget, or default arguments if no such widget exists
        self.configure = configure

        # kwargs passed to self.creator function
        self.widget_kwargs = kwargs
        if self.enabled:
            self.apply()
            self.set_color()

    @property
    def widget(self):
        """ Get widget

        Returns
        -------
        VTK widget

        """

        return self.plotter.widgets.get(self.name)

    def get_creator_func(self):
        """ If :attr:`self.creator` is a string, get the corresponding callable

        Returns
        -------
        callable()

        """

        if isinstance(self.creator, str):
            creator_func = getattr(self.plotter, self.creator)
        else:
            creator_func = self.creator
        assert callable(creator_func)
        return creator_func

    def check_existing_widget_configuration(self):
        """ Get information to configure widget according to previous settings

        Returns
        -------
        (bool, tuple)
            Whether widget is enabled; widget arguments.

        """

        self.enabled, self.widget_args = self.configure(self.widget)

    def update(self):
        """ Update mesh controlled by widget.

        Configure widget according to previous settings,
        then apply the widget's callback.

        """

        self.check_existing_widget_configuration()
        self.callback(self.plotter, self.mesh_name)(*self.widget_args)

    def apply(self):
        """ Create the widget and apply it to create the mesh it controls """
        creator_func = self.get_creator_func()
        if callable(self.configure):
            self.check_existing_widget_configuration()
        self.plotter.widgets[self.name] = creator_func(
            self.callback(self.plotter, self.mesh_name),
            *self.widget_args, **self.widget_kwargs
        )
        self.widget.SetEnabled(self.enabled)

        self.plotter.get_filter_formula(self.mesh_name).widget_name = self.name

    def set_color(self, *widget_color):
        """ Set widget color

        Note that this does not control the color of any mesh.

        Parameters
        ----------
        widget_color : tuple
            Color as rgb floats, or as one-element tuple containing string
            of color name or hex value.

        """

        widget = self.widget
        plotter_settings = self.plotter.settings
        if len(widget_color) == 0:
            widget_color = (plotter_settings["widget_outline_color"],)
        if len(widget_color) == 1:
            widget_color = widget_color[0]
            if widget_color[0] == "#":
                widget_color = np.array(
                    ovm_utilities.hex_to_rgb(widget_color)
                ) / 256
            elif isinstance(widget_color, str):
                widget_color = matplotlib.colors.to_rgb(widget_color)
        assert len(widget_color) in [3, 4]
        self.color = widget_color

        for attr_name in [
            "GetNormalProperty",
            "GetLineProperty",
            "GetOutlineProperty",
            "GetHandleProperty"
        ]:
            if hasattr(widget, attr_name):
                attr = getattr(widget, attr_name)
                widget_property = attr()
                widget_property.SetDiffuse(plotter_settings["widget_diffuse"])
                widget_property.SetSpecular(plotter_settings["widget_specular"])
                widget_property.SetSpecularPower(
                    plotter_settings["widget_specular_power"]
                )
                widget_property.SetColor(widget_color)

    def set_outline_visibility(self, visibility: int):
        """ Set visibility of a widget's box outline

        Parameters
        ----------
        visibility : int
            Make outline visible unless `visibility` equals `0`.

        """

        attr_name = "GetOutlineProperty"
        if hasattr(self.widget, attr_name):
            widget_property = getattr(self.widget, attr_name)()
            opacity = 0 if visibility == 0 else 1
            widget_property.SetOpacity(opacity)
