""" Define default values for mesh appearance in plots """

from matplotlib import colormaps


def make_settings_dict(user_settings, open_viewmin_theme, off_screen):
    """ Create a dictionary of plotter default settings

    Parameters
    ----------
    user_settings : dict
        User-provided settings
    open_viewmin_theme : dict
        open-viewmin theme for default colors
    off_screen : bool
        Whether plotter is off-screen

    Returns
    -------
    dict
        Settings dictionary for plotter

    """

    settings_dict = dict(
        actor_color=open_viewmin_theme['yellow'],
        slice_color=open_viewmin_theme['mediumgray'],
        widget_color="white",
        isosurface_color=open_viewmin_theme['lightblue'],
        bounding_box_color='white',
        background_color=(0.1, 0.1, 0.1),
        background_top_color=(0.5, 0.5, 0.5),
        lighting_rescale_factor=0.4,
        cylinder_resolution=20,
        # angular resolution for glyphs such as cylinders and ellipsoids;
        # larger values look nicer but take longer to compute

        widget_factor=1,
        # widget outlines exceed box size by this factor

        widget_diffuse=0.1,
        widget_specular=0.5,
        widget_specular_power=50,

        scalar_bar_maxheight=500,
        scalar_bar_maxwidth=600,
        scalar_bar_text_pad=10,
        default_mesh_kwargs=dict(
            pbr=False,
            metallic=0,
            roughness=0.25,
            diffuse=0.15,
            ambient=0,
            specular=0.3,
            specular_power=24,
            point_size=16,
            show_scalar_bar=True,
            scalars=None,
            scalar_bar_args=dict(
                interactive=not off_screen,
                vertical=True,
                title_font_size=14,
                label_font_size=12,
                n_labels=3,
                height=50,
                n_colors=1000,
                fmt="%.3f",
                title="",
                render=True
            )
        ),
        rod_aspect_ratio=5,
        smoothing_iterations=50,  # for smoothing contours
        line_width=24,
        glyph_stride=2,
        glyph_shape="line",
        scale_mesh_name="ones",
        categories_colormap="Dark2",
        scalar_bar_aspect_ratio=0.2,
        actors_color_cycle=list(colormaps["tab10"].colors),
        title="open-ViewMin: Visualization Environment for 3D " \
              "Orientation Field Data",
        default_mesh_name="fullmesh"
    )

    settings_dict["widget_outline_color"] = settings_dict["widget_color"]
    settings_dict["default_mesh_kwargs"]["color"] = (
        settings_dict["actor_color"]
    )
    settings_dict['isosurface_kwargs'] = (
        settings_dict['default_mesh_kwargs'].copy()
    )
    settings_dict['isosurface_kwargs']["color"] = (
        settings_dict["isosurface_color"]
    )
    settings_dict["default_slice_kwargs"] = dict(
        opacity=1, ambient=1, diffuse=0, specular=0, roughness=0, pbr=False
    )

    # override and/or add to settings with those passed by user
    for key, value in zip(user_settings.keys(), user_settings.values()):
        settings_dict[key] = value

    return settings_dict


# Colors for University of California, Merced, color scheme
UCMerced_theme = dict(
    darkblue='#002856',
    yellow='#DAA900',
    lightblue='#0091B3',
    mediumblue='#005487',
    darkgray='#5B5B5B',
    mediumgray='#E5E5E5',
    lightgray='#EFEFEF',
    orange='#F18A00',
    green='#64A45A'
)

# Colors for Johns Hopkins University color scheme
Hopkins_theme = dict(
    darkblue='#002D72',
    lightblue='#68ACE5',
    yellow='#F1C400',
    orange='#FF9E1B',
    mediumgray='#E5E5E5',
    lightgray='#EFEFEF',
    green="#009B77",
    magenta="#A15A95"
)

# Colors for Emory University color scheme
Emory_theme = dict(
    darkblue='#012169',
    lightblue='#007dba',
    yellow='#f2a900',
    orange='#c35413',
    mediumgray='#b1b3b3',
    lightgray='#d9d9d6',
    green='#348338',
    magenta='#c6007e'
)
