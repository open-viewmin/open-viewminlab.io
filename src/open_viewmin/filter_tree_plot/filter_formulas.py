""" Define classes for reusable PyVista filters and widgets, which can be
re-applied in the Plotter based on changes to other meshes. """


import inspect
import numpy as np
import pyvista as pv
import vtkmodules.vtkRenderingOpenGL2

from open_viewmin.filter_tree_plot.utilities import ovm_utilities, scalar_bars
from open_viewmin.filter_tree_plot.filters import sample, glyphs
from open_viewmin.filter_tree_plot.widget_formulas import WidgetFormula


class FilterFormula:
    """ Class for reusable PyVista filters.


    Parameters
    ----------
    plotter : :class:`~open_viewmin.FilterTreePlot`
        Parent plotter
    name : str or `None`
        Name of filter formula and its mesh. Pass `None` to default to
        plotter's default mesh name.
    parent_mesh_name : str or `None`
        Name of parent mesh. Pass `None` to default to
        plotter's default mesh name.
    filter_callable : callable(), str, or `None`
        Function of parent mesh that returns a callable, which when called
        applies a filter to (re-)create a child mesh. Pass `None` to use an
        "identity filter", making the parent its own child.
    filter_kwargs : dict or `None`, optional
        Dictionary of keyword arguments supplied to the function returned by
        `filter_callable`. `None` is equivalent to `dict()`.
    mesh_kwargs : dict or `None`, optional
        Dictionary of keyword arguments to `pyvista.Plotter.add_mesh` for
        customization of mesh visualization. `None` is equivalent to `dict()`.
    random : bool or `None`, optional
        Whether to use random sampling, if applicable.
    has_actor : bool, optional
        Whether to create an actor visualizing the mesh.
    in_place : bool, optional
        Whether to modify a mesh in-place rather than creating a child mesh.
    ovm_info : dict or `None`
        Dictionary of keyword arguments for ad-hoc purposes.
        `None` is equivalent to `dict()`.
    widget_name : str or `None`
        Name of widget, if applicable, that creates this filter formula
    apply_upon_creation : bool, optional
        Whether to apply the filter formula, creating its mesh, immediately
        upon creating the filter formula.
    children : list of str, or `None`, optional
        List of names of child meshes. `None` is equivalent to `[]`.
    colorbar : VTK scalar bar actor or `None`, optional
        Existing colorbar actor, if applicable.
    **extra_filter_kwargs : dict, optional
        Extra keyword arguments to append to `filter_kwargs` (for convenience).

    """

    def __init__(
        self, plotter, name=None, parent_mesh_name=None,
        filter_callable=None, filter_kwargs=None, mesh_kwargs=None,
        random=None, has_actor=True, in_place=False, ovm_info=None,
        widget_name=None, apply_upon_creation=True, children=None,
        colorbar=None, **extra_filter_kwargs
    ):

        self.plotter = plotter

        self._allowed_args_to_add_mesh = (
            inspect.getfullargspec(plotter.add_mesh).args
        )
        # parent mesh defaults to first root mesh of plotter
        if parent_mesh_name is None:
            parent_mesh_name = plotter.default_mesh_name
        if name is None:
            self.name = self.plotter.default_mesh_name
        else:
            self.name = name
        if filter_kwargs is None:
            filter_kwargs = dict()
        filter_kwargs = {**filter_kwargs, **extra_filter_kwargs}
        if ovm_info is None:
            ovm_info = dict()
        self.ovm_info = ovm_info
        if random is not None:
            self.ovm_info["random"] = random
        self.parent_mesh_name = parent_mesh_name
        if children is None:
            self.children = []
        else:
            self.children = children

        if filter_callable is None:
            self.filter_callable = self.identity_callable
        elif isinstance(filter_callable, str):
            self.filter_callable = self.string_to_filter(filter_callable)
        else:
            self.filter_callable = filter_callable

        self.filter_kwargs = filter_kwargs
        self.has_actor = has_actor
        self.in_place = in_place
        self.colorbar = colorbar
        self.widget_name = widget_name

        # A list of tasks, written as function of filter_formula,
        # to perform after updating self but before updating children,
        # such as any dataset calculations that should be passed to children.
        self.do_after_update = []

        if mesh_kwargs is None:
            mesh_kwargs = dict()
        if (
            mesh_kwargs.get("scalars") is None
            and mesh_kwargs.get("color") is None
            and self.has_actor
        ):
            mesh_kwargs["color"] = self.get_auto_color()
        self.mesh_kwargs = mesh_kwargs
        if apply_upon_creation and not self.is_filter_tree_root:
            self.update(update_actor=False)
            if self.has_actor:
                self.update_actor(is_new=True)

    def __repr__(self):
        """ Print keyword arguments to :class:`FilterFormula` and their current values

        Returns
        -------
        str

        """
        ret = str(self.__class__.__name__) + "\n"
        dict_rep = self.__dict__
        for key in dict_rep.keys():
            if key == 'plotter':
                val = self.plotter.__class__.__name__
            elif key[0] == "_":
                continue
            else:
                val = dict_rep[key]
            ret += f"  {key}: {val}\n"
        return ret

    def __getitem__(self, key):
        """ Access mesh's datasets in dictionary-like manner """
        # noinspection PyUnresolvedReferences
        return self.mesh.__getitem__(key)

    def __setitem__(self, key, set_to):
        """ Set mesh's datasets in dictionary-like manner """
        # noinspection PyUnresolvedReferences
        if callable(set_to):
            return self.add_calculation(key, set_to)
        return self.mesh.__setitem__(key, set_to)

    @property
    def points(self):
        """ Get mesh's points

        Returns
        -------
        numpy.ndarray

        """
        return self.mesh.points

    @property
    def array_names(self):
        """ Get mesh's dataset names

        Returns
        -------
        list[str]

        """

        # noinspection PyUnresolvedReferences
        return self.mesh.array_names

    @staticmethod
    def identity_callable(mesh):
        """ Get a filter formula callable that simply copies the parent mesh

        Parameters
        ----------
        mesh : pyvista.dataset

        Returns
        -------
        callable()

        """

        return lambda: mesh

    @property
    def mesh(self):
        """ Get child mesh

        Returns
        -------
        pyvista.dataset

        """

        return self.plotter.get_mesh(self.name)

    @property
    def parent_mesh(self):
        """ Get parent mesh

        Returns
        -------
        pyvista.dataset

        """

        return self.plotter.get_mesh(self.parent_mesh_name)

    @property
    def is_filter_tree_root(self):
        """ Check whether mesh is a root of the filter tree

        Returns
        -------
        bool

        """

        return self.parent_mesh_name == self.name

    @property
    def root_ancestor(self):
        """ root (possibly self) of filter tree from which this filter formula
        descends
        """
        if self.is_filter_tree_root:
            return self
        else:
            return self.parent_filter_formula.root_ancestor  # recurse

    @property
    def random(self):
        """ Check whether random sampling has been selected

        Returns
        -------
        bool

        """

        return self.ovm_info.get("random", False)

    def add_filter(self, filter_callable, **kwargs):
        """ Create a callable that returns a filter formula whose parent mesh
        is this formula's child mesh

        Used for dictionary-style assignment of new filter formula to plotter.

        Parameters
        ----------
        filter_callable : callable()
            Function mapping new mesh name to new filter formula

        kwargs : dict, optional
            Keyword arguments to `FilterFormula`

        Returns
        -------
        callable()

        """

        def filter_formula_lambda(mesh_name):
            """ Callable that returns a filter formula whose parent mesh
            is the calling formula's child mesh

            Parameters
            ----------
            mesh_name : str
            """
            self.plotter.add_filter_formula(
                name=mesh_name,
                parent_mesh_name=self.name,
                filter_callable=filter_callable,
                **kwargs
            )
        return filter_formula_lambda

    def add_calculation(self, name, calc_callable):
        """ Create a data array for this mesh that is recalculated whenever
        the mesh is updated. The function accomplishing this is added to the
        :class:`~open_viewmin.filter_tree_plot.filter_formulas.FilterFormula`'s :attr:`~do_after_update` list.

        Example: If we have a
        :class:`~open_viewmin.nematic.nematic_plot.NematicPlot` `p`, with a
        glyphs `filter_formula` called "director", the following creates a
        scalar data array holding the normal component of the "director"
        orientation field.

        >>> p["director"].add_calculation("normal_component", lambda a: np.inner(a.orientations, a.normal))

        Equivalently, you can assign `calc_callable` as the value corresponding
        to key `name`:

        >>> p["director"]["normal_component"] = lambda a: np.inner(a.orientations, a.normal)

        Parameters
        ----------
        name : str

        calc_callable : callable()
            Function mapping filter_formula to calculated data array.

        """

        def task(_filter_formula):
            """ Recalculate data array for mesh upon update of a `FilterFormula`

            Parameters
            ----------
            _filter_formula : FilterFormula
            """

            _filter_formula.mesh[name] = calc_callable(_filter_formula)
        self.do_after_update.append(task)
        task(self)  # create the array for the first time now

    def add_glyphs(self, **kwargs):
        # noinspection GrazieInspection
        """ Create a glyphs child filter formula

        Parameters
        ----------
        kwargs : dict, optional

        Returns
        -------
        callable()
            Function of new mesh name; adds glyphs filter formula

        """

        return lambda new_mesh_name: glyphs.add_glyphs_to_mesh(
            self.plotter, new_mesh_name, mesh_name=self.name, **kwargs
        )

    def make_sure_parent_knows_about_me(self):
        """ Make sure that this filter formula's name is in the parent
        mesh filter formula's list of children

        """

        if not self.is_filter_tree_root:
            parent_filter_formula = self.plotter.filter_formulas[
                self.parent_mesh_name
            ]
            if self.name not in parent_filter_formula.children:
                parent_filter_formula.children.append(self.name)

    def remove_child_relationship_to_all_parents(self):
        """
        Cause any meshes claiming this mesh as a child to forget that
        relationship, a prerequisite for deleting this mesh.

        """

        for other_filter_formula in self.plotter.filter_formulas.values():
            if self.name in other_filter_formula.children:
                other_filter_formula.children.remove(self.name)

    @staticmethod
    def handle_filter_or_numpy(filter_result, parent_mesh, filter_name):
        # noinspection GrazieInspection
        """ Properly handle filter results that may be mesh or array

                If the filter result is an array, assigns the filter result as a new
                dataset on a copy of the parent mesh.

                Parameters
                ----------

                filter_result : pyvista.dataset or numpy.ndarray
                    Result of filter

                parent_mesh : pyvista.dataset
                    Parent mesh

                filter_name : str
                    Name for new dataset in case `filter_result` is an array.

                Returns
                -------
                pyvista.dataset
                    New child mesh

                """

        if isinstance(filter_result, np.ndarray):
            # noinspection PyUnresolvedReferences
            filtered_mesh = parent_mesh.copy()
            filtered_mesh[filter_name] = filter_result
        elif isinstance(filter_result, pv.DataSet):
            filtered_mesh = filter_result
        elif isinstance(filter_result, pv.MultiBlock):
            filtered_mesh = filter_result.combine()
        else:
            filtered_mesh = None
        return filtered_mesh

    @staticmethod
    def string_to_filter(filter_string):
        """ Convert filter name to callable

        Convert string name of PyVista filter to a callable that produces that
        filter. If the argument is not a string, return the argument (for cases
        where the filter callable is passed rather than its string name).

        Parameters
        ----------
        filter_string : str or object

        Returns
        -------
        callable()
            Function mapping existing mesh to a new mesh via the named filter.

        object
            If `filter_string` is not a string, returns `filter_string`

        """

        if isinstance(filter_string, str):
            return lambda mesh: getattr(mesh, filter_string)
        else:
            return filter_string

    def update(self, update_actor=True, ovm_info=None, **filter_kwargs):
        """  Update a mesh by calling its creator filter

        Parameters
        ----------

        update_actor : bool, optional
            Whether to update the actor visualizing the mesh

        ovm_info : dict or `None`, optional
            New or modified items for mesh settings dictionary

        **filter_kwargs :
            New or modified keyword arguments for filter callable

        Returns
        -------
        pyvista.dataset
            Updated child mesh

        """

        self.make_sure_parent_knows_about_me()
        ovm_utilities.copy_from_dict_recursively(
            filter_kwargs, self.filter_kwargs
        )
        if ovm_info is not None:
            ovm_utilities.copy_from_dict_recursively(
                ovm_info, self.ovm_info
            )
        try:
            # noinspection PyCallingNonCallable
            filter_result = (
                self.filter_callable(self.parent_mesh)(**self.filter_kwargs)
            )
        # ignore calls to PyVista filters with incorrect types for filter_kwargs
        except (TypeError, ValueError, KeyError) as err:
            print(
                f"{err.__class__.__name__} "
                f"while updating mesh \'{self.name}\':"
            )
            print(err)
            self.hide()
            if isinstance(err, TypeError):
                print(
                    f"Consider deleting bad keyword from "
                    f"{self.plotter.__class__.__name__}[\"{self.name}\"]"
                    f".filter_kwargs"
                )
            return None
        else:
            filtered_mesh = (
                self.handle_filter_or_numpy(
                    filter_result, self.parent_mesh, self.name
                )
            )

            self.plotter.meshes_dict[self.name] = filtered_mesh

            # do custom tasks after updating mesh but before updating actor,
            # in case custom tasks define arrays used make actor
            for task in self.do_after_update:
                task(self)

            if update_actor and self.has_actor:
                try:
                    self.update_actor()
                except ValueError:
                    self.plotter.remove_actor(self.name)
            for task in self.do_after_update:
                task(self)

            self.update_children()
        return filtered_mesh

    def get_auto_color(self):
        """ Get next color in plotter's color sequence

        Returns
        -------
        tuple(float)
            color rgb values

        """

        auto_color = self.plotter.actors_color_cycle.pop(0)
        self.plotter.actors_color_cycle.append(auto_color)
        return auto_color

    def _interpret_color_or_scalars(self, mesh_kwargs):
        """ Check if mesh coloring should be updated as a solid color or
        color array.

        Parameters
        ----------
        mesh_kwargs : dict
            Mesh visualizing settings

        Returns
        -------
        (bool, bool, dict)
            Whether to update solid color, whether to update color array, and
            mesh visualizing settings dictionary

        """

        updated_color = mesh_kwargs.get('color')
        updated_scalars = mesh_kwargs.get('scalars')
        if updated_color is not None:
            mesh_kwargs['scalars'] = None
            if updated_color == "auto":
                mesh_kwargs['color'] = self.get_auto_color()
        elif updated_scalars is not None:
            mesh_kwargs['color'] = None
        do_update_scalars = (mesh_kwargs.get('scalars') is not None)
        do_update_colors = (mesh_kwargs.get('colors') is not None)
        return do_update_colors, do_update_scalars, mesh_kwargs

    def _remove_bad_mesh_kwargs(self, mesh_kwargs):
        """ Remove unexpected keyword arguments to `pyvista.Plotter.add_mesh`

        Parameters
        ----------
        mesh_kwargs : dict
            Mesh visualization settings

        Returns
        -------
        dict
            Mesh visualization settings with bad keyword arguments removed

        """
        good_mesh_kwargs = dict()
        for key in mesh_kwargs.keys():
            if key in self._allowed_args_to_add_mesh:
                good_mesh_kwargs[key] = mesh_kwargs[key]
            else:
                print(
                    f"{key} is not a valid keyword argument to "
                    f"{self.plotter.__class__.__name__}.add_mesh; ignoring."
                )
        return good_mesh_kwargs

    def update_actor(self, is_new=False, **mesh_kwargs):
        """ Update or create mesh visualizing actor

        Parameters
        ----------
        is_new : bool, optional
            Whether actor has just been created (to avoid duplicate warnings)

        mesh_kwargs : dict, optional
            New or modified keyword arguments to `pyvista.Plotter.add_mesh`

        Returns
        -------
        vtkmodules.vtkRenderingOpenGL2.vtkOpenGLActor
            Updated actor

        """

        do_update_color, do_update_scalars, mesh_kwargs = (
            self._interpret_color_or_scalars(mesh_kwargs)
        )
        mesh_kwargs = self._remove_bad_mesh_kwargs(mesh_kwargs)
        ovm_utilities.copy_from_dict_recursively(
            mesh_kwargs, self.mesh_kwargs
        )
        # Maintain actor's current visibility
        actor = self.plotter.renderer.actors.get(self.name)
        if actor is None:
            visibility = 1  # If actor is newly created, make it visible.
        else:
            # noinspection PyUnresolvedReferences
            visibility = actor.GetVisibility()

        actor_mesh_kwargs = self.mesh_kwargs
        # Copy any missing kwargs from defaults
        ovm_utilities.add_if_dict_lacks(
            actor_mesh_kwargs,
            self.plotter.settings["default_mesh_kwargs"]
        )
        scalar_bar_args = actor_mesh_kwargs['scalar_bar_args']

        # Make sure mesh's "name" is the same as actor name
        actor_mesh_kwargs["name"] = self.name

        # Update the color array if new scalars were passed
        if do_update_scalars:
            self._update_actor_color_array()

        # Otherwise, update solid color
        elif do_update_color:
            self._update_actor_solid_color()

        if self.colorbar is not None:
            # Record colorbar's current properties
            scalar_bars.copy_scalar_bar_properties(
                self.colorbar, scalar_bar_args
            )
            self.remove_colorbar()

        # If we will be making a colorbar...
        if (
            actor_mesh_kwargs['scalars'] is not None
            and actor_mesh_kwargs["show_scalar_bar"]
        ):
            do_adjust_scalar_bar = True
            self.give_colorbar_unique_title()
            # scalar_bar_args['title'] = actor_mesh_kwargs['scalars']
        else:
            do_adjust_scalar_bar = False
            defaults = self.plotter.settings['default_mesh_kwargs']
            scalar_bar_args['title'] = defaults['scalar_bar_args']['title']

        # Use Pyvista's "add_mesh()" to (re-)assign filtered_mesh,
        # visualized with mesh_kwargs, to the plotter
        try:
            # noinspection PyTypeChecker
            self.plotter.add_mesh(self.mesh, **actor_mesh_kwargs)
        except (TypeError, ValueError) as err:
            # if there's a problem with an actor, print the error if the actor
            # is for a filter being newly created, otherwise raise the error
            if visibility and is_new:
                print(f"{err.__class__.__name__} while updating actor \'{self.name}\':", err)
            else:
                raise err
        else:
            self.make_sure_parent_knows_about_me()

            # If we are coloring by scalars and mesh_kwargs['show_scalar_bar']
            # is True, then the scalar bar has been updated.
            if do_adjust_scalar_bar:
                scalar_bar_title = scalar_bar_args['title']
                this_scalar_bar = self.plotter.scalar_bars[scalar_bar_title]
                self.colorbar = this_scalar_bar
                scalar_bars.update_scalar_bar(
                    self.plotter, self.name, this_scalar_bar
                )

            # Set actor visibility as decided above
            # noinspection PyTypeChecker
            self.plotter.set_actor_visibility(self.name, visibility)
        return actor

    def set(self, **mesh_kwargs):
        """ Shorthand for :meth:`update_actor`

        Parameters
        ----------
        **mesh_kwargs :
            New or modified keyword arguments to `pyvista.Plotter.add_mesh`

        Returns
        -------
        vtkmodules.vtkRenderingOpenGL2.vtkOpenGLActor
            Updated actor

        """

        return self.update_actor(**mesh_kwargs)

    def hide(self):
        """ Make mesh's actor invisible """

        self.plotter.set_actor_visibility(self.name, False)
        toolbar = self.plotter.toolbars.get(self.name)
        if toolbar is not None:
            toolbar.relink_visibility_checkbox()

    def update_children(self):
        """ Update child meshes

        Calls `update` function of each filter formula named in `self.children`

        """

        for child_name in self.children:
            if (
                child_name in self.plotter.mesh_names()
                and child_name != self.name
            ):
                child_filter_formula = (
                    self.plotter.filter_formulas.get(child_name)
                )
                child_filter_formula.update()

    def remove_colorbar(self):
        """ Remove color bar for this filter formula's mesh """
        if self.colorbar is not None:
            old_scalar_bar = self.colorbar
            try:
                old_scalar_bar_name = old_scalar_bar.GetTitle()
                self.plotter.remove_scalar_bar(old_scalar_bar_name)
            except KeyError:
                pass
            del self.colorbar
            self.colorbar = None

    def give_colorbar_unique_title(self):
        """ Check if color bar lacks a title, and assign one if so

        Returns
        -------
        str
            Color bar title

        """

        scalar_bar_args = self.mesh_kwargs["scalar_bar_args"]
        color_array_name = self.mesh_kwargs["scalars"]
        scalar_bar_args["title"] = f"{color_array_name} ({self.name})"
        return scalar_bar_args["title"]

    def _update_actor_solid_color(self):
        """ Visualize mesh with solid color """

        actor_mesh_kwargs = self.mesh_kwargs
        actor_mesh_kwargs["scalars"] = None
        actor_mesh_kwargs["show_scalar_bar"] = False
        self.remove_colorbar()

    def _update_actor_color_array(self):
        """ Visualize mesh with color array """

        actor_mesh_kwargs = self.mesh_kwargs
        actor_mesh_kwargs["color"] = None

        # make scalar bar visible whenever we change color to new scalars source
        actor_mesh_kwargs["show_scalar_bar"] = True
        if "scalar_bar_args" not in actor_mesh_kwargs.keys():
            # if we haven't made a scalar bar already for this actor,
            # use default settings
            actor_mesh_kwargs["scalar_bar_args"] = (
                self.plotter.settings["default_mesh_kwargs"]["scalar_bar_args"]
            )

        if self.colorbar is not None:
            old_scalar_bar = self.colorbar
            scalar_bar_args = actor_mesh_kwargs["scalar_bar_args"]

            # record scalar bar's properties, then remove it
            scalar_bars.copy_scalar_bar_properties(
                old_scalar_bar, scalar_bar_args
            )
            self.remove_colorbar()

    @property
    def is_glyphs(self):
        """ Check whether mesh is glyphs

        Returns
        -------
        bool

        """

        return "orient" in self.filter_kwargs.keys()

    @property
    def orientations(self):
        """ Get orientations data array for glyphs; `None` for non-glyphs """

        if self.is_glyphs:
            return self.parent_mesh[self.filter_kwargs["orient"]]
        return None

    def _inherit(self, filter_kwarg):
        """ If this `FilterFormula` has no value currently assigned to
        `filter_kwarg`, try to inherit the value from an ancestor
        `FilterFormula`

        Parameters
        ----------
        filter_kwarg : str

        Returns
        -------
        inherited_filter_keyword_argument

        """

        if self.is_filter_tree_root:
            return None
        ret = self.filter_kwargs.get(filter_kwarg, None)
        if ret is None:
            # look recursively in parent's filter kwargs
            return self.parent_filter_formula._inherit(filter_kwarg)
        return ret

    @property
    def normal(self):
        """ Normal vector to plane

        Returns
        -------
        numpy.ndarray
        """
        return self._inherit("normal")

    @property
    def origin(self):
        """ Origin of plane

        Returns
        -------
        numpy.ndarray

        """

        return self._inherit("origin")

    def set_glyph_shape(self, new_shape, **geom_args):
        """ Modify shape of glyphs

        Parameters
        ----------
        new_shape : str
            Shape name
        geom_args : dict, optional
            Keyword arguments for shape

        """

        assert self.is_glyphs
        assert isinstance(new_shape, str)
        assert new_shape in self.plotter.geometric_objects.keys()
        geom_objs = self.plotter.geometric_objects
        self.filter_kwargs["geom"] = geom_objs[new_shape](**geom_args)
        self.ovm_info["glyph_shape"] = new_shape
        self.update()

    @property
    def parent_filter_formula(self):
        """ Get parent mesh's filter formula

        Returns
        -------
        FilterFormula

        """

        return self.plotter.get_filter_formula(self.parent_mesh_name)

    def set_glyphs_stride(self, new_stride):
        """ Set spacing between glyphs

        Parameters
        ----------
        new_stride : int
            New spacing
        """
        assert self.is_glyphs
        assert isinstance(new_stride, (int, float))
        parent_filter_formula = self.parent_filter_formula
        assert "stride" in parent_filter_formula.filter_kwargs.keys()
        parent_filter_formula.update(stride=new_stride)

    def set_glyphs_scale(self, new_scale):
        """ Set global scale factor for glyphs

        Parameters
        ----------
        new_scale : float
            New scale factor

        """

        assert self.is_glyphs
        assert isinstance(new_scale, (int, float))
        self.update(factor=new_scale)

    def get_centroids(
        self, num_centroids=10, ref_pt_idx=0, mesh_name_to_probe=None
    ):
        """ Add centroids filter formula

        Child filter formula probes a given mesh at centroids calculated
        from the points of this filter formula's mesh.

        Parameters
        ----------
        num_centroids : int, optional
            Number of centroids
        ref_pt_idx : int, optional
            Index of starting point for calculating centroids
        mesh_name_to_probe : str or `None`, optional
            Name of mesh to probe (source of datasets).
            Pass `None` to default to `self.plotter.default_mesh_name`.

        Returns
        -------
        str
            Name of sampled (child) mesh

        """

        return sample.add_centroids_probe_filter_formula(
            self.plotter,
            parent_mesh_name=mesh_name_to_probe,
            find_centroids_of_mesh_name=self.name,
            num_centroids=num_centroids,
            ref_pt_idx=ref_pt_idx,
            sampled_mesh_name=None,
        )

    def get_circuits(
        self, mesh_to_probe_name=None, normals_name=None,
        radius=3, n_samples=100, use_ints=False
    ):
        """ Add circuits filter formula

        Parameters
        ----------
        mesh_to_probe_name : str
            Name of mesh to probe

        normals_name : str or `None`, optional
            Name of vectors dataset defined on `mesh_of_circuit_centers`
            to be used as normal directions to circuits.
            Pass `None` to default to name of `active_vectors` dataset of parent
            mesh.

        radius : float, optional
            Radius of each circuit

        n_samples : int, optional
            Number of circuits

        use_ints : bool, optional
            Whether to round coordinates of circuit points to integers

        Returns
        -------
        str or `None`
            Name of sampled (child) mesh,
            or `None` if no child mesh was created.

        """

        return sample.add_circuit_probe_filter_formula(
            self.plotter,
            mesh_to_probe_name=mesh_to_probe_name,
            mesh_of_circuit_centers_name=self.name,
            normals_name=normals_name,
            radius=radius,
            n_samples=n_samples,
            use_ints=use_ints
        )

    def get_eigenvec(
        self, tensors_name=None, evec_idx=-1, new_dataset_name=None
    ):
        """ Calculate an eigenvector of symmetric second-rank tensor dataset

        Parameters
        ----------

        tensors_name : str or `None`, optional
            Name of symmetric rank-2 tensors dataset.
            Pass `None` to default to name of most recently created
            symmetric rank-2 tensors dataset.

        evec_idx : int, optional
            Index of eigenvector to take, ordered by ascending eigenvalues

        new_dataset_name : str or `None`
            Name of vector dataset storing eigenvector.
            Pass `None` to default to `"_eigenvec"` appended to `tensors_name`.

        Returns
        -------
        numpy.ndarray

        """
        if tensors_name is None:
            tensors_name = self.plotter.symmetric_tensor_fields(
                mesh_name=self.name
            )[-1]
        if new_dataset_name is None:
            new_dataset_name = tensors_name + "_eigenvec"
        # noinspection PyUnresolvedReferences
        dataset = self.mesh[tensors_name]
        if dataset is not None:
            dataset = dataset.reshape(-1, 3, 3)
            eigvecs = np.linalg.eigh(dataset)[1]
            # noinspection PyUnresolvedReferences
            self.mesh[new_dataset_name] = eigvecs[:, :, evec_idx]
            # noinspection PyUnresolvedReferences
            return self.mesh[new_dataset_name]

    # noinspection PyUnresolvedReferences
    def color_by_active_scalars(self, categories=None):
        """ Set mesh's color array to most recently used scalar dataset

        Parameters
        ----------
        categories : bool, optional
        """

        scalars_name = self.mesh.active_scalars_name
        scalars_data = self.mesh[scalars_name]
        self.set(
            scalars=scalars_name,
            clim=[np.min(scalars_data), np.max(scalars_data)]
        )
        if categories is None:
            dtype_str = str(self[scalars_name].dtype)
            categories = dtype_str.startswith("int")
        if categories:
            self.set(cmap=self.plotter.settings["categories_colormap"])
