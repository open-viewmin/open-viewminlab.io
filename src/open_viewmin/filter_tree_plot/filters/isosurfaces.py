""" Utilities for creating single-value contour surfaces filtered from a 3D mesh """

import numpy as np


def update_isosurface(
    plotter, mesh_name, dataset_name=None, contour_values=None,
    **contour_kwargs
):
    """ Create, or alter and re-apply, a filter formula for a `contour` filter.

    Parameters
    ----------
    plotter : open_viewmin.FilterTreePlot

    mesh_name : str
        Name of isosurface mesh to update.

    dataset_name : str or `None`, optional
        Name of scalar dataset from which to calculate isosurface. Pass
        `None` to keep existing dataset name.

    contour_values : float, list of floats, or `None`
        Contour value or values. Pass `None` to keep existing value (defaults
        to `0.5` if current value is not defined).

    contour_kwargs : dict or `None`, optional
        Keyword arguments to `pyvista.Plotter.add_mesh`

    Returns
    -------
    open_viewmin.FilterFormula
        Updated filter formula for isosurface

    """

    filter_kwargs = dict()
    if dataset_name is not None:
        filter_kwargs["scalars"] = dataset_name
    if contour_values is None:
        if "isosurfaces" not in filter_kwargs.keys():
            filter_kwargs["isosurfaces"] = [0.5]
    else:
        # keep existing contour values
        if type(contour_values) is not list:
            contour_values = [contour_values]
        filter_kwargs["isosurfaces"] = contour_values

    def isosurface_filter_creator(parent_mesh):
        return make_smooth_contour_func(
            parent_mesh,
            plotter.settings["smoothing_iterations"]
        )

    return plotter.add_filter_formula(
        name=mesh_name,
        filter_callable=isosurface_filter_creator,
        filter_kwargs=filter_kwargs,
        mesh_kwargs=contour_kwargs
    )


def make_smooth_contour_func(mesh, n_iter):
    """ Make a callable that applies Pyvista's `contour` filter to a mesh,
    and smooths the result with the `smooth` filter.

    Parameters
    ----------
    mesh : pyvista.dataset
        Mesh from which to calculate contour.

    n_iter : int
        Number of smoothing iterations.

    Returns
    -------
    callable()

    """

    def return_func(*args, **kwargs):
        if "compute_normals" not in kwargs.keys():
            kwargs["compute_normals"] = True
        # noinspection PyUnresolvedReferences
        ret = mesh.contour(*args, **kwargs)
        if ret.n_points > 0:
            try:
                ret = ret.smooth(n_iter)
            # if smoothing fails, return un-smoothed surface
            except ValueError:
                pass
        return ret

    return return_func


def calculate_pontryagin_thom_surface(
    mesh, vectors_name, normal_dir=None, theta_key=None, phi_key=None
):
    """ Calculate angles for Pontryagin-Thom construction

    For vector array named `vectors_name` defined on `mesh`, calculates angle
    data for the vectors relative to a reference direction `normal_dir`, for
    use in making a Pontryagin-Thom surface.

    Parameters
    ----------
    mesh : pyvista.DataSet
        PyVista mesh containing the orientation field vectors.
    vectors_name : str
        Name of vector array in `mesh`.
    normal_dir : [int, int, int], optional
        Normal direction in orientation-space.
    theta_key : str, optional
        Name to give calculated array of polar angles between the orientation
        field and `normal_dir`.
    phi_key : str, optional
        Name to give calculated array of azimuthal angles of the orientation
        field about the `normal_dir` axis, with an arbitrary zero.

    Returns
    -------
    str, str
        Keys for scalar arrays containing the calculated polar and azimuthal
        angles, respectively, of the orientation field relative to the given
        normal direction.

    """

    if normal_dir is None:
        normal_dir = [1, 0, 0]
    vectors = mesh[vectors_name]

    normal_dir = np.asarray(normal_dir, dtype=float)
    normal_dir /= np.sqrt(np.sum(normal_dir ** 2))  # normalize

    vectors_mag = np.sqrt(np.sum(vectors ** 2, axis=-1))
    vectors_mag += (vectors_mag == 0)
    for i in range(3):
        vectors[..., i] /= vectors_mag

    # get basis vectors for plane perpendicular to normal_dir
    pt_perp1 = np.cross(
        np.array(
            [0, 0, 1]
            if not np.array_equal(normal_dir, np.array([0, 0, 1]))
            else [1, 0, 0]
        ),
        normal_dir
    )
    pt_perp1 = (
        np.asarray(pt_perp1, dtype=float) / np.sqrt(np.sum(pt_perp1 ** 2))
    )
    pt_perp2 = np.cross(normal_dir, pt_perp1)

    if theta_key is None:
        theta_key = vectors_name + '_PT_theta'

    if phi_key is None:
        phi_key = vectors_name + '_PT_phi'

    apolar_phi_key = phi_key + " [mod pi]"

    dot_prod = vectors.dot(normal_dir)
    dot_prod_sgn = np.sign(dot_prod)
    for i in range(3):
        vectors[..., i] *= dot_prod_sgn
    dot_prod = np.abs(dot_prod)
    mesh[theta_key] = np.arccos(dot_prod)

    pt_phi = np.arctan2(vectors.dot(pt_perp2), vectors.dot(pt_perp1))
    mesh[phi_key] = np.mod(pt_phi, 2 * np.pi)
    mesh[apolar_phi_key] = np.mod(mesh[phi_key], np.pi)
    return theta_key, phi_key, apolar_phi_key
