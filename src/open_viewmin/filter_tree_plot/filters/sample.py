""" Utilities for sampling a mesh, either at regular intervals or randomly, e.g.
for plotting glyphs """

import numpy as np
from scipy.cluster.vq import kmeans
from open_viewmin.filter_tree_plot.filters import topology
import pyvista as pv


def probe(mesh, sampled_points, **kwargs):
    """ Workaround for PyVista's removal of `probe` filter

    Essentially reverses the `sample` filter.

    Parameters
    ----------
    mesh : pyvista.DataSet
    sampled_points : numpy.ndarray or pyvista.pyvista_ndarray
    **kwargs
        Keyword arguments to `pyvista.DataSet.sample`
    """
    sampled_mesh = pv.PolyData(sampled_points)
    return sampled_mesh.sample(mesh, **kwargs)


def make_sampled_mesh(
    plotter, parent_mesh_name, stride, sampled_mesh_name=None, random=False
):
    """ Create a sampled mesh, either evenly or randomly sampled

    Parameters
    ----------
    plotter : :class:`~open_viewmin.FilterTreePlot`

    parent_mesh_name : str
        Name of mesh to sample

    stride : float
        Spacing (or, for random sampling, average spacing) between sampled
        points

    sampled_mesh_name : str or `None`, optional
        Name of child (sampled) mesh. Pass `None` to append `"_sampled"` to
        `parent_mesh_name`.

    random : bool
        Whether to use random sampling instead of evenly spaced sampling.

    Returns
    -------
    str
        Name of sampled (child) mesh

    """
    if sampled_mesh_name is None:
        sampled_mesh_name = plotter.name_mesh_without_overwriting(
            parent_mesh_name + "_sampled"
        )
    filter_info = plotter.get_filter_formula(sampled_mesh_name)
    if random is None and filter_info is not None:
        random = filter_info.ovm_info.get("random", False)
    if not random:
        stride = int(stride)
    mesh_pts_sampler = _make_mesh_pts_sampler(plotter.dims())
    plotter.add_filter_formula(
        name=sampled_mesh_name,
        parent_mesh_name=parent_mesh_name,
        filter_callable=mesh_pts_sampler,
        filter_kwargs=dict(stride=stride, random=random),
        random=random,
        has_actor=False
    )
    return sampled_mesh_name


def _make_mesh_pts_sampler(dims):
    """ Create a callable that samples a mesh evenly or randomly

    Parameters
    ----------
    dims : (int, int, int)
        System dimensions

    Returns
    -------
    callable()
        Function to sample a mesh.

    """

    def mesh_pts_sampler(mesh):
        def return_func(
            stride=1, random=False, tolerance=0, tolerance_step=0.05,
            **kwargs
        ):
            if stride > 1:
                mesh_pts = mesh.points
                try:
                    if random:
                        sampled_points = mesh_pts[
                            np.random.choice(
                                range(len(mesh_pts)),
                                size=int(len(mesh_pts)/stride**2),
                                replace=False
                            )
                        ]
                    else:
                        stride_mod_dists = np.sqrt(
                            np.sum(
                                (
                                    np.mod(mesh_pts + stride / 2, stride)
                                    - stride / 2
                                ) ** 2,
                                axis=-1
                            )
                        )
                        sampled_points = mesh_pts[
                            np.argwhere(
                                stride_mod_dists <= tolerance
                            )[:, 0]
                        ]
                        while len(sampled_points) < (
                            (np.prod(dims))**(2/3) / stride**2
                        ) and tolerance < stride:
                            tolerance += tolerance_step
                            sampled_points = mesh_pts[
                                np.argwhere(
                                    stride_mod_dists <= tolerance
                                )[:, 0]
                            ]
                except (ValueError, IndexError):
                    sampled_points = np.empty((0, 3))
                return probe(mesh, sampled_points, **kwargs)
            else:
                return mesh
        return return_func
    return mesh_pts_sampler


def _make_centroids_probe_filter_callable(
    plotter, find_centroids_of_mesh_name
):
    """ Create callable that produces a filter formula probing a parent mesh at
    calculated centroids.

    Parameters
    ----------
    plotter : open_viewmin.FilterTreePlot

    find_centroids_of_mesh_name : str
        Name of parent mesh

    Returns
    -------
    callable()

    """

    if find_centroids_of_mesh_name is None:
        find_centroids_of_mesh_name = plotter.default_mesh_name

    def centroids_probe_filter_callable(mesh):
        def return_func(num_centroids=10, ref_pt_idx=0, **kwargs):
            find_centroids_of_mesh = plotter.get_mesh(find_centroids_of_mesh_name)
            points = find_centroids_of_mesh.points
            if len(points) > 0:
                ref_pt = points[ref_pt_idx]
                dims = np.array(plotter.dims())
                seps_from_ref_pt = np.mod(points - ref_pt + dims / 2, dims) - dims / 2
                centroids, _ = kmeans(seps_from_ref_pt, num_centroids)
                centroids += ref_pt
                centroids = np.mod(centroids, dims)
            else:
                centroids = np.empty((0, 3))
            return probe(mesh, centroids, **kwargs)
        return return_func
    return centroids_probe_filter_callable


def add_centroids_probe_filter_formula(
    plotter, parent_mesh_name=None, num_centroids=10, ref_pt_idx=0,
    sampled_mesh_name=None, find_centroids_of_mesh_name=None
):
    """ Add a filter formula to a plotter to probe a mesh at calculated
    centroids.

    Parameters
    ----------
    plotter : open_viewmin.FilterTreePlot

    parent_mesh_name : str or `None`, optional
        Name of mesh to probe. Pass `None` to default to
        `plotter.default_mesh_name`. Note that this is not generally the same
        as `find_centroids_of_mesh_name`.

    num_centroids : int, optional
        Number of centroids to calculate. Larger value means finer
        subdivisions.

    ref_pt_idx : int, optional
        Index of point in parent mesh's array of points to use as arbitrary
        starting point or centroids calculation.

    sampled_mesh_name : str or `None`, optional
        Name of child (sampled) mesh. Pass `None` to use `"_centroids"`
        appended to `parent_mesh_name`.

    find_centroids_of_mesh_name : str or `None`, optional
        Name of mesh from whose points the centroids will be calculated.
        Pass `None` to default to `parent_mesh_name`.

    Returns
    -------
    str
        Name of sampled (child) mesh

    """

    if parent_mesh_name is None:
        parent_mesh_name = plotter.default_mesh_name
    if find_centroids_of_mesh_name is None:
        find_centroids_of_mesh_name = parent_mesh_name
    if sampled_mesh_name is None:
        sampled_mesh_name = plotter.name_mesh_without_overwriting(
            find_centroids_of_mesh_name + "_centroids"
        )

    filter_callable = _make_centroids_probe_filter_callable(
        plotter,
        find_centroids_of_mesh_name
    )
    plotter.add_filter_formula(
        name=sampled_mesh_name,
        parent_mesh_name=parent_mesh_name,
        filter_callable=filter_callable,
        filter_kwargs=dict(num_centroids=num_centroids, ref_pt_idx=ref_pt_idx)
    )
    # centroids mesh must be registered as child of
    # find_centroids_of_mesh_name (from whose points centroids are calculated)
    # even though it was created as a probe of parent_mesh_name
    plotter.get_filter_formula(find_centroids_of_mesh_name).children.append(
        sampled_mesh_name
    )
    plotter.get_filter_formula(parent_mesh_name).children.remove(
        sampled_mesh_name
    )

    return sampled_mesh_name


def _unit_vector_along(vec):
    """ Normalize a vector.

    Parameters
    ----------
    vec : numpy.ndarray

    Returns
    -------
    numpy.ndarray
        Normalized vector.

    """
    vec_size = np.sqrt(np.dot(vec, vec))
    if vec_size == 0:
        return vec
    else:
        return vec / vec_size


def _get_basis_for_normal_plane(normal_dir):
    """ Calculate an orthonormal basis for the plane perpendicular to a given
    direction.

    Parameters
    ----------
    normal_dir : numpy.ndarray
        Normal to the plane.

    Returns
    -------
    numpy.ndarray, numpy.ndarray
        Two basis vectors.

    """
    # normalize
    normal_dir = _unit_vector_along(normal_dir)

    arbitrary_direction = np.array([0, 0, 1])
    if np.all(arbitrary_direction == normal_dir):
        arbitrary_direction = np.array([1, 0, 0])

    basis_vec_1 = np.cross(arbitrary_direction, normal_dir)
    basis_vec_1 = _unit_vector_along(basis_vec_1)

    basis_vec_2 = np.cross(normal_dir, basis_vec_1)
    basis_vec_2 = _unit_vector_along(basis_vec_2)

    return basis_vec_1, basis_vec_2


def get_circuit(
    plotter, point, normal_dir, radius=3, n_samples=32, use_ints=False
):
    """ Calculate points on a small circuit at a given point, normal to a given
    direction.

    Parameters
    ----------
    plotter : open_viewmin.FilterTreePlot

    point : numpy.ndarray
        Coordinates of the circuit center.

    normal_dir : numpy.ndarray
        Normal vector to the circuit.

    radius : float, optional
        Radius of the circuit.

    n_samples : int, optional
        Number of points in the circuit.

    use_ints : bool, optional
        Whether to round coordinates of circuit points to integers.

    Returns
    -------
    numpy.ndarray
        Array of circuit points.

    """

    normal_dir = _unit_vector_along(normal_dir)
    basis_vec_1, basis_vec_2 = _get_basis_for_normal_plane(normal_dir)
    angles = np.arange(0, 2*np.pi, 2*np.pi/n_samples)
    circuit_pts = (
        np.array([np.cos(angles) * bv1_comp for bv1_comp in basis_vec_1])
        + np.array([np.sin(angles) * bv2_comp for bv2_comp in basis_vec_2])
    )
    circuit_pts = radius * circuit_pts.T + point
    circuit_pts = np.mod(circuit_pts, plotter.dims())
    if use_ints:
        circuit_pts = np.asarray(circuit_pts, int)

    circuit_pts = np.unique(circuit_pts, axis=0)
    return circuit_pts


def _make_circuit_probe_filter_callable(
    plotter, mesh_of_circuit_centers_name, normals_name=None
):
    """ Create a callable that adds a filter formula probing a mesh along
    a collection of small circuits around the points of a parent mesh.

    Parameters
    ----------
    plotter : open_viewmin.FilterTreePlot

    mesh_of_circuit_centers_name : str
        Name of mesh whose points will be the centers of the circuits.

    normals_name : str or `None`
        Name of vector dataset, defined on the mesh of circuit centers,
        to be used as normals to circuits.
        Pass `None` to default to name of `active_vectors` dataset of parent
        mesh.

    Returns
    -------
    callable()

    """

    if mesh_of_circuit_centers_name is None:
        mesh_of_circuit_centers_name = plotter.default_mesh_name
    if normals_name is None:
        mesh_of_circuit_centers = plotter.get_mesh(mesh_of_circuit_centers_name)
        normals_name = mesh_of_circuit_centers.active_vectors_name

    def circuit_probe_filter_callable(mesh):
        def return_func(**kwargs):
            callback_mesh_of_circuit_centers = plotter.get_mesh(
                mesh_of_circuit_centers_name
            )
            circuit_centers = callback_mesh_of_circuit_centers.points
            normal_direction_dataset = (
                callback_mesh_of_circuit_centers[normals_name]
            )

            circuit_meshes = []
            pts_normals_zipped = zip(circuit_centers, normal_direction_dataset)
            for circuit_center, normal_dir in pts_normals_zipped:
                circuit_pts = get_circuit(
                    plotter, circuit_center, normal_dir, **kwargs
                )
                # probe the parent mesh (not the mesh of circuit centers)
                # at circuit_pts
                circuit_meshes.append(probe(mesh, circuit_pts))
            return topology.merge_all(circuit_meshes)
        return return_func
    return circuit_probe_filter_callable


def add_circuit_probe_filter_formula(
    plotter, mesh_of_circuit_centers_name,
    sampled_mesh_name=None, mesh_to_probe_name=None,
    normals_name=None, radius=3, n_samples=32, use_ints=False
):
    """ Add a filter formula probing a mesh along
    a collection of small circuits around the points of a parent mesh.

    Parameters
    ----------

    plotter : open_viewmin.FilterTreePlot

    mesh_to_probe_name : str or `None`, optional
        Name of mesh to probe. Note that this is not in general the same as
        `mesh_of_circuit_centers_name`.

    sampled_mesh_name : str or `None`, optional
        Name of sampled (child) mesh. Pass `None` to default to `"_circuit"`
        appended to `normals_name`.

    mesh_of_circuit_centers_name : str
        Name of mesh whose points will be the centers of the circuits.

    normals_name : str or `None`, optional
        Name of vectors dataset defined on `mesh_of_circuit_centers`
        to be used as normal directions to circuits.
        Pass `None` to default to name of `active_vectors` dataset of parent
        mesh.

    radius : float, optional
        Radius of each circuit.

    n_samples : int, optional
        Number of circuits.

    use_ints : bool, optional
        Whether to round coordinates of circuit points to integers.

    Returns
    -------
    str or `None`
        Name of sampled (child) mesh, or `None` if no child mesh was created.

    """

    if mesh_to_probe_name is None:
        mesh_to_probe_name = plotter.default_mesh_name
    if normals_name is None:
        mesh_of_circuit_centers = plotter.get_mesh(
            mesh_of_circuit_centers_name
        )
        normals_name = mesh_of_circuit_centers.active_vectors_name
    if sampled_mesh_name is None:
        sampled_mesh_name = plotter.name_mesh_without_overwriting(
            normals_name + "_circuit"
        )

    filter_callback = _make_circuit_probe_filter_callable(
        plotter, mesh_of_circuit_centers_name, normals_name=normals_name
    )

    plotter.add_filter_formula(
        name=sampled_mesh_name,
        parent_mesh_name=mesh_to_probe_name,
        filter_callable=filter_callback,
        filter_kwargs=dict(
            # keyword arguments to get_circuit
            radius=radius, n_samples=n_samples, use_ints=use_ints
        )
    )

    # circuit mesh must be registered as child of
    # mesh_of_circuit_centers_name (from whose points circuits are calculated)
    # even though it was created as a probe of mesh_to_probe_name
    plotter.get_filter_formula(mesh_of_circuit_centers_name).children.append(
        sampled_mesh_name
    )
    plotter.get_filter_formula(mesh_to_probe_name).children.remove(
        sampled_mesh_name
    )

    return sampled_mesh_name

