""" Define topological calculations for ViewMinPlot meshes """

import numpy as np
from open_viewmin.filter_tree_plot.utilities import ovm_utilities


def get_connected_sets(plotter, mesh, pbc=False):
    """ Find connected subsets of a mesh.

    Apply PyVista's `connectivity` filter and store the resulting subset
    identification index as a new dataset called "connected".

    Parameters
    ----------

    plotter : :class:`~open_viewmin.filter_tree_plot.filter_tree_plot.FilterTreePlot`

    mesh : pyvista.dataset
        Parent mesh.

    pbc : bool, optional
        Whether to apply periodic boundary conditions.

    Returns
    -------

    """

    # noinspection PyUnresolvedReferences
    mesh_connectivity = mesh.connectivity()
    region_id = mesh_connectivity.active_scalars  # gives connectivity's RegionID
    if pbc:
        pbc_dist_thresh = 1.1  # threshold distance for joining meshes
        nr = max(region_id) + 1  # number of RegionIDs

        # Find points within threshold distance of each boundary
        bdy_pts = [[[] for _ in range(3)] for __ in range(nr)]
        for r in range(nr):
            pts = mesh_connectivity.points[region_id == r]
            for i in range(3):
                bdy_pts[r][i] = [
                    pts[pts[:, i] <= plotter.data_stride * pbc_dist_thresh],
                    pts[
                        pts[:, i] >= plotter.dims()[i] - (
                            plotter.data_stride * (1 + pbc_dist_thresh)
                        )
                    ]
                ]

        # Check point-pairwise for RegionIDs that should be joined.
        # Begin with trivial plotter-equivalence.
        same_as = [[r] for r in range(nr)]
        for r1 in range(nr):
            for r2 in range(nr):
                if r2 != r1:
                    for i in range(3):
                        pts1_shifted = (
                                bdy_pts[r1][i][0]
                                + np.eye(3)[i] * plotter.dims()[i]
                        )
                        pts2 = bdy_pts[r2][i][1]
                        if len(pts1_shifted) > 0 and len(pts2) > 0:
                            if np.min(
                                    [
                                        np.sum(
                                            (pts1_shifted - pt2) ** 2,
                                            axis=-1
                                        )
                                        for pt2 in bdy_pts[r2][i][1]
                                    ]
                            ) <= pbc_dist_thresh ** 2:
                                same_as[r1].append(r2)
                                same_as[r2].append(r1)
                                break

        # Replace each RegionID with the smallest equivalent one
        true_rid = np.arange(nr)
        r = nr - 1
        while r >= 0:
            min_r = min([min(row) for row in same_as if r in row])
            true_rid[true_rid == r] = min_r
            r -= 1
        # Shift remaining RegionID values to be consecutive
        true_rid2 = [list(np.unique(true_rid)).index(i) for i in true_rid]

        # replace RegionID at each point accordingly
        true_rid_array = np.empty_like(np.array(region_id))
        for r1, r2 in enumerate(true_rid2):
            true_rid_array[region_id == r1] = r2
        region_id = true_rid_array

    mesh_connectivity['connected'] = region_id
    return mesh_connectivity


def get_connected_subsets(plotter, mesh, pbc=False):
    """  Create a list of connected subset meshes.

    Uses `threshold` filter on the result of the `connectivity` filter
    to extract an individual connected subset of a mesh.

    Parameters
    ----------

    plotter : :class:`~open_viewmin.filter_tree_plot.filter_tree_plot.FilterTreePlot`

    mesh : pyvista.dataset
        Parent mesh.

    pbc : bool, optional
        Whether to use periodic boundary conditions.

    Returns
    -------
    list[`pyvista.dataset`].

    """

    connectivity_mesh = get_connected_sets(plotter, mesh, pbc=pbc)
    connectivity_array = connectivity_mesh["connected"]

    return [
        connectivity_mesh.threshold(
            scalars='connected',
            value=[c - 0.1, c + 0.1]
        )
        for c in range(1 + max(connectivity_array))
    ]


def add_connected_sets(plotter, parent_mesh_name, pbc=False):
    """ Add a filter formula that colors a mesh according to its connected
    subsets.

    Parameters
    ----------

    plotter : :class:`~open_viewmin.filter_tree_plot.filter_tree_plot.FilterTreePlot`

    parent_mesh_name : str
        Name of parent mesh.

    pbc : bool, optional
        Whether to use periodic boundary conditions.

    Returns
    -------
    pyvista.dataset
        New child mesh

    """

    child_mesh_name = plotter.name_mesh_without_overwriting(
            parent_mesh_name + "_connected"
        )

    def connected_sets_filter_creator(mesh):
        return lambda: get_connected_sets(plotter, mesh, pbc=pbc)
    mesh_kwargs = dict(
        scalars="connected",
        categories=True,
        cmap=plotter.settings["categories_colormap"],
        scalar_bar_args={
            **plotter.settings["default_mesh_kwargs"]["scalar_bar_args"],
            "interactive": True,
            "fmt": "%.0f"
        }
    )

    plotter.add_filter_formula(
        name=child_mesh_name,
        parent_mesh_name=parent_mesh_name,
        filter_callable=connected_sets_filter_creator,
        mesh_kwargs=mesh_kwargs
    )
    return plotter.get_mesh(child_mesh_name)


def merge_all(list_of_meshes):
    """  Merge several meshes.

    Parameters
    ----------
    list_of_meshes : Iterable of [pyvista.dataset]

    Returns
    -------
    pyvista.dataset
        Merged mesh.
    """

    if len(list_of_meshes) > 0:
        # noinspection PyUnresolvedReferences
        ret = list_of_meshes[0]
        for mesh in list_of_meshes[1:]:
            ret = ret.merge(mesh)
        return ret


# noinspection PyPep8Naming
def calculate_Euler_characteristic(plotter, mesh, keep_chi=None):
    # noinspection GrazieInspection
    """ Calculate Euler characteristic

        Calculate Euler characteristic for each connected subset in a mesh,
        using the formula :math:`V - E + F`. Add a new scalar dataset named
        "chi" to the mesh.

        Parameters
        ----------

        plotter : :class:`~open_viewmin.filter_tree_plot.filter_tree_plot.FilterTreePlot`

        mesh : pyvista.dataset
            Parent mesh.

        keep_chi : int, list of ints, or `None`
            Resulting mesh includes only those connected subsets whose Euler
            characteristic matches this value(s). Pass `None` to keep all
            chi values.

        Returns
        -------
        pyvista.dataset
            New child mesh.

        """

    if isinstance(mesh, str):
        mesh = plotter.get_mesh(mesh)
    connected_subsets = get_connected_subsets(plotter, mesh)

    for connected_subset in connected_subsets:
        chi = (
            connected_subset.n_points
            - connected_subset.extract_all_edges().n_cells
            + connected_subset.n_cells
        )
        connected_subset['chi'] = (
            chi * np.ones(connected_subset.n_points, dtype=int)
        )

    if keep_chi is not None:
        if type(keep_chi) is not list:
            keep_chi = [keep_chi]
        connected_subsets = [
            connected_subset
            for connected_subset in connected_subsets
            if connected_subset['chi'][0] in keep_chi
        ]

    return merge_all(connected_subsets)


# noinspection PyPep8Naming
def add_Euler_characteristic_filter(
        plotter, mesh_name, new_mesh_name=None, keep_chi=None, **mesh_kwargs
):
    """ Add a filter formula that colors a mesh by Euler characteristic
    of each connected subset.

    Parameters
    ----------

    plotter : :class:`~open_viewmin.filter_tree_plot.filter_tree_plot.FilterTreePlot`

    mesh_name : str
        Name of parent mesh.

    new_mesh_name : str or `None`, optional
        Name of new (child) mesh. Pass `None` to default to `"_chi"` appended
        to `parent_mesh_name`.

    keep_chi : int, list of ints, or `None`
        Resulting mesh includes only those connected subsets whose Euler
        characteristic matches this value(s). Pass `None` to keep all
        chi values.

    **mesh_kwargs : dict, optional

    Returns
    -------
    pyvista.dataset or `None`
        New child mesh created by filter formula, or `None` if no filter
        formula was created.

    """

    if new_mesh_name is None:
        new_mesh_name = mesh_name + "_chi"

    def filter_callback(mesh):
        return lambda: calculate_Euler_characteristic(
            plotter,
            mesh,
            keep_chi=keep_chi
        )

    use_mesh_kwargs = dict(
        categories=True,
        cmap=plotter.settings["categories_colormap"],
        scalars="chi"
    )

    ovm_utilities.copy_from_dict_recursively(mesh_kwargs, use_mesh_kwargs)

    return plotter.add_filter_formula(
        name=new_mesh_name,
        parent_mesh_name=mesh_name,
        filter_callable=filter_callback,
        mesh_kwargs=use_mesh_kwargs,
    )


def add_find_loops_filter(
        plotter, mesh_name, new_mesh_name=None, **mesh_kwargs
):
    """  Add a filter formula that keeps only loops

    Parameters
    ----------

    plotter : :class:`~open_viewmin.filter_tree_plot.filter_tree_plot.FilterTreePlot`

    mesh_name : str
        Name of parent mesh

    new_mesh_name : str or `None`, optional
        Name of new (child) mesh. Pass `None` to default to `"_loops"` appended
        to `parent_mesh_name`.

    mesh_kwargs : dict, optional
        Keyword arguments to `pyvista.Plotter.add_mesh`.

    Returns
    -------
    :class:`~open_viewmin.filter_tree_plot.filter_formulas.FilterFormula`

    """

    if new_mesh_name is None:
        new_mesh_name = mesh_name + "_loops"
    return add_Euler_characteristic_filter(
        plotter,
        mesh_name,
        new_mesh_name,
        keep_chi=0,
        **mesh_kwargs
    )
