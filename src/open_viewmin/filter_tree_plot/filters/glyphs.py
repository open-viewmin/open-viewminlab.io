""" Utilities for creating and analyzing glyphs """

import pyvista as pv
import numpy as np
from open_viewmin.filter_tree_plot.filters import sample


def add_glyphs_to_mesh(
    plotter, actor_name, mesh_name=None, glyph_shape=None,
    glyph_stride=None, glyph_kwargs=None,
    orient=None, scale=None, factor=None, random=True,
    **mesh_kwargs
):
    """ Create a filter formula to sample an existing mesh, then create a
    filter formula to add glyphs to the sampled mesh.

    Parameters
    ----------

    plotter : :class:`~open_viewmin.filter_tree_plot.filter_tree_plot.FilterTreePlot`

    actor_name : str
        name of child (glyphs) actor

    mesh_name : str, optional
        name of existing parent mesh

    glyph_shape : str, `None`, or PyVista named geometric object
        Shape of each glyph.
        If a string is passed, it must be the name of a PyVista named
        geometric object; see
        https://docs.pyvista.org/api/utilities/geometric.html.
        Pass `None` to default to plotter setting.

    glyph_stride : int or `None`, optional
        Spacing between sampled points where glyphs will be placed. For random
        sampling, the number of sampled points is the total number of points
        divided by `glyph_stride`. Pass `None` to default to plotter setting.

    glyph_kwargs : dict or `None`, optional
        Keyword arguments accepted by PyVista `glyph` filter. Pass `None`
        to default to plotter setting.

    orient : str or `None`
        Name of vector dataset belonging to parent mesh to use as glyph
        orientations. Pass `None` to omit orientation information.

    scale : str or `None`
        Name of scalar dataset belonging to parent mesh to use for glyph
        size scale. Pass `None` to omit scale information.

    factor : float or `None`
        Global size rescaling factor for glyphs.
        Pass `None` to default to the value of `glyph_stride`.

    random : bool
        Whether sampling for glyph locations should be random or evenly spaced.

    **mesh_kwargs : dict, optional
        Keyword arguments accepted by `plotter.add_mesh`

    Returns
    -------

    pyvista.dataset
        Created mesh
    """

    ovm_info = dict()
    if glyph_kwargs is None:
        glyph_kwargs = dict()

    if glyph_stride is None:
        glyph_stride = plotter.settings["glyph_stride"]

    if factor is None:
        factor = glyph_stride

    if glyph_shape is None:
        glyph_shape = plotter.settings["glyph_shape"]
    if type(glyph_shape) is str:
        ovm_info["glyph_shape"] = glyph_shape
        if glyph_shape in plotter.geometric_objects.keys():
            glyph_shape = plotter.geometric_objects[glyph_shape]()
    glyph_kwargs["geom"] = glyph_shape

    if orient is not None:
        glyph_kwargs["orient"] = orient

    if scale is not None:
        glyph_kwargs["scale"] = scale

    glyph_kwargs["factor"] = factor

    glyph_kwargs["tolerance"] = None  # forbid interpolation

    sampled_mesh_name = sample.make_sampled_mesh(
        plotter, mesh_name, glyph_stride, random=random
    )

    # lines must have ambient lighting in order to show their true colors
    if ovm_info["glyph_shape"] == "line":
        current_ambient = mesh_kwargs.get("ambient")
        if current_ambient is None or current_ambient == 0:
            mesh_kwargs["ambient"] = 1

    return plotter.add_filter_formula(
        name=actor_name,
        parent_mesh_name=sampled_mesh_name,
        filter_callable="glyph",
        filter_kwargs=glyph_kwargs,
        random=random,
        mesh_kwargs=mesh_kwargs,
        ovm_info=ovm_info
    )


def make_geometric_objects_dict(settings_dict):
    """  Create a dictionary of glyph shapes.

    Parameters
    ----------
    settings_dict : dict
        plotter settings

    Returns
    -------
    dict
        Dictionary of glyph shapes

    """

    def ellipsoid_func(
        xradius=0.5,
        yradius=0.5 / settings_dict["rod_aspect_ratio"],
        zradius=0.5 / settings_dict["rod_aspect_ratio"],
        u_res=settings_dict["cylinder_resolution"],
        v_res=settings_dict["cylinder_resolution"],
        w_res=settings_dict["cylinder_resolution"]
    ):
        """ Set up standard ellipsoid glyph

        Parameters
        ----------
        xradius : float
        yradius : float
        zradius : float
        u_res : float
        v_res : float
        w_res : float

        Returns
        -------
        pyvista.ParametricEllipsoid

        """
        return pv.ParametricEllipsoid(
            xradius=xradius,
            yradius=yradius,
            zradius=zradius,
            u_res=u_res,
            v_res=v_res,
            w_res=w_res
        )

    def cylinder_func(
        center=(0.0, 0.0, 0.0),
        direction=(1.0, 0.0, 0.0),
        radius=0.9 * 0.5 / settings_dict['rod_aspect_ratio'],
        height=0.9,
        resolution=settings_dict['cylinder_resolution'],
        capping=True
    ):
        """ Set up standard cylinder glyph

        Parameters
        ----------
        center : (float, float, float)
        direction : (float, float, float)
        radius : float
        height : float
        resolution : float
        capping : bool

        Returns
        -------
        pyvista.Cylinder

        """
        return pv.Cylinder(
            center=center,
            direction=direction,
            radius=radius,
            height=height,
            resolution=resolution,
            capping=capping
        )

    def dbl_arrow_func(
        direction=(1.0, 0.0, 0.0),
        start=(0.0, 0.0, 0.0),
        tip_length=0.5,
        tip_radius=0.2,
        tip_resolution=20,
        shaft_radius=0.05,
        shaft_resolution=20,
        scale=None
    ):
        """ Set up standard double-headed arrow glyph

        Parameters
        ----------
        direction : (float, float, float)
        start : (float, float, float)
        tip_length : float
        tip_radius : float
        tip_resolution : int
        shaft_radius : float
        shaft_resolution : int
        scale : float, str, or `None`

        Returns
        -------
        pyvista.PolyData
            Merged mesh of two arrows

        """
        kwargs = dict(
            start=start,
            tip_length=tip_length,
            tip_radius=tip_radius,
            tip_resolution=tip_resolution,
            shaft_radius=shaft_radius,
            shaft_resolution=shaft_resolution,
            scale=scale
        )
        arrow1 = pv.Arrow(direction=direction, **kwargs)
        arrow2 = pv.Arrow(direction=-np.array(direction), **kwargs)
        return arrow1.merge(arrow2)

    geometric_objects = dict(
        cylinder=cylinder_func,
        ellipsoid=ellipsoid_func,
        line=pv.Line,
        arrow=pv.Arrow,
        dbl_headed_arrow=dbl_arrow_func,
        sphere=pv.Sphere,
        plane=pv.Plane,
        box=pv.Box,
        cone=pv.Cone,
        polygon=pv.Polygon,
        disc=pv.Disc,
    )

    return geometric_objects


def calculate_tangent(
    dims, mesh, weights=None, threshold=None, sigma=4, field_name='tangent'
):
    # noinspection GrazieInspection
    """ Calculate tangent at each point in a mesh, assuming the mesh
        approximates
        a curve or is parent to an isosurface tube contouring a value related
        to "weights". Beware: Will show nonsense for regions excluded by
        `threshold`, since calculation is omitted there.

        Parameters
        ----------
        dims : (int, int, int)
            system dimensions

        mesh : pyvista.dataset
            PyVista mesh

        weights : numpy.ndarray or `None`, optional
            Tangent is calculated where `weights` is not less than `threshold`

        threshold : float or `None`, optional
            Threshold value for `weights`. Pass `None` to include all points.

        sigma : float, optional
            Falloff distance for a Gaussian
            weighting function of distance by which each point weighs the direction
            to each other point.

        field_name : str or `None`, optional
            Name of tangents dataset added to `mesh`, unless `None` is passed.

        Returns
        -------
        numpy.ndarray
            Array of tangent vectors

        """

    half_dims = np.array(dims) / 2
    pts = mesh.points
    if weights is None:
        weights = np.ones(len(pts))
    if threshold is not None:
        idxs = np.argwhere(weights >= threshold).flatten()
    else:
        idxs = range(len(pts))
    good_pts = pts[idxs]
    good_weights = weights[idxs]
    tans = np.zeros((len(pts), 3))
    for idx, ref_pt in enumerate(pts):
        ref_vecs = np.mod(good_pts - ref_pt + half_dims, dims) - half_dims
        ref_dists_q = np.sum(ref_vecs ** 2, axis=-1)
        ref_dist_sq_inv = (
                (ref_dists_q != 0) / (ref_dists_q + (ref_dists_q == 0))
        )
        eigvecs = np.linalg.eigh(
            np.sum(
                np.einsum(
                    "...i, ...j, ...", ref_vecs, ref_vecs,
                    (
                        good_weights
                        * np.exp(-(ref_dists_q / (sigma ** 2)))
                        * ref_dist_sq_inv
                    )
                ),
                axis=0
            )
        )[1]
        max_eigvec = eigvecs[:, -1]
        tans[idx] = max_eigvec
    if field_name is not None:
        # noinspection PyUnresolvedReferences
        mesh[field_name] = tans
    return tans


def add_tensor_ellipsoids(
    plotter, ellipsoid_mesh_name, tensor_field_name, parent_mesh_name,
    stride=None, ellipsoid_kwargs=None,
    scale=None, factor=None, random=True, eigenvalue_shift=1.,
    **mesh_kwargs
):
    """ Add ellipsoid glyphs visualizing a symmetric tensor array's
    eigenvectors and eigenvalues.

    Parameters
    ----------
    plotter : :class:`~open_viewmin.filter_tree_plot.filter_tree_plot.FilterTreePlot`
    ellipsoid_mesh_name : str
        Name of new mesh
    tensor_field_name : str
        Name of tensor array
    parent_mesh_name : str
        Name of parent mesh
    stride : int or None, optional
        Spacing between ellipsoids
    ellipsoid_kwargs : dict or None, optional
        Filter kwargs: keyword arguments to
        pyvista.parametric_objects.ParametricEllipsoid()
    scale : numpy.ndarray of floats or None, optional
        Scalar array for ellipsoid size
    factor : float or None, optional
        Global size scale factor
    random : bool
        Whether glyphs will be placed at a random (as opposed to regularly
        spaced) subset of points
    eigenvalue_shift : float, optional
        Value added to all eigenvalues before conversion to ellipsoid axes
        lengths
    **mesh_kwargs : dict, optional
        Keyword arguments accepted by `plotter.add_mesh`


    Returns
    -------
    pyvista.DataSet
        Created mesh

    """

    ovm_info = dict(tensor_ellipsoids=True)
    if ellipsoid_kwargs is None:
        ellipsoid_kwargs = dict()

    if stride is None:
        stride = plotter.settings["glyph_stride"]

    if factor is None:
        factor = stride

    if scale is not None:
        ellipsoid_kwargs["scale"] = scale
    else:
        ellipsoid_kwargs["scale"] = "ones"

    ellipsoid_kwargs["factor"] = factor
    ellipsoid_kwargs["eigenvalue_shift2"] = eigenvalue_shift

    sampled_mesh_name = sample.make_sampled_mesh(
        plotter, parent_mesh_name, stride, random=random
    )

    if ellipsoid_mesh_name is None:
        ellipsoid_mesh_name = plotter.name_mesh_without_overwriting(
            tensor_field_name + "_ellipsoids"
        )

    def ellipsoid_from_traceless_tensor(
        tensors, pos, scale_here=1., eigenvalue_shift2=1.,
        u_res=10, v_res=10, w_res=10, **kwargs
    ):
        """

        Parameters
        ----------
        tensors : pyvista.pyvista_ndarray
            Dataset of second-rank, traceless, symmetric tensors
        pos : Iterable(float)
            $x$, $y$, $z$ coordinates
        scale_here : float
            size scale factor
        eigenvalue_shift2 : float
            shift for eigenvalues, so that they are all positive
        u_res : int
            rendering resolution
        v_res : int
            rendering resolution
        w_res : int
            rendering resolution
        **kwargs
            keyword arguments to :class:`pyvista.ParametricEllipsoid`

        Returns
        -------
        pyvista.ParametricEllipsoid

        """
        degrees_per_radian = 180 / np.pi
        tensors_nonnegative = (
                tensors.reshape(3, 3) + (eigenvalue_shift2 / 3) * np.eye(3)
        )
        eigvals, eigvecs = np.linalg.eigh(tensors_nonnegative)
        # noinspection PyTypeChecker
        ellipsoid = pv.ParametricEllipsoid(
            *(scale_here * eigval for eigval in eigvals),
            u_res=u_res, v_res=v_res, w_res=w_res, clean=True,
            **kwargs
        )
        n_dir = eigvecs[:, -1]
        n_theta = np.arccos(n_dir[2])
        n_phi = np.arctan2(n_dir[1], n_dir[0])
        # x-axis after first rotation (about z)
        ellipsoid.rotate_z(
            n_phi * degrees_per_radian, inplace=True
        )
        x_prime_axis = np.array([-np.sin(n_phi), np.cos(n_phi), 0])
        ellipsoid.rotate_vector(
            x_prime_axis, n_theta * degrees_per_radian, inplace=True
        )
        # after last rotation (by psi about z''), x''' axis -> min eigenvector
        psi = np.arctan2(
            eigvecs[1].dot(x_prime_axis),
            eigvecs[0].dot(x_prime_axis)
        )
        ellipsoid.rotate_vector(
            n_dir, psi * degrees_per_radian, inplace=True
        )
        ellipsoid.translate(pos, inplace=True)
        return ellipsoid

    def filter_callable(parent_mesh):
        """ Create callable that generates tensor ellipsoid glyphs

        Parameters
        ----------
        parent_mesh : pyvista.Dataset

        Returns
        -------
        callable

        """
        def return_func(**kwargs):
            tensor_field = parent_mesh[tensor_field_name]
            coords = np.asarray(parent_mesh.points, dtype=int)
            factor2 = kwargs.get("factor", 1)
            scale_array = parent_mesh[kwargs.get("scale", "ones")] * factor2
            parametric_ellipsoid_kwargs = {
                key: val
                for key, val in kwargs.items()
                if key not in ["factor", "scale"]
            }
            ellipsoids = [
                ellipsoid_from_traceless_tensor(
                    Q, pos, scale_here=ellipsoid_scale,
                    **parametric_ellipsoid_kwargs
                )
                for Q, pos, ellipsoid_scale in zip(
                    tensor_field, coords, scale_array
                )
            ]
            parent_name = plotter.get_mesh_name(parent_mesh)
            parent_filter_formula = plotter.get_filter_formula(parent_name)
            root_ancestor = parent_filter_formula.root_ancestor
            ellipsoids_mesh = pv.merge(ellipsoids).sample(root_ancestor.mesh)
            return ellipsoids_mesh
        return return_func

    return plotter.add_filter_formula(
        name=ellipsoid_mesh_name,
        parent_mesh_name=sampled_mesh_name,
        filter_callable=filter_callable,
        filter_kwargs=ellipsoid_kwargs,
        random=random,
        mesh_kwargs=mesh_kwargs,
        ovm_info=ovm_info
    )
