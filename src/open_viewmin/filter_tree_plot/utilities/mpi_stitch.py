""" Utility to merge data from separate files for a single scene, such as from
an MPI run"""

import numpy as np
import re


def mpi_stitch(in_filenames, out_filename=None):
    """ Stitch together a set of data text files from an MPI run into a single
    file with the proper ordering of lines for use by openViewMin. First three
    columns must be x y z coordinates.

    Parameters
    ----------
    in_filenames : list of str
        Names of existing data files.

    out_filename : str or `None`
        Output filename. Pass `None` to skip writing to file.

    Returns
    -------
    numpy.ndarray
        Ordered, combined data.

    """

    data = np.concatenate([np.loadtxt(f) for f in in_filenames])
    coords = np.asarray(data[:, :3], dtype=int)
    order = np.lexsort(coords.T)
    ordered_data = data[order]
    if out_filename is not None:
        fmt = "%d\t%d\t%d"
        for i in range(data.shape[1] - 3):
            fmt += "\t%f"
        np.savetxt(
            out_filename,
            ordered_data,
            fmt=fmt
        )
        print(in_filenames, "->", out_filename)
    return ordered_data


def mpi_group(filenames):
    """ Check whether some filenames come from the same MPI run
    (formatted as same_prefix_x#y#z#.txt) and if so, stitch them together
    using `mpi_stitch()`.

    Parameters
    ----------
    filenames : list of str
        Names of existing data files.

    Returns
    -------
    list of str
        Names of new output files containing combined data.

    """

    filenames_mpi = ([], [])
    filenames_no_mpi = []
    for f in filenames:
        if bool(re.search(r'_x\d+y\d+z\d+.', f)):
            file_prefix = '_'.join(f.split('_')[:-1])
            if file_prefix in filenames_mpi[0]:
                filenames_mpi[1][filenames_mpi[0].index(file_prefix)].append(f)
            else:
                filenames_mpi[0].append(file_prefix)
                filenames_mpi[1].append([f])
        else:
            filenames_no_mpi.append(f)
    for file_prefix, f_names in zip(filenames_mpi[0], filenames_mpi[1]):
        if len(f_names) > 1:
            out_f_name = file_prefix + '.txt'
            mpi_stitch(f_names, out_filename=out_f_name)
        else:
            out_f_name = f_names[0]
        filenames_no_mpi.append(out_f_name)
    return filenames_no_mpi
