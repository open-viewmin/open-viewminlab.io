""" Utilities for customizing and auto-updating scalar bars (colorbars)
"""

from matplotlib.colors import to_rgb


def update_scalar_bar(plotter, actor_name, this_scalar_bar):
    """ Update a scalar bar based on a mesh's state.

    Parameters
    ----------

    plotter : open_viewmin.FilterTreePlot

    actor_name : str
        Name of mesh corresponding to this scalar bar.

    this_scalar_bar : vtkmodules.vtkRenderingAnnotation.vtkScalarBarActor
        Scalar bar VTK actor.

    """

    standardize_scalar_bar(plotter, this_scalar_bar)
    if actor_name in plotter.filter_formulas.keys():
        actor_info = plotter.get_filter_formula(actor_name)
        mesh_kwargs = actor_info.mesh_kwargs
        scalar_bar_args = mesh_kwargs["scalar_bar_args"]
        if all(
            key in scalar_bar_args.keys()
            for key in ["position_x", "position_y"]
        ):
            move_scalar_bar_to_previous_position(
                plotter, this_scalar_bar, scalar_bar_args
            )
    rename_scalar_bar_actor_to_title(plotter, this_scalar_bar)


# noinspection PyProtectedMember
def move_scalar_bar_to_previous_position(
    plotter, this_scalar_bar, scalar_bar_args
):
    """ Reposition scalar bar to its pre-update position

    Parameters
    ----------

    plotter : open_viewmin.FilterTreePlot

    this_scalar_bar : vtkmodules.vtkRenderingAnnotation.vtkScalarBarActor
        Scalar bar VTK actor

    scalar_bar_args : dict
        Dictionary of scalar bar properties.

    """

    scalar_bar_title = this_scalar_bar.GetTitle()
    # Check if scalar bar is controlled by an interactive widget
    scalar_bar_widgets = plotter._scalar_bars._scalar_bar_widgets
    if scalar_bar_title in scalar_bar_widgets.keys():
        object_to_reposition = (
            scalar_bar_widgets[scalar_bar_title].GetRepresentation()
        )
    else:
        object_to_reposition = this_scalar_bar
    object_to_reposition.SetPosition(
        scalar_bar_args["position_x"], scalar_bar_args["position_y"]
    )


def rename_scalar_bar_actor_to_title(plotter, scalar_bar_actor):
    """ Rename a scalar bar actor to match its title.

    Parameters
    ----------

    plotter : open_viewmin.FilterTreePlot

    scalar_bar_actor : vtkmodules.vtkRenderingAnnotation.vtkScalarBarActor
        Scalar bar VTK actor

    """

    scalar_bar_title = scalar_bar_actor.GetTitle()
    scalar_bar_ref_string = "colorbar: " + scalar_bar_title
    for ref_string in plotter.renderer.actors.keys():
        if (
            plotter.renderer.actors[ref_string] is scalar_bar_actor
            and ref_string != scalar_bar_ref_string
        ):
            del plotter.renderer.actors[ref_string]
            plotter.renderer.actors[scalar_bar_ref_string] = scalar_bar_actor
            break


# noinspection PyProtectedMember
def standardize_scalar_bar(plotter, scalar_bar):
    """ Apply default settings to scalar bar.

    Includes workaround for bugs in PyVista's display of scalar bars.

    Parameters
    ----------

    plotter : open_viewmin.FilterTreePlot

    scalar_bar : vtkmodules.vtkRenderingAnnotation.vtkScalarBarActor
        Scalar bar VTK actor

    """

    scalar_bar.SetBarRatio(
        plotter.settings['scalar_bar_aspect_ratio']
    )
    scalar_bar.SetMaximumHeightInPixels(
        plotter.settings["scalar_bar_maxheight"]
    )
    scalar_bar.SetMaximumWidthInPixels(
        plotter.settings["scalar_bar_maxwidth"]
    )
    scalar_bar_title = scalar_bar.GetTitle()
    scalar_bar_widgets = plotter._scalar_bars._scalar_bar_widgets
    if scalar_bar_title in scalar_bar_widgets.keys():
        scalar_bar_widget = scalar_bar_widgets[scalar_bar_title]
        scalar_bar_widget_rep = scalar_bar_widget.GetRepresentation()
        scalar_bar_widget_rep.SetMaximumSize(
            scalar_bar.GetMaximumWidthInPixels(),
            scalar_bar.GetMaximumHeightInPixels()
        )
    scalar_bar.SetTextPad(1)
    scalar_bar.SetNumberOfLabels(
        4 if len(scalar_bar.GetTitle()) < 7 else 2
    )
    title_text_property = scalar_bar.GetTitleTextProperty()
    title_text_property.SetVerticalJustificationToTop()
    title_text_property.SetUseTightBoundingBox(False)

    scalar_bar.SetVerticalTitleSeparation(10)
    if plotter.is_enabled_eye_dome_lighting:
        """ 
        Eye dome lighting makes text look ugly. It helps to set the text 
        background to be the same color as the scene background 
        """
        bg_col = to_rgb(plotter.theme['background'])
        scalar_bar.GetTitleTextProperty().SetBackgroundColor(bg_col)
        scalar_bar.GetTitleTextProperty().SetBackgroundOpacity(1)
        scalar_bar.GetLabelTextProperty().SetBackgroundColor(bg_col)
        scalar_bar.GetLabelTextProperty().SetBackgroundOpacity(1)
        scalar_bar.GetAnnotationTextProperty().SetBackgroundColor(bg_col)
        scalar_bar.GetAnnotationTextProperty().SetBackgroundOpacity(1)
        scalar_bar.SetTextPad(plotter.settings["scalar_bar_text_pad"])


def copy_scalar_bar_properties(old_scalar_bar, scalar_bar_args):
    """ Copy scalar bar properties to a dictionary.

    Parameters
    ----------
    old_scalar_bar : vtkmodules.vtkRenderingAnnotation.vtkScalarBarActor
        Scalar bar VTK actor

    scalar_bar_args : dict
        Dictionary of scalar bar properties.

    """

    scalar_bar_args["render"] = old_scalar_bar.GetVisibility()
    scalar_bar_args["vertical"] = (old_scalar_bar.GetOrientation() == 1)
    scalar_bar_args["n_labels"] = old_scalar_bar.GetNumberOfLabels()
    scalar_bar_args["width"] = old_scalar_bar.GetWidth()
    scalar_bar_args["height"] = old_scalar_bar.GetHeight()
    scalar_bar_args["position_x"], scalar_bar_args["position_y"] = (
        old_scalar_bar.GetPosition()
    )
    scalar_bar_args["n_colors"] = old_scalar_bar.GetMaximumNumberOfColors()
    title_text_property = old_scalar_bar.GetTitleTextProperty()
    label_text_property = old_scalar_bar.GetLabelTextProperty()
    scalar_bar_args["italic"] = title_text_property.GetItalic()
    scalar_bar_args["bold"] = title_text_property.GetBold()
    scalar_bar_args["title_font_size"] = title_text_property.GetFontSize()
    scalar_bar_args["label_font_size"] = label_text_property.GetFontSize()
    scalar_bar_args["font_family"] = title_text_property.GetFontFamilyAsString()
    scalar_bar_args["color"] = title_text_property.GetColor()
    scalar_bar_args["fmt"] = old_scalar_bar.GetLabelFormat()
