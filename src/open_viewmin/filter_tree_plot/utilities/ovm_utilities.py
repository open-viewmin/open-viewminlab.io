"""

Define utility functions and classes for use throughout project.

"""

from pandas import read_csv
import pyvista as pv
import inspect

pv.set_jupyter_backend('trame')


def fast_import(filename, sep="\t"):
    """ Use `pandas.read_csv` to import `.txt`/`.dat` files quickly

    Parameters
    ----------
    filename : str
        Name of file to import

    sep : str
        Delimiter

    Returns
    -------
    np.ndarray
        Imported data.

    """

    try:
        ret = read_csv(filename, sep=sep, header=None)
    except FileNotFoundError as err:
        print(err)
        ret = None
    else:
        ret = ret.to_numpy()
    return ret


def available_filters(mesh):
    """ Get filters available for a given mesh.

    Excludes: `"boolean_*"` and `"plot_*"` filters

    Parameters
    ----------
    mesh : pyvista.dataset

    Returns
    -------
    list of str
        List of filter names

    """

    if isinstance(mesh, pv.MultiBlock):
        avail = pv.MultiBlock
    elif isinstance(mesh, pv.ImageData):
        avail = pv.ImageDataFilters
    elif isinstance(mesh, pv.UnstructuredGrid):
        avail = pv.UnstructuredGridFilters
    elif isinstance(mesh, pv.PolyData):
        avail = pv.PolyDataFilters
    elif isinstance(mesh, pv.DataSet):
        avail = pv.DataSetFilters
    else:
        return []
    pv_filters = [
        pv_filter
        for pv_filter in dir(avail)
        if (
            pv_filter[0] != "_"
            and not pv_filter.startswith("plot_")
            and not pv_filter.startswith("boolean_")
            and pv_filter not in ["clip_closed_surface"]
        )
    ]
    if isinstance(mesh, pv.PolyData):
        if "tessellate" in pv_filters:
            pv_filters.remove("tessellate")
    other_operations = [
        "flip_x", "flip_y", "flip_z", "rotate_vector", "rotate_x", "rotate_y",
        "rotate_z"
    ]
    pv_filters += [
        other_operation
        for other_operation in other_operations
        if hasattr(mesh, other_operation)
    ]

    return sorted(pv_filters)


def sort_filenames_by_timestamp(filenames):
    """ Sort names of output files from the same run by timestamp

    Parameters
    ----------
    filenames : sequence of str

    Returns
    -------
    list of str

    """

    timestamps = []
    for filename in filenames:
        filename_trimmed = ''.join(filename.split(".")[:-1])
        filename_suffix = filename_trimmed.split("_")[-1]
        # check for open-Qmin mpi x#y#z# string, to ignore for timestamp
        if (
                "x" in filename_suffix
                and "y" in filename_suffix
                and "z" in filename_suffix
        ):
            check_for_mpi_suffix = filename_suffix.replace("x", "")
            check_for_mpi_suffix = check_for_mpi_suffix.replace("y", "")
            check_for_mpi_suffix = check_for_mpi_suffix.replace("z", "")
            if check_for_mpi_suffix.isdigit():
                filename_suffix = filename_trimmed.split("_")[-2]
        if filename_suffix.isnumeric():
            timestamps.append(int(filename_suffix))
        else:
            # give up on sorting by timestamp
            return filenames

    sorted_filenames = []
    for timestamp in sorted(timestamps):
        # noinspection PyUnresolvedReferences
        sorted_filenames.append(filenames[timestamps.index(timestamp)])
    return sorted_filenames


def name_without_overwriting(try_new_name, dict_of_old_names):
    """ Modify (if necessary) a new dictionary key to be distinct from existing keys.

    Parameters
    ----------

    try_new_name : str
        Proposed new key

    dict_of_old_names : dict
        Dictionary to which new key will be added.

    Returns
    -------
    str
        Modified key

    """

    if isinstance(dict_of_old_names, (list, tuple, type({}.keys()))):
        old_names = dict_of_old_names
    elif isinstance(dict_of_old_names, dict):
        old_names = list(dict_of_old_names.keys())
    else:
        old_names = []
    new_name = try_new_name
    i = 1
    while new_name in old_names:
        i += 1
        new_name = try_new_name + "_" + str(i)
    return new_name


# noinspection PyProtectedMember
def get_kwargs_and_defaults(func, as_strings=False):
    """ Find the names of a function's keyword arguments
    and their default values

    Parameters
    ----------

    func : callable()
        The function in question

    as_strings : bool, optional
        Whether to force conversion of default values to str.

    Returns
    -------
    dict
        Dictionary mapping keyword argument keys to default values.

    """

    sig = inspect.signature(func)
    params = sig.parameters
    # noinspection PyUnresolvedReferences
    kwargs = {
        key: (
            params[key].default
            if params[key].default != inspect._empty
            else None
        )
        for key in params.keys()
    }
    last_arg_name = list(params)[-1]
    # for **kwargs case
    if "**" + last_arg_name in str(sig):
        del kwargs[last_arg_name]
        kwargs["**" + last_arg_name] = {}

    if as_strings:
        for key in kwargs.keys():
            if isinstance(kwargs[key], str):
                kwargs[key] = f"\'{kwargs[key]}\'"
            else:
                kwargs[key] = str(kwargs[key])
    return kwargs


def get_arg_types(func):
    docstring = func.__doc__
    docstring_parameters = "".join(
        inspect.cleandoc(docstring).split("Parameters\n----------\n")[1:]
    ).split("Returns")[0]
    params_strings = docstring_parameters.split("\n\n")
    param_list = [
        tuple(param_str.splitlines()[0].split(" : "))
        for param_str in params_strings
        if " : " in param_str
    ]
    param_dict = {key: val for (key, val) in param_list}
    return param_dict


def copy_from_dict_recursively(from_dict, to_dict=None):
    """ Copy a dictionary, with recursion for values that are dictionaries.

    Parameters
    ----------
    from_dict : dict
        Source dictionary.
    to_dict : dict or `None`
        Destination dictionary. Pass `None` to copy into a new, empty
        dictionary.

    Returns
    -------
    dict
        Dictionary containing copied values.

    """
    if to_dict is None:
        to_dict = dict()
    for key in from_dict.keys():
        val = from_dict[key]
        if isinstance(val, dict):
            if key not in to_dict.keys():
                to_dict[key] = dict()
            copy_from_dict_recursively(val, to_dict[key])
        else:
            # non-dictionary values are simply copied
            to_dict[key] = val
    return to_dict


def add_if_dict_lacks(to_dict, from_dict):
    """ Copy key-value pairs for keys from source dictionary that are missing from
    destination dictionary.

    Parameters
    ----------
    to_dict : dict
        Destination dictionary

    from_dict : dict
        Source dictionary

    """

    for key in from_dict.keys():
        if key not in to_dict.keys():
            to_dict[key] = from_dict[key]


def hex_to_rgb(hex_color):
    """ Convert hex color to RGB (integer-valued).

    Parameters
    ----------
    hex_color : str

    Returns
    -------
    (int, int, int)
        RGB values in [0, 255].

    """

    hex_color = hex_color.lstrip('#')

    # from https://stackoverflow.com/questions/29643352/converting-hex-to-rgb-value-in-python
    RGB_color = tuple(int(hex_color[i:i+2], 16) for i in (0, 2, 4))

    return RGB_color
