"""
General calculation utilities.
"""

import numpy as np

levi_civita = np.zeros((3, 3, 3), dtype=int)
levi_civita[0, 1, 2] = levi_civita[1, 2, 0] = levi_civita[2, 0, 1] = 1
levi_civita[0, 2, 1] = levi_civita[2, 1, 0] = levi_civita[1, 0, 2] = -1


def safe_inverse(arr):
    """ take inverse $x \\rightarrow 1/x$ but with $0 \\rightarrow 0$

    Parameters
    ----------
    arr: :class:`np.ndarray`

    Returns
    -------
    :class:`np.ndarray`
    """
    return np.asarray(arr != 0, dtype=float) / (arr + (arr == 0))


def matrix_times_transpose(arr):
    """ Compute matrix product $A A^T$ for a matrix $A$.

    Parameters
    ----------
    arr: :class:`np.ndarray`

    Returns
    -------
    :class:`np.ndarray`

    """
    return arr @ arr.transpose((0, 2, 1))


def transpose_times_matrix(arr):
    """ Compute matrix product $A^T A$ for a matrix $A$.

    Parameters
    ----------
    arr: :class:`np.ndarray`

    Returns
    -------
    :class:`np.ndarray`

    """
    return arr.transpose((0, 2, 1)) @ arr


def list_to_xyzmat(arr, dims):
    """ Unflatten a list of values so that the first three dimensions of the
    returned array correspond to the spatial grid.

    Parameters
    ----------
    arr: :class:`np.ndarray`
    dims: tuple(int)

    Returns
    -------
    :class:`np.ndarray`

    """

    # `arr` (flat array) is assumed to cycle over x innermost and z outermost,
    # whereas we want the returned array to be addressed as [x,y,z]
    # so we must first reshape `arr` to (Lz, Ly, Lx, ...)
    # and then swap the order of the z and x axes.
    Lx, Ly, Lz = dims
    return np.swapaxes(
        arr.reshape((Lz, Ly, Lx) + arr.shape[1:]),
        0, 2
    )


def xyzmat_to_list(arr):
    """ Partially flatten an array from shape `(Lx, Ly, Lz, ...)` to
    `(Lx * Ly * Lz, ...)`.

    Parameters
    ----------
    arr: :class:`np.ndarray`

    Returns
    -------
    :class:`np.ndarray`

    """
    return arr.reshape((np.prod(arr.shape[:3]),) + arr.shape[3:])


def di(arr, array_dims, flatten=False, component=None, data_stride=1):
    """ Gradient

    Parameters
    ----------
    arr: :class:`np.ndarray`
    array_dims: tuple(int)
    flatten: bool
    component: int or `None`
    data_stride: int

    Returns
    -------
    :class:`np.ndarray`

    """
    if len(arr.shape) < 4:
        arr = list_to_xyzmat(arr, array_dims)

    if component in [0, 1, 2]:
        ans = 0.5 / data_stride * (
                np.roll(arr, -1, axis=component)
                - np.roll(arr, 1, axis=component)
        )
    else:
        ans = np.empty(arr.shape[:3] + (3,) + arr.shape[3:])
        for i in range(3):
            ans[:, :, :, i, ...] = 0.5 / data_stride * (
                    np.roll(arr, -1, axis=i) - np.roll(arr, 1, axis=i)
            )
    if flatten:
        ans = ans.reshape((np.prod(ans.shape[:3]),) + ans.shape[3:])
    return ans


def dij(arr, array_dims, flatten=False, data_stride=1):
    """ Hessian

    Parameters
    ----------
    arr: :class:`np.ndarray`
    array_dims: tuple(int)
    flatten: bool
    data_stride: int

    Returns
    -------
    :class:`np.ndarray`

    """
    if len(arr.shape) < 4:
        arr = list_to_xyzmat(arr, array_dims)
    ans = np.empty(arr.shape[:3] + (3, 3) + arr.shape[3:])
    for i in range(3):
        ans[:, :, :, i, i, ...] = 1 / (data_stride ** 2) * (
                np.roll(arr, -1, axis=i) - 2 * arr + np.roll(arr, 1, axis=i)
        )
    for i, j in [[0, 1], [1, 2], [0, 2]]:
        ans[:, :, :, i, j, ...] = 0.25 / (data_stride ** 2) * (
              np.roll(np.roll(arr, -1, axis=i), -1, axis=j)
            - np.roll(np.roll(arr, -1, axis=i),  1, axis=j)
            - np.roll(np.roll(arr,  1, axis=i), -1, axis=j)
            + np.roll(np.roll(arr,  1, axis=i),  1, axis=j)
        )
        ans[:, :, :, j, i, ...] = ans[:, :, :, i, j, ...]
    if flatten:
        ans = ans.reshape((np.prod(ans.shape[:3]),) + ans.shape[3:])
    return ans


def einstein_sum(sum_str, *arrays, reshape=True):
    """ Increase flexibility of numpy.einsum, for convenience in calculating
    datasets

    Parameters
    ----------
    sum_str: str
    arrays: tuple(:class:`np.ndarray`)
    reshape: bool

    Returns
    -------
    :class:`np.ndarray`

    """

    sum_str = "..." + sum_str
    sum_str = (
        sum_str.replace(" ", "").replace(",", ",...").replace("->", "->...")
    )
    answer = np.einsum(sum_str, *arrays)
    if reshape:
        answer = answer.reshape((-1,) + answer.shape[3:])
    return answer


def calculate_from_scalar_field(
    plotter, scalar_field_name, operation_string, mesh_name="fullmesh"
):
    """ Create callable that will apply a calculation to a scalar dataset.

    Parameters
    ----------

    plotter : :class:`~open_viewmin.filter_tree_plot.filter_tree_plot.FilterTreePlot`

    scalar_field_name : str
        Name of scalar dataset

    operation_string : str
        String key for calculation; must be one of: `"gradient"` or `"Laplacian"`


    mesh_name : str, optional
        Name of mesh from which to extract dataset

    Returns
    -------
    callable()

    """
    def return_func():
        mesh = plotter.get_mesh(mesh_name)
        scalar_field = mesh[scalar_field_name]
        result_name = scalar_field_name + '_' + operation_string
        if operation_string == 'abs':
            mesh[result_name] = np.abs(scalar_field)
        elif operation_string == 'gradient':
            mesh[result_name] = di(scalar_field, plotter.array_dims(), flatten=True)
        elif operation_string == 'Laplacian':
            mesh[result_name] = einstein_sum(
                "ii", dij(scalar_field, plotter.array_dims(), flatten=True)
            )
        plotter.update_filter(mesh_name)
    return return_func


def calculate_from_vector_field(
    plotter, vector_field_name, operation_string, mesh_name="fullmesh"
):
    """ Create callable that will apply a calculation to a vector dataset.

    Parameters
    ----------

    plotter : :class:`~open_viewmin.filter_tree_plot.filter_tree_plot.FilterTreePlot`

    vector_field_name : str
        Name of vector dataset

    operation_string : str
        String key for calculation; must be one of:
        `'x'`, `'y'`, `'z'`, `'|x|'`, `'|y|'`, `'|z|'`, `'div'`, or `'curl'`

    mesh_name : str, optional
        Name of mesh from which to extract dataset

    Returns
    -------
    callable()

    """

    def return_func():
        mesh = plotter.get_mesh(mesh_name)
        vector_field = mesh[vector_field_name]
        result_name = vector_field_name + '_' + operation_string
        if operation_string in ['x', 'y', 'z']:
            idx = ['x', 'y', 'z'].index(operation_string)
            mesh[result_name] = vector_field[:, idx]
        elif operation_string in ['|x|', '|y|', '|z|']:
            idx = ['|x|', '|y|', '|z|'].index(operation_string)
            mesh[result_name] = np.abs(vector_field[:, idx])
        elif operation_string == 'div':
            ans = np.zeros(vector_field.shape[0])
            for i in range(3):
                ans += di(
                    vector_field[:, i], plotter.array_dims(), component=i, flatten=True
                )
            mesh[result_name] = ans
        elif operation_string == 'curl':
            djvi = [di(
                vector_field[:, i], plotter.array_dims(), flatten=True
            ) for i in range(3)]
            ans = np.zeros(vector_field.shape)
            for i in range(3):
                for j in range(3):
                    for k in range(3):
                        eps = plotter.levi_civita[i, j, k]
                        if eps != 0:
                            ans[:, i] += eps * djvi[k][:, j]
            mesh[result_name] = ans
        plotter.update_filter(mesh_name)

    return return_func


def calculate_from_two_vector_fields(
    plotter,
    vector_field1_name,
    vector_field2_name,
    operation_string,
    mesh_name="fullmesh"
):
    """ Create a callable that defines a new dataset on a given mesh from a
    named operation on two vector fields  already defined on that mesh.

    Parameters
    ----------
    plotter : :class:`~open_viewmin.filter_tree_plot.filter_tree_plot.FilterTreePlot`
    vector_field1_name: str
    vector_field2_name: str
    operation_string: str
        Must be `"cross"` or `"dot"`
    mesh_name: str

    Returns
    -------
    callable()

    """

    def return_func():
        mesh = plotter.get_mesh(mesh_name)
        vector_field1 = mesh[vector_field1_name]
        vector_field2 = mesh[vector_field2_name]
        result_name = (
            f"{vector_field1_name}_{operation_string}_{vector_field2_name}"
        )
        if operation_string == "cross":
            ans = np.cross(vector_field1, vector_field2, axis=-1)
        elif operation_string == "dot":
            ans = np.sum(vector_field1 * vector_field2, axis=-1)
        else:
            raise ValueError("operation_string must be \"cross\" or \"dot\"")
        mesh[result_name] = ans
        plotter.update_filter(mesh_name)

    return return_func


def calculate_from_one_vector_field_and_one_scalar_field(
    plotter,
    vector_field_name,
    scalar_field_name,
    operation_string,
    mesh_name="fullmesh"
):
    """ Create a callable that defines a new dataset on a given mesh from a
    named operation on one vector field and one scalar field already defined
    on that mesh.

    Parameters
    ----------
    plotter : :class:`~open_viewmin.filter_tree_plot.filter_tree_plot.FilterTreePlot`
    vector_field_name: str
    scalar_field_name: str
    operation_string: str
        Must be `"scale"`
    mesh_name: str

    Returns
    -------
    callable()

    """
    def return_func():
        mesh = plotter.get_mesh(mesh_name)
        vector_field = mesh[vector_field_name]
        scalar_field = mesh[scalar_field_name]
        result_name = (
            f"{vector_field_name}_{operation_string}_{scalar_field_name}"
        )
        if operation_string == "scale":
            ans = scalar_field[:, np.newaxis] * vector_field
        else:
            raise ValueError("operation_string must be \"scale\"")
        mesh[result_name] = ans
        plotter.update_filter('fullmesh')

    return return_func


def calculate_from_tensor_field(
    plotter, tensor_field_name, operation_string, mesh_name="fullmesh"
):
    """ Create callable that will apply a calculation to a dataset of
    Hermitian, rank-2 tensors.

    Parameters
    ----------

    plotter : :class:`~open_viewmin.filter_tree_plot.filter_tree_plot.FilterTreePlot`

    tensor_field_name : str
        Name of tensor dataset

    operation_string : str
        String key for calculation; must be 'eigvalsh' or 'eigh'

    mesh_name : str, optional
        Name of mesh from which to extract dataset

    Returns
    -------
    callable()

    """

    def return_func():
        mesh = plotter.get_mesh(mesh_name)
        tensor_field = mesh[tensor_field_name].reshape(-1, 3, 3)
        if operation_string == 'eigvalsh':
            eigvals = np.linalg.eigvalsh(tensor_field)
            for idx, m_str in enumerate(['min', 'mid', 'max']):
                mesh[tensor_field_name + '_eigval_' + m_str] = eigvals[:, idx]
        elif operation_string == 'eigh':
            eigvals, eigvecs = np.linalg.eigh(tensor_field)
            for idx, m_str in enumerate(['min', 'mid', 'max']):
                mesh[tensor_field_name + '_eigval_' + m_str] = eigvals[:, idx]
                mesh[tensor_field_name + '_eigvvec_' + m_str] = (
                    eigvecs[:, :, idx]
                )
        plotter.update_filter('fullmesh')
    return return_func
